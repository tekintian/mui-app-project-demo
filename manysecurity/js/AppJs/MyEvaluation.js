﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    MyEvaluation.FillMyEvaluation();
});
Cmn.App.CurPage.OnInit.Add(function (data) {
    //返回个人中心
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });
});
(MyEvaluation = new function () {
    var _Self = this;
    this.FillMyEvaluation = function () {
        //好保安-一般保安-差保安-黑名单
        CmnAjax.FillData(".Js_EvaluateList", SiteFunc.AjaxUrl + "GetMyEvaluateDtl", "", function (data) {
            if(!data.data || data.data.length == 0) {
                $('form').addClass('emptyContent');
            }
            else {
                $('form').removeClass('emptyContent');
            }
        });
    }
})