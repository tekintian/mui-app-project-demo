﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {
    
});

function GetAgeByCard(userCardNo) {
    var myDate = new Date();
    var month = myDate.getMonth() + 1;

    var day = myDate.getDate();
    var age = myDate.getFullYear() - userCardNo.substring(6, 10) - 1;
    if (userCardNo.substring(10, 12) < month || userCardNo.substring(10, 12) == month && userCardNo.substring(12, 14) <= day) {
        age++;
    }
    return age;
}

function GetSexByCard(userCardNo){
    var Sex="";
    if(userCardNo.length=="18"){
                  if(parseInt(userCardNo.substring(17,1))%2==1)
                  {
                    Sex=1;
                    return Sex;
                  }
                  else
                  {
                    Sex=2;
                    return Sex;
                  }
             }
              else if(userCardNo.length=="15")
              {
                  if(parseInt(userCardNo.substring(14,1))%2==1)
                  {
                    Sex=1;
                    return Sex;
                  }
                  else
                  {
                    Sex=2;
                    return Sex;
                  }
              }
}


//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {

    //照片头像
    var _HeadImage = "";
    //姓名
    var _UserName = "";
    //身份证
    var _ICNo = "";
    //性别
    var _Sex = "";
    //年龄
    var _Age = "";
    //手机号
    var _Phone = "";
    //净身高
    var _Height = "";
    //体重
    var _Weight = "";
    //上岗证号
    var _SecurityStaffCardNo = "";
    //意向城市代码
    var _IntentionOpenCityID = "";
    //是否自备服装
    var _HasClothes = "否";
    //套装1
    var _Clothes1PicPath = "";
    //套装2
    var _Clothes2PicPath = "";
    //自我介绍
    var _Introduction = "";
    //自备服装（是否）
    var _IsNo = "";
    //服装照片1
    var _SecurityServiceLicence_fz1="";
    var _SecurityServiceLicence_fz2="";
    var _SecurityServiceLicence_fz3="";
    var _SecurityServiceLicence_fz4="";
    var _SecurityServiceLicence_fz5="";


    CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateEmployeesTheQueryInfo", "", function (data) {
        if (data.IsSuccess == 1) {
        	if(data.data[0].IsApprove=="True")
        	{
        		$(".Js_Name").attr("disabled",true);
        		$(".Js_ICNo").attr("disabled",true);
        		$(".Js_Sex").attr("disabled",true);
        		$(".Js_Age").attr("disabled",true);
        		$(".Js_Height").attr("disabled",true);
        		$(".Js_Weight").attr("disabled",true);
        	}
            _HeadImage = data.data[0].HeadImage;
            if (_HeadImage != "") { $(".Js_MyPhoto img").attr("src", SiteFunc.UploadImgUrl + data.data[0].HeadImage); }
            $(".Js_Name").val(data.data[0].UserName);
            $(".Js_ICNo").val(data.data[0].ICNo);
            $(".Js_Sex").val(data.data[0].Sex);
            $(".Js_Age").val(data.data[0].Age);
            $(".Js_Phone").val(data.data[0].Phone);
            $(".Js_Height").val(data.data[0].Height);
            $(".Js_Weight").val(data.data[0].Weight);
            $(".Js_SecurityStaffCardNo").val(data.data[0].SecurityStaffCardNo);
            $(".Js_IntentionOpenCityID").val(data.data[0].IntentionOpenCityID);
            //新增的3个服装照片
            if(data.data[0].Clothes3PicPath=="")
            {
            	$("#img_fz1").attr("src","images/Evaluation/mrphoto3.png"); //
            }
            else{
            	$("#img_fz1").attr("src",SiteFunc.UploadImgUrl + data.data[0].Clothes3PicPath);
            }
            if(data.data[0].Clothes1PicPath=="")
            {
            	$("#img_fz2").attr("src","images/Evaluation/mrphoto2.png");
            }
            else{
            	$("#img_fz2").attr("src",SiteFunc.UploadImgUrl + data.data[0].Clothes1PicPath);
            }
            if(data.data[0].Clothes2PicPath=="")
            {
            	$("#img_fz3").attr("src","images/Evaluation/mrphoto3.png");
            }
            else{
            	$("#img_fz3").attr("src",SiteFunc.UploadImgUrl + data.data[0].Clothes2PicPath);
            }
            
            //新增2个保安员证正反面照片
            if(data.data[0].SecurityStaffCardPath1=="")
            {
            	$("#img_fz4").attr("src","images/Evaluation/mrphoto4.png");
            }
            else{
            	$("#img_fz4").attr("src",SiteFunc.UploadImgUrl + data.data[0].SecurityStaffCardPath1);
            }
            if(data.data[0].SecurityStaffCardPath==""){
            	$("#img_fz5").attr("src","images/Evaluation/mrphoto6.png");
            }
            else{
            	$("#img_fz5").attr("src",SiteFunc.UploadImgUrl + data.data[0].SecurityStaffCardPath);
            }
  
            //新加银行信息
            $('#mui-ModifyData-bank').find('option').each(function (index, el){
            	if($(el).text().trim() == data.data[0].BankName) {
            		$(el).attr('selected', 'selected');
            		document.getElementById("mui-ModifyData-bank").selectedIndex = index;
            	}
            });
            $("#mui-ModifyDate-khyh").val(data.data[0].BankAddress);
            $("#mui-ModifyDate-yhzh").val(data.data[0].BankAccount);
            _IntentionOpenCityID = data.data[0].IntentionOpenCityID;

            $(".Register2-Cloth").val(data.data[0].HasClothes);
       
            $(".Js_Introduction").val(data.data[0].Introduction);
            $(".Js_word").text(data.data[0].Introduction.length);
            //          _HasClothes = (data.data[0].HasClothes == 'True' ? "是" : "否");
            _HasClothes = (data.data[0].HasClothes);
            if (_HasClothes == 'True') {
                _HasClothes = '是';
            } else {
                _HasClothes = '否';
            }
            if (_HasClothes == '是') {
                $(".Register1-YesIcon img").eq(0).removeClass("YesIcon2");
                $(".Register1-YesIcon img").eq(1).addClass("YesIcon2");
                $(".Register2-Cloth").text("是");
                $(".Register1_Costume").show();
//              $(".Js_Register1-Text1").text(data.data[0].Clothes1PicPath);
//              $(".Js_Register1-Text2").text(data.data[0].Clothes2PicPath);
            } else {
                $(".Register1-YesIcon img").eq(1).removeClass("YesIcon2");
                $(".Register1-YesIcon img").eq(0).addClass("YesIcon2");
                $(".Register2-Cloth").text("否");
                $(".Register1_Costume").hide();
//              $(".Js_Register1-Text1").text("请上传您黑白套装的照片");
//              $(".Js_Register1-Text2").text("请上传您黑色套装的照片");
            }
            //填充已开通城市
            CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", "", function (data) {
                var _cityList = "<option value='-1'>请选择你的意向城市</option>";
                if (data.data.length > 1) {
                    for (var i = 0; i < data.data.length; i++) {
                        _cityList += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                    }
                }
                $(".Js_IntentionOpenCityID").html(_cityList);
                $(".Js_IntentionOpenCityID").off().change(function () {
                    $(".Js_WantCityName").text($(this).find("option:selected").text());
                });
                $(".mui-sele-provice").val(_IntentionOpenCityID).change();
            });
            Cmn.App.CloseWaiting();
        }
    });




    $("#mui-LoginOKBtn").off().on("touchstart", function () {
        //照片头像
        //_HeadImage = $(".Js_MyPhoto").attr("src");
        //姓名
        _UserName = $(".Js_Name").val();
        //身份证
        _ICNo = $(".Js_ICNo").val();
        //性别
        _Sex = $(".Js_Sex").val();
        if (_Sex == '男') {
            _Sex = '1'
        }
        if (_Sex == '女') {
            _Sex = '2'
        }

        //年龄
        _Age = $(".Js_Age").val();
        //手机号
        _Phone = $(".Js_Phone").val();
        //净身高
        _Height = $(".Js_Height").val();
        //体重
        _Weight = $(".Js_Weight").val();
        //上岗证号
        _SecurityStaffCardNo = $(".Js_SecurityStaffCardNo").val();
        //意向城市代码
        _IntentionOpenCityID = $(".Js_IntentionOpenCityID").val();
        //是否自备服装
        _HasClothes = $(".Register2-Cloth").text();
        if (_HasClothes == '否') {
            _HasClothes = '0'
        }
        if (_HasClothes == '是') {
            _HasClothes = '1'
        }
//      _Clothes1PicPath = $(".Js_Register1-Text1").text();
//      _Clothes2PicPath = $(".Js_Register1-Text2").text();

        //自我介绍
        _Introduction = $(".Js_Introduction").val();
        //选择银行信息
        _BankName=$("#mui-ModifyData-bank").val().trim();
        //选择开户银行
        _BankAddress=$("#mui-ModifyDate-khyh").val().trim();
        //填写银行账号
        _BankAccount=$("#mui-ModifyDate-yhzh").val().trim();
        if(_BankName=="请选择你的银行"){SiteFunc.Alert("请选择银行");return;}
        if(_BankAddress==""){SiteFunc.Alert("请输入开户行");return;}
        if(_BankAccount==""){SiteFunc.Alert("请输入银行账号");return;}
        //if (_HeadImage == "") { SiteFunc.Alert("请上传头像"); return; }
        if (_UserName == "") { SiteFunc.Alert("请输入姓名"); return; }
        if (!SiteFunc.CheckCardID(_ICNo)) { SiteFunc.Alert("请输入正确的身份证号"); return; }
        if (_Age == "") { SiteFunc.Alert("请输入年龄"); return; }
        if (_Age <= 18 || _Age >= 99) { SiteFunc.Alert("请输入正确年龄"); return; }
        if (_Height == "") { SiteFunc.Alert("请输入身高"); return; }
        if (_Weight == "") { SiteFunc.Alert("请输入体重"); return; }
        if (isNaN(_Height)) {
            SiteFunc.Alert("请输入正确身高！");
            return;
        }
        if (_Height <= 0 || _Height >= 250) { SiteFunc.Alert("请输入正确身高"); return; }
        if (isNaN(_Weight)) {
            SiteFunc.Alert("请输入正确体重！");
            return;
        }
        if (_Weight <= 0 || _Weight >= 999) { SiteFunc.Alert("请输入正确体重"); return; }
        //if (_IntentionOpenCityID == "") { SiteFunc.Alert("请选择意向城市"); return; }
        if (_HasClothes == '1') {
//          if (_Clothes1PicPath == "" || _Clothes1PicPath == "请上传您黑白套装的照片") { SiteFunc.Alert("请上传您的黑白套装照片"); return; }
//          if (_Clothes2PicPath == "" || _Clothes2PicPath == "请上传您黑色套装的照片") { SiteFunc.Alert("请上传您的黑色套装照片"); return; }
            	if($("#img_fz1").attr("src")=="images/Evaluation/mrphoto1.png"&&$("#img_fz2").attr("src")=="images/Evaluation/mrphoto2.png"&&$("#img_fz3").attr("src")=="images/Evaluation/mrphoto3.png")
            	{
            		SiteFunc.Alert("请上传您的照片"); return;
            	}
        }
//      if($("#img_fz4").attr("src")=="images/Evaluation/mrphoto4.png"){SiteFunc.Alert("请上传您的保安员证照片"); return;}
//      if($("#img_fz5").attr("src")=="images/Evaluation/mrphoto6.png"){SiteFunc.Alert("请上传您的保安员证盖章页照片"); return;}
        var _parm = {
            "HeadImg": _HeadImage,
            "UserName": _UserName,
            "ICNo": _ICNo,
            "Sex": _Sex,
            "Age": _Age,
            "Phone": _Phone,
            "Height": _Height,
            "Weight": _Weight,
            "SecurityStaffCardNo": _SecurityStaffCardNo,
            "WantCity": _IntentionOpenCityID,
            "HasClothes": _HasClothes,
            //服装照片显示
            "ClothesOne": _SecurityServiceLicence_fz2,  //黑白套装图片Clothes1PicPath
            "ClothesTwo": _SecurityServiceLicence_fz3,  //黑色套装图片Clothes2PicPath
            "ClothesThree":_SecurityServiceLicence_fz1, //保安作训服图片Clothes3PicPath 
            "Introduction": _Introduction,
            "BankName":_BankName,
            "BankAddress":_BankAddress,
            "BankAccount":_BankAccount,
            //保安员证显示
            "SecurityStaffCardPath1":_SecurityServiceLicence_fz4,
            "SecurityStaffCardPath":_SecurityServiceLicence_fz5
        }

        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateUserInfo", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("修改成功！");
                SiteFunc.GoTo("Main.html?Menu=Home.html");
            } else {
                SiteFunc.Alert("修改失败！");
            }
        });

    });

    //返回
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });

    //我的照片
    SiteFunc.UploadImg(".Js_HeadImage", function (demo) {
        //$(".Js_CardImgTip").text($(demo).val());
        $(".LoadTip").show();
    }, function (data) {
        _HeadImage = data.Path;
        $(".Js_WoDeZhaoPian").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    }, function (data) {
        SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });

    //套装1
    SiteFunc.UploadImg(".Js_Clothes1PicPath", function (demo) {
        $(".Js_Register1-Text1").text($(demo).val());
        $(".LoadTip").show();
    }, function (data) {
        _Clothes1PicPath = data.Path;
        $(".LoadTip").hide();
    }, function (data) {
        SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });

    //套装2
    SiteFunc.UploadImg(".Js_Clothes2PicPath", function (demo) {
        $(".Js_Register1-Text2").text($(demo).val());
        $(".LoadTip").show();
    }, function (data) {
        _Clothes2PicPath = data.Path;
        $(".LoadTip").hide();
    }, function (data) {
        SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });

    //点击按钮切换服装
    $(".Register1-YesIcon").off().on("touchstart", function () {
        _IsNo = $(".Register2-Cloth").text();
        if (_IsNo == "是") {
            $(".Register1-YesIcon img").eq(1).removeClass("YesIcon2");
            $(".Register1-YesIcon img").eq(0).addClass("YesIcon2");
            $(".Register2-Cloth").text("否");
            $(".Register1_Costume").hide();
            
        }
        if (_IsNo == "否") {
            $(".Register1-YesIcon img").eq(0).removeClass("YesIcon2");
            $(".Register1-YesIcon img").eq(1).addClass("YesIcon2");
            $(".Register2-Cloth").text("是");
            $(".Register1_Costume").show();
            $("#img_fz1").attr("src","images/Evaluation/mrphoto1.png");
            $("#img_fz2").attr("src","images/Evaluation/mrphoto2.png");
            $("#img_fz3").attr("src","images/Evaluation/mrphoto3.png");
        }
    });
    //自我介绍
    $(".Js_Introduction").on("keyup keydown", function () {
        var _length = $(this).val().length;
        $(".Js_word").text(200 - _length);
    });
    //自动获取年龄
    $(".Js_ICNo").on("keyup keydown", function () {
        var _icNo = $(this).val();
        if (SiteFunc.CheckCardID(_icNo)) {
            $(".Js_Age").val(GetAgeByCard(_icNo));
            $(".Js_Sex").val(GetSexByCard(_icNo));
            $(".Js_Age").get(0).setAttribute('disabled', 'disabled'); 
            $(".JS_Sex").get(0).setAttribute('disabled', 'disabled');
        }
    });
    
    
    //服装上传1
    SiteFunc.UploadImg("#fz1",function(demo){
     $(".LoadTip").show();		
    },function(data){
    	_SecurityServiceLicence_fz1=data.Path;
    	$("#img_fz1").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    },function(data){
    	SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    //服装上传2
    SiteFunc.UploadImg("#fz2",function(demo){
     $(".LoadTip").show();		
    },function(data){
    	_SecurityServiceLicence_fz2=data.Path;
    	$("#img_fz2").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    },function(data){
    	SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    //服装上传3
    SiteFunc.UploadImg("#fz3",function(demo){
     $(".LoadTip").show();		
    },function(data){
    	_SecurityServiceLicence_fz3=data.Path;
    	$("#img_fz3").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    },function(data){
    	SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    //保安员证上传
    SiteFunc.UploadImg("#fz4",function(demo){
     $(".LoadTip").show();		
    },function(data){
    	_SecurityServiceLicence_fz4=data.Path;
    	$("#img_fz4").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    },function(data){
    	SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    //服装上传1
    SiteFunc.UploadImg("#fz5",function(demo){
     $(".LoadTip").show();		
    },function(data){
    	_SecurityServiceLicence_fz5=data.Path;
    	$("#img_fz5").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    },function(data){
    	SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    
});








