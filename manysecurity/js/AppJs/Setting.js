﻿// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {
    //意见反馈
    $(".Js_Idea").click(function () {
        $(".Js_IdeaPage").show();
        $(".Js_Back").show();
        $(".mui-title").text("意见反馈");
    });
    //返回
    $(".Js_Back").off().click(function () {
        $(this).hide();
        $(".Js_IdeaPage").hide();
        $(".mui-title").text("设置");
    });
    //提交
    $(".Js_Sub").click(function () {
        var _idea = $(".Js_IdeaText").val();
        if (_idea == "") {
            SiteFunc.Alert("请输入您的意见");
            return;
        }
        var _parm = {
            "FeedbackContent": _idea
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "AddUserIdea", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("提交成功！");
                $(".Js_IdeaText").val("");
                $('.Js_Back').trigger('click');
            } else {
                SiteFunc.Alert("提交失败！");
            }
        });
    });
    //关于我们
    $(".Js_About").parent().click(function () {
        SiteFunc.GoTo("Businessdescription.html");
    });

    //检查更新
    $(".Js_Version").parent().click(function () {
        checkUpdate();
    });
    
    //联系我们
    $('#lnkCallUS').click(function() {
        if(window['plus']) {
            plus.device.dial('4008216890', true);
        }
    });
});
//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    $("body").css("opacity", 1);
    Cmn.App.CloseWaiting();
    var _MessageSetting='1';

    $(".Js_mui-active").off().on("touchstart", function () {
        var _Push = $(".Js_mui-switch-handle").css("transform");
        if (_Push == 'matrix(1, 0, 0, 1, 0, 0)') {
            _MessageSetting = '1';
        } else {
            _MessageSetting = '0';
        }
        _Pram = {
            "MessageSetting": _MessageSetting
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdatePushInfo", _Pram, function (data) {
            if (data.IsSuccess == '1') {
                if (_MessageSetting == '1') {
                    SiteFunc.Alert("推送功能已开启！");
                } else {
                    SiteFunc.Alert("推送功能已取消！");
                }
            } else {

            }
        }, 1000);
    });


})

var wgtVer = null;
function plusReady() {
    // ......
    // 获取本地应用资源版本号
    plus.runtime.getProperty(plus.runtime.appid, function (inf) {
        wgtVer = inf.version;
        $(".Js_Version").text("版本信息(" + wgtVer + ")");
    });
}
if (window.plus) {
    plusReady();
} else {
    document.addEventListener('plusready', plusReady, false);
}


// 检测更新
var checkUrl = SiteFunc.UploadImgUrl + "GetVersion.aspx";
function checkUpdate() {
    console.log("检测更新...");
    plus.nativeUI.showWaiting("检测更新...");
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        switch (xhr.readyState) {
            case 4:
                plus.nativeUI.closeWaiting();
                if (xhr.status == 200) {
                    var newVer = xhr.responseText;
                    if (wgtVer && newVer && (wgtVer != newVer)) {
                        plus.nativeUI.confirm('检测到有更新版本，是否需要更新?', function(e){
                                if(e.index == 0) {
                                    downWgt();
                                }
                            }, '检查更新', '立即更新', '稍后更新');
                    } else {
                        plus.nativeUI.alert("您已经是最新版本！");
                    }
                } else {
                    plus.nativeUI.alert("检测更新失败，请稍后再试！");
                }
                break;
            default:
                break;
        }
    }
    xhr.open('GET', checkUrl);
    xhr.send();
}

// 下载wgt文件
//var wgtUrl = "http://demo.dcloud.net.cn/test/update/H5EF3C469.wgt";
var wgtUrl = SiteFunc.UploadImgUrl + "update/H56EC34ED.wgt";

function downWgt() {
    plus.nativeUI.showWaiting("正在下载更新...");
    plus.downloader.createDownload(wgtUrl, { filename: "_doc/update/" }, function (d, status) {
        if (status == 200) {
            console.log("下载成功：" + d.filename);
            installWgt(d.filename); // 安装wgt包
        } else {
            console.log("下载失败！");
            plus.nativeUI.alert("下载失败！");
            plus.nativeUI.closeWaiting();
        }
    }).start();
}

// 更新应用资源
function installWgt(path) {
    plus.nativeUI.closeWaiting();
    plus.nativeUI.showWaiting("下载更新完成，正在更新...");
    console.log("正在更新...");
    plus.runtime.install(path, {}, function () {
        plus.nativeUI.closeWaiting();
        console.log("版本更新成功！");
        plus.nativeUI.alert("应用资源更新完成！", function () {
            plus.runtime.restart();
        });
    }, function (e) {
        plus.nativeUI.closeWaiting();
        console.log("安装文件失败[" + e.code + "]：" + e.message);
        plus.nativeUI.alert("安装文件失败[" + e.code + "]：" + e.message);
    });
}