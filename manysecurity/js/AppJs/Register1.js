﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {
    $("#mui-Click-Tip").click(function () {
        SiteFunc.GoTo("UserProtocol.html");
    });
});
var _CompanyType = "";//公司类型  1物业  2保安公司  3企事业单位
var _CanSendSms = true,//是否可以发送短信
    _RegistPhone = "",        //手机号
    _HasSendSms = false,//是否已经发送短信
    _Password = "";//密码
var _AjaxUrl = SiteFunc.AjaxUrl;
var _Step = 1;

//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {

    Cmn.App.CloseWaiting();
    //状态初始化 
    _CompanyType = (data.CompanyType == undefined ? "1" : data.CompanyType);
    _CanSendSms = true;
    _RegistPhone = "";
    _HasSendSms = false;
    _Password = "";
    _Step = 1;
    _FanHuei = true;
    Registerd1.ShowDiv(0);

    $(".Js_Verify").off().click(function () {
        if (!_CanSendSms) {
            return;
        }
        var _phone = $(".Js_Phone").val();
        if (!(/^0?1[3|4|5｜7|8][0-9]\d{8}$/.test(_phone))) {
            SiteFunc.Alert("手机号格式错误");
            return;
        }
        _CanSendSms = false;
        CmnAjax.PostData(_AjaxUrl + "CompanyRegistCheckPhone", { Phone: _phone }, function (data) {
            if (data.IsSuccess == 1) {
                //发送短信
                CmnAjax.PostData(_AjaxUrl + "SendSms", { Phone: _phone }, function (data) {
                    if (data.IsSuccess == 1) {
                        _RegistPhone = _phone;
                        _HasSendSms = true;
                        Registerd1.CutDown();
                    }
                    else {
                        _CanSendSms = true;
                        SiteFunc.Alert(data.ErrMsg);
                    }
                });
            }
            else {
                _CanSendSms = true;
                SiteFunc.Alert(data.ErrMsg);
            }
        });

    });
    //下一步
    $(".Js_Next").off().click(function () {
        if (!_HasSendSms) {
            SiteFunc.Alert("请先发送短信验证");
            return;
        }
        var _code = $(".Js_Code").val();
        if (_code == "") {
            SiteFunc.Alert("请输入验证码");
            return;
        }
        CmnAjax.PostData(_AjaxUrl + "CheckSMSCode", { Code: _code }, function (data) {
            if (data.IsSuccess == 1) {
                _Step = 2;
                Registerd1.ShowDiv(1);
            }
            else {
                SiteFunc.Alert("验证码错误");
            }
        });
    });
    //设置密码
    $(".Js_Next1").off().click(function () {
        var _pwd = $(".Js_Password").val();
        if (_pwd == "") {
            SiteFunc.Alert("请输入密码");
            return;
        }
        if (_pwd.length < 6) {
            SiteFunc.Alert("密码长度不够");
            return;
        }
        Registerd1.ShowDiv(2);
        _Step = 3;
        _Password = _pwd;
        $(".Js_RegistPhone").text(_RegistPhone);
    });
    //$(".Js_Password").on("keyup", function () {
    //    if ($(this).val().length > 0) {
    //        $(this).attr("type", "password");
    //    }
    //    else {
    //        $(this).attr("type", "text");
    //    }
    //});
    Registerd1.UpDateInfo();
    SiteFunc.FillProvince();
    //返回上一页
    $(".Js_Back").off().click(function () {
        if (_HasRegist) {
            SiteFunc.GoTo("Main.html");
            return;
        }
        if (_FanHuei == true) {
            $(".Js_CompanyName").val("");
            $(".Js_Address").val("");
            $(".Js_LinkName").val("");
            $(".Js_LinkPhone").val("");
            $(".Js_Introduce").val("");
            $(".Js_Province").val("");
            $(".Js_City").val("");
            $(".Js_County").val("");
            $(".Js_Tip1").text("请上传保安服务许可证");
            $(".Js_Tip2").text("请上传营业执照");
            $(".Js_Tip3").text("请上传营业LOGO");
            SiteFunc.GoTo("Registered.html");
            _FanHuei = false;
        } else {
            $(".Js_YongHuXieYi").hide();
            _FanHuei = true;
        }

    });
});
var _HasRegist = false;
(Registerd1 = new function () {
    var _Self = this;
    //第三步保存数据
    this.UpDateInfo = function () {
        var _licenseImg = "",
            _businessLicenseImg = "",
            _logo = "",
            _flag = false;
        $(".Js_SubUserInfo").off().click(function () {
            if (_flag) { return; }
            var _name = $(".Js_CompanyName").val();
            var _address = $(".Js_Address").val();
            var _linkName = $(".Js_LinkName").val();
            var _linkPhone = $(".Js_LinkPhone").val();
            var _introduce = $(".Js_Introduce").val();
            var _provinceID = $(".Js_Province").val();
            var _cityID = $(".Js_City").val();
            var _countyID = $(".Js_County").val();
            if (_provinceID == "-1") {
                _provinceID = "";
            }
            if (_cityID == "-1") {
                _cityID = "";
            }
            if (_countyID == "-1") {
                _countyID = "";
            }
            //if (_name == "") {
            //    SiteFunc.Alert("请输入公司名称");
            //    return;
            //}
            //if (_provinceID == "-1") {
            //    SiteFunc.Alert("请选择省份");
            //    return;
            //}
            //if (_cityID == "-1") {
            //    SiteFunc.Alert("请选择城市");
            //    return;
            //}
            //if (_countyID == "-1" && $(".Js_County option").length>1) {
            //    SiteFunc.Alert("请选择区");
            //    return;
            //}
            //if (_address == "") {
            //    SiteFunc.Alert("请输入公司地址");
            //    return;
            //}
            //if (_linkName == "") {
            //    SiteFunc.Alert("请输入联系人姓名");
            //    return;
            //}
            //if (_linkPhone == "") {
            //    SiteFunc.Alert("请输入联系人电话");
            //    return;
            //}
            //if (_licenseImg == "") {
            //    SiteFunc.Alert("请上传保安服务许可证");
            //    return;
            //}
            //if (_businessLicenseImg == "") {
            //    SiteFunc.Alert("请上传营业执照");
            //    return;
            //}
            //if (_businessLicenseImg == "") {
            //    SiteFunc.Alert("请上公司Logo");
            //    return;
            //}
            //if (_introduce == "") {
            //    SiteFunc.Alert("请输入企业介绍");
            //    return;
            //}
            //if ($(".Js_County option").length == 1) {
            //    _countyID = "";
            //}
            var parm = {
                Phone: _RegistPhone,
                Password: _Password,
                CompanyType: _CompanyType,
                CompanyName: _name,
                ProvinceID: _provinceID,
                CityID: _cityID,
                CountryID: _countyID,
                CompanyAddress: _address,
                LinkMan: _linkName,
                LinkPhone: _linkPhone,
                SecurityServiceLicence: _licenseImg,
                BusinessLicense: _businessLicenseImg,
                CompanyLogo: _logo,
                CompanyIntroduce: _introduce
            }
            _flag = true;
            //注册公司完善资料
            CmnAjax.PostData(_AjaxUrl + "CompanyRegist", parm, function (data) {
                if (data.IsSuccess == 1) {
                    $(".Js_CompanyName").val("");
                    $(".Js_Address").val("");
                    $(".Js_LinkName").val("");
                    $(".Js_LinkPhone").val("");
                    $(".Js_Introduce").val("");
                    $(".Js_Province").val("");
                    $(".Js_City").val("");
                    $(".Js_County").val("");
                    $(".Js_Tip1").text("请上传保安服务许可证");
                    $(".Js_Tip2").text("请上传营业执照");
                    $(".Js_Tip3").text("请上传营业LOGO");
                    SiteFunc.Alert("注册成功");
                    SiteFunc.SetLoginStatus(true, data.CorpID, data.UserID);
                    setTimeout(function () {

                        _flag = false;
                        _HasRegist = true;
                        SiteFunc.GoTo("Main.html");
                    }, 1000);
                }
                else {
                    SiteFunc.Alert("注册失败，请稍后在试");
                    _flag = false;
                }
            });

        });
        //查看协议
        $(".Js_XieYi").off().click(function () {
            $(".Js_YongHuXieYi").show();
            _FanHuei = false;
        });


        //保安服务许可证
        SiteFunc.UploadImg(".Js_LicenseImg", function (demo) {
            $(".Js_Tip1").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _licenseImg = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //营业执照
        SiteFunc.UploadImg(".Js_BusinessLicenseImg", function (demo) {
            $(".Js_Tip2").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _businessLicenseImg = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //企业Logo
        SiteFunc.UploadImg(".Js_Logo", function (demo) {
            $(".Js_Tip3").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _logo = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
    }
    //倒计时
    this.CutDown = function () {
        var _time = 60;
        $(".Js_Verify").css("background-color", "#e3e3e3");
        var _countdownTimer = setInterval(function () {
            $(".Js_Verify").html((--_time) + "S");
            if (_time == 0) {
                $(".Js_Verify").text("验证");
                $(".Js_Verify").css("background-color", "#4183ed");
                clearInterval(_countdownTimer);
                _CanSendSms = true;
            }
        }, 1000);
    }
    //显示步骤
    this.ShowDiv = function (step) {
        if (step == 0) { $("#LoginTitle").text("物业/保安公司注册"); }
        else if (step == 1) { $("#LoginTitle").text("设置密码"); }
        else if (step == 2) { $("#LoginTitle").text("完善企业资料"); }
        $(".mui-input-group").hide();
        $(".mui-input-group").eq(step).show();
    }
});