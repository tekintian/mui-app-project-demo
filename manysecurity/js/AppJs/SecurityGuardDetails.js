﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

    });


//每次都执行
    Cmn.App.CurPage.BeforeShow.Add(function (data) {
        window._aid = data.ActivityID;
        window._data = data;
    	    //姓名
    var _UserName = "";
    //身份证号
    var _ICNo = "";
    //性别
    var _Sex = "";
    //年龄
    var _Age = "";
    //手机号
    var _Phone = "";
    //身高
    var _Height = "";
    //体重
    var _Weight = "";
    //保安员证编号
    var _SecurityStaffCardNo = "";
    //个人介绍
    var _Introduction = "";
    //体重
    var _Weight = "";
    //黑白套装图片1
    var _Clothes1PicPath = "";
    //黑白套装图片2
    var _Clothes2PicPath = "";
    var _activityApplyID = (data.ActivityApplyID == undefined ? "" : data.ActivityApplyID);
    var _activityID = (data.ActivityID==undefined ? "" : data.ActivityID );
    var _parm = {
        "ActivityApplyID": _activityApplyID
    }

    CmnAjax.PostData(SiteFunc.AjaxUrl + "GetToSignUpList",_parm, function (data) {
        if (data.data.length == 1) {
        	if(data.data[0].HeadImage=="")
        	{
        		if(data.data[0].Sex=="1")
        		{
        		    $("#privateimg").attr('src','images/headman.png');	
        		}
        		else
        		{
        			$("#privateimg").attr('src','images/headwoman.png');
        		}
        		
        	}
        	else
        	{
        		$("#privateimg").attr('src',SiteFunc.UploadImgUrl+data.data[0].HeadImage);
        	}
        	$('#LoginTitle').text(data.data[0].UserName);
            $(".Js_CompanyName").text(data.data[0].CompanyName);
            $(".Js_Name").text(data.data[0].UserName);
            $(".Js_Count").text(data.data[0].ping);
            if(data.data[0].Status == 2) {
				$(".Js_ICNo").text(data.data[0].ICNo);
            }
            else {
            	var icn = data.data[0].ICNo.substring(0,14);
            	$('.Js_ICNo').text(icn + '****');
            }

            if (data.data[0].Sex == "1") {
                $(".Js_Sex").text("男");
            }
            if (data.data[0].Sex == "2") {
                $(".Js_Sex").text("女");
            }
//          if (data.data[0].Sex == "3") {
//              $(".Js_Sex").text("不限");
//          }
            $(".Js_Age").text(data.data[0].Age);
            _Phone = data.data[0].Phone;
            if (data.data[0].Status == 2) {
                $(".Js_Phone").text(_Phone);
            }
            else {
                var src1 = _Phone.substring(0, 2);
                var src2 = _Phone.substring(9, 10)
                $(".Js_Phone").text(src1 + "******" + src2);
            } 
            if(data.data[0].IsApprove !="")
            {
            	if(data.data[0]["IsApprove"]=="True")
	            {
	            	$("#cxba_img").show();
	            }
	            else
	            {
	            	$("#cxba_img").hide();
	            }
            }
            else
            {
            	$("#cxba_img").hide();
            }
            $(".Js_Height").text(data.data[0].Height);
            $(".Js_Weight").text(data.data[0].Weight);
            $(".Js_SecurityStaffCardNo").text(data.data[0].SecurityStaffCardNo);
            $(".Js_Introduction").text(data.data[0].Introduction);
            if(data.data[0].Clothes1PicPath!=""){
            	$(".Js_photo1").show();
                $(".Js_photo1 img").attr("src", SiteFunc.UploadImgUrl + data.data[0].Clothes1PicPath);
            }else{
                $(".Js_photo1").hide();
            }
            if(data.data[0].Clothes2PicPath!=""){
            	$(".Js_photo2").show();
                $(".Js_photo2 img").attr("src", SiteFunc.UploadImgUrl + data.data[0].Clothes2PicPath);
            }else{
                $(".Js_photo2").hide();
            }            
            if(data.data[0].Clothes3PicPath!=""){
            	$(".Js_photo3").show();
                $(".Js_photo3 img").attr("src", SiteFunc.UploadImgUrl + data.data[0].Clothes3PicPath);
            }else{
                $(".Js_photo3").hide();
            }
            
            if(data.data[0]["Status"]=="1"){
            	$(".Js_button").show();
            }
            else {
            	$(".Js_button").hide();
            }
            
        }else{
        
        }


		$(".Photographs").off().on("touchstart", function() {
			var img = $(this).find("img").attr("src");
			var Title = "着装信息";
			SiteFunc.GoTo(SiteFunc.GoTo("Photographs.html?ActivityApplyID=" + _activityApplyID + '&ActivityID=' + _activityID + '&Img='+img+'&Title='+Title));
		})
		
		
		$(".privateimg_").off().on("touchstart",function () {
			var img = $(this).find("img").attr("src");
			var Title = $(".mui-SecurityName").text();
			SiteFunc.GoTo(SiteFunc.GoTo("Photographs.html?ActivityApplyID=" + _activityApplyID + '&ActivityID=' + _activityID + '&Img='+img + '&Title='+Title));
		})

        var _Sert = this;
        var _Status = "";
        //点击通过
    $(".Js_Through").off().click(function () {
        _Status = 2;
        var _parm = {
            "Status": _Status,
            "ActivityApplyID": _activityApplyID
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateApplyXiouGaiZhuangTaiJia", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("修改成功！");
                $('.Js_Back').trigger('click');
            } else {
                SiteFunc.Alert("修改失败！");
            }
        });
    });
        //上一步
    $(".Js_Back").off().click(function () {
        var _parm = {
            "ActivityApplyID": _activityApplyID
        }
        
//      CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateApplyXiouGaiZhuangTaiJia", _parm, function (data) {
//          if (data.IsSuccess == 1) {
//              SiteFunc.GoTo("CheckUp.html?");
//          }
//      });

		SiteFunc.GoTo("CheckUp.html?ActivityID=" + window._aid);
    
    });
        //点击不通过
    $(".Js_NotThrough").off().on("touchstart", function () {
        _Status = 3;
        var _parm = {
            "Status": _Status,
            "ActivityApplyID": _activityApplyID
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateApplyXiouGaiZhuangTaiJia", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("修改成功！");
                $('.Js_Back').trigger('click');
            } else {
                SiteFunc.Alert("修改失败！");
            }
        });
    });

    });
    this.Kais = function () {

        var _parm = {
            "ActivityApplyID": _activityApplyID,
            "Status": _Status
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetToSignUpList", _parm, function (data) {
            if (data.data.length == 1) {
                var _html = "";
                for (var _i = 0; _i < data.data.length; _i++) {
                    _html += "<p class='mui-Recuitment-action' text=" + data.data[_i]["UserID"] + " html=" + data.data[_i]["Status"] + ">" +
                         "<span class='mui-CheckUp-Title'>" + data.data[_i]["UserName"] + "</span><span class='mui-CheckUp-Text'>金牌保安获评<b>" + data.data[_i]["ping"] + "</b>次</span><br />" +
                          "<span><img src='images/Evaluation/ThroughIcon2.jpg' /></span>" +
                           "<img src='images/Evaluation/AuditIcon.png' class='mui-AuditIconbtn' />"
                    "</p>"
                }
                $(".Js_mui-input-row Recr-Content").html(_html);
            }
        });
    }

});