﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Register2.ShowDiv(0);
})
(Register2 = new function () {
    var _CanSendSms = true,//是否可以发送短信
        _RegistPhone = "",        //手机号
        _HasSendSms = false;//是否已经发送短信
    var _Self = this;
    var _AjaxUrl = SiteFunc.AjaxUrl;
    var _Step = 1;
    $(function () {
        //发送短信
        $(".Js_Verify").click(function () {
            if (!_CanSendSms) {
                return;
            }
            var _phone = $(".Js_Phone").val();
            if (!SiteFunc.CheckPhone(_phone)) {
                SiteFunc.Alert("手机号格式错误");
                return;
            }
            _CanSendSms = false;
            CmnAjax.PostData(_AjaxUrl + "SendSms", { Phone: _phone }, function (data) {
                if (data.IsSuccess == 1) {
                    _Self.CutDown();
                    _HasSendSms = true;
                    _RegistPhone = _phone;
                }
            });
        });
        //下一步
        $(".Js_Next").click(function () {
            if (!_HasSendSms) {
                SiteFunc.Alert("请先发送短信验证");
                return;
            }
            var _code = $(".Js_Code").val();
            if (_code == "") {
                SiteFunc.Alert("请输入验证码");
                return;
            }
            CmnAjax.PostData(_AjaxUrl + "CheckSMSCode", { Code: _code }, function (data) {
                if (data.IsSuccess == 1) {
                    _Step = 2;
                    _Self.ShowDiv(1);
                }
                else {
                    SiteFunc.Alert("验证码错误");
                }
            });
        });
        //设置密码
        $(".Js_Next1").click(function () {
            var _pwd = $(".Js_Password").val();
            if (_pwd == "") {
                SiteFunc.Alert("请输入密码");
                return;
            }
            if (_pwd.length < 6) {
                SiteFunc.Alert("密码长度不够");
                return;
            }
            CmnAjax.PostData(_AjaxUrl + "Register", { Phone: _RegistPhone, Password: _pwd }, function (data) {
                if (data.IsSuccess == 1) {
                    _Self.ShowDiv(2);
                    _Step = 3;
                    //注册账号
                    $(".Js_RegistPhone").text(_RegistPhone);
                }
                else {
                    SiteFunc.Alert(data.ErrMsg);
                }
            });
        });
        //上一步
        $(".Js_Back").off().click(function () {
            SiteFunc.GoTo("Registered.html");
        });
        _Self.FillOpenCity();
        _Self.UpDateInfo();
    });
    //倒计时
    this.CutDown = function () {
        var _time = 60;
        $(".Js_Verify").css("background-color", "#e3e3e3");
        var _countdownTimer = setInterval(function () {
            $(".Js_Verify").html((--_time) + "S");
            if (_time == 0) {
                $(".Js_Verify").text("验证");
                $(".Js_Verify").css("background-color", "#4183ed");
                clearInterval(_countdownTimer);
                _CanSendSms = true;
            }
        }, 1000);
    }
    //第三步保存用户信息
    this.UpDateInfo = function () {
        var _idCardImg1 = "",
            _idCardImg2 = "",
            _workLicense = "",
            _hasCloth = true,
            _cloth1 = "",
            _cloth2 = "",
            _age = "";

        $(".Js_SubUserInfo").click(function () {
            var _name = $(".Js_Name").val().trim();
            var _idCard = $(".Js_IDCard").val().trim();
            var _sex = $(".Js_Sex").val();
            var _height = $(".Js_Height").val().trim();
            var _weight = $(".Js_Weight").val().trim();
            var _papers = $(".Js_Papers").val().trim();
            var _city = $(".Js_WantCity").val();
            var _introduce = $(".Js_Introduce").val().trim();
            if (_name == "") { SiteFunc.Alert("请输入姓名"); return; }
            if (!SiteFunc.CheckCardID(_idCard)) { SiteFunc.Alert("请输入正确身份证号"); return; }
            if (_idCardImg1 == "") { SiteFunc.Alert("请上传身份证正面照"); return; }
            if (_idCardImg2 == "") { SiteFunc.Alert("请上传身份证反面照"); return; }
            if (_sex == "-1") { SiteFunc.Alert("请选择您的性别"); return; }
            if (isNaN(_height)) { SiteFunc.Alert("请输入正确的身高"); return; }
            if (isNaN(_weight)) { SiteFunc.Alert("请输入正确的体重"); return; }
            if (_papers == "") { SiteFunc.Alert("请输入您的保安员证件编号"); return; }
            //if (_workLicense == "") { SiteFunc.Alert("请上传您的上岗证照片"); return; }
            if (_city == "-1") { SiteFunc.Alert("请选择意向城市"); return; }
            if (_hasCloth) {
                if (_cloth1 == "") { SiteFunc.Alert("请上传您的黑白套装照片"); return; }
                if (_cloth2 == "") { SiteFunc.Alert("请上传您的黑白套装照片"); return; }
            }
            if (_introduce == "") { SiteFunc.Alert("请输入个人介绍"); return; }
            var parm = {
                UserName: _name,
                ICNo: _idCard,
                ICNo1PicPath: _idCardImg1,
                ICNo2PicPath: _idCardImg2,
                Sex: _sex,
                Age: _age,
                Phone: _RegistPhone,
                Height: _height,
                Weight: _weight,
                SecurityStaffCardNo: _papers,
                SecurityStaffCardPath: _workLicense,
                WantCityID: _city,
                HasClothes: _hasCloth == true ? 1 : 0,
                ClothesOne: _cloth1,
                ClothesTwo: _cloth2,
                Introduce: _introduce
            }
            CmnAjax.PostData(_AjaxUrl + "UpdateUserInfo", parm, function (data) {
                if (data.IsSuccess == 1) {
                    $(".FloatError").show();
                    setTimeout(function () {
                        $(".FloatError").hide();
                        SiteFunc.GoTo("Main.html");
                    }, 3000);
                }
                else {
                    SiteFunc.Alert("资料提交失败");
                }
            });

        });
        //根据身份证号获取年龄
        $(".Js_IDCard").on("keyup keydown", function () {
            var _idCard = $(this).val();
            if (SiteFunc.CheckCardID(_idCard)) {
                _age = GetAgeByCard(_idCard);
            }
        });
        //身份证正面
        SiteFunc.UploadImg(".Js_CardImg1", function (demo) {
            $(".Js_CardImgTip1").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _idCardImg1 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //身份证反面
        SiteFunc.UploadImg(".Js_CardImg2", function (demo) {
            $(".Js_CardImgTip2").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _idCardImg2 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //上岗证
        SiteFunc.UploadImg(".Js_WorkLicense", function (demo) {
            $(".Js_WorkLicenseTip").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _workLicense = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //服装一
        SiteFunc.UploadImg(".Js_ClothImg1", function (demo) {
            $(".Js_ClothImgTip1").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _cloth1 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //服装一
        SiteFunc.UploadImg(".Js_ClothImg2", function (demo) {
            $(".Js_ClothImgTip2").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _cloth2 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });

        //服装自备与否
        $(".Js_HasCloth").click(function () {
            $(this).find("img").removeClass("YesIcon2");
            if (_hasCloth) {
                _hasCloth = false;
                $(".Js_HasClothTip").text("否");
                $(this).find("img").eq(0).addClass("YesIcon2");
                $(".Register1_Costume").hide();
                return;
            }
            _hasCloth = true;
            $(".Js_HasClothTip").text("是");
            $(this).find("img").eq(1).addClass("YesIcon2");
            $(".Register1_Costume").show();
        });
        $(".Js_Password").on("keydown keyup", function () {
            if ($(this).val().length > 0) {
                $(this).attr("type", "password");
            }
            else {
                $(this).attr("type", "text");
            }
        });
        $(".Js_Introduce").on("keydown keyup", function () {
            var _wordlength = $(this).val().length;
            $(".Js_Tip").text(200 - _wordlength);
        });
    }
    //填充已开通城市
    this.FillOpenCity = function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", "", function (data) {
            var _cityList = "<option value='-1'>请选择你的意向城市</option>";
            if (data.data.length > 1) {
                for (var i = 0; i < data.data.length; i++) {
                    _cityList += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                }
            }
            $(".Js_WantCity").html(_cityList);
        });
    }
    this.ShowDiv = function (step) {
        if (step == 0) { $("#LoginTitle").text("保安员注册"); }
        else if (step == 1) { $("#LoginTitle").text("设置密码"); }
        else if (step == 2) { $("#LoginTitle").text("完善个人资料"); }
        $(".mui-input-group").hide();
        $(".mui-input-group").eq(step).show();
    }
    function GetAgeByCard(userCardNo) {
        var myDate = new Date();
        var month = myDate.getMonth() + 1;
        var day = myDate.getDate();
        var age = myDate.getFullYear() - userCardNo.substring(6, 10) - 1;
        if (userCardNo.substring(10, 12) < month || userCardNo.substring(10, 12) == month && userCardNo.substring(12, 14) <= day) {
            age++;
        }
        return age;
    }
});