﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

});


//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {

    //反馈内容
    var _FeedbackContent = "";
 
    //上一步
    $(".Js_Back").off().click(function () {
        SiteFunc.Back();
    });
    $(".mui-btn-outlined").off().on("touchstart", function () {
        //反馈内容 
        _FeedbackContent = $(".Js_feedback").val();
        if (_FeedbackContent == "") {
            SiteFunc.Alert("请填写内容！");
            return;
        }
        var _parm = { 
            "FeedbackContent": _FeedbackContent
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "AddUserIdea", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("提交成功！");
            } else {
                SiteFunc.Alert("提交失败！");
            }
        });

    });










});












