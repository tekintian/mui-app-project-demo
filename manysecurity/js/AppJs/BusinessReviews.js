﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../jquery.cycle.all.js" />
//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {
    $(".Js_Back").off().click(function () {
    	if(data.From!=null && data.From=="ActionRement2.html")
    	{
    		SiteFunc.GoTo("ActionRement2.html?Back=1&CorpID="+data.CorpID);
    	}
    	else if(data.From!=null && data.From=="ActionRement.html")
    	{
    		SiteFunc.GoTo("ActionRement.html?Back=1&CorpID="+data.CorpID);
    	}
    	else
    	{
    		SiteFunc.GoTo("Main.html");
    	}
        
    });
});
//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    BusinessReviews.CorpID = (data.CorpID == undefined ? "" : data.CorpID);
    BusinessReviews.Company = (data.CompanyName == undefined ? "" : data.CompanyName);
    $(".Js_Company").text(BusinessReviews.Company);
    BusinessReviews.FillEvaluate();
});
(BusinessReviews = new function () {
    var _Self = this;
    this.CorpID = "";
    this.CompanyName = "";
    this.FillEvaluate = function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetAllEvaluate", { CorpID: _Self.CorpID }, function (data) {
            if (data.data.length > 0) {
                $(".Js_OnTime").text(data.data[0]["OnTime"] + "%");
                $(".Js_OneWeek").text(data.data[0]["OneWeek"] + "%");
                $(".Js_TwoWeek").text(data.data[0]["TwoWeek"] + "%");
                $(".Js_OneMonth").text(data.data[0]["OneMonth"] + "%");
                $(".Js_TwoMonth").text(data.data[0]["TwoMonth"] + "%");
            }
        });
    }
});
