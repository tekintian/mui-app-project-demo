﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

});


//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    var _indexOf = false;
    //上一步
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });

    CmnAjax.FillData(".Js_NeiRong", SiteFunc.AjaxUrl + "GetCompanyEvaluate", "", function (data) {

        if (data.data.length >= 1) {
            $(".DownBox").off().on("touchstart", function () {
                var _self = this;
                var _ActivityID = $(this).attr("activityid");
                var str = $(".DownBox img").eq(0).attr("class")
                if (_indexOf == false) {
                    $(this).children().eq(1).removeClass("YesIcon2");
                    $(this).children().eq(0).addClass("YesIcon2");
                    $(this).parent().nextAll(".Js_Date").show();
                    _indexOf = true;

                }
                else {
                    $(this).children().eq(0).removeClass("YesIcon2");
                    $(this).children().eq(1).addClass("YesIcon2");
                    $(this).parent().nextAll(".Js_Date").hide();
                    _indexOf = false;
                }
            });
            $('form').removeClass('emptyContent');
        } else {
            $(".mui-input-row").hide();
            $('form').addClass('emptyContent');
        }
    });


});