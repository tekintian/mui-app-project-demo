﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

});


//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    var _Sert = this;
    var _state = false;
    var _HeadImage = "";
    var _UserName = "";
    var _ICNo = "";
    var _Sex = "";
    var _Age = "";
    var _Phone = "";
    var _Height = "";
    var _Weight = "";
    var _SecurityStaffCardNo = "";
    var _IntentionOpenCityID = "";
    var _HasClothes = "";
    var _Clothes1PicPath = "";
    var _Clothes2PicPath = "";
    var _Introduction = "";
    var _Status = "";
    var _ActivityApplyID = "";
    var _ShengHe = false;
    var _SaiXuan = false;
    var _userID = "";
    var _count = "";
    var _PaiHang = true;
    var _activityID = (data.ActivityID == undefined ? "" : data.ActivityID);

    var _fillToBaoMingUpList = function (param) {
        //CmnAjax.FillData(".Js_CheckUpList", SiteFunc.AjaxUrl + "GetToBaoMingUpList", param, function (data) {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetToBaoMingUpList", param, function (data) {       	
            if (data.data.length >= 1) {
                var _html="";
                var _cxba="";
                var _tp="";
                for (var _i = 0; _i < data.data.length; _i++) {
                	debugger;
                	if(data.data[_i].IsApprove=="True")
                	{
                		_cxba="<img src='images/New/Goldsecurity.png' />";
                	}
                	else{
                		_cxba="<img src='' />";
                	}
                	if(data.data[_i].HeadImage=="")
                	{
                		if(data.data[_i].Sex=="2")
                		{
                			_tp="images/headman.png";
                		}
                		else
                		{
                			_tp="images/headwoman.png";
                		}
                		
                	}
                	else
                	{
                		_tp=SiteFunc.UploadImgUrl+data.data[_i].HeadImage;
                	}
                    _html += "<div style='width:100%;height:167px;'><p class='head_picture'><img aid=" + data.data[_i]["ActivityApplyID"] + " src="+_tp+" style='width:103px;height:103px;margin-top:18px;margin-left:10px;' /></p>"+
                      "<p class='mui-Recuitment-action Js_CheckUpList  'aid=" + data.data[_i]["ActivityApplyID"] + " text=" + data.data[_i]["UserName"] + ">" +
                      "<span class='mui-CheckUp-Title Js_Name' data-aid='" + data.data[_i]["ActivityID"] + "' data-id='" + data.data[_i]["UserID"] + "' >" + data.data[_i]["UserName"] + "</span>" +
                      "<span class='cxba_btn'>"+_cxba+"</span>"+
                      "<span><img class='dat-Status-src' src='" + (data.data[_i]["Status"] == "" ? "" : data.data[_i]["Status"]) + "' /></span>" +
                      data.data[_i]["Operate"] +
                      "<br /><span class='mui-CheckUp-Text'>好保安获评<b class='dat-ping'>" + data.data[_i]["ping"] + "</b>次</span>"+
                  "</p></div>"
                }
                $(".Js_mui-input-row").html(_html);
                
                //点击确定
                $(".Js_QueDing").off().on("click", function () {
                    //var _self = this;
                    //var _userID = $(this).parent().attr("text");
                    var _parm = {
                        "UserID":_userID,
                        "Status": _Status,
                        "ActivityApplyID": _ActivityApplyID
                    }
                    CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateApplyXiouGaiZhuangTaiJia", _parm, function (data) {
                        if (data.IsSuccess == 1) {
                            SiteFunc.Alert("操作成功！");
                            //_Sert.Kais();
                            $(".mui-ActivityRe-Float").hide();
                            _fillToBaoMingUpList({ ActivityID: _activityID });
                            
                        } else {
                            SiteFunc.Alert("操作失败！");
                        }
                    });
                });
                  //点击姓名跳转页面
                  $(".Js_Name").off().on("touchstart", function () {
                        var _activityApplyID = $(this).parent().attr("aid");
//                      var _activityID = $(this).attr('data-aid');
//                    alert(_activityApplyID);
//				      var _userID = $(this).attr('data-id');
                      SiteFunc.GoTo("SecurityDetails.html?ActivityApplyID=" + _activityApplyID + '&ActivityID=' + _activityID + '&From=CheckUp.html?ActivityID=' + _activityID);
                  });
                  $(".head_picture img").off().on("touchstart",function(){
                  	 var _activityApplyID = $(this).attr("aid");
                  	 SiteFunc.GoTo("SecurityDetails.html?ActivityApplyID=" + _activityApplyID + '&ActivityID=' + _activityID + '&From=CheckUp.html?ActivityID=' + _activityID);
                  });

                //点击加号
                $(".mui-AuditIconbtn").off().on("touchstart", function () {
                	//第一次默认为通过
                	$(".Js_state p").children().eq(0).children().eq(1).removeClass("mui-public-btn");
			        $(".Js_state p").children().eq(0).children().eq(0).addClass("mui-public-btn");
			        $(".Js_state p").children().eq(1).children().eq(0).removeClass("mui-public-btn");
			        $(".Js_state p").children().eq(1).children().eq(1).addClass("mui-public-btn");
                    $(".mui-ActivityRe-Float").show();
                    _ActivityApplyID = $(this).parent().attr("aid");
                    _userID = $(this).parent().attr("text");
                    _Status = 2;
                });
            }
            else {
                $(".Js_mui-input-row").html("");
            }
        });
        }
 
    ////点击查看详情
    //$(".Js_mui-pull-right").off().on("touchstart", function () {
    //    SiteFunc.GoTo("SecurityDetails.html?ActivityID=" + _activityID);
    //});

    _fillToBaoMingUpList({ ActivityID: _activityID });



    //点击取消
    $(".Js_QuXiao").off().on("touchstart", function () {
        $(".mui-ActivityRe-Float").hide();
    });

    //点击审核状态
    $(".Js_audit").off().on("touchstart", function () {
        if (_ShengHe == false) {
            $(".Js_ShengHeZuangTai").show();
            _Status = 2;
            _ShengHe = true;
        } else {
            $(".Js_ShengHeZuangTai").hide();
            _Status = "";
            _ShengHe = false;
        }
    });


    //选择审核通过
    $(".Js_ShengHes span").eq(0).off().on("touchstart", function () {
        $(".Js_ShengHes span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_ShengHes span").eq(0).children().eq(0).addClass("sexselect");
        $(".Js_ShengHes span").eq(1).children().eq(0).removeClass("sexselect");
        $(".Js_ShengHes span").eq(1).children().eq(1).addClass("sexselect");
        _Status = 2;
    });
    //选择审核不通过
    $(".Js_ShengHes span").eq(1).off().on("touchstart", function () {
        $(".Js_ShengHes span").eq(0).children().eq(0).removeClass("sexselect");
        $(".Js_ShengHes span").eq(0).children().eq(1).addClass("sexselect");
        $(".Js_ShengHes span").eq(1).children().eq(1).removeClass("sexselect");
        $(".Js_ShengHes span").eq(1).children().eq(0).addClass("sexselect");
        _Status = 3;
    });
    //审核状态选择确定
    $(".Js_More").off().on("touchstart", function () {
        $(".Js_ShengHeZuangTai").hide();
        _Sert.Kais();
    });
    //审核状态点击取消
    
    $(".Js_QuXiao_two").off().on("touchstart", function () {
        _Status = "";
        _Sert.Kais();
          $(".Js_ShengHeZuangTai").hide();
        $(".Js_ShengHes span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_ShengHes span").eq(0).children().eq(0).addClass("sexselect");
        $(".Js_ShengHes span").eq(1).children().eq(0).removeClass("sexselect");
        $(".Js_ShengHes span").eq(1).children().eq(1).addClass("sexselect");
    });
    //点击排行
    $(".Js_ranking").off().on("touchstart", function () {
        if (_PaiHang == false) {
            _count = 1;
            _ShengHe = true;
        } else {
            _count = "";
            _ShengHe = false;
        }
        _Sert.Kais();
    });


    //更多筛选
    $(".Js_ShaiXuan").off().on("touchstart", function () {
        if (_SaiXuan == false) {
            $(".Js_GengDuo").show();
            _Sex = 1;
            _SecurityStaffCardNo = 1;
            _HasClothes = 1;
            _SaiXuan = true;
        } else {
            $(".Js_GengDuo").hide();
            _Sex = "";
            _SecurityStaffCardNo = "";
            _HasClothes = "";
            _SaiXuan = false;
        }
    });
    //前往
    $(".Js_QianWang").off().on("touchstart", function () {
        $(".Js_GengDuo").hide();
        _Sert.Kais();
    });

    ////前往(确定)
    //$(".Js_QingChu").off().on("touchstart", function () {
    //    _Sert.Kais();
    //});

    //清除筛选(取消)
    $(".Js_QingChu").off().on("touchstart", function () {
        _Sex = "";
        _SecurityStaffCardNo = "";
        _HasClothes = "";
        $(".Js_GengDuo").hide();
        //性别
        $(".Js_Sex p span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_Sex p span").eq(0).children().eq(0).addClass("sexselect");
        $(".Js_Sex p span").eq(1).children().eq(0).removeClass("sexselect");
        $(".Js_Sex p span").eq(1).children().eq(1).addClass("sexselect");
        //上岗证
        $(".Js_CardNo p span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_CardNo p span").eq(0).children().eq(0).addClass("sexselect");
        $(".Js_CardNo p span").eq(1).children().eq(0).removeClass("sexselect");
        $(".Js_CardNo p span").eq(1).children().eq(1).addClass("sexselect");
        //服装自备
        $(".Js_HasClothes p span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_HasClothes p span").eq(0).children().eq(0).addClass("sexselect");
        $(".Js_HasClothes p span").eq(1).children().eq(0).removeClass("sexselect");
        $(".Js_HasClothes p span").eq(1).children().eq(1).addClass("sexselect");
        _Sert.Kais();
    });
    //选择是通过
    $(".Js_state p").eq(0).off().on("touchstart", function () {
        $(".Js_state p").children().eq(0).children().eq(1).removeClass("mui-public-btn");
        $(".Js_state p").children().eq(0).children().eq(0).addClass("mui-public-btn");
        $(".Js_state p").children().eq(1).children().eq(0).removeClass("mui-public-btn");
        $(".Js_state p").children().eq(1).children().eq(1).addClass("mui-public-btn");
        _Status = 2;
    });
    //选择否通过
    $(".Js_state p").eq(1).off().on("touchstart", function () {
        $(".Js_state p").children().eq(0).children().eq(0).removeClass("mui-public-btn");
        $(".Js_state p").children().eq(0).children().eq(1).addClass("mui-public-btn");
        $(".Js_state p").children().eq(1).children().eq(1).removeClass("mui-public-btn");
        $(".Js_state p").children().eq(1).children().eq(0).addClass("mui-public-btn");
        _Status = 3;
    });
    //选择性别（男）
    $(".Js_Sex p span").eq(0).off().on("touchstart", function () {
        $(".Js_Sex p span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_Sex p span").eq(0).children().eq(0).addClass("sexselect");
        if ($(".Js_Sex p span").eq(1).children().eq(0).attr("class") != "") {
            $(".Js_Sex p span").eq(1).children().eq(0).removeClass("sexselect");
            $(".Js_Sex p span").eq(1).children().eq(1).addClass("sexselect");
        }
        _Sex = 1;
    });
    //选择性别（女）
    $(".Js_Sex p span").eq(1).off().on("touchstart", function () {
        if ($(".Js_Sex p span").eq(0).children().eq(0).attr("class") != "") {
            $(".Js_Sex p span").eq(0).children().eq(0).removeClass("sexselect");
            $(".Js_Sex p span").eq(0).children().eq(1).addClass("sexselect");
        }
        $(".Js_Sex p span").eq(1).children().eq(1).removeClass("sexselect");
        $(".Js_Sex p span").eq(1).children().eq(0).addClass("sexselect");
        _Sex = 2;
    });
    //选择上岗证（有）
    $(".Js_CardNo p span").eq(0).off().on("touchstart", function () {
        $(".Js_CardNo p span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_CardNo p span").eq(0).children().eq(0).addClass("sexselect");
        if ($(".Js_Sex p span").eq(0).children().eq(0).attr("class") != "") {
            $(".Js_CardNo p span").eq(1).children().eq(0).removeClass("sexselect");
            $(".Js_CardNo p span").eq(1).children().eq(1).addClass("sexselect");
        }
        _SecurityStaffCardNo = 1;
    });
    //选择上岗证（无）
    $(".Js_CardNo p span").eq(1).off().on("touchstart", function () {
        $(".Js_CardNo p span").eq(0).children().eq(0).removeClass("sexselect");
        $(".Js_CardNo p span").eq(0).children().eq(1).addClass("sexselect");
        $(".Js_CardNo p span").eq(1).children().eq(1).removeClass("sexselect");
        $(".Js_CardNo p span").eq(1).children().eq(0).addClass("sexselect");
        _SecurityStaffCardNo = 2;
    });
    //选择服装自备（有）
    $(".Js_HasClothes p span").eq(0).off().on("touchstart", function () {
        $(".Js_HasClothes p span").eq(0).children().eq(1).removeClass("sexselect");
        $(".Js_HasClothes p span").eq(0).children().eq(0).addClass("sexselect");
        $(".Js_HasClothes p span").eq(1).children().eq(0).removeClass("sexselect");
        $(".Js_HasClothes p span").eq(1).children().eq(1).addClass("sexselect");
        _HasClothes = 1;
    });
    //选择服装自备（无）
    $(".Js_HasClothes p span").eq(1).off().on("touchstart", function () {
        $(".Js_HasClothes p span").eq(0).children().eq(0).removeClass("sexselect");
        $(".Js_HasClothes p span").eq(0).children().eq(1).addClass("sexselect");
        $(".Js_HasClothes p span").eq(1).children().eq(1).removeClass("sexselect");
        $(".Js_HasClothes p span").eq(1).children().eq(0).addClass("sexselect");
        _HasClothes = 0;
    });
    //上一步
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("MyRelease.html");
    });

    //点击批量招募
    $(".Js_mui-pull-right").off().on("touchstart", function () {
        SiteFunc.GoTo("MassRecruitment.html?ActivityID=" + _activityID);
    });
    this.Kais = function () {
        var _parm = {
            "ActivityID": _activityID,
            "Status": _Status,
            "HeadImage": _HeadImage,
            "UserName": _UserName,
            "ICNo": _ICNo,
            "Sex": _Sex,
            "Age": _Age,
            "Phone": _Phone,
            "Height": _Height,
            "Weight": _Weight,
            "SecurityStaffCardNo": _SecurityStaffCardNo,
            "IntentionOpenCityID": _IntentionOpenCityID,
            "HasClothes": _HasClothes,
            "Clothes1PicPath": _Clothes1PicPath,
            "Clothes2PicPath": _Clothes2PicPath,
            "Introduction": _Introduction,
            "Count": _count
        }
        _fillToBaoMingUpList(_parm);
        //  CmnAjax.FillData(".Js_CheckUpList", SiteFunc.AjaxUrl + "GetToBaoMingUpList", _parm, function (data) {
        //  	
        //  	
        //  });
    }
});