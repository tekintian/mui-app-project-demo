﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
Cmn.App.CurPage.OnInit.Add(function (data) {
    var _AjaxUrl = SiteFunc.AjaxUrl;//接口
    $(".jscNextBut").click(function () {
        var _phone = $(".Js_Phone").val();
        if (_phone == "") {
            SiteFunc.Alert("请输入手机号");
            return;
        }
        if (!SiteFunc.CheckPhone(_phone)) {
            SiteFunc.Alert("请输入正确的手机号");
            return;
        }
        CmnAjax.PostData(_AjaxUrl + "CheckPhoneExist", { Phone: _phone }, function (data) {
            if (data.IsSuccess == 1) {
                Cmn.App.ShowWaiting("请稍后，程序加载中...");
                SiteFunc.GoTo("VerificationCode.html?Phone=" + _phone);
            
            }
            else {
                SiteFunc.Alert(data.ErrMsg);
            }
        });
    });
    //返回按钮 
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Login.html");
    });
});
(ForgetPassword = new function () {
    
});
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Cmn.App.CloseWaiting();
});