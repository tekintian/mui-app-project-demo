﻿/// <reference path="../../Js/Cmn.js" />
/// <reference path="jquery-1.9.1.min.js" />
/// <reference path="../MUI/Js/mui.min.js" />
Cmn.Version.H5App_Js_CmnApp = "1.2.4";
(function () {

    //标准模块
    Cmn.Extend(Cmn.App = {}, new function () {

        //监测MUI JS 库 是否存在
        if (!window["mui"]) { Cmn.DebugLog("mui.js 未引用！"); return {}; }
        else {
            //监测js引用的位置
            if (!mui("body").length) { Cmn.DebugLog("js 未在页面底部引用！"); return {}; }
        } 
        if (!window["jQuery"]) { Cmn.DebugLog("jQuery.js 未引用！"); return {}; };


        //--------------------------------------------------------
        this.Alert = function (text) {
            /// <summary>提示框</summary>
            /// <param name="text" type="String">提示内容</param>
            if (!window["plus"]) { Cmn.alert(text); }
            else { plus.nativeUI.alert(text); }
            return this;
        };

        //--------------------------------------------------------
        this.SystemToast = function (text) {
            /// <summary>系统提示框</summary>
            /// <param name="text" type="String">提示内容</param>
            if (!window["plus"]) { }
            else { plus.nativeUI.toast(text); }
        };

        //--------------------------------------------------------
        this.ShowWaiting = function (text) {
            /// <summary>显示等待框</summary>
            /// <param name="text" type="String">提示内容</param>
            if (!window["plus"]) { }
            else { 
                //iphone 菊花问题没有解决，iphone就免了吧
                if(plus.os.name == 'Android') {
                    plus.nativeUI.showWaiting(text);
                }
            }
        };

        //--------------------------------------------------------
        this.CloseWaiting = function () {
            /// <summary>关闭等待框框</summary>
            if (!window["plus"]) { }
            else { 
                //iphone 菊花问题没有解决，iphone就免了吧
                if(plus.os.name == 'Android') {
                    plus.nativeUI.closeWaiting();
                }
            }
        };

        //--------------------------------------------------------
        this.SetLocalStorageData = function (key, val) {
            /// <summary>设置本地缓存数据</summary>
            /// <param name="key" type="String">key</param>
            /// <param name="val" type="JSON">存储数据</param>

            //将json类型的值 转成字符串存储
            var _data = Cmn.IsType(val, "object") ?
                (JSON ? JSON.stringify(val) : Cmn.Func.JsonToStr(val)) : val;

            if (window["plus"]) { plus.storage.setItem(key, _data); }
            else {

                if (window.localStorage) {
                    window.localStorage.setItem(key, _data);
                }
                else {
                    //错误日志
                    Cmn.DebugLog('This browser does NOT support localStorage');
                }
            }
        }

        //--------------------------------------------------------
        this.AddLocalStorageData = function (key, val) {
            /// <summary>添加本地缓存数据</summary>
            /// <param name="key" type="String">key</param>
            /// <param name="val" type="JSON">存储是数据</param>

            //将json类型的值 转成字符串存储
            var _val = Cmn.IsType(val, "string") ? JSON.parse(val) : val,
                _data = Cmn.Extend(this.GetLocalStorageDataByKey(key), _val);

            if (window["plus"]) { plus.storage.setItem(key, _data); }
            else {

                if (window.localStorage) {
                    window.localStorage.setItem(key, _data);
                }
                else {
                    //错误日志
                    Cmn.DebugLog('This browser does NOT support localStorage');
                }
            }
        }

        //--------------------------------------------------------
        this.GetLocalStorageDataByKey = function (key) {
            /// <summary>根据key获取本地缓存数据</summary>
            /// <param name="key" type="String">key</param>

            var _data = "{}";

            if (window["plus"]) { _data = plus.storage.getItem(key); }
            else {

                if (window.localStorage) {
                    _data = window.localStorage.getItem(key);
                }
                else {
                    //错误日志
                    Cmn.DebugLog('This browser does NOT support localStorage');
                }
            }

            return JSON ? JSON.parse(_data) : $.parseJSON(_data);
        }

        //--------------------------------------------------------
        this.CurPage =Cmn.Extend({},new function () {

            var _self = this;

            //获取返回的切换样式
            var _getBackSwtichStyle = function (switchStyle) {
                /// <summary>获取返回的切换样式</summary>
                /// <param name="switchStyle" type="Cmn.App.CurPage.SwitchStyles">切换样式</param>

                if (switchStyle.indexOf("right")) {
                    switchStyle = switchStyle.replace("right", "left");
                }
                else { switchStyle = switchStyle.replace("left", "right"); }

                if (switchStyle.indexOf("in")) {
                    switchStyle = switchStyle.replace("in", "out");
                }
                else { switchStyle = switchStyle.replace("out", "in"); }

                return switchStyle;
            };

            //切换样式
            this.SwitchStyles = {
                //左侧滑动进场
                SlideInLeft: "slide-in-left",
                //右侧滑动进场
                SlideInRight: "slide-in-right",
                //无动画
                None: "none",
                //渐变进场
                FadeIn: "fade-in",
                //由小变大进场
                ZoomOut: "zoom-out",
                //由小变大渐变进场
                ZoomFadeOut: "zoom-fade-out",
                //页面从屏幕右侧滑入显示，同时上一个页面带阴影效果从屏幕左侧滑出隐藏
                PopIn: "pop-in",
                //页面从屏幕左侧滑入显示，同时上一个页面带阴影效果从屏幕右侧滑出隐藏
                PopOut: "pop-out"
            };

            //上一个页面的名称
            this.BeforePageName = "";
            //上一个webview对象
            this.CurWebView = null;

            //页面初始默认size
            this.DefaultSize = null;

            //--------------------------------------------------------
            //页面准备好事件
            this.OnReady = new Cmn.Event();
            //--------------------------------------------------------
            //页面初始化事件
            this.OnInit = new Cmn.Event();
            //--------------------------------------------------------
            //页面显示之前的事件
            this.BeforeShow = new Cmn.Event();
            //--------------------------------------------------------
            //页面显示之后的事件
            this.AfterShow = new Cmn.Event();
            //--------------------------------------------------------
            //页面隐藏之前的事件
            this.BeforeHide = new Cmn.Event();
            //--------------------------------------------------------
            //监听接受页面消息的事件
            this.OnReceivePageMsg = new Cmn.Event();

            this.Config = {
                //页面尺寸
                PageSize: 640,
                //子页面配置
                SubPageCfg: []
            };

            //--------------------------------------------------------
            //页面初始化
            this.Init = function (pageConfig) {
                /// <summary>页面初始化</summary>
                /// <param name="pageConfig" type="JSON">页面配置</param>

                //当前页面的配置
                this.Config = Cmn.Extend(this.Config, pageConfig);
                //检测子页面配置

                if (Cmn.IsType(this.Config.SubPageCfg, "array") && this.Config.SubPageCfg.length > 0) {

                    for (var _i = 0; _i < _self.Config.SubPageCfg.length; _i++) {

                        if (_self.Config.SubPageCfg[_i].active) {

                            //选中
                            $("a[href*='" + _self.Config.SubPageCfg[_i].url + "']").addClass("mui-active").
                                siblings("a").removeClass("mui-active");

                            //创建当前活动子页面

                            mui.plusReady(function () {

                                var _webview = mui.appendWebview({
                                    url: _self.Config.SubPageCfg[_i].url,
                                    id: _self.Config.SubPageCfg[_i].id
                                        || _self.Func.GetPageNameByUrl(_self.Config.SubPageCfg[_i].url),
                                    styles: Cmn.Extend({
                                        scalable: Cmn.Func.IsAndroid(),
                                        top: "0px",//新页面顶部位置
                                        bottom: "0px"
                                    }, _self.Config.SubPageCfg[_i].styles),
                                    extras: _self.Config.SubPageCfg[_i].param
                                });

                                _webview.addEventListener("loaded", function () {

                                      _webview.evalJS('mui.plusReady(function(){ ' +
                                                          'Cmn.App.CurPage.__Param = {};' +
                                                          'Cmn.App.CurPage.OnInit.Trigger([Cmn.App.CurPage||{}]);' +
                                                          'window.addEventListener("__BeforeShow__", function (event) {' +
                                                          'Cmn.App.CurPage.__Param = event.detail;' +
                                                          'Cmn.App.CurPage.BeforeShow.Trigger([event.detail]);' +
                                                          '});' +
                                                          'mui.fire(plus.webview.currentWebview(), "__BeforeShow__", ' + JSON.stringify({}) + ');' +
                                                       '});');
                                });

                                  _webview.addEventListener("show", function () {
                                      _webview.evalJS('Cmn.App.CurPage.AfterShow.Trigger(Cmn.App.CurPage.__Param||{});');
                                  });

                            });



                            break;
                        }
                    }
                }

                //mui 初始化
                if (Cmn.IsType(pageConfig, "object")) mui.init();

                //页面自适应
                if (this.DefaultSize == null) { this.DefaultSize = window.innerWidth; }
                var _scale = this.DefaultSize / this.Config.PageSize;
                if (Cmn.Func.IsAndroid()) {
                    $("meta[name=viewport]").attr("content", "width=" + this.Config.PageSize +
                        ",initial-scale=" + _scale + ",maximum-scale=" + _scale);
                }
                else {
                    $("meta[name=viewport]").attr("content", "width=" + this.Config.PageSize
                        + ",initial-scale=" + _scale + ",maximum-scale=" + _scale + ",minimum-scale=" + _scale +
                        ",user-scalable=no;");
                }
            }

            //--------------------------------------------------------
            //发送消息到指定页面
            this.SendMsgToPageByPageName = function (pageName, data) {
                /// <summary>发送消息到指定页面</summary>
                /// <param name="pageName" type="String">页面名称</param>
                /// <param name="data" type="JSON">广播的数据</param>

                //存在 App API 
                if (window["plus"]) {

                    if (this.Func.GetPageNameByUrl(pageName) == "") {
                        if (pageName == "") { Cmn.DebugLog(pageName + " not found"); }
                    }
                    else { pageName = this.Func.GetPageNameByUrl(pageName); }

                    //获取webview对象
                    var _webview = plus.webview.getWebviewById(pageName);
                    //进行消息广播
                    if (_webview) { mui.fire(_webview, '__PageMsg__', data || {}); }
                    else { Cmn.DebugLog(pageName + " not found"); }
                }
                else { Cmn.DebugLog("App plus not defined"); }

            }

            //页面相关的函数
            this.Func = {
                //根据页面地址获取页面名称
                GetPageNameByUrl: function (pageUrl) {
                    /// <summary>根据页面地址获取页面名称</summary>
                    /// <param name="pageUrl" type="String">页面地址</param>
                    pageUrl = pageUrl || window.location.href;
                    var _locIndex = pageUrl.lastIndexOf("/") > 0 ? pageUrl.lastIndexOf("/") + 1 : 0;
                    return pageUrl.substr(_locIndex, pageUrl.lastIndexOf(".") - _locIndex);
                },
                //获取来自url的参数数据集
                GetParamDataFromUrl: function (pageUrl) {
                    /// <summary>获取来自url的参数数据集</summary>
                    /// <param name="pageUrl" type="String">页面地址</param>
                    pageUrl = pageUrl || window.location.href;
                    var _paramStr = pageUrl.substr(pageUrl.indexOf("?") + 1),
                        _paramData = {};

                    //检测有没有#号  存在的话 去掉#好的内容
                    if (_paramStr.indexOf("#") > 0) {
                        if (_paramStr.indexOf("#") > _paramStr.indexOf("&")) {
                            _paramStr = _paramStr.split("#")[1];
                        }
                        else { _paramStr = _paramStr.split("#")[0]; }
                    }
                    _paramStr = _paramStr.split("&");

                    for (var _i = 0; _i < _paramStr.length; _i++) {
                        var _paramInfo = _paramStr[_i].split("=");
                        if (_paramInfo.length == 2) { _paramData[_paramInfo[0]] = _paramInfo[1]; }
                    }
                    return _paramData;
                },
                //是否是子页面
                IsSubPage: function (pageUrl) {
                    for (var _i = 0; _i < _self.Config.SubPageCfg.length; _i++) {

                        if (Cmn.Func.DelParamFromUrl(pageUrl) ==
                            Cmn.Func.DelParamFromUrl(_self.Config.SubPageCfg[_i].url)) {
                            return true;
                        }
                    }
                    return false;
                },
                //获取当前子页面所在list里面的索引
                GetCurSubPageIndexByList: function (pageUrl) {
                    for (var _i = 0; _i < _self.Config.SubPageCfg.length; _i++) {
                        if (this.GetPageNameByUrl(pageUrl) ==
                            this.GetPageNameByUrl(_self.Config.SubPageCfg[_i].url)) {
                            return _i;
                        }
                    }
                    return 0;
                },
                //获取当前子页面的webview对象
                GetCurSubWebview: function () {
                    var _webview = plus.webview.currentWebview(),
                        _subList = _webview.children();
                    for (var _i = 0; _i < _subList.length; _i++) {
                        if (_subList[_i].isVisible()) { return _subList[_i]; }
                    }
                    return null;
                }
            };

            //--------------------------------------------------------
            //跳转页面
            this.GoTo = function (pageUrl, param, swtichStyle, initStyles) {
                /// <summary>根据url跳转页面</summary>
                /// <param name="pageUrl" type="String">要去的页面URL</param>
                /// <param name="param" type="JSON">打开页面传递的参数</param>
                /// <param name="swtichStyle" type="Cmn.App.CurPage.SwitchStyles">切换页面动画的方向默认pop-in</param>
                /// <param name="initStyles" type="JSON">目标页面打开时候的样式</param>

                if (window["plus"]) {
                    //页面名称
                    var _pageName = this.Func.GetPageNameByUrl(pageUrl);

                    var _webview = plus.webview.getWebviewById(_pageName);

                    //切换样式
                    swtichStyle = swtichStyle || Cmn.App.CurPage.SwitchStyles.PopIn;

                    //当前的webview对象
                    _self.CurWebView = plus.webview.currentWebview();

                    //跳转的页面和当前页面一致的时候无反应
                    if (_self.Func.GetPageNameByUrl(_self.CurWebView.getURL()) ==
                        _self.Func.GetPageNameByUrl(pageUrl)) { return false; }

                    //看看当前也是否是子页面 如果是子页面吧目标页面交给他爸爸看看是不是它不知道的兄弟页面
                    //如果是兄弟页面按照兄弟页面的方式处理
                    if (_self.CurWebView.parent()) {

                        mui.fire(_self.CurWebView.parent(), '__Sys_PageMsg__', {
                            //动作 子页面代理行为
                            Action: "SubpageAgent",
                            //要去的Url
                            GoToPageUrl: pageUrl,
                            //参数
                            Param: param,
                            //切换样式
                            SwtichStyle: swtichStyle,
                            //webview初始养死
                            InitStyles: initStyles
                        });

                        return false;
                    }


                    //如果当前跳转的目标页面是子页面的话 进行特殊处理
                    if (_self.Func.IsSubPage(pageUrl)) {

                        //获取当前显示的webview
                        _self.CurWebView = _self.Func.GetCurSubWebview() || _self.CurWebView;

                        //切换的样式
                        swtichStyle = _self.SwitchStyles.None;

                        //webview的样式
                        initStyles = Cmn.Extend(initStyles,
                            _self.Config.SubPageCfg[_self.Func.GetCurSubPageIndexByList(pageUrl)].styles);

                    }

                    //当前页面隐藏
                    if (_self.CurWebView) { _self.CurWebView.hide("auto"); }

                    //打开页面
                    if (_webview) {

                        if (_webview.isVisible()) { _webview.hide(); }

                        mui.fire(_webview, '__BeforeShow__', param);

                        _webview.evalJS('Cmn.App.CurPage.Back = function(param){ ' +
                                            'Cmn.App.CurPage.GoTo("' + _self.CurWebView.getURL() +
                                            '",param,"' + _getBackSwtichStyle(swtichStyle) + '");' +
                                        '};');

                        _webview.show(swtichStyle, 200);
                    }
                    else {

                        _webview = mui.openWindow({
                            url: pageUrl,
                            id: _pageName,
                            styles: Cmn.Extend({
                                scalable: Cmn.Func.IsAndroid(),
                                top: "0px",//新页面顶部位置
                                bottom: "0px"
                            }, initStyles),
                            extras: param,
                            //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
                            createNew: false,
                            show: {
                                //页面loaded事件发生后自动显示，默认为true
                                autoShow: true,
                                //页面显示动画，默认为”slide-in-right“；
                                aniShow: swtichStyle,
                                //页面动画持续时间，Android平台默认100毫秒，iOS平台默认200毫秒；
                                duration: 200
                            },
                            waiting: {
                                //自动显示等待框，默认为true
                                autoShow: false,
                                //等待对话框上显示的提示内容
                                title: '正在加载...'
                            }
                        });

                        //如果是子页面的话添加到子页面
                        if (_self.Func.IsSubPage(pageUrl)) { plus.webview.currentWebview().append(_webview); }

                        _webview.addEventListener("loaded", function () {

                            _webview.evalJS('mui.plusReady(function(){ ' +
                                                'Cmn.App.CurPage.__Param = ' + JSON.stringify(param || {}) + ';' +
                                                'Cmn.App.CurPage.OnInit.Trigger([Cmn.App.CurPage.__Param||{}]);' +
                                                'window.addEventListener("__BeforeShow__", function (event) {' +
                                                    'Cmn.App.CurPage.__Param = event.detail;' +
                                                    'Cmn.App.CurPage.BeforeShow.Trigger([event.detail]);' +
                                                '});' +
                                                'mui.fire(plus.webview.currentWebview(), "__BeforeShow__", ' + JSON.stringify(param || {}) + ');' +
                                             '});');
                            _webview.evalJS('Cmn.App.CurPage.Back = function(param){ ' +
                                         'Cmn.App.CurPage.GoTo("' + _self.CurWebView.getURL() +
                                         '",param,"' + _getBackSwtichStyle(swtichStyle) + '");' +
                                     '};');
                        });

                        _webview.addEventListener("show", function () {
                            _webview.evalJS('Cmn.App.CurPage.AfterShow.Trigger(Cmn.App.CurPage.__Param||{});');
                        });
                    }
                }
                else {
                    window.location.href = pageUrl;
                }

            }
            //--------------------------------------------------------
            //返回
            this.Back = function (param) {
                /// <summary>返回上一页</summary>
                /// <param name="param" type="JSON">参数</param>
                mui.back();
            }

        });
        //app准备好
        mui.plusReady(function () {
            //页面消息
            window.addEventListener("__PageMsg__", function (event) {
                Cmn.App.CurPage.OnReceivePageMsg.Trigger([event.detail]);
            });
            //系统内部使用的消息通道
            window.addEventListener("__Sys_PageMsg__", function (event) {
                //子页面行为代理
                if (event.detail.Action == "SubpageAgent") {
                    Cmn.App.CurPage.GoTo(event.detail.GoToPageUrl, event.detail.Param,
                        event.detail.SwtichStyle, event.detail.InitStyles);
                }
            });
            //页面准备好了
            Cmn.App.CurPage.OnReady.Trigger([]);
        });

        //jQuery存在的话  能够自动检测a标签的link自动跳转

        $("body").delegate("a", "tap", function (e) {
            var _url = $(this).attr("href");
            if (_url != "" && _url != undefined) {
                if (_url == "back") { Cmn.App.CurPage.Back(); }
                else { Cmn.App.CurPage.GoTo(_url, Cmn.App.CurPage.Func.GetParamDataFromUrl(_url)); }
            }
        }).delegate("a", "click", function (e) {
            var _url = $(this).attr("href");
            if (_url != "") { return false; }
        });

        //返回
        $("body").delegate(".cmn-action-back", "tap", function (e) {
            Cmn.App.CurPage.Back();
        });

        //页面初始化
        this.CurPage.Init();

    });

    //关于app的其他扩展模块
    Cmn.Extend(Cmn.App, {
        //支付模块
        Play: new function () {


        }
    });


})();

