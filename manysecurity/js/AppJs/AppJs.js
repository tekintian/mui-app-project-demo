﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnAjax.js" />



//appjs
var App = Cmn.Extend({}, new function () {

    //指向自己
    var _Self = this;
    //当前用户信息
    this.CurUserInfo = {
        Phone: "",
        Password: "",
        Token: "",
        UuID: "",
        Sex: "2",
        BloodLabelDesc: [],
        BloodType: "",
        HeadImgUrl: "",
        City:"",
        Time: new Date().getTime()
    };

    //接口地址
    this.ItfHost = "http://didisecurity.nn.cagoe.com/";

    this.ItfUrl = this.ItfHost+"Ajax/Ajax.aspx";
 
    //根据 url获取页面名称
    var _GetPageNameByUrl = function(pageUrl){
        /// <summary>根据页面地址获取页面名称</summary>
        /// <param name="pageUrl" type="String">页面地址</param>
        var _locIndex = pageUrl.lastIndexOf("/") > 0 ? pageUrl.lastIndexOf("/") + 1 : 0;
        return pageUrl.substr(_locIndex, pageUrl.lastIndexOf(".") - _locIndex);
    }

    //--------------------------------------------------------
    this.WebAlert = function (text) {
        if (!window["plus"]) { alert(text); }
        else { plus.nativeUI.alert(text); }
    }

    //--------------------------------------------------------
    this.Alert = function(text){
        /// <summary>提示框</summary>
        /// <param name="text" type="String">提示内容</param>
        plus.nativeUI.toast(text);
        return this;
    };

    //--------------------------------------------------------
    this.SystemToast = function (text) { 
        /// <summary>系统提示框</summary>
        /// <param name="text" type="String">提示内容</param>
        return this.Alert(text);
    };

    //--------------------------------------------------------
    this.ShowWaiting = function (text) {
        /// <summary>显示等待框</summary>
        /// <param name="text" type="String">提示内容</param>
        plus.nativeUI.showWaiting(text);
    };

    //--------------------------------------------------------
    this.CloseWaiting = function () {
        /// <summary>Waiting框</summary>
        plus.nativeUI.closeWaiting();
    };


    //--------------------------------------------------------
    //获取用户信息
    this.GetUserInfo = function () {
        /// <summary>获取当前用户信息</summary>

        if (window["plus"]) {

            var _userInfo = plus.storage.getItem("UserInfo");

            if (!_userInfo) { return {}; }

            if (Cmn.IsType(_userInfo, "string")) { _userInfo = $.parseJSON(_userInfo); }

            //未过期
            if (_userInfo["Time"] && (Math.abs(_userInfo["Time"] * 1 - new Date().getTime()) <= 18 * 60 * 1000)) {
                return _userInfo;
            }

            //过期了更新token
            var _data = CmnAjax.GetData(_Self.ItfUrl, { method: "UpdateToken", Token: _userInfo["Token"] });

            if (_data && _data.Token) {
                _userInfo.Token = _data.Token;
                _userInfo["Time"] = new Date().getTime();
                plus.storage.setItem("UserInfo", JSON.stringify(_userInfo));
            }
            else {
                plus.storage.setItem("UserInfo", JSON.stringify({}));
                return {};
            }

            return _userInfo;
        }
        else { return {}; }

    };

    //--------------------------------------------------------
    //设置用户信息缓存
    this.SetUserInfo = function (userinfo) {
        /// <summary>设置用户信息</summary>
        /// <param name="userinfo" type="JSON">用户信息</param>
        window["plus"] && plus.storage.setItem("UserInfo", JSON.stringify(userinfo));
    };
    //--------------------------------------------------------
    //登陆
    this.UserLogin = function (phone, pwd,callback) {
        /// <summary>登陆</summary>
        /// <param name="phone" type="String">手机号码</param>
        /// <param name="pwd" type="String">密码</param>
        /// <returns type="bool" />

        if (callback) {
            CmnAjax.PostData(App.ItfUrl, { Phone: phone, Password: pwd, method: "UserLogin" }, function (data) {
                if (data.IsSuccess == 1) {

                    //用户登录信息缓存
                    App.SetUserInfo({
                        Phone: phone,
                        Password: pwd,
                        Token: data.Token,
                        UuID: data.UuID,
                        Sex: data.Sex,
                        BloodLabelDesc: data.BloodLabelDesc,
                        BloodType: data.BloodType,
                        HeadImgUrl: data.HeadImgUrl,
                        PointBalance: data.PointBalance,
                        Time: new Date().getTime()
                    });
                }
                else { App.SetUserInfo({}); }

                callback((data.IsSuccess == 1));
            });
        }
        else {
            var _data = CmnAjax.GetData(App.ItfUrl, { Phone: phone, Password: pwd, method: "UserLogin" });

            if (_data.IsSuccess == 1) {
                //用户登录信息缓存
                App.SetUserInfo({
                    Phone: phone,
                    Password: pwd,
                    Token: _data.Token,
                    UuID: _data.UuID,
                    Sex: _data.Sex,
                    BloodLabelDesc: _data.BloodLabelDesc,
                    BloodType: _data.BloodType,
                    HeadImgUrl: _data.HeadImgUrl,
                    PointBalance: _data.PointBalance,
                    Time: new Date().getTime()
                });
            };

            return (_data.IsSuccess == 1);
        }

    }

    //获取昵称
    this.GetNickName = function (userinfo) {
        /// <summary>获取昵称</summary>

        this.CurUserInfo = this.GetUserInfo();

        var _str = "",
            _userInfo = userinfo || this.CurUserInfo;

        if (_userInfo.City) { _str += _userInfo.City + " "; }
        if (_userInfo.BloodType) { _str += _userInfo.BloodType + "型 "; }
        if (_userInfo.BloodLabelDesc == "") {
            _userInfo.BloodLabelDesc = [];
        }
        else {
            if (Cmn.IsType(_userInfo.BloodLabelDesc, "string")) {
                _userInfo.BloodLabelDesc = $.parseJSON(_userInfo.BloodLabelDesc);
            }
        }

        for (var _i = 0; _i < _userInfo.BloodLabelDesc.length; _i++) {
            _str += _userInfo.BloodLabelDesc[_i].Desc + (_i + 1 < _userInfo.BloodLabelDesc.length ? " " : "");
        }

        return _str;

    }


    //--------------------------------------------------------
    var _BeforeWebView = null;
    //跳转页面
    this.ToPageByUrl = function (pageUrl,showDir,pageParam,pageStyles) {
        /// <summary>根据url跳转页面</summary>
        /// <param name="pageUrl" type="String">要去的页面URL</param>
        /// <param name="showDir" type="String">切换页面动画的方向默认right</param>
        /// <param name="pageParam" type="JSON">打开页面传递的参数</param>
        /// <param name="pageStyles" type="String">目标页面打开时候的样式</param>

        //页面名称
        var _pageName = _GetPageNameByUrl(pageUrl);

        var _webview = plus.webview.getWebviewById(_pageName);
       
        //显示方向
        showDir = (showDir || "right");

        //之前的页面
        _BeforeWebView = plus.webview.currentWebview();

        if (_BeforeWebView) { _BeforeWebView.hide("slide-out-" + (showDir != "right" ? "right" : "left"), 200); }

        //打开页面
        if (_webview) {
            if (_webview.isVisible()) { _webview.hide(); }
            mui.fire(_webview, 'BeforeShow', pageParam);
            _webview.evalJS('App.Back = function(){ App.ToPageByUrl("' + _BeforeWebView.getURL() + '","left");};');
            _webview.show("slide-in-" + showDir, 200);
        }
        else {
            _webview = mui.openWindow({
                url: pageUrl,
                id: _pageName,
                styles: Cmn.Extend({
                    top: "0px",//新页面顶部位置
                    bottom: "0px",//新页面底部位置
                    width: "100%",//新页面宽度，默认为100%
                    height: "100%",//新页面高度，默认为100%
                }, pageStyles),
                extras: pageParam,
                //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
                createNew: false,
                show: {
                    //页面loaded事件发生后自动显示，默认为true
                    autoShow: true,
                    //页面显示动画，默认为”slide-in-right“；
                    aniShow: "slide-in-" + (showDir || "right"),
                    //页面动画持续时间，Android平台默认100毫秒，iOS平台默认200毫秒；
                    duration: 200
                },
                waiting: {
                    autoShow: true,//自动显示等待框，默认为true
                    //等待对话框上显示的提示内容
                    title: '正在加载...'
                }
            });
 
            _webview.addEventListener("loaded", function () {
               
                _webview.evalJS('mui.plusReady(function(){ ' +
                                    'if (App.GetUserInfo().Token) { ' +
                                        'App.CurUserInfo = App.GetUserInfo(); ' +
                                    '}; ' +
                                    'App.' + _pageName + '.Init && App.' + _pageName + '.Init(); ' +
                                    'window.addEventListener("BeforeShow", function (event) {' +
                                        'if (App.GetUserInfo().Token) { ' +
                                            'App.CurUserInfo = App.GetUserInfo(); ' +
                                        '}; ' +
                                        'App.' + _pageName + '.BeforeShow && App.' + _pageName + '.BeforeShow(event.detail);' +
                                    '});' +
                                    'plus.navigator.setStatusBarBackground("#f7f7f7");' +
                                    'plus.navigator.setStatusBarStyle("UIStatusBarStyleDefault");' +
                                    'mui.fire(plus.webview.currentWebview(), "BeforeShow", ' + JSON.stringify(pageParam) + ');' +
                                 '});');
                _webview.evalJS('App.Back = function(){ App.ToPageByUrl("' + _BeforeWebView.getURL() + '","left");};');
            });

            _webview.addEventListener("show", function () {
                _webview.evalJS('App.' + _pageName + '.AfterShow && App.' + _pageName + '.AfterShow();');
            });
        }
        

       

    }
    //--------------------------------------------------------
    //创建子页面
    this.CreateSubPage = function (pageUrl,  pageParam, pageStyles) {
        /// <summary>创建子页面</summary>
        /// <param name="pageUrl" type="String">要去的页面URL</param>
        /// <param name="pageParam" type="JSON">打开页面传递的参数</param>
        /// <param name="pageStyles" type="String">目标页面打开时候的样式</param>

        //页面名称
        var _pageName = _GetPageNameByUrl(pageUrl);

        var _webview = mui.preload({
            url: pageUrl,//子页面HTML地址，支持本地地址和网络地址
            id: _pageName,//子页面标志
            styles: pageStyles,
            extras: pageParam
        });

        if (window["plus"]) { plus.webview.currentWebview().append(_webview); };
       
        _webview.addEventListener("loaded", function () {

            _webview.evalJS('mui.plusReady(function(){ ' +
                                'if (App.GetUserInfo().Token) { ' +
                                    'App.CurUserInfo = App.GetUserInfo(); ' +
                                '}; ' +
                                'App.' + _pageName + '.Init && App.' + _pageName + '.Init(); ' +
                                'window.addEventListener("BeforeShow", function (event) {' +
                                    'App.' + _pageName + '.BeforeShow && App.' + _pageName + '.BeforeShow(event.detail);' +
                                '});' +
                                  'mui.fire(plus.webview.currentWebview(), "BeforeShow", ' + JSON.stringify(pageParam) + ');' +
                             '});');
          //  _webview.evalJS('mui.plusReady(function(){ if (App.GetUserInfo().Token) { App.CurUserInfo = App.GetUserInfo(); }; App.' + _pageName + '.Init && App.' + _pageName + '.Init();});');
        });
       
        return _webview;

    }

    //自适应页面size
    this.Scale = 1;
    this.ResizeWindow = function () {
        this.Scale = window.innerWidth / 640;
        
        $("meta[name=viewport]").attr("content", "width=device-width,initial-scale=" + this.Scale +
            ",maximum-scale=" + this.Scale + ",minimum-scale=" + this.Scale + ",user-scalable=no;");
    }
});


//扩展功能模块
Cmn.Extend(App, {
    //支付模块
    DefrayModel: {
        //支付方式
        PayBy: {},
        //初始化支付通道
        Init: function () {
            // 初始化支付通道
            plus.payment.getChannels(function (channels) {
                var txt = "支付通道信息：";
                for (var i in channels) {
                    var channel = channels[i];
                    App.DefrayModel.PayBy[channel.id] = channel;
                    txt += "id:" + channel.id + ", ";
                    txt += "description:" + channel.description + ", ";
                    txt += "serviceReady:" + channel.serviceReady + "； ";
                    App.DefrayModel.CheckServices(channel);
                };
                App.WebAlert(txt);
            }, function (e) { outLine("获取支付通道失败：" + e.message); });

        },
        //检测支付环境 
        CheckServices: function (pc) {
            if (!pc.serviceReady) {
                var txt = null;
                switch (pc.id) {
                    case "alipay":
                        txt = "是否安装支付宝？";
                        break;
                    default:
                        txt = "您未安装“" + pc.description + "”服务，无法完成支付，是否立即安装？";
                        break;
                }
                plus.nativeUI.confirm(txt, function (e) {  if (e.index == 0) {    pc.installService();  } }, pc.description);
            }
        },
        //支付
        Pay: function (paybyId, price, callback) {
            var w = null;
            // 支付
            function pay(id) {
                if (w) { return; }//检查是否请求订单中
                var url = App.ItfUrl;
                if (id == 'alipay' || id == 'wxpay') {
                    url += '?method=GetPayInfo&payid=' + id;
                }
                else {
                    plus.nativeUI.alert("不支持此支付通道！", null, "捐赠");
                    return;
                }
                url += '&app_id=' + plus.runtime.appid + '&total_fee=';
                w = plus.nativeUI.showWaiting();
                // 请求支付订单
                var amount = price;
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    switch (xhr.readyState) {
                        case 4:
                            w.close(); w = null;
                            if (xhr.status == 200) {
                              
                                plus.payment.request(App.DefrayModel.PayBy[id], xhr.responseText, function (result) {
                                    callback && callback(true);
                                }, function (e) {
                                    callback && callback(false);
                                });

                            }
                            else {  callback && callback(false); }
                            break;
                        default:

                            break;
                    }
                }
                xhr.open('GET', url + amount);

                xhr.send();
            }
            pay(paybyId);
        }
    }

});


//历史消息管理
App.HisMsgManage = Cmn.Extend({},new function(){
    //历史消息管理
    var _DateTime = new Date(),

        _Self = this;

    this.Today = _DateTime.getFullYear() + "-" + (_DateTime.getMonth() + 1) + "-" + _DateTime.getDate();

    this.NowTime = _DateTime.getFullYear() + "-" + (_DateTime.getMonth() + 1) + "-" + _DateTime.getDate()
               + " " + _DateTime.getHours() + ":" + _DateTime.getMinutes() + ":" + _DateTime.getSeconds();

    this.Set = function (msgData,uuID) {

        _Self.HistMsg = $.parseJSON(plus.storage.getItem("HisMsg") || "{}");
 
        var _msgKey = _Self.Today.replace(/[\-|\s|:]+/g, "") + "_" + uuID;

        if (!_Self.HistMsg[_msgKey]) { _Self.HistMsg[_msgKey] = []; }

        if (Cmn.IsType(msgData, "object")) { _Self.HistMsg[_msgKey].push(msgData); }

        else if (Cmn.IsType(msgData, "array")) {

            $.each(msgData, function () { _Self.HistMsg[_msgKey].push(this); });

        }

        window["plus"] && plus.storage.setItem("HisMsg", JSON.stringify(_Self.HistMsg));

    }

    this.Clear = function () { window["plus"] && plus.storage.setItem("HisMsg", "{}");}

    this.GetByDays = function (day,uuID) {

        this.HistMsg = $.parseJSON(plus.storage.getItem("HisMsg") || "{}");

        var _diffDate = new Date(_DateTime.getTime() - (day * 24 * 60 * 60 * 1000)),
            _msgKey = _Self.Today.replace(/[\-|\s|:]+/g, "") + "_" + uuID;

        return this.HistMsg[_msgKey] || [];
    }

    this.GetTodayMsg = function (uuID) {
        this.HistMsg = $.parseJSON(plus.storage.getItem("HisMsg") || "{}");
        var _msgKey = _Self.Today.replace(/[\-|\s|:]+/g, "") + "_" + uuID;
        return this.HistMsg[_msgKey] || [];
    }

   
});
 
Cmn.Ajax.ShowAjaxHandleHint = function () { };
 