﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    ActivityRelease.FillActivityType();
});
Cmn.App.CurPage.OnInit.Add(function (data) {
});
(ActivityRelease = new function () {
    var _Self = this;
    //填充活动类型
    this.FillActivityType = function () {
    	var replace = function(template, obj) {
    		for(var key in obj) {
    			template = template.replace('${' + key + '}', obj[key]);
    		}
    		return template.trim();
    	};
    	
    	$('.Tproject_ li').off();
    	$('.Tproject_').empty();
    	
    	
        $.post(SiteFunc.AjaxUrl + "GetActivityType", {}, function(r){
        	var result = $.parseJSON(r);
        	
        	if(result.data && result.data.length > 0) {
        		var template = document.getElementById("template").innerHTML;
        		for(var i = 0; i< result.data.length; i++) {
        			var o = result.data[i];
        			var _html = $(replace(template, o));
        			if(o.ActivityCategory == 1) {
        				$('.pjShortTerm').append(_html);
        			}
        			else {
        				$('.pjLongTerm').append(_html);
        			}
        		}
        	}
        	
        	Cmn.App.CloseWaiting();
          	$(".Js_ActivityTypeList").show();
            $(".Js_ActivityTypeList").off().click(function () {
                if (!SiteFunc.CheckLoginStatus()|| SiteFunc.GetCorpID() == "") {
                    SiteFunc.Alert("请先登录");
                    return;
                }
                //alert($(this).attr("typeid") + "|" + $(this).find(".Js_TypeName").text());
                if($(this).attr("data-type")=="1")
                {
                	SiteFunc.GoTo("ActivityRelease2.html?ActivityCategory="+$(this).attr("data-type")+"&ActivityTypeID=" + $(this).attr("typeid") + "&ActivityTypeName=" + $(this).find(".Js_TypeName").text());
                }
                else
                {
                	SiteFunc.GoTo("ActivityRelease_longtime.html?ActivityCategory="+$(this).attr("data-type")+"&ActivityTypeID=" + $(this).attr("typeid") + "&ActivityTypeName=" + $(this).find(".Js_TypeName").text());
                }
                
            });
        });
    } 
//  this.FillActivityType = function () {
//      CmnAjax.FillData(".Js_ActivityTypeList", SiteFunc.AjaxUrl + "GetActivityType", "", function (data) {
//
//          $("body").css("opacity", 1);
//          Cmn.App.CloseWaiting();
//
//          $(".Js_ActivityTypeList").show();
//          $(".Js_ActivityTypeList").off().click(function () {
//              if (!SiteFunc.CheckLoginStatus()|| SiteFunc.GetCorpID() == "") {
//                  SiteFunc.Alert("请先登录");
//                  return;
//              }
//              //alert($(this).attr("typeid") + "|" + $(this).find(".Js_TypeName").text());
//              SiteFunc.GoTo("ActivityRelease2.html?ActivityTypeID=" + $(this).attr("typeid") + "&ActivityTypeName=" + $(this).find(".Js_TypeName").text());
//          });
//      });
//  } 
});

