var ActionParm = {
	ActivityID:"",
	CorpID:"",
    ActiveTime: "",
    WorkTime: "",
    Height: "",
    ArriveTime: "",
    Weight: "",
    Need: "",
    Cloth: "",
    Sex: "",
    Other:""
}
var ActivityCategory="";//判断是长期还是短期1（短期）2（长期）

Cmn.App.CurPage.OnInit.Add(function (data) {
	
	
});

Cmn.App.CurPage.BeforeShow.Add(function (data) {
    console.log(data);
	ActivityCategory=data.ActivityCategory;
	if(data!="")
	{
		var parm = $.parseJSON(data.parm);
	var status = parm.status;
	if(status==1){
		$(".Js_ActiveDesc").text(parm.ActiveDesc);
		$(".Js_ActiveTime").text(parm.ActiveTime);
		$(".Js_WorkTime").text(parm.WorkTime);
		$(".Js_Height").text(parm.Height);
		$(".Js_ArriveTime").text(parm.ArriveTime);
		$(".Js_Weight").text(parm.Weight);
		$(".Js_Need").text(parm.Need);
		$(".Js_Cloth").text(parm.Cloth);
		$(".Js_Sex").text(parm.Sex);
		$(".Js_Other").text(parm.Other);
	}
	else {
		$(".ActionRement2").hide();
		$(".Js_ActiveDesc").text(parm.ActiveDesc);
		$(".Js_ActiveTime").text(parm.ActiveTime);
		$(".Js_WorkTime").text(parm.WorkTime);
		$(".Js_Other").text(parm.Other);
		
	}
	$(".Js_Apply_").off().click(function(){
		
		CmnAjax.PostData(SiteFunc.AjaxUrl + "AddApplyRec", { ActivityID: parm.ActivityID, CorpID: window._corpID,ActivityCategory: parm.ActivityCategory   }, function (data) {
	        if (data.IsSuccess == 1) {
	        	if(ActivityCategory==1)
	        	{
	        		SiteFunc.GoTo("ActionRement.html?From=NewAction.html&ActivityID="+parm.ActivityID);
	                SiteFunc.Alert("报名申请提交成功，报名审核结果会及时通知，请随时关注");
	        	}
	        	else
	        	{
	        		SiteFunc.GoTo("ActionRement2.html?From=NewAction.html&ActivityID="+parm.ActivityID);
	                SiteFunc.Alert("报名申请提交成功，报名审核结果会及时通知，请随时关注");
	        	}
	        }
	        else {
	            SiteFunc.Alert(data.ErrMsg);
	        }
        });
	});
	
	$(".Js_Consider").off().click(function(){
		if(ActivityCategory=="1")
		{
			SiteFunc.GoTo("ActionRement.html?From=ActionRement_details.html&ActivityID="+parm.ActivityID);
		}
		else
		{
			SiteFunc.GoTo("ActionRement2.html?From=ActionRement_details.html&ActivityID="+parm.ActivityID);	
		}
	})
	
	
	$(".Js_Back").off().click(function () {
//		var parm = JSON.parse(data.parm);
		var status = parm.status;
		if(status==1){
			SiteFunc.GoTo("ActionRement.html?ActivityID="+parm.ActivityID+"&From=NewAction.html");
		}
		else{
			SiteFunc.GoTo("ActionRement2.html?ActivityID="+parm.ActivityID+"&From=NewAction.html");
		}
		
	});
	
	}
});