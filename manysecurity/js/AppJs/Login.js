﻿/// <reference path="../Cmn.js" />
/// <reference path="../../dist/js/mui.min.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="AppJs.js" />
/// <reference path="SiteFunc.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {
    var UrlItf = SiteFunc.AjaxUrl;
    $(".jscConfirmBtn").click(function () {
        var _phone = $(".jscPhone").val();
        var _passWord = $(".jscPassWord").val();
        if (!SiteFunc.CheckPhone(_phone)) {
            SiteFunc.Alert("请输入正确的手机号");
            return;
        }
        if (_passWord == "") {
            SiteFunc.Alert("密码不能为空");
            return;
        }
        CmnAjax.PostData(UrlItf + "UserLogin", { "Phone": _phone, "Password": _passWord,DeviceID:SiteFunc.UUID }, function (dat) {
            if (dat.IsSuccess == 1) {
                //记住账号信息
                SiteFunc.RememberLoginInfo(_phone, _passWord);
                //设置登录状态，记录公司ID
                SiteFunc.SetLoginStatus(true, dat.CorpID, dat.UserID);
                //记录意向城市
                SiteFunc.SetIntentionOpenCityID(dat.IntentionOpenCityID);
                if (dat.CorpID != "") {
                    SiteFunc.GoTo("Main.html");
                }
                else {
                    SiteFunc.GoTo("Main.html");
                }
            } else {
                $(".jscFloatError").show();
            }
        });
    });
    //$(".jscPassWord").on("keyup", function () {
    //    if ($(this).val().length > 0) {
    //        $(this).attr("type", "password");
    //    }
    //    else {
    //        $(this).attr("type", "text");
    //    }
    //});
    //点击关闭浮层
    $(".FloatError").click(function () {
        if ($(".FloatError").is(":visible")) {
            $(".jscFloatError").hide();
        }
    });
    //注册
    $(".jscRegister").click(function () {
        Cmn.App.ShowWaiting("请稍后，程序加载中...");
        SiteFunc.GoTo("Registered.html")
    });
    //密码找回
    $(".Js_ForGetPwd").click(function () {
        Cmn.App.ShowWaiting("请稍后，程序加载中...");
        SiteFunc.GoTo("ForgetPassword.html")
    });
});
////每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Cmn.App.CloseWaiting();
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });
});

//Cmn.Extend(App.Login = {}, new function () {
 
//    this.Init = function () {
//        //点击登陆按钮
//        //$(".jscConfirmBtn").on("touchstart", function () {
//        $(".jscConfirmBtn").undelegate(".jscConfirmBtn", "tap").delegate(".jscConfirmBtn", "tap", function () {
       
//       // mui(".jscConentMui").on("tap", ".jscConfirmBtn", function () {
//            var _phone = $(".jscPhone").val();
//            var _passWord = $(".jscPassWord").val();

//            //验证手机号码是否正确
//            if (!Cmn.VerifyWay.Phone.test(_phone)) {
//                App.Alert("请输入正确的11位手机号！");
//                return false;
//            }

//            //验证手机号码
//            if (_phone == "") {
//                $(".jscNotNull").shou();
//                return false;
//            }

//            //验证密码
//            if (_passWord == "") {
//                App.Alert("密码不能为空！");
//                return false;
//            }

//            //验证登陆信息
//            App.UserLogin(_phone, _passWord, function (isLogin) {
//                if (isLogin) { App.ToPageByUrl("Home.html"); }
//                else { App.Alert("登录失败,账号或密码错误！"); }
//            });
//        });
         
//    };

//});
