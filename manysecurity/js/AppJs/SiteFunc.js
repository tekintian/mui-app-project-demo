/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../mui.min.js" />
/// <reference path="CmnApp.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnFuncExd.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="CmnApp.js" />
(SiteFunc = new function () {

    //设备号
    this.UUID = "";
    
    //敬请期待页(false)/正常流程(true)
    this.IsTest = true;

    var _Self = this;
//  //接口调用地址
//  this.AjaxUrl = "http://192.168.1.39:8111/Ajax/Ajax.aspx?Method=";
//  //上传图片接口地址
//  this.UploadImgItf = "http://192.168.1.39:8111/Itf/CSharp/CmnMisItf.aspx?method=Upload";
//  //图片路径
//  this.UploadImgUrl = "http://192.168.1.39:8111/";

//接口调用地址
//  this.AjaxUrl = "http://192.168.1.90:9090/Ajax/Ajax.aspx?Method=";
//  //上传图片接口地址
//  this.UploadImgItf = "http://192.168.1.90:9090/Itf/CSharp/CmnMisItf.aspx?method=Upload";
//  //图片路径
//  this.UploadImgUrl = "http://192.168.1.90:9090/";

    //接口调用地址
//  this.AjaxUrl = "http://haoduoapi.8baoan.com/Ajax/Ajax.aspx?Method=";
//  //上传图片接口地址
//  this.UploadImgItf = "http://haoduoapi.8baoan.com/Itf/CSharp/CmnMisItf.aspx?method=Upload";
//  //图片路径
//  this.UploadImgUrl = "http://haoduoapi.8baoan.com/";
//  //接口调用地址
    //this.AjaxUrl = "http://didisecurity.nn.cagoe.com/Ajax/Ajax.aspx?Method=";
    //上传图片接口地址
    //this.UploadImgItf = "http://didisecurity.nn.cagoe.com/Itf/CSharp/CmnMisItf.aspx?method=Upload";
    //图片路径
    //this.UploadImgUrl = "http://didisecurity.nn.cagoe.com/";
    //接口调用地址
      this.AjaxUrl = "http://120.27.31.111:8088/Ajax/Ajax.aspx?Method=";
    //上传图片接口地址
      this.UploadImgItf = "http://120.27.31.111:8088/Itf/CSharp/CmnMisItf.aspx?method=Upload";
    //图片路径
      this.UploadImgUrl = "http://120.27.31.111:8088/";
////  //页面跳转
    this.GoTo = function (url) {
        var parm = PramToJson(url);
        Cmn.App.CurPage.GoTo(url.split("?")[0], parm, Cmn.App.CurPage.SwitchStyles.PopIn);
    }

    //将url参数转换为json格式
    function PramToJson(url) {
        var str = url;
        var num = str.indexOf("?")
        str = str.substr(num + 1); 
        var arr = str.split("&"); 
        var _json = "{";
        for (var i = 0; i < arr.length; i++) {
            num = arr[i].indexOf("=");
            if (num > 0) {
                name = arr[i].substring(0, num);
                value = arr[i].substr(num + 1);
                _json += "'" + name + "':" + "'" + value + "',";
            }
        }
        _json += "}";
        _json = eval("(" + _json + ")");
        return _json;
    }

    //返回前页
    this.Back = function () {
        Cmn.App.CurPage.Back();
    }
    //检查是否是数字
    this.CheckNum=function(num)
    {
    	if(num=="")
    	{
    		return false;
    	}
    	else
    	{
    		var a=parseInt(num);
	    	if(isNaN(a))
	    	{
	    		return false;
	    	}
	    	else
	    	{
	    		return true;
	    	}
    	}
    }
    //check 手机号返回bool
    this.CheckPhone = function (phone) {
        return /^0?1[3|4|5|7|8][0-9]\d{8}$/.test(phone);
    }

    //弹框
    this.Alert = function (msg) {
        if (!window["plus"]) { alert(msg); }
        else { plus.nativeUI.alert(msg); }
    }

    //图片上传
    this.UploadImg = function (demo, start,success,fail) {
        $(demo).off().on("change", function () {
            var _file = this.files[0],
            _self = this;
            var reader = new FileReader();
            reader.onload = function () {
                start && start(_self);
                var _url = reader.result;
                var _re = /^data:;base64,/;
                if (_re.test(_url)) {
                    _url = _url.replace(_re, 'data:image/png;base64,');
                }

                var _canvas = document.createElement("canvas");
                _canvas.id = "myCanvas";
                
                $("body").append(_canvas);
                $("#myCanvas").css({ "position": "absolute", "margin-top": "-10000px" });
                var _ctx = _canvas.getContext("2d");
                var img = new Image();
                img.onload = function () {

                    _canvas.height = img.height;
                    _canvas.width = img.width;

                    _ctx.drawImage(img,0,0);

                    _url = myCanvas.toDataURL("image/jpeg", 0.4);
                    //调用上传接口
                    var _uploadTack = plus.uploader.createUpload(_Self.UploadImgItf, { method: "POST" }, function (t, status) {
                        var _data = {};
                        try { _data = JSON.parse(_uploadTack.responseText) } catch (e) { }
                        if (_data.Path) {
                            //_HeadImgUrl = _data.Path;
                            success && success(_data);
                        }
                        else {
                            fail && fail(_data);
                        } 
                        $("#myCanvas").remove();
                    });
                    // _uploadTack.addFile(path, { key: "ImageData" });
                    _uploadTack.addData("limitSize", "2");
                    _uploadTack.addData("InputName", "ImageData");
                    _uploadTack.addData("ImageData", _url);
                    _uploadTack.addData("SavePath", "/Upload");
                    _uploadTack.addData("LimitSuffixName", "jpg,png,jpeg");
                    _uploadTack.addData("IsAutoCreateFolder", "1");
                    _uploadTack.addData("InputName", "ImageData");
                    //直接上传好了
                    _uploadTack.start();
                }
                img.src = _url;
                
            }
            reader.readAsDataURL(_file);
        });
    }

    //验证身份证合法性
    var aCity = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外" }
    this.CheckCardID = function (sId) {
        var iSum = 0;
        var info = "";

        if (sId == "") {
            return false;
        }
        if (!/^\d{17}(\d|x)$/i.test(sId)) {
            return false;
        }
        sId = sId.replace(/x$/i, "a");
        if (aCity[parseInt(sId.substr(0, 2))] == null) {
            return false;
        }

        sBirthday = sId.substr(6, 4) + "-" + Number(sId.substr(10, 2)) + "-" + Number(sId.substr(12, 2));
        var d = new Date(sBirthday.replace(/-/g, "/"));
        if (sBirthday != (d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate())) {
            return false;
        }

        for (var i = 17; i >= 0; i--) {
            iSum += (Math.pow(2, i) % 11) * parseInt(sId.charAt(17 - i), 11);
        }
        if (iSum % 11 != 1) {
            return false;
        }
        return true;
    }

    //自动登录 登录成功，返回true，否则为false
    this.Login = function (callback) {
        var _login = Cmn.App.GetLocalStorageDataByKey("Login");
        if (_login != null) {
            var _phone = _login.Phone;
            var _pwd = _login.Password;
            var _ret = false;
            if (_phone != "" && _pwd != "") {
                CmnAjax.PostData(_Self.AjaxUrl + "UserLogin", { "Phone": _phone, "Password": _pwd }, function (data) {
                    if (data.IsSuccess == 1) {
                        //记录登录状态
                        _Self.SetLoginStatus(true, data.CorpID, data.UserID);
                        _Self.SetIntentionOpenCityID(data.IntentionOpenCityID);
                        _ret = true;
                    }
                    else {
                        _Self.SetLoginStatus(false, "", "");
                        _ret = false;
                    }
                    callback && callback(_ret);
                });
            }
            else {
                callback && callback(false);
            }
        }
        else {
            callback && callback(false);
        }
    }
    //记录意向城市
    this.SetIntentionOpenCityID = function (cityID) {
        Cmn.App.SetLocalStorageData("IntentionOpenCityID", { CityID: cityID });
    }
    //获取意向城市
    this.GetIntentionOpenCityID = function () {
        var _intentionOpenCityID = Cmn.App.GetLocalStorageDataByKey("IntentionOpenCityID");
        if (_intentionOpenCityID != null) {
            return _intentionOpenCityID.CityID
        }
        else {
            return "";
        }
    }

    //记住账号登录信息
    this.RememberLoginInfo = function (phone,pwd) {
        Cmn.App.SetLocalStorageData("Login", { Phone: phone, Password: pwd });
    }
    //检查登录状态 返回ture(已登录)/false(未登录)
    this.CheckLoginStatus = function () {
        var _hasLogin = Cmn.App.GetLocalStorageDataByKey("LoginStatus");
        if (_hasLogin != null) {
            return _hasLogin.HasLogin;
        }
        else {
            return false;
        }
    }
    //获取公司ID
    this.GetCorpID = function () {
        var _hasLogin = Cmn.App.GetLocalStorageDataByKey("LoginStatus");
        var _has = false;
        if (_hasLogin != null) {
            return _hasLogin.CorpID;
        }
        else {
            return "";
        }
    }
    //获取用户ID
    this.GetUserID = function() {
        var _hasLogin = Cmn.App.GetLocalStorageDataByKey("LoginStatus");
        var _has = false;
        if (_hasLogin != null) {
            return _hasLogin.UserID;
        }
        else {
            return "";
        }
    }
    //设置登录状态
    this.SetLoginStatus = function (hasLogin,corpID, userId) {
        Cmn.App.SetLocalStorageData("LoginStatus", { HasLogin: hasLogin, CorpID: corpID, UserID: userId });
    }
    //更新活动状态
    this.UpdateActiveStatus = function (activityID, status, callback) {
        /// 取消招募 1 删除招募 2 查看报名 3 评价 4 查看评价 5
        CmnAjax.PostData(_Self.AjaxUrl + "UpdateActivitStatus", { ActivityID: activityID, Status: status }, function (data) {
            callback && callback(data);
        });
    }

    //填充省份
    this.FillProvince = function () {
        var _html = "<option value='-1'>请选择省份</option>";
        CmnAjax.PostData(_Self.AjaxUrl + "GetProvince", "", function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["ProvinceID"] + "'>" + data.data[i]["ProvinceDesc"] + "</option>";
                }
            }
            $(".Js_Province").html(_html);
            $(".Js_Province").off().change(function () {
                _Self.FillCity($(this).val())
            });
        });
    }
    //填充城市
    this.FillCity = function (provinceID) {
        var _html = "<option value='-1'>请选择城市</option>";
        CmnAjax.PostData(_Self.AjaxUrl + "GetCity", { ProvinceID: provinceID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                }
            }
            $(".Js_City").html(_html);
            $(".Js_City").off().change(function () {
                _Self.FillCounty($(this).val())
            });
        });
    }
    //填充区
    this.FillCounty = function (cityID) {
        var _html = "<option value='-1'>请选择区</option>";
        CmnAjax.PostData(_Self.AjaxUrl + "GetCounty", { CityID: cityID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CountryID"] + "'>" + data.data[i]["CountryDesc"] + "</option>";
                }
            }
            $(".Js_County").html(_html);
        });
    }

    var first = null;
    mui.back = function () {
        //有返回按钮的优先执行返回按钮
        if($(".Js_Back").length) {
            $('.Js_Back').trigger('click');
        }
        else {
        //首次按键，提示‘再按一次退出应用’
        if (!first) {
            first = new Date().getTime();
            mui.toast('再按一次退出应用');
            setTimeout(function () {
                first = null;
            }, 1000);
        } else {
            if (new Date().getTime() - first < 1000) {
                SiteFunc.SetLoginStatus(false, "", "");
                plus.runtime.quit();
            }
        }
        }
    }
    
    //获取格式化的日期
    Date.prototype.getFormattedString = function() {
        var result = [];
        result.push(this.getFullYear());
        result.push('/');
        result.push(this.getMonth() + 1);
        result.push('/');
        result.push(this.getDate());
        return result.join('');
    }
    
    //静默检查更新
    this.silentCheck = function(wgtVer) {
        // 下载wgt文件
        var wgtUrl = SiteFunc.UploadImgUrl + "update/H56EC34ED.wgt";

        function _downWgt() {
            plus.nativeUI.showWaiting("正在下载更新...");
            plus.downloader.createDownload(wgtUrl, { filename: "_doc/update/" }, function (d, status) {
                if (status == 200) {
                    _installWgt(d.filename); // 安装wgt包
                } else {
                    plus.nativeUI.alert("下载失败！");
                }
                plus.nativeUI.closeWaiting();
            }).start();
        }

        // 更新应用资源
        function _installWgt(path) {
            plus.nativeUI.showWaiting("下载更新完成，正在更新...");
            plus.runtime.install(path, {}, function () {
                plus.nativeUI.closeWaiting();
                plus.nativeUI.alert("应用资源更新完成！", function () {
                    plus.runtime.restart();
                });
            }, function (e) {
                plus.nativeUI.closeWaiting();
                plus.nativeUI.alert("安装文件失败[" + e.code + "]：" + e.message);
            });
        }
        
        var checkUrl = SiteFunc.UploadImgUrl + "GetVersion.aspx";
        var xhr = new XMLHttpRequest();
        
        xhr.onreadystatechange = function () {
            switch (xhr.readyState) {
                case 4:
                    plus.nativeUI.closeWaiting();
                    if (xhr.status == 200) {
                        var newVer = xhr.responseText;
                        if (wgtVer && newVer && (wgtVer != newVer)) {
                            plus.nativeUI.confirm('检测到有更新版本，是否需要更新?', function(e){
                                if(e.index == 0) {
                                    _downWgt();
                                }
                            }, '检查更新', '立即更新', '稍后更新');
                        } else {
                        }
                    } else {
                    }
                    break;
                default:
                    break;
            }
        }
        xhr.open('GET', checkUrl);
        xhr.send();
        }
});

