﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
Cmn.App.CurPage.OnInit.Add(function (data) {
    
})
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    if (data.Back == 1) {
        return;
    }

    ActionRement.ActivityID = (data.ActivityID == undefined ? "" : data.ActivityID);
    ActionRement.Status = (data.Status);
    ActionRement.FillActiveDtl();
    $(".Js_Back").off().click(function () {
        //SiteFunc.Back();
        if(data.From!=null && data.From=="ActionRement_details.html")
        {
        	SiteFunc.GoTo("ActionRement_details.html?From=NewAction.html&CorpID="+data.CorpID);	
        }
        else if (data.From != null && data.From != "MySign.html" && data.From!="Recruitment.html") {
            SiteFunc.GoTo("Main.html?Menu=NewAction.html");
        }
        else if(data.From != null && data.From == "MySign.html") {
        	SiteFunc.GoTo("MySign.html");
        }
        else if(data.From!=null && data.From=="Recruitment.html") {
        	var Bareak_=1;
        	SiteFunc.GoTo("Recruitment.html?From=NewAction.html&CorpID="+data.CorpID+"&CityID="+data.CityID + "&break="+ Bareak_);	
        }
        else {
            SiteFunc.GoTo("MyRelease.html?Back=1");
        }
    });
});
(ActionRement = new function () {
    var _Self = this;
    var OrderCode;
    var Mydata,Year,Moth,Day;
    this.ActivityID = "";
    this.Status = "";
    this.FillActiveDtl = function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetActivityDtl", { ActivityID: _Self.ActivityID }, function (data) {
            if (data.data.length > 0) {
                window._corpID = data.data[0]['CorpID'];
                $(".Js_ActiveDesc").text(data.data[0]["ActiveDesc"]);
                $(".Js_ActiveDesc_").text(data.data[0]["ActiveDesc"]);
                $(".Js_Plan").text(data.data[0]["RecruitPersonQty"]);
                $(".Js_HasApply").text(data.data[0]["ApplyPersonQty"]);
                $(".Js_HasEnroll").text(data.data[0]["RecruitedPersonQty"]);
                $(".Js_Tip").html(TipImg(data.data[0]["ActivityTypeID"], data.data[0]["IsRecommend"]));
                Mydata=new Date(data.data[0]["CreateTime"]);
                Year= Mydata.getFullYear();
                Moth= Mydata.getMonth()+1;
                Day= Mydata.getDate();
                if(Moth<10){
                	Moth="0"+Moth;
                }
                if(Day<10) {
                	Day="0"+Day;
                }
                OrderCode=data.data[0]["ActivityTypeID"]+"-"+ Year+Moth+Day+"-"+data.data[0]["ActivityID"];
                $(".Js_Order").text(OrderCode);
                if (data.data[0]["ActivityTypeID"] == "1") {
                    $(".Js_ActiveType").text("临时项目");
                }
                else {
                    $(".Js_ActiveType").text("驻场项目");
                }
                $(".Js_Money").text(data.data[0]["DaySalary"]);
                $(".Js_MoneyTime").text(data.data[0]["PaySalaryTime"]+"之前");
                if (data.data[0]["SexRequire"] == '1') {
                    $(".Js_Sex").text('不限');
                }
                if (data.data[0]["SexRequire"] == '2') {
                    $(".Js_Sex").text('男');
                }
                if (data.data[0]["SexRequire"] == '3') {
                    $(".Js_Sex").text('女');
                }
                $(".Js_WorkDate").text(data.data[0]["WorkDate"]);
                $(".Js_ActiveStartTime").text(data.data[0]["ActiveDateBegin"]);
                $(".Js_ActiveEndTime").text(data.data[0]["ActiveDateEnd"]);
                $(".Js_WorkTime").text(data.data[0]["WorkTime"]);
                $(".Js_Evaluate").text((data.data[0]["Evaluate"] == "" ? "0" : data.data[0]["Evaluate"]) + "%");
                var _provinceDesc = data.data[0]["ProvinceDesc"];
                var _cityDesc = data.data[0]["CityDesc"];
                var _countryDesc = data.data[0]["CountryDesc"];
                var _DiZhi = _provinceDesc + _cityDesc + _countryDesc;
                var _IsHideAddress=data.data[0]["IsHideAddress"].toLowerCase();
                if (data.data[0]["HasApply"] == "2" && _IsHideAddress== 'false') {
                    $(".Js_CompanyName").text(data.data[0]["CompanyName"]);
                    $(".Js_LinkMan").val(data.data[0]["LinkMan"]);
                    $(".Js_LinkPhone").val(data.data[0]["LinkPhone"]);
                    $(".Js_Address").text(_DiZhi+data.data[0]["Address"]);
                    $(".Js_ApplyTip").hide();
                }
                else {
                    var _companyName = data.data[0]["CompanyName"];
                    var _linkMan = data.data[0]["LinkMan"];
                    var _linkPhone = data.data[0]["LinkPhone"];
                    var _address = data.data[0]["Address"];
                    $(".Js_CompanyName").text(_companyName);
                  	$(".Js_LinkMan").val(_linkMan.replace(_linkMan.substring(1, _linkMan.length), "**"));
                  	$(".Js_LinkPhone").val(_linkPhone.replace(_linkPhone.substring(4, _linkPhone.length), "*******"));
                    $(".Js_Address").text(_DiZhi + "****");
                     $(".Js_ApplyTip").show();
                    if(_IsHideAddress=="true"){
                    	$(".Js_ApplyTip").hide();
                    }
                }
                $(".Js_Height").text(data.data[0]["HeightRequire"]);
                $(".Js_ArriveTime").text(data.data[0]["ArrivalTimeRequire"]);
                $(".Js_Weight").text(data.data[0]["WeightRequire"]);
                $(".Js_Need").text(data.data[0]["SecurityStaffCardRequire"]);
                //$(".Js_Cloth").val(data.data[0]["ClothesRequire"]);
                
	        	$('.Js_Cloth').empty();
	        	$('.Js_Cloth').append($('<option value="1">无要求</option>'));
	        	$('.Js_Cloth').get(0).selectedIndex = 0;
                if(data.data[0]["ClothesRequire"] != '无要求') {
                	var value;
                	switch (data.data[0]["ClothesRequire"]) {
                		case '全黑西服':
                			value=2;
                			break;
                			case '黑白配':
                			value = 3;
                			break;
                			case '保安制服':
                			value=4;
                			break;
                			default: 
                			value=0;
                	}
                	var option = $('<option value="' + value + '" disabled="disabled">' + data.data[0]["ClothesRequire"] + '</option>');
                	$('.Js_Cloth').append(option);
                	$('.Js_Cloth').get(0).selectedIndex=1;
                }
                
                
                $(".Js_Other").text(data.data[0]["OtherRequire"].length == 0 ? '无' : data.data[0]["OtherRequire"]);
                $(".Js_Apply").parent().attr("activityID", data.data[0]["ActivityID"]);
                $(".Js_AllJs_Evaluate").attr("corpID", data.data[0]["CorpID"]);
                if (data.data[0]["HasApply"] != "" && data.data[0]["HasApply"] != "0") {
                    $(".Js_Apply").html('<img src="images/Action/SignIcon2.png">已申请');
                    $(".Js_Apply").attr("hasApply", "1");
                }
                else {
                    $(".Js_Apply").html('<img src="images/Action/SignIcon.png">立即申请');
                    $(".Js_Apply").attr("hasApply", "0");
                }
                if (data.data[0]["HasCollect"] != "0") {
                    $(".Js_Collect").html('<img src="images/Action/CollectionIcon2.png">已收藏');
                    $(".Js_Collect").attr("hasCollect", "1");
                }
                else {
                    $(".Js_Collect").html('<img src="images/Action/CollectionIcon.png">收藏此公司');
                    $(".Js_Collect").attr("hasCollect", "0");
                }
                //甲方隐藏按钮
                if( SiteFunc.GetCorpID().length >0 ) {
                	$('.Js_Apply').hide();
                	$('.Js_Collect').hide();
                	if(data.data[0]["status"] == "1" || data.data[0]["status"]=="2"){
                		if(SiteFunc.GetUserID()==data.data[0]["PublishUserID"]){
                			$('.Js_Submit').hide();
                			$(".Js_LinkMan").get(0).setAttribute('disabled', 'disabled'); 
		                	$(".Js_LinkPhone").get(0).setAttribute('disabled', 'disabled'); 
		                	$(".Js_Cloth").get(0).setAttribute('disabled', 'disabled'); 
                		}
                		else{
                			$('.Js_Submit').hide();
                			$(".Js_LinkMan").get(0).setAttribute('disabled', 'disabled'); 
		                	$(".Js_LinkPhone").get(0).setAttribute('disabled', 'disabled'); 
		                	$(".Js_Cloth").get(0).setAttribute('disabled', 'disabled'); 
                		}
                	}
                	else {
                		$('.Js_Submit').hide();
                		$(".Js_LinkMan").get(0).setAttribute('disabled', 'disabled'); 
	                	$(".Js_LinkPhone").get(0).setAttribute('disabled', 'disabled'); 
	                	$(".Js_Cloth").get(0).setAttribute('disabled', 'disabled'); 
                	}
                }
                else {
                	
                	//已经结束的项目不让申请
	                if(data.data[0]['status'] != 1) {
	                    $('#btnsCorp .Js_Apply').hide();
	                }
	                else {
	                    $('#btnsCorp .Js_Apply').show();
	                }
                	$('.Js_Collect').show();
                	$('.Js_Submit').hide();
                	if($('.Js_LinkMan').get(0)) {
                        $(".Js_LinkMan").get(0).setAttribute('disabled', 'disabled');
                	}
                	if($('.Js_LinkPhone').get(0)) {
                        $(".Js_LinkPhone").get(0).setAttribute('disabled', 'disabled'); 
                	}
                	if($('.Js_Cloth').get(0)) {
                        $(".Js_Cloth").get(0).setAttribute('disabled', 'disabled'); 
                	}
                }

                
                //申请
                $(".Js_Apply").off().click(function () {
                    if (SiteFunc.GetCorpID() != "") {
                        SiteFunc.Alert("甲方不能申请项目");
                        return;
                    }
                    if ($(this).attr("hasApply") == "1") {
                        SiteFunc.Alert("您已经申请过这个项目了");
                        return;
                    }
                    var parm_;
					var _ActivityID = $(this).parent().attr("activityID");
					var _CorpID = window._corpID;
					var _ActiveDesc = $(".Js_ActiveDesc").text();
                    var _ActiveTime = $(".Js_ActiveStartTime").text()+"-"+$(".Js_ActiveEndTime").text();
                    var _WorkTime = $(".Js_WorkTime").text();
                    var _Height = $(".Js_Height").text();
                    var _ArriveTime = $(".Js_ArriveTime").text();
                    var _Weight = $(".Js_Weight").text();
                    var _Need = $(".Js_Need").text();
                    var _Cloth = $(".Js_Cloth").find('option:selected').text();
                    var _Sex = $(".Js_Sex").text();
                    var _Other = $(".Js_Other").text();
                     
                    parm_ = {
                    	"ActivityID" : _ActivityID.trim(),
                    	"CorpID" : _CorpID,
                    	"ActivityCategory" : 1,
                    	"status" : 1,//短期活动
                    	"ActiveDesc" : _ActiveDesc.trim(),
                    	"ActiveTime" : _ActiveTime.trim(),
                    	"WorkTime" : _WorkTime.trim(),
                    	"Height" : _Height.trim(),
                    	"ArriveTime" : _ArriveTime.trim(),
                    	"Weight" : _Weight.trim(),
                    	"Need" : _Need.trim(),
                    	"Cloth" : _Cloth.trim(),
                    	"Sex" : _Sex.trim(),
                    	"Other" : _Other.trim()
                    	
                    }
                    var a =JSON.stringify(parm_);
//                  Cmn.App.GoTo("ActionRement_details.html", parm_, Cmn.App.CurPage.SwitchStyles.PopIn, {});
                    SiteFunc.GoTo("ActionRement_details.html?ActivityCategory=1&parm="+ a);
//                  CmnAjax.PostData(SiteFunc.AjaxUrl + "AddApplyRec", { ActivityID: $(this).parent().attr("activityID"), CorpID: window._corpID,ActivityCategory: 1   }, function (data) {
//                      if (data.IsSuccess == 1) {
//                          SiteFunc.Alert("报名申请提交成功，报名审核结果会及时通知，请随时关注");
//                          ActionRement.FillActiveDtl();
//                      }
//                      else {
//                          SiteFunc.Alert(data.ErrMsg);
//                      }
//                  });
                });
                //收藏
                $(".Js_Collect").off().click(function () {
                    if (SiteFunc.GetCorpID() != "") {
                        SiteFunc.Alert("甲方不能收藏公司");
                        return;
                    }
                    if ($(this).attr("hasCollect") == "1") {
                        SiteFunc.Alert("您已经收藏过这个公司了");
                        return;
                    }
                    CmnAjax.PostData(SiteFunc.AjaxUrl + "AddCollectRec", { ActivityID: $(this).parent().attr("activityID"), CorpID: window._corpID }, function (data) {
                        if (data.IsSuccess == 1) {
                            SiteFunc.Alert("收藏成功");
                            ActionRement.FillActiveDtl();
                        }
                        else {
                            SiteFunc.Alert(data.ErrMsg);
                        }
                    });
                });
                var _companyName = data.data[0]["CompanyName"];
                //所有评价
                $(".Js_AllJs_Evaluate").off().click(function () {
                    var _corpID = $(this).attr("corpID");
                    if (_corpID != undefined) {
                        SiteFunc.GoTo("BusinessReviews.html?ActivityCategory=2&From=ActionRement.html&CorpID=" + _corpID + "&CompanyName=" + _companyName);
                    }
                });
               
                
                $(".Js_Submit").off().on("touchstart",function(){
                	var _ActivityID=data.data[0]["ActivityID"];
                	var _LinkMan = $(".Js_LinkMan").val();
                	var _LinkPhone = $(".Js_LinkPhone").val();
                	var _Cloth = $(".Js_Cloth").val();
                	if(_LinkMan=="") { SiteFunc.Alert("请输入联系人名称"); return; }
                	if(_LinkPhone=="") { SiteFunc.Alert("请输入联系人电话"); return; }
                	var _parm ={
                		"ActivityID": _ActivityID,
                		"LinkMan" : _LinkMan,
                		"LinkPhone" : _LinkPhone,
                		"Cloth" : _Cloth
            }
                	CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateContactInfo", _parm, function (data) {
			            if (data.IsSuccess == 1) {
			                SiteFunc.Alert("修改成功！");
			                SiteFunc.GoTo("ActionRement.html?Release=1&ActivityID=" + _ActivityID);
			            } else {
			                SiteFunc.Alert("修改失败！");
			            }
        });
                	
                })
    }
        });
    }

    //是否推荐，或驻/临
    function TipImg(type, isRec) {
        var html = "";
        if (type == 1) {
            html += '<span ><img src="images/Action/picture-xiang.png" /></span>';
        }
        else if (type == 2) {
            html += '<span ><img src="images/Action/picture-zhu.png" /></span>';
        }
        else if (type==3)
        {
            html += '<span ><img src="images/Action/picture-zhi.png" /></span>';
        }
        else if (type==4)
        {
            html += '<span ><img src="images/Action/picture-pai.png" /></span>';
        }
        if (isRec == "True") {
            html += '<span ><img src="images/Action/RecommendIcon.png" style="width: 49px; height: 38px;" /></span>';
        }
        return html;
    }
});