﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//企业Logo
var _CompanyLogo = "";
//企业名称
var _CompanyName = "";
//企业地址（省）
var _ProvinceDesc = "";
//企业地址（市）
var _CityDesc = "";
//企业地址（区）
var _CountryDesc = "";
//企业地址
var _ProvinceID = "";
var _CityID = "";
var _CountryID = "";
var _CompanyAddress = "";
//联系人姓名
var _LinkMan = "";
//联系人电话
var _LinkPhone = "";
//保安许可证
var _SecurityServiceLicence = "";
//营业执照
var _BusinessLicense = "";

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {
    ModifyData2.FillProvince();
});
//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    //填充当前公司信息
    CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateEnterpriseTheQueryInfo", "", function (data) {
        if (data.IsSuccess == 1) {
            $(".MyPhoto img").attr("src", SiteFunc.UploadImgUrl + data.data[0].CompanyLogo);//公司logo
            _CompanyLogo = data.data[0].CompanyLogo;//公司logo
            $(".Js_CompanyName").val(data.data[0].CompanyName);//公司名称
            _ProvinceID = data.data[0].ProvinceID;//省份ID
            _CityID = data.data[0].CityID;//城市DI
            _CountryID = data.data[0].CountryID;//区ID
            $(".Js_ProvinceDesc").text(data.data[0].ProvinceDesc);
            $(".Js_CityDesc").text(data.data[0].CityDesc);
            $(".Js_CountryDesc").text(data.data[0].CountryDesc);
            $(".Js_Address").val(data.data[0].CompanyAddress);//详细地址
            $(".Js_Man").val(data.data[0].LinkMan);//联系人
            $(".Js_Phone").val(data.data[0].LinkPhone);//联系人电话
            //$(".Js_SecurityService").text(data.data[0].SecurityServiceLicence);//许可证
            $(".Js_Business").attr("src", SiteFunc.UploadImgUrl + data.data[0].SecurityServiceLicence);//
            $(".Js_Business_2").attr("src", SiteFunc.UploadImgUrl + data.data[0].BusinessLicense);
            _SecurityServiceLicence = data.data[0].SecurityServiceLicence;
            _BusinessLicense = data.data[0].BusinessLicense;
            Cmn.App.CloseWaiting();
        }
    });
    //资料
    $(".mui-LoginOKBtn").off().on("touchstart", function () {
        _CompanyName = $(".Js_CompanyName").val();
        _CompanyAddress = $(".Js_Address").val();
        _LinkMan = $(".Js_Man").val();
        _LinkPhone = $(".Js_Phone").val();
        //if (_CompanyLogo == "") { SiteFunc.Alert("请上传公司Logo"); return; }
        if (_CompanyName == "") { SiteFunc.Alert("请填写公司名称"); return; }
        if (_ProvinceID == "") { SiteFunc.Alert("请选择公司所在省份"); return; }
        if (_CityID == "") { SiteFunc.Alert("请选择公司所在城市"); return; }
        if (_CountryID == "") { SiteFunc.Alert("请选择公司所在区"); return; }
        if (_CompanyAddress == "") { SiteFunc.Alert("请填写公司详细地址"); return; }
        //if (_LinkMan == "") { SiteFunc.Alert("请填写公司联系人"); return; }
        //if (_LinkPhone == "") { SiteFunc.Alert("请填写公司联系人电话"); return; }
        if (_SecurityServiceLicence == "") { SiteFunc.Alert("请上传保安服务许可证"); return; }
        if (_BusinessLicense == "") { SiteFunc.Alert("请上传营业执照"); return; }
        var _parm = {
            CompanyLogo: _CompanyLogo,
            CompanyName: _CompanyName,
            ProvinceID:_ProvinceID,
            CityID:_CityID,
            CountryID:_CountryID,
            CompanyAddress:_CompanyAddress,
            LinkMan:_LinkMan,
            LinkPhone:_LinkPhone,
            SecurityServiceLicence:_SecurityServiceLicence,
            BusinessLicense: _BusinessLicense
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateEnterpriseChangeInfo", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("修改成功！");
                SiteFunc.GoTo("Main.html?Menu=Home.html");
            } else {
                SiteFunc.Alert("修改失败！");
            }
        });

    });

    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });

    //企业LOGO
    SiteFunc.UploadImg(".Js_Logo", function (demo) {
        $(".LoadTip").show();
    }, function (data) {
        _CompanyLogo = data.Path;
        $(".MyPhoto img").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    }, function (data) {
        SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    //保安服务许可证
    SiteFunc.UploadImg(".Js_SecurityServiceLicence", function (demo) {
        $(".LoadTip").show();
    }, function (data) {
        _SecurityServiceLicence = data.Path;
        $(".Js_Business").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    }, function (data) {
        SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
    //营业执照
    SiteFunc.UploadImg(".Js_BusinessLicense", function (demo) {
        $(".LoadTip").show();
    }, function (data) {
        _BusinessLicense = data.Path;
        $(".Js_Business_2").attr("src", SiteFunc.UploadImgUrl + data.Path);
        $(".LoadTip").hide();
    }, function (data) {
        SiteFunc.Alert(data.ErrMsg);
        $(".LoadTip").hide();
    });
});
(ModifyData2 = new function () {
    var _Self = this;

    //填充省份
    this.FillProvince = function () {
        var _html = "<option value='-1'>请选择省份</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetProvince", "", function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["ProvinceID"] + "'>" + data.data[i]["ProvinceDesc"] + "</option>";
                }
            }
            $(".Js_Province").html(_html);
            $(".Js_Province").off().change(function () {
                $(".Js_ProvinceDesc").text($(this).find("option:selected").text());
                _ProvinceID = $(this).val();
                _Self.FillCity($(this).val())
            });
        });
    }
    //填充城市
    this.FillCity = function (provinceID) {
        var _html = "<option value='-1'>请选择城市</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetCity", { ProvinceID: provinceID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                }
            }
            $(".Js_City").html(_html);
            $(".Js_City").off().change(function () {
                $(".Js_CityDesc").text($(this).find("option:selected").text());
                _CityID = $(this).val();
                _Self.FillCounty($(this).val())
            });
        });
    }
    //填充区
    this.FillCounty = function (cityID) {
        var _html = "<option value='-1'>请选择区</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetCounty", { CityID: cityID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CountryID"] + "'>" + data.data[i]["CountryDesc"] + "</option>";
                }
            }
            $(".Js_Country").html(_html);
            $(".Js_Country").off().change(function () {
                $(".Js_CountryDesc").text($(this).find("option:selected").text());
                _CountryID = $(this).val();
            });
        });
    }
})











