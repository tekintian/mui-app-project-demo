﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    MyRelease.FillMyPublish("", "", "", "");
    var _release = (data.Release == undefined ? "" : data.Release);
    if (_release == '1') { return; }
});
Cmn.App.CurPage.OnInit.Add(function (data) {
 
    //报名查看
    $(".Js_Look").click(function () {
        $(this).parent().hide();
        $(this).parent().prev().show();
        //报名查看
        SiteFunc.GoTo("CheckUp.html?ActivityID=" + $(this).parent().attr("activeID"));
    });


    var _html = "";
    //活动状态
    $(".Js_ActiveStatus").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" status="">项目状态</p>';
        _html += '<p class="mui-recruitment-DownList" status="0">审核中</p>';
        _html += '<p class="mui-recruitment-DownList" status="1">招募中</p>';
        _html += '<p class="mui-recruitment-DownList" status="2">招募结束</p>';
        _html += '<p class="mui-recruitment-DownList" status="3">招募取消</p>';

        MyRelease.FillSelect(_html, 1);
    });
    //活动类型
    $(".Js_ActiveType").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" type="">项目类型</p>';
        _html += '<p class="mui-recruitment-DownList" type="1">临时</p>';
        _html += '<p class="mui-recruitment-DownList" type="2">驻场</p>';
        MyRelease.FillSelect(_html, 2);
    });
    //评价状态
    $(".Js_EvaluateStatus").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" evalute="">评价状态</p>';
        _html += '<p class="mui-recruitment-DownList" evalute="0">待评</p>';
        _html += '<p class="mui-recruitment-DownList" evalute="1">已评</p>';
        MyRelease.FillSelect(_html, 3);
    });

    //查询按钮
    $(".Js_Search").click(function () {
        if ($(".Js_SearchBox").css("display") == "none") {
            $(".Js_SearchBox").show();
        }
        else {
            $(".Js_SearchBox").hide();
        }
    });
    //关键字搜索
    $(".Js_SearchBtn").click(function () {
        $(".Js_Select").hide();
        var _word = $(this).prev().val();
        if (_word == "") {
            SiteFunc.Alert("请输入关键字");
            return;
        }
        $(".Js_SearchBox").hide();
        MyRelease.Index = "";
        MyRelease.FillMyPublish("", "", "", _word);
    });
    //返回
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });
});
var _AjaxUrl = SiteFunc.AjaxUrl;
(MyRelease = new function () {
    var _Self = this;
    var _activityID="";
    this.ActiveStatus = "";//项目活动状态(【审核中、审核失败、招募中、招募结束、招募取消】)
    this.ActiveType = "";//项目活动类型(临时/驻场)
    this.EvaluateStatus = "";//评价状态(已评/待评)
    this.KeyWord = "";//关键字

    //填充我的发布
    this.FillMyPublish = function (status, type, evaluate, keyword) {
        CmnAjax.PostData(_AjaxUrl + "GetMyPublishActive", { Status: status, ActivityTypeID: type, EvaluateType: evaluate, KeyWord: keyword }, function (data) {
            var _html = "";
            if (data.data.length > 0) {
                for (var _i = 0; _i < data.data.length; _i++) {
                	var Details =(data.data[_i]["ListMenu"]).split(",");
                    if (evaluate == "1") {
                    	if (data.data[_i]["Operation"] == "详情") { 
                            _html += '<p class="mui-Recuitment-action Js_List" style="padding-bottom:0px;">' +
                            '<span class="mui-MyRelease-Title Js_Name" ActivityCategory="'+data.data[_i]["ActivityCategory"]+'" activeID="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br>' +
                            '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span><br>' +
                            '<button class="mui-btn Js_Operation mui-stateBtn2" id="mui-stateBtn2" onclick="return false;" activeID="' + data.data[_i]["ActivityID"] + '"   ' + (data.data[_i]["Operation"] == "" ? "style='display:none;'" : "style='display:block;'") + '>' + data.data[_i]["Operation"] + '</button>';
	                    	
	                        if(data.data[_i]["Status"]=="1") {//招募中
	                        	_html += '<span class="mui-stateBtn2-btn" onclick="return false;" style="display: none;right: 35px" activeID="' + data.data[_i]["ActivityID"] + '">' +
                                '<a class="mui-statebtn2-SignUp Js_Look">' + Details[0] + ' </a>' +
                                '<a class="mui-statebtn2-close Js_Off">' + Details[1] + '</a><br>' +
                                '<a class="mui-statebtn2-close Js_Cancel">' + Details[2] + '</a><br>' +
                                '</span><span class="cbtn_">' +
                                '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
                                '</span><br><br>';
	                        }
	                        else if(data.data[_i]["Status"]=="2") {//招募结束
	                        	_html += '<span class="mui-stateBtn2-btn" onclick="return false;" style="display: none;right: 35px" activeID="' + data.data[_i]["ActivityID"] + '">' +
                                '<a class="mui-statebtn2-SignUp Js_Look">' + Details[1] + '</a>' +
                                '<a class="mui-statebtn2-close Js_Evaluate">' + Details[0] + '</a><br>' +
                                '<a class="mui-statebtn2-close Js_Cancel">' + Details[2] + '</a><br>' +
                                
                                '</span><span class="cbtn_">' +
                                '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
                                '</span><br><br>';
	                        }
                            _html += '</p>';
                        }
                    	else {//项目取消
                    		
                    		html += '<p class="mui-Recuitment-action Js_List" style="padding-bottom:0px;">' +
                            '<span class="mui-MyRelease-Title Js_Name" ActivityCategory="'+data.data[_i]["ActivityCategory"]+'" activeID="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br>' +
                            '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span><br>' +
                            '<button class="mui-btn Js_Operation mui-stateBtn2" id="mui-stateBtn2" onclick="return false;" activeID="' + data.data[_i]["ActivityID"] + '"   ' + (data.data[_i]["Operation"] == "" ? "style='display:none;'" : "style='display:block;'") + '>' + data.data[_i]["Operation"] + '</button>';
                            '<span class="cbtn_">' +
	                        '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
	                        '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
	                        '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
	                        '</span><br><br>';
                            _html += '</p>';
                    	}
                    }
                    else if (evaluate == "0") {
                        if (data.data[_i]["Operation"] == "详情") {
                            _html += '<p class="mui-Recuitment-action Js_List" style="padding-bottom:0px;">' +
                            '<span class="mui-MyRelease-Title Js_Name" ActivityCategory="'+data.data[_i]["ActivityCategory"]+'" activeID="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br>' +
                            '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span><br>' +
                            '<button class="mui-btn Js_Operation mui-stateBtn2" id="mui-stateBtn2" onclick="return false;" activeID="' + data.data[_i]["ActivityID"] + '"  ' + (data.data[_i]["Operation"] == "" ? "style='display:none;'" : "style='display:block;'") + ' >' + data.data[_i]["Operation"] + '</button>';
                            
                            if(data.data[_i]["Status"]=="1") {//招募中
	                        	_html += '<span class="mui-stateBtn2-btn" onclick="return false;" style="display: none;right: 35px" activeID="' + data.data[_i]["ActivityID"] + '">' +
                                '<a class="mui-statebtn2-SignUp Js_Look">'+Details[0]+'</a>' +
                                '<a class="mui-statebtn2-close Js_Off">'+Details[1]+'</a><br>' +
                                '<a class="mui-statebtn2-close Js_Cancel">'+Details[2]+'</a><br>' +
                                '</span><span class="cbtn_">' +
                                '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
                                '</span><br><br>';
	                        }
	                        else if(data.data[_i]["Status"]=="2") {//招募结束
	                        	_html += '<span class="mui-stateBtn2-btn" onclick="return false;" style="display: none;right: 35px" activeID="' + data.data[_i]["ActivityID"] + '">' +
                                '<a class="mui-statebtn2-SignUp Js_Look">'+Details[1]+'</a>' +
                                '<a class="mui-statebtn2-close Js_Evaluate">'+Details[0]+'</a><br>' +
                                '<a class="mui-statebtn2-close Js_Cancel">'+Details[2]+'</a><br>' +
                                '</span><span class="cbtn_">' +
                                '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
                                '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
                                '</span><br><br>';
	                        }
	                        
                            _html += '</p>';
                        }
                        else {
                        	html += '<p class="mui-Recuitment-action Js_List" style="padding-bottom:0px;">' +
                            '<span class="mui-MyRelease-Title Js_Name" ActivityCategory="'+data.data[_i]["ActivityCategory"]+'" activeID="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br>' +
                            '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span><br>' +
                            '<button class="mui-btn Js_Operation mui-stateBtn2" id="mui-stateBtn2" onclick="return false;" activeID="' + data.data[_i]["ActivityID"] + '"   ' + (data.data[_i]["Operation"] == "" ? "style='display:none;'" : "style='display:block;'") + '>' + data.data[_i]["Operation"] + '</button>';
                            '<span class="cbtn_">' +
	                        '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
	                        '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
	                        '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
	                        '</span><br><br>';
                            _html += '</p>';
                        }
                    }
                    else {
                    	if(data.data[_i]["Operation"]=="详情") {
	                        _html += '<p class="mui-Recuitment-action Js_List" style="padding-bottom:0px;">' +
	                        '<span class="mui-MyRelease-Title Js_Name" ActivityCategory="'+data.data[_i]["ActivityCategory"]+'" activeID="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br>' +
	                        '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span><br>' + 
	                        '<button class="mui-btn Js_Operation mui-stateBtn2" id="mui-stateBtn2" onclick="return false;" activeID="' + data.data[_i]["ActivityID"] + '"  ' + (data.data[_i]["Operation"] == "" ? "style='display:none;'" : "style='display:block;'") + ' >' + (data.data[_i]["Operation"] == '查看评价' ? '详情' : data.data[_i]["Operation"]) + '</button>';
	                        if (data.data[_i]["Status"]=="1") {//招募中
	                            _html += 
	                            '<span class="mui-stateBtn2-btn" onclick="return false;" style="display: none;right: 35px" activeID="' + data.data[_i]["ActivityID"] + '">' +
	                            '<a class="mui-statebtn2-SignUp Js_Look">'+Details[0]+'</a>' +
                                '<a class="mui-statebtn2-close Js_Off">'+Details[1]+'</a><br>' +
                                '<a class="mui-statebtn2-close Js_Cancel">'+Details[2]+'</a><br>' +
	                            '</span><span class="cbtn_">' +
	                            '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
	                            '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
	                            '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
	                            '</span><br><br>';
	                        }
	                        if (data.data[_i]["Status"]=="2") {//招募结束
	                            _html += 
	                            '<span class="mui-stateBtn2-btn" onclick="return false;" style="display: none;right: 35px" activeID="' + data.data[_i]["ActivityID"] + '">' +
	                            '<a class="mui-statebtn2-SignUp Js_Look">'+Details[1]+'</a>' +
                                '<a class="mui-statebtn2-close Js_Evaluate">'+Details[0]+'</a><br>' +
                                '<a class="mui-statebtn2-close Js_Cancel">'+Details[2]+'</a><br>' +
	                            '</span><span class="cbtn_">' +
	                            '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
	                            '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
	                            '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
	                            '</span><br><br>';
	                        }
	                        _html += '</p>';
                       }
                    	else {//项目结束
                    		_html += '<p class="mui-Recuitment-action Js_List" style="padding-bottom:0px;">' +
	                        '<span class="mui-MyRelease-Title Js_Name" ActivityCategory="'+data.data[_i]["ActivityCategory"]+'" activeID="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br>' +
	                        '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span><br>' + 
	                        '<button class="mui-btn Js_Operation mui-stateBtn2" id="mui-stateBtn2" onclick="return false;" activeID="' + data.data[_i]["ActivityID"] + '"  ' + (data.data[_i]["Operation"] == "" ? "style='display:none;'" : "style='display:block;'") + ' >' + (data.data[_i]["Operation"] == '查看评价' ? '详情' : data.data[_i]["Operation"]) + '</button>';
	                        '<span class="cbtn_">' +
	                        '<label id="mui-MyRelease-Num">计划招募' + data.data[_i]["RecruitPersonQty"] + '人</label>' +
	                        '<label id="mui-MyRelease-Num2">已报名' + data.data[_i]["ApplyPersonQty"] + '人</label>' +
	                        '<label id="mui-MyRelease-Num3">已录取' + data.data[_i]["RecruitedPersonQty"] + '人</label>' +
	                        '</span><br><br>';
	                        _html += '</p>';
                    	}
                    }

                }
                $('.Js_MyActiveList').removeClass('emptyContent');
            }
            else {
                $('.Js_MyActiveList').addClass('emptyContent');
            }
            $(".Js_MyActiveList").html(_html);
            //$(".Js_Operation").show();
           
           //结束招募
           $(".Js_QueDing").off().on("click",function () {
           		SiteFunc.UpdateActiveStatus(_activityID, 2, function (data) {
                    if (data.IsSuccess == 1) {
                    	$(".End-Recruitment").hide();
//                      SiteFunc.Alert("操作成功！");
                        MyRelease.FillMyPublish("", "", "", "");
                    }
                    else {
                        SiteFunc.Alert("操作失败，请稍后再试");
                    }
                });
           });
           
           $(".Js_QuXiao").off().on("click",function() {
           		$(".End-Recruitment").hide();
           })
           
           //项目取消
           $(".Js_QueDing_").off().on("click",function(){
           		//更新状态
                /// 0:审核中;1:招募中;2:招募结束;3:招募取消;4:项目结束;5:审核失败;6:项目取消
                SiteFunc.UpdateActiveStatus(_activityID, 6, function (data) {
                    if (data.IsSuccess == 1) {
//                      SiteFunc.Alert("操作成功！");
						$(".project-cance").hide();
                        MyRelease.FillMyPublish("", "", "", "");
                    }
                    else {
                    	$(".project-cance").hide();
                        SiteFunc.Alert("操作失败，请稍后再试");
                    }
                });
           })
           
           $(".Js_QuXiao_").off().on("click",function() {
           		$(".project-cance").hide();
           })
           
            //点击姓名跳转页面
            $(".Js_Name").off().click(function () {
                var _self = this;
                if($(this).attr("ActivityCategory")=="1")
                {
                	SiteFunc.GoTo("ActionRement.html?Release=1&ActivityID=" + $(this).attr("activeID"));
                }
                else
                {
                	SiteFunc.GoTo("ActionRement2.html?Release=1&ActivityID=" + $(this).attr("activeID"));
                }
            });
            //结束招募
            $(".Js_Off").off().click(function () {
            	var _self = this;
//          	$(this).parent().hide();
            	$(".End-Recruitment").show();
                _activityID = $(this).parent().attr("activeID");
                //更新状态
                /// 0:审核中;1:招募中;2:招募结束;3:招募取消;4:项目结束;5:审核失败;6:项目取消
                
            });
            
            //项目取消
            $(".Js_Cancel").off().click(function () {
//          	$(this).parent().hide();
                var _self = this;
                $(".project-cance").show();
                _activityID = $(this).parent().attr("activeID");
                
            });
            //报名查看
            $(".Js_Look").click(function () {
                $(this).parent().hide();
                $(this).parent().prev().show();
                SiteFunc.GoTo("CheckUp.html?ActivityID=" + $(this).parent().attr("activeID"));
            });
            
            $(".Js_Evaluate").click(function(){
            	$(this).parent().hide();
                $(this).parent().prev().show();
                SiteFunc.GoTo("SafetyEvaluation.html?ActivityID=" + $(this).parent().attr("activeID"));
            });
            //操作
            var _operateindex = "";
            $(".Js_Operation").off().click(function () {
                var _self = this;
                if ($(this).text() == "查看报名") {
                    SiteFunc.GoTo("CheckUp.html?ActivityID=" + $(this).parent().attr("activeID"));
                }
              //if ($(this).text() == "取消招募") {
//	                if ($(this).text() == "项目结束") {
//                  var _activityID = $(this).attr("activeID");
//                  /// 取消招募 1 删除招募 2 查看报名 3 评价 4 查看评价 5
//                  SiteFunc.UpdateActiveStatus(_activityID, 1, function (data) {
//                      if (data.IsSuccess == 1) {
//                          //$(_self).parent().prev().show().text("取消招募").attr("id", "mui-stateBtn");
//                          //$(_self).parent().next().hide();
//                          //SiteFunc.Alert("取消成功");
//                          SiteFunc.Alert("操作成功");
//                          MyRelease.FillMyPublish("", "", "", "");
//                      }
//                      else {
//                          SiteFunc.Alert("操作失败，请稍后再试");
//                      }
//                  });
//                  return;
//              }
//              if ($(this).text() == "评价") {
//                  SiteFunc.GoTo("SafetyEvaluation.html?ActivityID=" + $(this).attr("activeID"));
//              }
                

//              if ($(this).text() == "删除招募") {
//                  var _activityID = $(this).attr("activeID");
//                  /// 取消招募 1 删除招募 2 查看报名 3 评价 4 查看评价 5
//                  SiteFunc.UpdateActiveStatus(_activityID, 2, function (data) {
//                      if (data.IsSuccess == 1) {
//                          MyRelease.FillMyPublish("", "", "", "");
//                          SiteFunc.Alert("删除成功");
//                      }
//                      else {
//                          SiteFunc.Alert("删除失败");
//                      }
//                  });
//              }
                if ($(this).text() == "详情") {
                    $(this).next().show();
                    $(this).hide();
                    _operateindex = $(_self).index();
                }
//              if($(this).text()=="查看评价"){
//              	$(this).next().show();
//                  $(this).hide();
//                  _operateindex = $(_self).index();
//              }
            });
        });
    }
    //获取对应活动状态图
    this.GetStatus = function (state) {
        //0：审核中；1：招募中；2：招募结束；3：招募取消
        var _img = "";
        switch (state) {
            case "0":
                _img = '<img src="images/Evaluation/State2.jpg">';
                break;
            case "1":
                _img = '<img src="images/Evaluation/State1.jpg">';
                break;
            case "2":
                _img = '<img src="images/Evaluation/State4.jpg">';
                break;
            case "3":
                _img = '<img src="images/Evaluation/RecruitBtn3.jpg">';
                break;
            default:
                _img = "";
                break;
        }
        return _img;
    }
    this.Index = "";
    //填充选项
    this.FillSelect = function (select, index) {

        ////填充
        //if (_Self.Index == index && $(".Js_Select").is(":visible")) {
        //    $(".Js_Select").hide();
        //    return;
        //}
        _Self.Index = index;
        if ($(".Js_Select").css("display") == "none") {
            $(".Js_Select").html(select).show();
        }
        else {
            $(".Js_Select").html(select).hide();
        }
        //选择
        $(".Js_Select p").off().click(function () {
            //mui-morebtn
            var _text = $(this).text();
            if (index == 1) {
                $(".Js_ActiveStatus").find(".mui-morebtn").text(_text);
                _Self.ActiveStatus = $(this).attr("status");
            }
            if (index == 2) {
                $(".Js_ActiveType").find(".mui-morebtn").text(_text);
                _Self.ActiveType = $(this).attr("type");
            }
            if (index == 3) {
                $(".Js_EvaluateStatus").find(".mui-morebtn").text(_text);
                _Self.EvaluateStatus = $(this).attr("evalute");
            }
            _Self.FillMyPublish(_Self.ActiveStatus, _Self.ActiveType, _Self.EvaluateStatus, "");

        });
        $(".Js_Select").off().click(function () {
            $(this).hide();
        });
    }
})
