﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />

Cmn.App.CurPage.OnInit.Add(function (data) {

    //提交
    $(".Js_AnNiou").click(function () {

        var _usedpassword = $(".Js_usedpassword").val().trim();
        var _pwd1 = $(".Js_newpassword_1").val().trim();
        var _pwd2 = $(".Js_newpassword_2").val().trim();
        if (_pwd1.length < 6) {
            SiteFunc.Alert("密码长度不能小于6位");
            return;
        }
        if (_usedpassword == _pwd1) {
            SiteFunc.Alert("新密码不能与当前密码一致");
            return;
        }
        if (_pwd1 != _pwd2) {
            SiteFunc.Alert("两次密码输入不一致");
            return;
        }


        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetXiouGaiMiMa", { PassWord: _pwd1, UsedPassword: _usedpassword }, function (data) {
            if (data.IsSuccess == 1 && data.ErrMsg=='修改成功') {
                $(".FloatError").show();
                $(".Js_usedpassword").val("");
                $(".Js_newpassword_1").val("");
                $(".Js_newpassword_2").val("");
                setTimeout(function () {
                    $(".FloatError").hide();
                    SiteFunc.GoTo("Login.html");
                },3000);
            }
            else if(data.IsSuccess == 0 && data.ErrMsg=='修改失败'){
            	SiteFunc.Alert("密码修改失败,请稍后再试");
            }
            else {
                SiteFunc.Alert("原密码不正确");
            }
        });
    });
    //返回上一层
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });
    //$(".Js_usedpassword,.Js_newpassword_1,.Js_newpassword_2").on("keyup", function () {
    //    var _pwd = $(this).val().trim();
    //    if (_pwd != "") {
    //        $(this).attr("type", "password");
    //    }
    //    else {
    //        $(this).attr("type", "text");
    //    }
    //});
});
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Cmn.App.CloseWaiting();
});