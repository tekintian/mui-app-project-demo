﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />


//只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

});
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Cmn.App.CloseWaiting();
    //我是保安员注册跳转
    $(".Js_Security").off().click(function () {
        Cmn.App.ShowWaiting("请稍后，程序加载中...");
        SiteFunc.GoTo("Register2.html")
    
    });
    //物业
    $(".Js_Company").off().click(function () {
        Cmn.App.ShowWaiting("请稍后，程序加载中...");
        SiteFunc.GoTo("Register1.html?CompanyType=1")
    });
    //保安公司
    $(".Js_Tenement").off().click(function () {
        Cmn.App.ShowWaiting("请稍后，程序加载中...");
        SiteFunc.GoTo("Register1.html?CompanyType=2")
    });
    //企事业单位
    $(".Js_Enterprise").off().click(function() {
    	Cmn.App.ShowWaiting("请稍后，程序加载中...");
        SiteFunc.GoTo("Register1.html?CompanyType=3")
    });
    
    //返回登录界面
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Login.html");
    });
})
