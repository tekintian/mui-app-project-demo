﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />

Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Collection.FillCollectInfo();
});
Cmn.App.CurPage.OnInit.Add(function (data) {
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });
});
(Collection = new function () {
    var _Self = this;
    //填充收藏信息
    this.FillCollectInfo = function () {
        CmnAjax.FillData(".Js_CollectList", SiteFunc.AjaxUrl + "GetMyCollect", "", function (data) {
            //去到活动列表 全部活动
            $(".Js_NewAction").off().click(function () {
                var _corpID = $(this).attr("corpid");
                SiteFunc.GoTo("Recruitment.html?CorpID=" + _corpID);
            });

            var _flag = false;
            $(".Js_OffCollect").off().click(function () {
                if (_flag) return;
                var _isDelete = $(this).attr("isDelete");
                var _corpID = $(this).attr("corpid");
                if (_isDelete == 0) {
                    $(this).attr("isDelete", "1");
                    _isDelete = 1;
                }
                else {
                    $(this).attr("isDelete", "0");
                    _isDelete = 0;
                }
                _flag = true;
                Collection.UpdateCollectStates(_corpID, _isDelete, function () {
                    _flag = false;
                    _Self.FillCollectInfo();
                });
            });
        });
    }
    //更新收藏状态
    this.UpdateCollectStates = function (corpID,isDelete,callback) {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateCollectInfo", { CorpID: corpID, IsDelete: isDelete }, function (data) {
            callback();
        });
    }
});