﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />
var ActiveParm = {
    ActiveDesc: "",//活动名称
    ActivityTypeID: "",//活动类型ID
    LinkMan: "",//联系人
    LinkPhone: "",//联系人电话
    ProvinceID: "",//省份ID
    CityID: "",//城市ID
    CountyID: "",//区ID
    Address: "",//详细地址
    IsHideAddress: "",//是否隱藏详细地址
    DaySalary: "",//薪资水平
    PaySalaryTime: "",//发薪时间
    RecruitPersonQty: "",//招募人数
    SexRequire: "",//性别要求 1:不限；2:男；3：女
    WorkDateStart: "",//工作日期（开始）
    WorkDateEnd: "",//工作日期（结束）
//  SignDateBegin: "",//报名日期（开始）
//  SignDateEnd: "",//报名时间（结束）
    ActiveDateBegin: "",//活动日期（开始）
    ActiveDateEnd: "",//活动日期（结束）
    WorkTimeStart: "",//工作时间（开始）
    WorkTimeEnd: "",//工作时间（结束）
    HeightRequire: "",//身高要求
    WeightRequire: "",//体重要求
    SecurityStaffCardRequire: "",//上岗证要求 1：不限；2：是；3：否
    ClothesRequire: "",//着装要求 1：无要求；2：全黑西服；3:黑白配；4：保安制服
    ArrivalTimeRequire: "",//到场时间 1：提前半小时；2：提前1小时；3：提前2小时；
    OtherRequire: ""//其他要求
}
//计算出来的天数变量
var compute="";
var ActivityCategory="";//活动类型
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    ActivityRelease2.Step = 1;
    ActivityRelease2.ShowStep();
    $("input").val("");
    $("select").val("").change();
    var d = new Date();
    $(".Js_date").val(d.getFormattedString());
    $(".Js_StartWorkTime").val("08:00");
    $(".Js_EndWorkTime").val("18:00");
    ActiveParm.ActivityTypeID = data.ActivityTypeID;
    //填充活动类型
    $(".Js_ActivitType").val(data.ActivityTypeName);
    ActivityCategory=data.ActivityCategory;
});
Cmn.App.CurPage.OnInit.Add(function (data) {
    //填充省份
    ActivityRelease2.FillProvince();
    //第一步
    $(".Js_StepSub1").click(function () {
        var _money = $(".Js_Money").val().trim();
        var _moneytime=$(".Js_MoneyTime").val().trim();
        var _people = $(".Js_People").val();
        var _sex = $(".Js_Sex").val();
        var _startDate = $(".Js_StartTime").val();
        var _endDate = $(".Js_EndTime").val();
        if (_money == "") { SiteFunc.Alert("请输入薪资水平"); return; }
        if(!SiteFunc.CheckNum(_moneytime)) {SiteFunc.Alert("请输入发薪时间"); return;}
        if (_people == "") { SiteFunc.Alert("请输入招募人数"); return; }
        //if (_startDate == "") { SiteFunc.Alert("请选择工作开始时间"); return; }
        //if (_endDate == "") { SiteFunc.Alert("请选择工作结束时间"); return; }
        //if (ActivityRelease2.GetTime(_startDate) > ActivityRelease2.GetTime(_endDate))
        //{ SiteFunc.Alert("项目开始时间不能大于项目结束时间"); return; }
        ActiveParm.DaySalary = _money; 
        ActiveParm.PaySalaryTime=_moneytime;
        ActiveParm.RecruitPersonQty = _people;
        ActiveParm.SexRequire = _sex;
        ActiveParm.WorkDateStart = _startDate;
        ActiveParm.WorkDateEnd = _endDate;
        ActivityRelease2.Step = 2;
        ActivityRelease2.ShowStep();
    });
    //第二步
    $(".Js_StepSub2").click(function () {
        var _signStartTime = $(".Js_ApplyStartTime").val();
        var _signEndTime = $(".Js_ApplyEndTime").val();
        var _activeStartTime = $(".Js_ActivityStartTime").val();
        var _activeEndTime = $(".Js_ActivityEndTime").val();
        var _workStartTime = $(".Js_StartWorkTime").val();
        var _workEndTime = $(".Js_EndWorkTime").val();
//      if (_signStartTime == "") { SiteFunc.Alert("请选择报名开始时间"); return; }
//      if (_signEndTime == "") { SiteFunc.Alert("请选择报名结束时间"); return; }
//      if (ActivityRelease2.GetTime(_signStartTime) > ActivityRelease2.GetTime(_signEndTime))
//      { SiteFunc.Alert("项目报名开始时间不能大于项目报名结束时间"); return; }
        compute=ActivityRelease2.DateDiff(_activeEndTime,_activeStartTime);
        if (_activeStartTime == "") { SiteFunc.Alert("请选择项目开始时间"); return; }
        if (_activeEndTime == "") { SiteFunc.Alert("请选择项目结束时间"); return; }
        if (ActivityRelease2.GetTime(_activeStartTime) > ActivityRelease2.GetTime(_activeEndTime))
        { SiteFunc.Alert("项目开始时间不能大于项目结束时间"); return; }

        if (_workStartTime == "") { SiteFunc.Alert("请选择工作开始时间"); return; }
        if (_workEndTime == "") { SiteFunc.Alert("请选择工作结束时间"); return; }
//      if (ActivityRelease2.GetTime("2015/11/11 "+_workStartTime) > ActivityRelease2.GetTime("2015/11/11 "+_workEndTime))
//      { SiteFunc.Alert("工作时间不能跨24点"); return; }
////      alert(Math.abs(ActivityRelease2.GetHour("2015/11/11 " + _workStartTime) - ActivityRelease2.GetHour("2015/11/11 " + _workEndTime)))
//      if (Math.abs((ActivityRelease2.GetHour("2015/11/11 " + _workStartTime) - ActivityRelease2.GetHour("2015/11/11 " + _workEndTime))) > 14)
//      { SiteFunc.Alert("工作时间（开始）工作时间（结束）间隔不得大于14小时"); return; }
//      ActiveParm.SignDateBegin = _signStartTime;
//      ActiveParm.SignDateEnd = _signEndTime;
        ActiveParm.ActiveDateBegin = _activeStartTime;
        ActiveParm.ActiveDateEnd = _activeEndTime;
        ActiveParm.WorkTimeStart = _workStartTime;
        ActiveParm.WorkTimeEnd = _workEndTime;
        ActivityRelease2.Step = 3;
        ActivityRelease2.ShowStep();
    });
    //第三步
    $(".Js_StepSub3").click(function () {
        var _activeName = $(".Js_ActiveName").val().trim();
        var _linkName = $(".Js_LinkName").val().trim();
        var _linkPhone = $(".Js_LinkPhone").val().trim();
        var _provinceID = $(".Js_Province").val();
        var _cityID = $(".Js_City").val();
        var _countyID = $(".Js_County").val();
        var _address = $(".Js_Address").val().trim();
        var _IsHideAddress = $(".Js_IsHideAddress").val().trim();
        if (_activeName == "") { SiteFunc.Alert("请输入项目名称"); return; }
        if (_linkName == "") { SiteFunc.Alert("请输入项目联系人姓名"); return; }
        if (_linkPhone == "") { SiteFunc.Alert("请输入项目联系人电话"); return; }
        if (_provinceID == "") { SiteFunc.Alert("请选择项目所在省份"); return; }
        if (_cityID == "") { SiteFunc.Alert("请选择项目所在城市"); return; }
        if (_countyID == "") { SiteFunc.Alert("请选择项目所在区"); return; }
        if (_address == "") { SiteFunc.Alert("请输入具体地址"); return; }
        ActiveParm.ActiveDesc = _activeName;
        ActiveParm.LinkMan = _linkName;
        ActiveParm.LinkPhone = _linkPhone;
        ActiveParm.ProvinceID = _provinceID;
        ActiveParm.CityID = _cityID;
        ActiveParm.CountyID = _countyID;
        ActiveParm.Address = _address;
        ActiveParm.IsHideAddress = _IsHideAddress;
        ActivityRelease2.Step = 4;
        ActivityRelease2.ShowStep();

    });
    //第4步
    $(".Js_StepSub4").click(function () {
        var _height = $(".Js_Height").val().trim();
        var _weight = $(".Js_Weight").val().trim();
        var _workLicense = $(".Js_WorkLicense").val();
        var _cloth = $(".Js_NeedCloth").val();
        var _arriveTime = $(".Js_ArriveTime").val();
        var _other = $(".Js_Other").val().trim();
        //if (_height == "") { SiteFunc.Alert("请输入身高要求"); return; }
        if (_weight == "") { SiteFunc.Alert("请输入体重要求"); return; }
        if (_workLicense == "") { SiteFunc.Alert("请选择是否需要上岗证"); return; }
        ActiveParm.HeightRequire = _height;
        ActiveParm.WeightRequire = _weight;
        ActiveParm.SecurityStaffCardRequire = _workLicense;
        ActiveParm.ClothesRequire = _cloth;
        ActiveParm.ArrivalTimeRequire = _arriveTime;
        ActiveParm.OtherRequire = _other;
        CmnAjax.PostData(SiteFunc.AjaxUrl + "AddActivity", ActiveParm, function (data) {
//          if (data.IsSuccess == 1) {
//              $(".Js_SuccessWarp").show();
//              setTimeout(function () {
//                  $(".Js_SuccessWarp").hide();
//                  SiteFunc.GoTo("Main.html");
//              }, 3000)
//          }
//          else {
//              SiteFunc.Alert("项目发布失败，请稍后再试");
//          }
            if (data.IsSuccess == 1) {
                    SiteFunc.GoTo("Servicefee.html?ActivityCategory="+ActivityCategory+"&projectTS="+compute+"&ActivityID="+data.ID+"&orderID="+data.ONum+"&projectName="+ActiveParm.ActiveDesc+"&projectZMRS="+ActiveParm.RecruitPersonQty);
            }
            else {
                SiteFunc.Alert("项目发布失败，请稍后再试");
                SiteFunc.GoTo("home.html");
            }
        })
    });

    //返回
    $(".Js_Back").off().click(function () {
        if (ActivityRelease2.Step == 1) {
            SiteFunc.GoTo("Main.html?Menu=ActivityRelease.html");
        }
        else if (ActivityRelease2.Step == 2) {
            ActivityRelease2.Step = 1;
            ActivityRelease2.ShowStep();
        }
        else if (ActivityRelease2.Step == 3) {
            ActivityRelease2.Step = 2;
            ActivityRelease2.ShowStep();
        }
        else if (ActivityRelease2.Step == 4) {
            ActivityRelease2.Step = 3;
            ActivityRelease2.ShowStep();
        }
    });
});

(ActivityRelease2 = new function () {
    var _Self = this;
    this.Step = 1;
    //显示对应步骤
    this.ShowStep = function () {
        $(".mui-input-group").hide();
        $(".mui-input-group").eq(_Self.Step - 1).show();
    }

    //填充省份
    this.FillProvince = function () {
        var _html = "<option value='-1'>请选择省份</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetProvince", "", function (data) {
            console.log(JSON.stringify(data));
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["ProvinceID"] + "'>" + data.data[i]["ProvinceDesc"] + "</option>";
                }
            }
            $(".Js_Province").html(_html);
            $(".Js_Province").off().change(function () {
                _Self.FillCity($(this).val())
            });
        });
    }
    //填充城市
    this.FillCity = function (provinceID) {
        var _html = "<option value='-1'>请选择城市</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", { ProvinceID: provinceID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                }
            }
            $(".Js_City").html(_html);
            $(".Js_City").off().change(function () {
                _Self.FillCounty($(this).val())
            });
        });
    }
    //填充区
    this.FillCounty = function (cityID) {
        var _html = "<option value='-1'>请选择区</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetCounty", { CityID: cityID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CountryID"] + "'>" + data.data[i]["CountryDesc"] + "</option>";
                }
            }
            $(".Js_County").html(_html);
        });
    }
    //获取时间
    this.GetTime = function (date) {
        var _d = new Date(date);
        return _d.getTime();
    }
    this.GetHour = function (date) {
        var _time = new Date(date);
//      alert(_time.getHours())
        return _time.getHours();
    }
    //计算天数
    this.DateDiff=function(sDate1,sDate2){
    var  aDate,  oDate1,  oDate2,  iDays;
       aDate  =  sDate1.split("-");
       oDate1  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0]); //转换为12-18-2006格式  
       aDate  =  sDate2.split("-");  
       oDate2  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0]);  
       iDays  =  parseInt(Math.abs(oDate1  -  oDate2)  /  1000  /  60  /  60  /24); //把相差的毫秒数转换为天数  
       return  iDays+1;  
    }
});