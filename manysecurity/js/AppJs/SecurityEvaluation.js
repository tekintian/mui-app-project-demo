﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

});

var _ActivityApplyID = "";
var _HowMany = "1";
var _ActivityID = "";
//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {

    //上一步
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("MyRelease.html?Release=1");
    });
  
    _ActivityID = (data.ActivityID == undefined ? "" : data.ActivityID);

    FillEvaluate(_ActivityID);

    //点击已评
    $(".Js_YiPingAnNou").off().on("tap", function () {
        $(".Js_YiPing").show();
        $(".Js_WeiPing").hide();
    });
    //点击未评
    $(".Js_WeiPingAnNou").off().on("tap", function () {
        $(".Js_YiPing").hide();
        $(".Js_WeiPing").show();
    });

    //评论
    $(".Js_FuCheng p").off().click(function () {
        _HowMany = $(this).index() + 1;
        $(".mui-Evaluation-Title").each(function () {
            $(this).find("#Radio1 img").eq(0).removeClass("mui-public-btn");
            $(this).find("#Radio1 img").eq(1).addClass("mui-public-btn");
        });
        $(this).find("#Radio1 img").eq(0).addClass("mui-public-btn");
        $(this).find("#Radio1 img").eq(1).removeClass("mui-public-btn");
    });

    //确定
    $(".Js_QueDing").off().on("touchstart", function () {
        var _parm = {
            "ActivityApplyID": _ActivityApplyID,
            "CorpEvaluateOptionID": _HowMany,
            "CropID": SiteFunc.GetCorpID()
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateApplyStatusPingJia", _parm, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.Alert("评价成功！");
                FillEvaluate(_ActivityID);
                $(".Js_PingJiafucheng").hide();
                $(this).find("#Radio1 img").eq(0).addClass("mui-public-btn");
        		$(this).find("#Radio1 img").eq(1).removeClass("mui-public-btn");
            } else {
                SiteFunc.Alert("评价失败！");
            }
        });
    });
    //取消
    $(".Js_QuXiao").off().on("touchstart", function () {
        $(".Js_PingJiafucheng").hide();
    });
});
FillEvaluate = function (activityID) {
    //已评
    CmnAjax.PostData(SiteFunc.AjaxUrl + "GetToYiPingUpList", { "ActivityID": activityID }, function (data) {
        var _has = "", not = "";
         console.log(JSON.stringify(data));
        if (data.data.length > 0) {
            for (var i = 0; i < data.data.length; i++) {
                if (data.data[i]["HasEvaluate"] != "") {
                    _has += "<p class=\"mui-Recuitment-action\">" +
                            "<span class=\"mui-Safety-Name\">" + data.data[i]["UserName"] + "</span>" +
                            "<span class=\"mui-Safety-with\">获得<b>" + data.data[i]["GoodCount"] + "</b>次好保安</span><br />" +
                            "<span class=\"mui-Safety-Title\">" + data.data[i]["HasEvaluate"] + "</span>" +
                            "</p>";
                }
                else {
                    not += "<p class=\"mui-Safe-Unrated\">" +
                            "<span class=\"mui-Safety-Name dat-UserName\">" + data.data[i]["UserName"] + "</span>" +
                            "<span class=\"mui-Safety-with\">获得<b>" + data.data[i]["GoodCount"] + "</b>次好保安</span><br />" +
                            "<button class=\"mui-btn  Js_PingJia dat-UserID-userid \" activityapplyid='" + data.data[i]["ActivityApplyID"] + "' id=\"mui-stateBtn\" type=\"button\">评价" +
                            "</button>" +
                            "</p>";
                }
            }
            $(".Js_YiPing").html(_has);
            $(".Js_WeiPing").html(not);
            //评价
            $(".Js_PingJia").off().on("tap", function () {
                _ActivityApplyID = $(this).attr("activityapplyid");
                $(".Js_PingJiafucheng").show();
            });
        }
    });
}
