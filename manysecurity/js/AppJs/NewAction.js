﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../jquery.cycle.all.js" />
//生成 只执行一次
var activ_type=""; //活动类型 1为短期 2为长期



Cmn.App.CurPage.OnInit.Add(function (data) {
    NewAction.Init();
    //获取意向城市ID
    NewAction.CityID = SiteFunc.GetIntentionOpenCityID();
    if (!NewAction.CityID || NewAction.CityID == "") {
        NewAction.CityID = 3;
    }
    //填充OpenCity
    NewAction.FillOpenCity(NewAction.CityID, function () {
        //填充活动list
        NewAction.BindActiveList(NewAction.CityID);
        NewAction.HasLoadMap = true;
    });
});
//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    //自动登录
    NewAction.IsLogin = SiteFunc.CheckLoginStatus();
    if(!NewAction.HasLoadMap) {
        NewAction.CityID = 3;
    }
    else {
        NewAction.BindActiveList(NewAction.CityID);
    }
    $(".Js_ActiveBox").empty();
});
(NewAction = new function () {
    var _Self = this;
    this.IsLogin = false;
    this.CityID = "";
    this.HasLoadMap = false;
    this.PageIndex="";
    this.PageCount ="";
    var item=1;
    //初始化
    this.Init = function () {
        _Self.GetAdvertisement();
        
        //检查更新
        silentCheckUpdate();

        //选择城市
        $(".Js_ChoseCity").click(function () {
            var _css = $(".Js_OpenListWarp").css("display");
            if (_css == "none") {
                $(".Js_OpenListWarp").fadeIn(300);
            }
            else {
                $(".Js_OpenListWarp").fadeOut(300);
            }
        });
        $(".Js_OpenListWarp").click(function () {
            $(this).fadeOut(300);
        });

        //全部活动
        $(".Js_AllActivity").click(function () {
            SiteFunc.GoTo("Recruitment.html?From=NewAction.html&CityID=" + _Self.CityID);
        });
        
        
//      $(window).bind("touchmove", function () {
//	        //当滚动条滚动时
//	        var top = $(".mui-input-group").scrollTop(); //滚动条的高度
//	        var height = $(".Js_ActiveBox").height(); //窗口文本高度
//	        var windowH = $(window).height(); //可视高度
//	        if(NewAction.pageIndex!=NewAction.PageCount){
//	        	if (top >= height - windowH) {
//		        	if(!window.isposting) {
//		        		window.isposting = true;
//		        		item = item + 1;
//		        		Cmn.App.ShowWaiting("请稍后，程序加载中...");
//		        		NewAction.BindActiveList(NewAction.CityID,item);
//		        	}
//	        	}
//	        }
//	        else{
//	        	Cmn.App.SystemToast("没有更多数据可加载！");
////	        	SiteFunc.Alert("没有更多数据可加载");
//	        }
//      })

    }
    //获取宣传栏图片
    this.GetAdvertisement = function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetAdvertisementByPart", { "AdvKey": "ActivePoster" }, function (data) {
            var _advPicList = "";
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _advPicList += "<img src='" + SiteFunc.UploadImgUrl + data.data[i].PicPath + "' onclick='" + data.data[i].AdvUrl + "'>";
                }
            }
            $(".Js_CyclePic").html(_advPicList);
            $(".Js_CyclePic").cycle({
                fx: 'scrollHorz',
                pager: $(".Carousel"),
                activePagerClass: 'Carouselbg'
            });
            Cmn.Func.TouchSlide(".Js_CyclePic", 10, function (dir) {
                if (dir == "left") {
                    $(".Js_CyclePic").cycle("next")
                }
                else if (dir == "right") {
                    $(".Js_CyclePic").cycle("prev");
                }
            });
        });
    }

    //绑定活动列表信息
    this.BindActiveList = function (cityID,item) {
        var parm = {
            CityID: (cityID == undefined ? "3" : cityID),
            PageIndex: (item == undefined ? "1" : item),
        }
        //var _activeList = new CmnAjax.DataStepLoad(".Js_ActiveList", SiteFunc.AjaxUrl + "GetActivityList", parm, "", 200, 10, function (data) { }, function () { }, "",
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetActivityList", parm, function (data) {
            $("body").css("opacity", 1);
            Cmn.App.CloseWaiting();

            var _html = $(".Js_ActiveBox").html();
            var a="";var b="";var c="";var d="";
            var data = data.data;
            if (data.length > 0) {
                for (var _i = 0; _i < data.length; _i++) {
                    _html += '<div class="mui-input-row Recr-Content Js_ActiveList main-content ' + (data[_i]['Status'] == 1 ? '' : 'gray') + '" id="mui-NewAction-Bgcolor">' ;
                    activ_type=data[_i]['ActivityCategory'];
                    if(data[_i]['Status'] != 1) {
                        _html += '<div class="stamp"></div>';
                    }
                    if(data[_i]['ActivityCategory']==1)
                    {
                    	a=data[_i]["WorkTime"];
                    	b="企业名称：";
                    	c=data[_i]["CompanyName"];
                    	d=data[_i]["DaySalary"];
                    }
                    else
                    {
                    	a=data[_i]["WorkTime"];
                    	b="其他福利：";
                    	c=data[_i]["OtherRequire"];
                    	d=data[_i]["MonthSalary"]+"元/月";
                    }
                    _html += '<p class="mui-Recuitment-action Js_ActivityDtl" opertype='+data[_i]["IsRecommend"]+' activ_type="'+activ_type+'" activityID="' + data[_i]["ActivityID"] + '" status="' + data[_i]["Status"] + '">' + TipImg(data[_i]["ActivityTypeID"], data[_i]["IsRecommend"]) +
                                '<span class="mui-NewAction-Action Js_ActiveTilte" style="margin-left: 0.5em;">' + data[_i]["ActiveDesc"] + '</span>' +
                                '<span class="mui-monuday Js_ActiveMoney">' + d+ '</span>' +
                                '</p>' +
                                '<p class="mui-Recuitment-time"><span>工作时间：</span><span style="color: #eca825;" class="Js_ActiveWorkTime dat-WorkTime">' + a + '</span></p>' +
                                '<p class="mui-Recuitment-address"><span>项目地址：</span><span style="color: #ccc7c7;" class="Js_ActiveAddress ">' + data[_i]["ProvinceDesc"] + data[_i]["CityDesc"] +data[_i]["CountryDesc"] + '</span></p>'+
                                '<p class="mui-Recuitment-address"><span>'+b+'</span><span style="color:#ccc7c7;" class="Js_Company dat-CompanyName">' + c+ '</span></p>' +
                                '<div style="margin-top:15px;">' +
                                '<label id="mui-rec-Number">计划招募<span class="Js_Plan">' + data[_i]["RecruitPersonQty"] + '</span>人</label>' +
                                '<label id="mui-rec-Number2">已报名<span class="Js_Real">' + data[_i]["ApplyPersonQty"] + '</span>人</label>' +
                                '<label id="mui-rec-Number3">已录取<span class="Js_Admission">' + data[_i]["RecruitedPersonQty"] + '</span>人</label>' +
                                '</div>' +
                                '<div class="mui-input-row Recr-Content"></div></div>'

                }
                NewAction.pageIndex=data[0]["CurrentPage"];
                NewAction.PageCount = data[0]["TotalPage"];
            }
            $(".Js_ActiveBox").html(_html);
            Cmn.App.CloseWaiting();
            //去到活动详情页
            $(".Js_ActivityDtl").off().click(function () {
                var _activityID = $(this).attr("activityID");
                var _status = $(this).attr('status');
                if($(this).attr("activ_type")=="1")
                {
                	SiteFunc.GoTo("ActionRement.html?activ_type="+activ_type+"&ActivityID=" + _activityID+"&Status="+ _status + "&From=NewAction.html");
                }
                else{
                	SiteFunc.GoTo("ActionRement2.html?activ_type="+activ_type+"&ActivityID=" + _activityID+"&Status="+ _status + "&From=NewAction.html");
                }
            });
            //立即申请
            $(".Js_ApplyNow").off().click(function () {
                var _activityID = $(this).attr("activityID");
                if (!SiteFunc.CheckLoginStatus()) {
                    SiteFunc.Alert("登录后方可申请项目");
                    return;
                }
                if (SiteFunc.GetCorpID() != "") {
                    SiteFunc.Alert("甲方不能申请项目");
                    return;
                }
                if (!_Self.IsLogin) {
                    $(".Js_LoginTip").fadeIn(300).find(".Js_Tip").text("登录后方可申请项目！");
                    $(".Js_GotoLogin").text("前往");
                    $(".Js_Off").text("取消");
                    $(".Js_GotoLogin").off().click(function () {
                        SiteFunc.GoTo("Login.html?From=NewAction.html");
                        $(".Js_LoginTip").hide();
                    });
                    return; 
                }

                $(".Js_LoginTip").fadeIn(300).find(".Js_Tip").text("您确定要提交报名申请吗？");
                $(".Js_GotoLogin").text("是");
                $(".Js_Off").text("否");

                $(".Js_GotoLogin").off().click(function () {
                    CmnAjax.PostData(SiteFunc.AjaxUrl + "AddApplyRec", { ActivityID: _activityID }, function (data) {
                        if (data.IsSuccess == 1) {
                            SiteFunc.Alert("报名申请提交成功，报名审核结果会及时通知，请随时关注");
                            $(".Js_LoginTip").hide();
                            NewAction.BindActiveList(NewAction.CityID);
                            //setTimeout(function () {
                            //    SiteFunc.GoTo("NewAction.html");
                            //}, 3000);
                        }
                        else {
                            if (data.ErrMsgID == 1) {
                                $(".Js_LoginTip").fadeIn(300).find(".Js_Tip").text("登录后方可申请项目！");
                                $(".Js_GotoLogin").text("前往");
                                $(".Js_Off").text("取消");
                                $(".Js_GotoLogin").off().click(function () {
                                    $(".Js_LoginTip").hide();
                                    SiteFunc.GoTo("Login.html");
                                });
                            }
                            else if (data.ErrMsgID == 2) {
                                SiteFunc.Alert("距您上一个报名项目不足3小时，请适度选择！");
                            }
                            else if (data.ErrMsgID == 3) {
                                SiteFunc.Alert("报名申请提交成功，报名审核结果会及时通知，请随时关注");
                                setTimeout(function () {
                                    $(".Js_LoginTip").hide();
                                    SiteFunc.GoTo("Home.html");
                                }, 3000);
                            }
                            else if (data.ErrMsgID == 4) {
                                SiteFunc.Alert("甲方规定每天工作时间不得超过14小时！");
                            }
                            else if (data.ErrMsgID == 5) {
                                //显示提示浮层
                                $(".Js_LoginTip").fadeIn(300).find(".Js_Tip").text("您需要完善个人信息");
                                $(".Js_GotoLogin").text("前往");
                                $(".Js_Off").text("取消");

                                $(".Js_GotoLogin").off().click(function () {
                                    $(".Js_LoginTip").hide();
                                    SiteFunc.GoTo("ModifyData.html");
                                });
                            }
                        }
                    });
                });
                
                
            });
            //取消
            $(".Js_Off").off().click(function () {
                $(".Js_LoginTip").fadeOut(300);
            });
            //收藏
            $(".Js_Collect").click(function () {
                if (!SiteFunc.CheckLoginStatus()) {
                    SiteFunc.Alert("登录后方可收藏此公司");
                    return;
                }
                if (SiteFunc.GetCorpID() != "") {
                    SiteFunc.Alert("甲方不能收藏此公司");
                    return;
                }
                CmnAjax.PostData(SiteFunc.AjaxUrl + "AddCollectRec", { ActivityID: $(this).attr("activityID") }, function (data) {
                    if (data.IsSuccess == 1) {
                        _Self.BindActiveList(NewAction.CityID);
                        SiteFunc.Alert("收藏成功");

                    }
                    else {
                        SiteFunc.Alert(data.ErrMsg);
                    }
                });
            });
            window.isposting=false;
        });
        //_activeList.BindScrollLoadData(".Js_ActiveList");
    }
    //是否推荐，或驻/临
    function TipImg(type, isRec) {
        var html = "";
        if (type == 1) {
            html += '<span ><img src="images/Action/picture-xiang.png" /></span>';
        }
        else if (type == 2) {
            html += '<span ><img src="images/Action/picture-zhu.png" /></span>';
        }
        else if (type==3)
        {
        	html += '<span ><img src="images/Action/picture-zhi.png" /></span>';
        }
        else if (type==4)
        {
        	html += '<span ><img src="images/Action/picture-pai.png" /></span>';
        }
        if (isRec == "True") {
            html += '<span ><img src="images/Action/RecommendIcon.png" style="width: 49px; height: 38px;" /></span>';
        }
        else
        {
        	
        }
        return html;
    }
    //工作时间
    function gzsj(type)
    {
    	if(type=="1")
    	{
    		return '+data[_i]["WorkTimeDesc"]+';
    		
    	}
    	else
    	{
    		return '+data[_i]["WorkTime"]+';
    	}
    }
    this.FillOpenCity = function (cityID, callback) {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", "", function (data) {
            var _cityList = "";
            if (data.data.length > 1) {
                for (var i = 0; i < data.data.length; i++) {
                    _cityList += "<span CityID='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</span>";
                    if (cityID == data.data[i]["CityID"]) {
                        $(".Js_ChoseCity span:eq(0)").text(data.data[i]["CityDesc"]);
                        $(".Js_CityTip").text(data.data[i]["CityDesc"]);
                    }
                }
            }
            $(".Js_OpenCityList").html(_cityList);
            $(".Js_OpenCityList span").click(function () {
                var _curCityName = $(this).text();
                $(".Js_ChoseCity span").eq(0).text(_curCityName);
                $('.Js_CityTip').text(_curCityName);
                _Self.CityID = $(this).attr("CityID");
                _Self.BindActiveList(_Self.CityID);
            });
            callback && callback();
        });
    }
    //填充已开通城市列表
    //this.FillOpenCity = function (cityName, callback) {
    //    CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", "", function (data) {
    //        var _cityList = "";
    //        if (data.data.length > 1) {
    //            for (var i = 0; i < data.data.length; i++) {
    //                _cityList += "<span CityID='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</span>";
    //                if (cityName == data.data[i]["CityDesc"]) {
    //                    NewAction.CityID = data.data[i]["CityID"];
    //                }
    //            }
    //        }
    //        $(".Js_OpenCityList").html(_cityList);
    //        $(".Js_OpenCityList span").click(function () {
    //            var _curCityName = $(this).text();
    //            $(".Js_ChoseCity span").eq(0).text(_curCityName);
    //            _Self.CityID = $(this).attr("CityID");
    //            _Self.BindActiveList(_Self.CityID);
    //        });
    //        callback && callback();
    //    });
    //}
});

//静默检查更新
function silentCheckUpdate(){
	//获取最后一次检查更新时间
	var lastCheckDate = plus.storage.getItem('lastCheckDate');
	var today = (new Date()).getFormattedString();
	if(!lastCheckDate || lastCheckDate != today) {
        var wgtVer = undefined;
        
        plus.runtime.getProperty(plus.runtime.appid, function (inf) {
            wgtVer = inf.version;
        });
        if(wgtVer) {
            SiteFunc.silentCheck(wgtVer);
        }
	}
	plus.storage.setItem('lastCheckDate', today);
}
