﻿/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="../Cmn.js" />
/// <reference path="CmnApp.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../../dist/js/mui.min.js" />

//未登录 我的收藏，我的发布，我的报名，我的评价
//保安 我的收藏，我的报名，我的评价，资料修改，密码修改
//公司 我的收藏，我的评价，资料修改，密码修改
Cmn.App.CurPage.OnInit.Add(function (data) {

    setTimeout(function () { plus.navigator.closeSplashscreen(); }, 1000);
    //退出登录
    $("#Exit").click(function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "ExitLogin", "", function (data) {
            Home.GetHomeInfo();
            SiteFunc.GoTo("Main.html?Menu=Home.html");
            SiteFunc.RememberLoginInfo("", "");
            SiteFunc.SetLoginStatus(false, "", "");
            $(".Js_Publish").show();
            $(".Js_SystemTip").html("0");
            $(".Js_ActiveTip").html("0");
            $(".Js_EvaluateTip").html("0");
            $(".Pubulic_RightPoint").hide();
            //$(".mui-table-view-cell").each(function () {
            //    $(this).find("span").removeAttr("id");
            //});
        });
    });
    //跳转登录界面
    $(".Js_Login").click(function () {
        SiteFunc.GoTo("Login.html?From=Home.html");
    });

    //我的收藏
    $(".Js_MyCollect").click(function () {
        if (!SiteFunc.IsTest) {
            SiteFunc.GoTo("Staytunedfor.html?Back=Home.html");
            return;
        }
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return false;
        }
        SiteFunc.GoTo("Collection.html");
    });
    //我的发布
    $(".Js_MyPublish").click(function () {
        if (!SiteFunc.IsTest) {
            SiteFunc.GoTo("Staytunedfor.html?Back=Home.html");
            return;
        }
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        SiteFunc.GoTo("MyRelease.html");
    });
    //我的报名
    $(".Js_MyApply").click(function () {
        if (!SiteFunc.IsTest) {
            SiteFunc.GoTo("Staytunedfor.html?Back=Home.html");
            return;
        }
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        SiteFunc.GoTo("MySign.html");
    });
    //我的评价
    $(".Js_MyEvaluate").click(function () {
        if (!SiteFunc.IsTest) {
            SiteFunc.GoTo("Staytunedfor.html?Back=Home.html");
            return;
        }
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        if (Home.IsCompany) {
            //公司
            SiteFunc.GoTo("MyEvaluation2.html");
        }
        else {
            //保安
            SiteFunc.GoTo("MyEvaluation.html");
        }
    });
    //资料修改
    $(".Js_UpdateInfo").click(function () {
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        if (Home.IsCompany) {
            //公司
            SiteFunc.GoTo("ModifyData2.html");
            Cmn.App.ShowWaiting("请稍后，程序加载中...");
        }
        else {
            //保安
            SiteFunc.GoTo("ModifyData.html");
            Cmn.App.ShowWaiting("请稍后，程序加载中...");
        }
    });
    //密码修改
    $(".Js_ChangePwd").click(function () {
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        SiteFunc.GoTo("ChangePassword2.html");
        Cmn.App.ShowWaiting("请稍后，程序加载中...");
    });

    //系统提示条数
    $(".Js_System").click(function () {
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        SiteFunc.GoTo("ActionInfo.html?MessageType=1")
    });
    //报名条数
    $(".Js_Apply").click(function () {
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        SiteFunc.GoTo("ActionInfo.html?MessageType=2")
    });
    //评价条数
    $(".Js_Evaluate").click(function () {
        if (!Home.HasLogin) {
            SiteFunc.Alert("您还未登录，请先登录！");
            return;
        }
        SiteFunc.GoTo("ActionInfo.html?MessageType=3")
    });
});
//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    $("body").css("opacity", 0);
    Home.GetHomeInfo();
});
(Home = new function () {
    this.IsCompany = false;
    var _Self = this;
    this.HasLogin = false;
    this.GetHomeInfo = function () {
        $(".Pubulic_RightPoint").hide();
        //获取个人信息
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetHomeTip", "", function (data) {
            $(".mui-table-view-cell").hide();
            //保安认证0未认证 1已经认证
            setTimeout(function () {
                $("body").css("opacity", 1);
                Cmn.App.CloseWaiting();
            }, 50);
            var _data = data.data;
            if (_data.length == 0) {
                $(".Js_Person,.Js_Company").hide();
                $(".Js_NoLogin").show();
                _Self.IsCompany = false;
                _Self.HasLogin = false;
                return;
            }
            if(_data[0]["IsApprove"]!="")
            {
            	if(data.data[0]["IsApprove"]=="True")
	            {
	            	$("#cxba_img").show();
	            }
	            else
	            {
	            	$("#cxba_img").hide();
	            }
            }
            else
            {
            	$("#cxba_img").hide();
            }
            if (_data[0]["CorpID"] != "") {
                $(".Js_Name").text(_data[0]["CompanyName"]);
                $(".Js_Head").attr("src", _data[0]["CompanyLogo"] == "" ? "images/Action/photoIcon.png" : SiteFunc.UploadImgUrl + _data[0]["CompanyLogo"]);
            }
            else {
                $(".Js_Name").text(_data[0]["UserName"]);
                $(".Js_Head").attr("src", _data[0]["HeadImage"] == "" ? "images/Action/photoIcon.png" : SiteFunc.UploadImgUrl + _data[0]["HeadImage"]);
            }
            $(".Js_SystemTip").html(_data[0]["SystemNews"] == "" ? "0" : _data[0]["SystemNews"]);
            $(".Js_ActiveTip").html(_data[0]["ActiveNews"] == "" ? "0" : _data[0]["ActiveNews"]);
            $(".Js_EvaluateTip").html(_data[0]["EvaluateNews"] == "" ? "0" : _data[0]["EvaluateNews"]);
            if (_data[0].HasNewCollect == "True") {
                $(".Js_MyCollect").find(".Pubulic_RightPoint").show();//.attr("id", "mui-badge");
            }
            if (_data[0].HasNewPublish == "True") {
                $(".Js_MyPublish").find(".Pubulic_RightPoint").show();//.find("span").attr("id", "mui-badge");
            }
            if (_data[0].HasNewRegist == "True") {
                $(".Js_MyApply").find(".Pubulic_RightPoint").show();//.find("span").attr("id", "mui-badge");
            }
            if (_data[0].HasNewEvaluate == "True") {
                $(".Js_MyEvaluate").find(".Pubulic_RightPoint").show();//.find("span").attr("id", "mui-badge");
            }
            if (_data[0]["CorpID"] != "") {
                //查询公司信息
                $(".Js_Login").hide();
                $(".Js_Company").show();
                _Self.IsCompany = true;
                _Self.HasLogin = true;
            }
            else if (_data[0]["CorpID"] == "" && _data.length > 0) {
                //查询保安信息
                $(".Js_Person").show();
                $(".Js_Login").hide();

                _Self.IsCompany = false;
                _Self.HasLogin = true;
            }
            else {
                //未登录
                $(".Js_Person,.Js_Company").hide();
                $(".Js_NoLogin").show();
                _Self.IsCompany = false;
                _Self.HasLogin = false;
            }
            
        });
    }
});
