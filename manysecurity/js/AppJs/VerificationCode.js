﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
var _Phone = "";
var _AjaxUrl = SiteFunc.AjaxUrl;//接口

Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Cmn.App.CloseWaiting();
    _Phone = data.Phone;
    VerificationCode.SendSms(_Phone);
});
//CheckSMSCode
Cmn.App.CurPage.OnInit.Add(function (data) {
    //下一步
    $(".Js_Next").click(function () {
        var _code = $(".Js_Code").val()
        if (_code == "") {
            SiteFunc.Alert("请输入验证码");
            return;
        }
        CmnAjax.PostData(_AjaxUrl + "CheckSMSCode", { Code: _code }, function (data) {
            if (data.IsSuccess == 1) {
                SiteFunc.GoTo("ChangePassword.html");
            }
            else {
                SiteFunc.Alert("验证码错误");
            }
        });
    });
    //发送短信
    $(".Js_SendSms").click(function () {
        VerificationCode.SendSms(_Phone);
    });
    //返回上一步
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("ForgetPassword.html");
    });
});
(VerificationCode = new function () { 
    var _Self = this;
    //是否可以发送短信
    this.CanSendSms = true;
    //倒计时
    this.CutDown = function () {
        var _time = 60;
        var _countdownTimer = setInterval(function () {
            $(".Js_TimeOut").html("("+(--_time) + "s)");
            if (_time == 0) {
                clearInterval(_countdownTimer);
                _Self.CanSendSms = true;
                $(".Js_SendSms").removeAttr("style");
                $(".Js_SendSms").find("span").hide();
            }
        }, 1000);
    }
    //发送短信
    this.SendSms = function (phone) {
        if (!_Self.CanSendSms) { return; }
        
        CmnAjax.PostData(_AjaxUrl + "SendSms", { Phone: phone, UseTo: 1 }, function (data) {
            if (data.IsSuccess == 1) {
                _Self.CanSendSms = false;
                _Self.CutDown();
                $(".Js_SendSms").css({ "border": "2px solid #A6A6A6", "color": "#989898" });
                $(".Js_SendSms").find("span").show();
            }
            else {
                SiteFunc.Alert(data.ErrMsg);
            }
        });
    }
});
