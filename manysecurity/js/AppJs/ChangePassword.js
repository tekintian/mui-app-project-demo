﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="../CmnAjax.js" />

Cmn.App.CurPage.OnInit.Add(function (data) {
    //提交
    $(".Js_Sub").click(function () {
        var _pwd1 = $(".Js_Pwd1").val().trim();
        var _pwd2 = $(".Js_Pwd2").val().trim();
        if (_pwd1.length < 6) {
            SiteFunc.Alert("密码长度不能小于6位");
            return;
        }
        if (_pwd1 != _pwd2) {
            SiteFunc.Alert("两次密码输入不一致");
            return;
        }
        CmnAjax.PostData(SiteFunc.AjaxUrl + "ResetPassWord", { PassWord: _pwd1 }, function (data) {
            if (data.IsSuccess == 1) {
                $(".FloatError").show();
                setTimeout(function () {
                    $(".FloatError").hide();
                    SiteFunc.GoTo("Login.html");
                },3000);
            }
            else {
                SiteFunc.Alert("密码修改失败,请稍后再试");
            }
        });
    });
    //返回上一层
    $(".Js_Back").off().click(function () {
        SiteFunc.Back();
    });
    //$(".Js_Pwd1,.Js_Pwd2").on("keyup", function () {
    //    var _length = $(this).val().trim().length;
    //    if (_length > 0) {
    //        $(this).attr("type", "password");
    //    }
    //    else {
    //        $(this).attr("type", "text");
    //    }
    //})
});
