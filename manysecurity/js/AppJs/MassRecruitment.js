﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
/// <reference path="CmnApp.js" />

//生成 只执行一次
Cmn.App.CurPage.OnInit.Add(function (data) {

});


//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    var _Sert = this;
    //招募总数
    var _Total = "";
    //已选
    var _Selected = 0;
    // 可选
    var _Optional = "";
    //已招募
    var _Already = "";
    var _indexOf=false;
    var _activityID = (data.ActivityID == undefined ? "" : data.ActivityID);

    CmnAjax.PostData(SiteFunc.AjaxUrl + "GetZhaoMuRenShuManInfo", { ActivityID: _activityID }, function (data) {
        if (data.data.length == 1) {
            $(".Js_CompanyName").text(data.data[0].CompanyName);
            _Total = data.data[0].RecruitPersonQty;
            _Already= data.data[0].RecruitedPersonQty;
            _Optional = _Total - _Already;
            $(".Js_YiXuan").text(_Selected);
            $(".Js_KeXuan").text(_Optional)

        }
    });

    var _fillToBaoMingUpList = function (param) {
        CmnAjax.FillData(".Js_Check", SiteFunc.AjaxUrl + "GetZhaoMuRenManInfo", param, function (data) {
            if (data.data.length >= 1) {
                //点击确定
                $(".Js_Check").each(function (index,ele) {
                    $(this).attr("check", "0");
                });
                $(".Js_Check").off().on("click", function () {
                    var _ckeck=$(this).attr("check");
                    if (_ckeck == "0") {
                        if (_Optional <= 0) { SiteFunc.Alert("可选人数已满！"); return; }
                        $(this).attr("check", "1");
                        $(this).children().eq(1).removeClass("mui-public-btn");
                        $(this).children().eq(0).addClass("mui-public-btn");
                        _Selected++;
                        _Optional--;
                    }
                    else {
                        $(this).attr("check", "0");
                        $(this).children().eq(0).removeClass("mui-public-btn");
                        $(this).children().eq(1).addClass("mui-public-btn");
                        _Selected--;
                        _Optional++;
                    }
                    $(".Js_YiXuan").text(_Selected);
                    $(".Js_KeXuan").text(_Optional);
         
                });
                $("#mui-Securitails-Content p").each(function(index,ele){
                	if(_Optional>0)
                	{
                	 $(this).trigger("click");	
                	}
                });
            }
        });
    }
        //上一步
        $(".Js_Back").off().click(function () {
            SiteFunc.GoTo("CheckUp.html?ActivityID=" + _activityID);
        });

        _fillToBaoMingUpList({ ActivityID: _activityID });
        //选择批量招募
        $(".Js_TongGuo").off().on("click", function () {
            var _Status = 2;
            var _ActivityApplyID = "";
            $(".Js_Check").each(function () {
                if ($(this).attr("check") == "1") {
                    _ActivityApplyID += $(this).attr("activityapplyid") + "|";
                }
            });
            _ActivityApplyID=_ActivityApplyID.substr(0, _ActivityApplyID.lastIndexOf("|"));
            if (_ActivityApplyID == "") { SiteFunc.Alert("请选择要通过的保安员"); return; }
            CmnAjax.PostData(SiteFunc.AjaxUrl + "UpdateApplyXiouGaiZhuangTaiJia", { ActivityApplyID: _ActivityApplyID, Status: _Status }, function (data) {
                if (data.IsSuccess == 1) {
                    SiteFunc.Alert("招募成功！");
                    _fillToBaoMingUpList({ ActivityID: _activityID });
                    _Selected = 0;
                    _Optional = _Total - _Already;
                    $(".Js_YiXuan").text(_Selected);
                    $(".Js_KeXuan").text(_Optional);
                } else {
                    SiteFunc.Alert("招募失败！");
                }
            });

        });
        //选择查询
        $(".Js_ChaXuen").off().on("click", function () {
            _fillToBaoMingUpList({ ActivityID: _activityID });
            _Selected = 0;
            _Optional = _Total - _Already;
            $(".Js_YiXuan").text(_Selected);
            $(".Js_KeXuan").text(_Optional);
        });


  
});