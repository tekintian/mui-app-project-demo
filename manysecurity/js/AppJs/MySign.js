﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />

Cmn.App.CurPage.BeforeShow.Add(function (data) {
    MySign.FillMyApply("", "", "", "");
});
var _AjaxUrl = SiteFunc.AjaxUrl;
Cmn.App.CurPage.OnInit.Add(function (data) {

    var _html = "";
    //报名状态
    $(".Js_ApplyStatus").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" status="">报名状态</p>';
        _html += '<p class="mui-recruitment-DownList" status="2">已录取</p>';
        _html += '<p class="mui-recruitment-DownList" status="3">未录取</p>';
        _html += '<p class="mui-recruitment-DownList" status="4">招募取消</p>';
        MySign.FillSelect(_html, 1);
    });
    //项目活动类型
    $(".Js_ActiveType").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" type="">项目类型</p>';
        _html += '<p class="mui-recruitment-DownList" type="1">临时</p>';
        _html += '<p class="mui-recruitment-DownList" type="2">驻场</p>';
        MySign.FillSelect(_html, 2);
    });
    //评价状态
    $(".Js_EvaluateStatus").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" evalute="">评价状态</p>';
        _html += '<p class="mui-recruitment-DownList" evalute="0">待评</p>';
        _html += '<p class="mui-recruitment-DownList" evalute="1">已评</p>';
        MySign.FillSelect(_html, 3);
    });

    //查询按钮
    $(".Js_Search").click(function () {
        if ($(".Js_SearchBox").css("display") == "none") {
            $(".Js_SearchBox").show();
        }
        else {
            $(".Js_SearchBox").hide();
        }
    });
    //关键字搜索
    $(".Js_SearchBtn").click(function () {
        var _word = $(this).prev().val();
        if (_word == "") {
            SiteFunc.Alert("请输入关键字");
            return;
        }
        $(".Js_SearchBox").hide();
        MySign.FillMyApply("", "", "", _word);
    });
    //评价
    var _evaluateID = 1;
    var _evaluateText = "付款准时";
    $(".Js_EvaluateList").click(function () {
        $(".Js_EvaluateList").find("#Radio1").each(function () {
            $(this).find("img").eq(0).removeClass("mui-public-btn");
            $(this).find("img").eq(1).addClass("mui-public-btn");
        });
        $(this).find("#Radio1 img").eq(0).addClass("mui-public-btn");
        $(this).find("#Radio1 img").eq(1).removeClass("mui-public-btn");
        _evaluateID = $(this).attr("evaluatid");
        _evaluateText = $(this).text().trim();
    });
    //确认
    $(".Js_Sub").click(function () {
        //评价
        //alert(_evaluateID + _evaluateText);
        CmnAjax.PostData(_AjaxUrl + "UpdateEvaluateForCorp", { ActivityApplyID: MySign.ActivityApplyID, EvaluateOptionID: _evaluateID }, function (data) {
            if (data.IsSuccess == 1) {
                $(".Js_EvaluateWarp").hide();//隐藏评论浮层
                if(window.activateOperationButton){
					window.activateOperationButton.off();//取消事件
					window.activateOperationButton.text(_evaluateText);//替换文案
                }
                SiteFunc.Alert("评价成功");
            }
            else {
//              alert(JSON.stringify(data));
                SiteFunc.Alert("评价失败，请稍后再试");
            }
        });
    });
    //取消
    $(".Js_Off").click(function () {
        $(".Js_EvaluateWarp").hide();//隐藏评论浮层
        $(".Js_EvaluateList").eq(0).click();
    });
    //返回
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    });
});

(MySign = new function () {
    var _Self = this;
    this.ApplyStatus = "";//报名状态(已录取、未录取、招募取消)
    this.ActiveType = "";//活动类型(临时/驻场)
    this.EvaluateStatus = "";//评价状态(已评/待评)
    this.ActivityApplyID = "";//活动申请ID
    this.KeyWord = "";//关键字
    this.OperationIndex = "";
    //填充我的报名
    this.FillMyApply = function (status, type, evaluate, keyword) {
        CmnAjax.PostData(_AjaxUrl + "GetMyApply", { Status: status, ActivityTypeID: type, EvaluateType: evaluate, KeyWord: keyword }, function (data) {
            var _html = "";
            if (data.data.length > 0) {
                for (var _i = 0; _i < data.data.length; _i++) {
                    if (evaluate == "0") {
                        //判断。。
                        if (data.data[_i]["Operation"] == "评价") {
                            _html += '<p class="mui-Recuitment-action">' +
                             '<span class="mui-MyRelease-Title" activ_type="'+data.data[_i]["ActivityCategory"]+'" data-id="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br />' +
                             '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span>' +
                             '<button type="button" class="mui-btn Js_Operation" id="mui-stateBtn" activityApplyID="' + data.data[_i]["ActivityApplyID"] + '">' + data.data[_i]["Operation"] + '</button>' +
                             '</p>';
                        }
                    }
                    else if (evaluate == "1") {
                        if (data.data[_i]["Operation"] != "评价" && data.data[_i]["Operation"] != "删除" && data.data[_i]["Operation"] != "取消") {
                            _html += '<p class="mui-Recuitment-action">' +
                             '<span class="mui-MyRelease-Title" activ_type="'+data.data[_i]["ActivityCategory"]+'" data-id="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br />' +
                             '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span>' +
                             '<button type="button" class="mui-btn Js_Operation" id="mui-stateBtn" activityApplyID="' + data.data[_i]["ActivityApplyID"] + '">' + data.data[_i]["Operation"] + '</button>' +
                             '</p>';
                        }
                    }
                    else {
                        _html += '<p class="mui-Recuitment-action">' +
                                '<span class="mui-MyRelease-Title" activ_type="'+data.data[_i]["ActivityCategory"]+'" data-id="' + data.data[_i]["ActivityID"] + '">' + data.data[_i]["ActiveDesc"] + '</span><br />' +
                                '<span>' + _Self.GetStatus(data.data[_i]["Status"]) + '</span>' +
                                '<button type="button" ' + (data.data[_i]["Operation"].length > 0 ? '' : 'style="display: none;" ') + ' class="mui-btn Js_Operation" id="mui-stateBtn" activityApplyID="' + data.data[_i]["ActivityApplyID"] + '">' + data.data[_i]["Operation"] + '</button>' +
                                '</p>';
                    }

                }
            }
            $(".Js_MyApplyList").html(_html);
            
            $('.mui-MyRelease-Title').off().click(function() {
            	if($(this).attr("activ_type")=="1")
            	{
            		SiteFunc.GoTo("ActionRement.html?ActivityID=" + $(this).attr('data-id') + "&From=MySign.html");
            	}
            	else
            	{
            		SiteFunc.GoTo("ActionRement2.html?ActivityID=" + $(this).attr('data-id') + "&From=MySign.html");
            	}
            });
            $(".Js_Operation").off().click(function () {

                var _text = $(this).text();
                var _self = this;
                if (_text == "删除" || _text == "取消") {
                    plus.nativeUI.confirm('您是否确定要' + _text + '这个项目的报名？', function(e) {
                        if(e.index == 0) {
                        //调取更新接口
                            CmnAjax.PostData(_AjaxUrl + "UserOperateRegist", { ActivityApplyID: $(_self).attr("activityApplyID"), Operate: (_text == '删除' ? 'delete' : 'cancel') }, function (data) {
                                if (data.IsSuccess == 1) {
                                    $(_self).parent().remove();
                                }
                                else {
                                    SiteFunc.Alert(data.ErrMsg);
                                }
                            });
                        }
                    },'确认',['确定', '取消']);
                }
                else if (_text == "评价") {
                    $(".Js_EvaluateWarp").show();
                    _Self.ActivityApplyID = $(_self).attr("activityApplyID");
                    _Self.OperationIndex = $(_self).index();
                    window.activateOperationButton = $(this);
                }
            });
        });
    }

    //获取对应活动状态图
    this.GetStatus = function (state) {
        //1：待定；2：通过；3：未通过； 4：招募取消
        var _img = "";
        switch (state) {
            case "1":
                _img = '';
                break;
            case "2":
                _img = '<img src="images/Evaluation/RecruitBtn1.jpg">';
                break;
            case "3":
                _img = '<img src="images/Evaluation/RecruitBtn2.jpg">';
                break;
            case "4":
                _img = '<img src="images/Evaluation/RecruitBtn3.jpg">';
                break;
            default:
                _img = "";
                break;
        }
        return _img;
    }

    //填充选项
    this.FillSelect = function (select, index) {
        //填充
        $(".Js_Select").html(select).show();
        //选择
        $(".Js_Select p").off().on("tap", function () {
            //mui-morebtn
            var _text = $(this).text();
            if (index == 1) {
                $(".Js_ApplyStatus").find(".mui-morebtn").text(_text);
                _Self.ActiveStatus = $(this).attr("status");
            }
            if (index == 2) {
                $(".Js_ActiveType").find(".mui-morebtn").text(_text);
                _Self.ActiveType = $(this).attr("type");
            }
            if (index == 3) {
                $(".Js_EvaluateStatus").find(".mui-morebtn").text(_text);
                _Self.EvaluateStatus = $(this).attr("evalute");
            }
            _Self.FillMyApply(_Self.ActiveStatus, _Self.ActiveType, _Self.EvaluateStatus, "");
        });
        $(".Js_Select").off().on("tap", function () {
            $(this).hide();
        });
    }
})