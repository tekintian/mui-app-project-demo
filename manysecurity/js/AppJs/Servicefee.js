var ActiviParm={
	ActivityID:"",//活动ID
	OrderID:"",//订单ID
	Amount:"",//总金额
}
var ActivityCategory="";//判断是否是长期项目或者是短期项目
//每次执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    ActiviParm.ActivityID=data.ActivityID;
    ActiviParm.OrderID=data.orderID;
    $("#orderId").text(data.orderID);
    var personnum=data.projectName;
    $("#orderName").text(personnum);
    $("#personnum").text(data.projectZMRS);
    var number=data.projectZMRS;
    var count=0;
    ActivityCategory=data.ActivityCategory;
    if(data.ActivityCategory=="2")
	{
		$("#countTS").hide();
		count=parseInt(number)*20;
	}
	else
	{
		var ts=data.projectTS;
		count=parseInt(number)*20*parseInt(ts);
		$("#ts").text(ts);
	}
	ActiviParm.Amount=count;
	$("#orderYufu").text(count.toFixed(2));
});

//页面加载执行
Cmn.App.CurPage.OnInit.Add(function (data) {
	$("#mui-LoginOKBtn").click(function(){
		CmnAjax.PostData(SiteFunc.AjaxUrl+"AddJournalLog",ActiviParm,function(data){
            if (data.IsSuccess == 1) {
                $(".Js_SuccessWarp").show();
                setTimeout(function () {
                    $(".Js_SuccessWarp").hide();
                    SiteFunc.GoTo("Main.html");
                }, 3000)
            }
            else {
                SiteFunc.Alert("项目发布失败，请稍后再试");
                SiteFunc.GoTo("home.html");
            }
		})
	});
   //返回
//  $(".Js_Back").off().click(function () {
//  	var a="returndata";
//  	if(ActivityCategory=="1")
//  	{
//  		SiteFunc.GoTo("ActivityRelease2.html?ss="+a);	
//  	}
//  	else
//  	{
//  	    SiteFunc.GoTo("ActivityRelease_longtime.html");	
//  	}
//  });
});