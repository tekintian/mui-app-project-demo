﻿/// <reference path="../Cmn.js" />
/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    Cmn.App.CloseWaiting();
    Register2.ShowDiv(0);
});

(Register2 = new function () {
    var _HasRegist = false;
    var _CanSendSms = true,//是否可以发送短信
        _RegistPhone = "",        //手机号
        _HasSendSms = false;//是否已经发送短信
    var _Self = this;
    var _AjaxUrl = SiteFunc.AjaxUrl;
    var _Step = 1;
    var _FanHuei = true;
    //服装照片1
    var _SecurityServiceLicence_fz1="";
    var _SecurityServiceLicence_fz2="";
    var _SecurityServiceLicence_fz3="";
    var _SecurityServiceLicence_fz4="";
    var _SecurityServiceLicence_fz5="";
    $(function () {
        //发送短信
        $(".Js_Verify").click(function () {
            if (!_CanSendSms) {
                return;
            }
            var _phone = $(".Js_Phone").val();

            if (!SiteFunc.CheckPhone(_phone)) {
                SiteFunc.Alert("手机号格式错误");
                return;
            }

            _CanSendSms = false;
            CmnAjax.PostData(_AjaxUrl + "CompanyRegistCheckPhone", { Phone: _phone }, function (data) {
                if (data.IsSuccess == 1) {
                    CmnAjax.PostData(_AjaxUrl + "SendSms", { Phone: _phone }, function (data) {
                        if (data.IsSuccess == 1) {
                            _Self.CutDown();
                            _HasSendSms = true;
                            _RegistPhone = _phone;
                        }
                    });
                }
                else {
                    _CanSendSms = true;
                    SiteFunc.Alert(data.ErrMsg);
                }
            });
        });
        //下一步
        $(".Js_Next").click(function () {
            if (!_HasSendSms) {
                SiteFunc.Alert("请先发送短信验证");
                return;
            }
            var _code = $(".Js_Code").val();
            if (_code == "") {
                SiteFunc.Alert("请输入验证码");
                return;
            }
            CmnAjax.PostData(_AjaxUrl + "CheckSMSCode", { Code: _code }, function (data) {
                if (data.IsSuccess == 1) {
                    _Step = 2;
                    _Self.ShowDiv(1);
                }
                else {
                    SiteFunc.Alert("验证码错误");
                }
            });
        });
        //设置密码
        $(".Js_Next1").click(function () {
            var _pwd = $(".Js_Password").val();
            if (_pwd == "") {
                SiteFunc.Alert("请输入密码");
                return;
            }
            if (_pwd.length < 6) {
                SiteFunc.Alert("密码长度不够");
                return;
            }
            CmnAjax.PostData(_AjaxUrl + "Register", { Phone: _RegistPhone, Password: _pwd }, function (data) {
                if (data.IsSuccess == 1) {
                    _Self.ShowDiv(2);
                    _Step = 3;
                    //注册账号
                    $(".Js_RegistPhone").text(_RegistPhone);
                    SiteFunc.RememberLoginInfo(_RegistPhone, _pwd);
                    //设置登录状态，记录公司ID
                    SiteFunc.SetLoginStatus(true, "", data.UserID);
                }
                else {
                    SiteFunc.Alert(data.ErrMsg);
                }
            });
        });
        //上一步
        $(".Js_Back").off().click(function () {
            if (_HasRegist) {
                SiteFunc.GoTo("Main.html");
                return;
            }
            if (_FanHuei == true) {
                $(".Js_Name").val("");
                $(".Js_IDCard").val("");
                $(".Js_Sex").val("");
                $(".Js_Height").val("");
                $(".Js_Weight").val("");
                $(".Js_Papers").val("");
                $(".Js_WantCity").val("");
                $(".Js_Introduce").val("");
                $(".Js_Phone").val("");
                $(".Js_Code").val("");
                $(".Js_Password").val("");
                $(".Js_CardImgTip1").text("请上传您身份证的正面照");
                $(".Js_CardImgTip1").text("请上传您身份证的反面照");
//              $(".Js_WorkLicenseTip").text("请上传你的上岗证");
                $(".Js_ClothImgTip1").text("请上传您黑白套装的照片");
                $(".Js_ClothImgTip1").text("请上传您黑白套装的照片");
                SiteFunc.GoTo("Registered.html");
                _FanHuei = false;
            } else {
                $(".Js_YongHuXieYi").hide();
                _FanHuei = true;
            }
        });
        _Self.FillOpenCity();
        _Self.UpDateInfo();
    });
    //倒计时
    this.CutDown = function () {
        var _time = 60;
        $(".Js_Verify").css("background-color", "#e3e3e3");
        var _countdownTimer = setInterval(function () {
            $(".Js_Verify").html((--_time) + "S");
            if (_time == 0) {
                $(".Js_Verify").text("验证");
                $(".Js_Verify").css("background-color", "#4183ed");
                clearInterval(_countdownTimer);
                _CanSendSms = true;
            }
        }, 1000);
    }
    //第三步保存用户信息
    this.UpDateInfo = function () {
        var _idCardImg1 = "",
            _idCardImg2 = "",
            _workLicense = "",
            _hasCloth = true,
            _cloth1 = "",
            _cloth2 = "",
            _age = "";

        $(".Js_SubUserInfo").click(function () {
            var _name = $(".Js_Name").val().trim();
            var _idCard = $(".Js_IDCard").val().trim();
            var _sex = $(".Js_Sex").val();
            var _height = $(".Js_Height").val().trim();
            var _weight = $(".Js_Weight").val().trim();
            var _papers = $(".Js_Papers").val().trim();
            var _city = $(".Js_WantCity").val();
            var _introduce = $(".Js_Introduce").val().trim();
		    var _BankName=$('#regiseter_bank').val().trim();
            var _BankAddress=$("#Register2-kfh").val().trim();
            var _BankAccount=$("#Register2-yhzh").val().trim();
            if(_BankName=="请选择您的银行"){SiteFunc.Alert("请选择您的银行名称");return;}
            if(_BankAddress==""){SiteFunc.Alert("请输入您的开户银行");return;}
            if(_BankAccount==""){SiteFunc.Alert("请输入银行账号");return;}
            if (_name == "") { SiteFunc.Alert("请输入姓名"); return; }
            if (!SiteFunc.CheckCardID(_idCard)) { SiteFunc.Alert("请输入正确身份证号"); return; }
            if (_idCardImg1 == "") { SiteFunc.Alert("请上传身份证正面照"); return; }
            if (_idCardImg2 == "") { SiteFunc.Alert("请上传身份证反面照"); return; }
            if (_sex == "-1") { SiteFunc.Alert("请选择您的性别"); return; }
            if (isNaN(_height)) { SiteFunc.Alert("请输入正确的身高"); return; }
            if (isNaN(_weight)) { SiteFunc.Alert("请输入正确的体重"); return; }
            //if (_papers == "") { SiteFunc.Alert("请输入您的保安员证件编号"); return; }
            //if (_workLicense == "") { SiteFunc.Alert("请上传您的上岗证照片"); return; }
            //if (_city == "-1") { SiteFunc.Alert("请选择意向城市"); return; }
            if (_hasCloth) {
            	if($("#img_fz1").attr("src")=="images/Evaluation/mrphoto1.png"&&$("#img_fz2").attr("src")=="images/Evaluation/mrphoto2.png"&&$("#img_fz3").attr("src")=="images/Evaluation/mrphoto3.png")
            	{
            		SiteFunc.Alert("请上传您的照片"); return;
            	}
//	            if($("#img_fz1").attr("src")=="images/Evaluation/mrphoto1.png"){SiteFunc.Alert("请上传您的制式作训服照片"); return;}
//	            if($("#img_fz2").attr("src")=="images/Evaluation/mrphoto2.png"){SiteFunc.Alert("请上传您的黑白套装照片"); return;}
//	            if($("#img_fz3").attr("src")=="images/Evaluation/mrphoto3.png"){SiteFunc.Alert("请上传您的黑色套装照片"); return;}
            }
//          if($("#img_fz4").attr("src")=="images/Evaluation/mrphoto4.png"){SiteFunc.Alert("请上传您的保安员证照片"); return;}
//          if($("#img_fz5").attr("src")=="images/Evaluation/mrphoto6.png"){SiteFunc.Alert("请上传您的保安员证盖章页照片"); return;}
            //if (_introduce == "") { SiteFunc.Alert("请输入个人介绍"); return; }
            var parm = {
                UserName: _name,
                ICNo: _idCard,
                ICNo1PicPath: _idCardImg1,
                ICNo2PicPath: _idCardImg2,
                Sex: _sex,
                Age: _age,
                Phone: _RegistPhone,
                Height: _height,
                Weight: _weight,
                SecurityStaffCardNo: _papers,
                SecurityStaffCardPath: _workLicense,
                WantCity: _city,
                HasClothes: _hasCloth == true ? 1 : 0,
                ClothesOne: _cloth1,
                ClothesTwo: _cloth2,
                Introduction: _introduce,
				BankName:_BankName,
                BankAddress:_BankAddress,
                BankAccount:_BankAccount,
                //服装照片显示
                ClothesOne: _SecurityServiceLicence_fz2,
                ClothesTwo: _SecurityServiceLicence_fz3,
                ClothesThree:_SecurityServiceLicence_fz1,
                //保安员证显示
                SecurityStaffCardPath1:_SecurityServiceLicence_fz5,
                SecurityStaffCardPath:_SecurityServiceLicence_fz4
            }
            CmnAjax.PostData(_AjaxUrl + "UpdateUserInfo", parm, function (data) {
                if (data.IsSuccess == 1) {
                    $(".Js_Name").val("");
                    $(".Js_IDCard").val("");
                    $(".Js_Sex").val("");
                    $(".Js_Height").val("");
                    $(".Js_Weight").val("");
                    $(".Js_Papers").val("");
                    $(".Js_WantCity").val("");
                    $(".Js_Introduce").val("");
                    $(".Js_Phone").val("");
                    $(".Js_Code").val("");
                    $(".Js_Password").val("");
                    $(".Js_CardImgTip1").text("请上传您身份证的正面照");
                    $(".Js_CardImgTip1").text("请上传您身份证的反面照");
                    $(".Js_WorkLicenseTip").text("请上传你的上岗证");
                    $(".Js_ClothImgTip1").text("请上传您黑白套装的照片");
                    $(".Js_ClothImgTip1").text("请上传您黑白套装的照片");
                    $(".FloatError").show();
                    setTimeout(function () {
                        $(".FloatError").hide();
                        _HasRegist = true;
                        SiteFunc.GoTo("Main.html");
                    }, 1000);
                }
                else {
                    SiteFunc.Alert("资料提交失败");
                }
            });

        });
        //查看协议
        $(".Js_XieYi").off().click(function () {
            $(".Js_YongHuXieYi").show();
            _FanHuei = false;
        });

        //根据身份证号获取年龄
        $(".Js_IDCard").on("keyup keydown", function () {
            var _idCard = $(this).val();
            if (SiteFunc.CheckCardID(_idCard)) {
                _age = GetAgeByCard(_idCard);
                $(".Js_Sex").val(GetSexByCard(_idCard));
                $(".Js_Age").get(0).setAttribute('disabled', 'disabled'); 
                $(".JS_Sex").get(0).setAttribute('disabled', 'disabled');
            }
        });
        //身份证正面
        SiteFunc.UploadImg(".Js_CardImg1", function (demo) {
            $(".Js_CardImgTip1").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _idCardImg1 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //身份证反面
        SiteFunc.UploadImg(".Js_CardImg2", function (demo) {
            $(".Js_CardImgTip2").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _idCardImg2 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //上岗证
        SiteFunc.UploadImg(".Js_WorkLicense", function (demo) {
            $(".Js_WorkLicenseTip").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _workLicense = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //服装一
        SiteFunc.UploadImg(".Js_ClothImg1", function (demo) {
            $(".Js_ClothImgTip1").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _cloth1 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
        //服装一
        SiteFunc.UploadImg(".Js_ClothImg2", function (demo) {
            $(".Js_ClothImgTip2").text($(demo).val());
            $(".LoadTip").show();
        }, function (data) {
            _cloth2 = data.Path;
            $(".LoadTip").hide();
        }, function (data) {
            SiteFunc.Alert(data.ErrMsg);
            $(".LoadTip").hide();
        });
	    //服装上传1
	    SiteFunc.UploadImg("#fz1",function(demo){
	     $(".LoadTip").show();		
	    },function(data){
	    	_SecurityServiceLicence_fz1=data.Path;
	    	$("#img_fz1").attr("src", SiteFunc.UploadImgUrl + data.Path);
	        $(".LoadTip").hide();
	    },function(data){
	    	SiteFunc.Alert(data.ErrMsg);
	        $(".LoadTip").hide();
	    });
	    //服装上传2
	    SiteFunc.UploadImg("#fz2",function(demo){
	     $(".LoadTip").show();		
	    },function(data){
	    	_SecurityServiceLicence_fz2=data.Path;
	    	$("#img_fz2").attr("src", SiteFunc.UploadImgUrl + data.Path);
	        $(".LoadTip").hide();
	    },function(data){
	    	SiteFunc.Alert(data.ErrMsg);
	        $(".LoadTip").hide();
	    });
	    //服装上传3
	    SiteFunc.UploadImg("#fz3",function(demo){
	     $(".LoadTip").show();		
	    },function(data){
	    	_SecurityServiceLicence_fz3=data.Path;
	    	$("#img_fz3").attr("src", SiteFunc.UploadImgUrl + data.Path);
	        $(".LoadTip").hide();
	    },function(data){
	    	SiteFunc.Alert(data.ErrMsg);
	        $(".LoadTip").hide();
	    });
	    //保安员证上传
	    SiteFunc.UploadImg("#fz4",function(demo){
	     $(".LoadTip").show();		
	    },function(data){
	    	_SecurityServiceLicence_fz4=data.Path;
	    	$("#img_fz4").attr("src", SiteFunc.UploadImgUrl + data.Path);
	        $(".LoadTip").hide();
	    },function(data){
	    	SiteFunc.Alert(data.ErrMsg);
	        $(".LoadTip").hide();
	    });
	    //服装上传1
	    SiteFunc.UploadImg("#fz5",function(demo){
	     $(".LoadTip").show();		
	    },function(data){
	    	_SecurityServiceLicence_fz5=data.Path;
	    	$("#img_fz5").attr("src", SiteFunc.UploadImgUrl + data.Path);
	        $(".LoadTip").hide();
	    },function(data){
	    	SiteFunc.Alert(data.ErrMsg);
	        $(".LoadTip").hide();
	    });
        //服装自备与否
        $(".Js_HasCloth").click(function () {
            $(this).find("img").removeClass("YesIcon2");
            if (_hasCloth) {
                _hasCloth = false;
                $(".Js_HasClothTip").text("否");
                $(this).find("img").eq(0).addClass("YesIcon2");
                $(".Register1_Costume").hide();
                return;
            }
            _hasCloth = true;
            $(".Js_HasClothTip").text("是");
            $(this).find("img").eq(1).addClass("YesIcon2");
            $(".Register1_Costume").show();
        });
        //$(".Js_Password").on("keydown keyup", function () {
        //    if ($(this).val().length > 0) {
        //        $(this).attr("type", "password");
        //    }
        //    else {
        //        $(this).attr("type", "text");
        //    }
        //});
        $(".Js_Introduce").on("keydown keyup", function () {
            var _wordlength = $(this).val().length;
            $(".Js_Tip").text(200 - _wordlength);
        });
        $(".Js_HasCloth").trigger("click");
    }
    //填充已开通城市
    this.FillOpenCity = function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", "", function (data) {
            var _cityList = "<option value='-1'>请选择你的意向城市</option>";
            if (data.data.length > 1) {
                for (var i = 0; i < data.data.length; i++) {
                    _cityList += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                }
            }
            $(".Js_WantCity").html(_cityList);
        });
    }
    this.ShowDiv = function (step) {
        if (step == 0) { $("#LoginTitle").text("保安员注册"); }
        else if (step == 1) { $("#LoginTitle").text("设置密码"); }
        else if (step == 2) { $("#LoginTitle").text("完善个人资料"); }
        $(".mui-input-group").hide();
        $(".mui-input-group").eq(step).show();
    }
    function GetAgeByCard(userCardNo) {
        var myDate = new Date();
        var month = myDate.getMonth() + 1;
        var day = myDate.getDate();
        var age = myDate.getFullYear() - userCardNo.substring(6, 10) - 1;
        if (userCardNo.substring(10, 12) < month || userCardNo.substring(10, 12) == month && userCardNo.substring(12, 14) <= day) {
            age++;
        }
        return age;
    }
    
function GetSexByCard(userCardNo){
    var Sex="";
    if(userCardNo.length=="18"){
                  if(parseInt(userCardNo.substring(17,1))%2==1)
                  {
                    Sex=1;
                    return Sex;
                  }
                  else
                  {
                    Sex=2;
                    return Sex;
                  }
             }
              else if(userCardNo.length=="15")
              {
                  if(parseInt(userCardNo.substring(14,1))%2==1)
                  {
                    Sex=1;
                    return Sex;
                  }
                  else
                  {
                    Sex=2;
                    return Sex;
                  }
              }
}
    
});