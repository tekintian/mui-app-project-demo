﻿var wgtVer = null;
function plusReady() {
    // ......
    // 获取本地应用资源版本号
    plus.runtime.getProperty(plus.runtime.appid, function (inf) {
        wgtVer = inf.version;
        console.log("当前应用版本：" + wgtVer);
    });
}
if (window.plus) {
    plusReady();
} else {
    document.addEventListener('plusready', plusReady, false);
}
