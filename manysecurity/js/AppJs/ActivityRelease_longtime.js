var ActiviParm = {
	ActivityTypeID:"",//项目类型
	ActiveDesc:"",//项目名称
	LinkMan:"",//项目联系人
	LinkPhone:"",//联系人电话号码
	MonthSalary:"",//打包工资
	MonthSalaryDay:"",//每月发薪日
	ContractDuration:"",//合同期限
	RecruitPersonQty:"",//招募人数
	ActiveDateBegin:"",//项目开始时间
	ActiveDateEnd:"",//项目结束时间
	WorkTimeDesc:"",//每天工作时间
	ArrangeDesc:"",//排版情况
	OtherRequire:"",//其他信息	
	ProvinceID: "",//省份ID
    CityID: "",//城市ID
    CountyID: "",//区ID
    Address: "",//详细地址
    IsHideAddress: "",//是否隱藏详细地址
}
var ActivityCategory="";//分辨长期还是短期的参数
//每次执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    //填充活动类型
    var hdtype="";
    var d=new Date();
    ActiviParm.ActivityTypeID=data.ActivityTypeID;
    ActivityCategory=data.ActivityCategory;
    $(".Js_ActivitType").val("长期项目"+"-"+data.ActivityTypeName);
    $(".Js_date").val(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
});
//页面加载执行
Cmn.App.CurPage.OnInit.Add(function (data) {
	//填充发薪日
	ActivityRelease_longtime.fullFXR();
	//填充合同期限
	ActivityRelease_longtime.fullHTQX();
	//填充省份
    ActivityRelease_longtime.FillProvince();
	//确定按钮
	$("#mui-LoginOKBtn").click(function(){
		var _projecttype=$("#projecttype").val();//项目类型
		var _projectName=$("#projectName").val();//项目名称
		if(_projectName==""){SiteFunc.Alert("请填写项目名称");return;}
		var _projectLianxiren=$("#projectLianxiren").val();//项目联系人
		if(_projectLianxiren==""){SiteFunc.Alert("请填写项目联系人");return;}
		var _projectLianxidianhua=$("#projectLianxidianhua").val();
		if(!SiteFunc.CheckPhone(_projectLianxidianhua)){SiteFunc.Alert("请填写正确的手机号码");return;}
		var _projectGongzi=$("#projectGongzi").val();
		if(!SiteFunc.CheckNum(_projectGongzi)){SiteFunc.Alert("请填写正确的工资数额");return;}
		var _projectFaxingri=$("#projectFaxingri").val();
		if(_projectFaxingri=="请选择发薪日"){SiteFunc.Alert("请填写每月发薪日");return;}
		var _projectHetongqixian=$("#projectHetongqixian").val();
//		if(_projectHetongqixian=="请选择合同期限"){SiteFunc.Alert("请填写合同期限");return;}
		var _projectZhaomurenshu=$("#projectZhaomurenshu").val();
		if(!SiteFunc.CheckNum(_projectZhaomurenshu)){SiteFunc.Alert("请填写招募个数");return;}
		var _activeStartTime = $(".Js_ActivityStartTime").val();
		if(_activeStartTime==""){SiteFunc.Alert("请选择项目开始时间");return}
        var _activeEndTime = $(".Js_ActivityEndTime").val();
        if(_activeEndTime==""){SiteFunc.Alert("请选择项目结束时间"); return}
		var _projectGongzuoshijian=$("#projectGongzuoshijian").val();
		if(_projectGongzuoshijian==""){SiteFunc.Alert("请填写每天工作时间");return;}
		var _projectPaibanqingkuang=$("#projectPaibanqingkuang").val();
		if(_projectPaibanqingkuang==""){SiteFunc.Alert("请填写排班情况");return;}
		var _projectQitaxinxi=$("#projectQitaxinxi").val();
		if(_projectQitaxinxi.trim().length>300)
		{
			SiteFunc.Alert("其他信息请输入300字以内");return;
		}
		var _cityID = $(".Js_City").val();
        var _countyID = $(".Js_County").val();
        var _address = $(".Js_Address").val().trim();
        var _provinceID = $(".Js_Province").val();
        var _IsHideAddress = $(".Js_IsHideAddress").val().trim();
        if (_cityID == "") { SiteFunc.Alert("请选择项目所在城市"); return; }
        if (_countyID == "") { SiteFunc.Alert("请选择项目所在区"); return; }
        if (_address == "") { SiteFunc.Alert("请输入具体地址"); return; }
		ActiviParm.ActiveDesc=_projectName;//项目名称
		ActiviParm.LinkMan=_projectLianxiren;//项目联系人
		ActiviParm.LinkPhone=_projectLianxidianhua;//项目联系电话
		ActiviParm.MonthSalary=_projectGongzi;//打包工资
		ActiviParm.MonthSalaryDay=_projectFaxingri;//每月发薪日
//		ActiviParm.ContractDuration=_projectHetongqixian;//合同期限
		ActiviParm.RecruitPersonQty=_projectZhaomurenshu;//招募人数
		ActiviParm.ActiveDateBegin =_activeStartTime;//项目开始时间
		ActiviParm.ActiveDateEnd =_activeEndTime;//项目结束时间
		ActiviParm.WorkTimeDesc=_projectGongzuoshijian;//每天工作时间
		ActiviParm.ArrangeDesc=_projectPaibanqingkuang;//排版情况
		ActiviParm.OtherRequire=_projectQitaxinxi;//其他信息
		ActiviParm.ProvinceID = _provinceID;
		ActiviParm.CityID = _cityID;
        ActiviParm.CountyID = _countyID;
        ActiviParm.Address = _address;
        ActiviParm.IsHideAddress = _IsHideAddress;
		CmnAjax.PostData(SiteFunc.AjaxUrl+"AddActivityLong",ActiviParm,function(data){
			if (data.IsSuccess == 1) {
                    SiteFunc.GoTo("Servicefee.html?ActivityCategory="+ActivityCategory+"&ActivityID="+data.ID+"&orderID="+data.ONum+"&projectName="+ActiviParm.ActiveDesc+"&projectZMRS="+ActiviParm.RecruitPersonQty);
            }
            else {
                SiteFunc.Alert("项目发布失败，请稍后再试");
                SiteFunc.GoTo("home.html");
            }
			
		})
	});
	//返回
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=ActivityRelease.html");
    });
});
(ActivityRelease_longtime=new function(){
	var _Self = this;
	//填充省份
    this.FillProvince = function () {
        var _html = "<option value='-1'>请选择省份</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetProvince", "", function (data) {
            console.log(JSON.stringify(data));
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["ProvinceID"] + "'>" + data.data[i]["ProvinceDesc"] + "</option>";
                }
            }
            $(".Js_Province").html(_html);
            $(".Js_Province").off().change(function () {
                _Self.FillCity($(this).val())
            });
        });
    }
    //填充城市
    this.FillCity = function (provinceID) {
        var _html = "<option value='-1'>请选择城市</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", { ProvinceID: provinceID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</option>";
                }
            }
            $(".Js_City").html(_html);
            $(".Js_City").off().change(function () {
                _Self.FillCounty($(this).val())
            });
        });
    }
    //填充区
    this.FillCounty = function (cityID) {
        var _html = "<option value='-1'>请选择区</option>";
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetCounty", { CityID: cityID }, function (data) {
            if (data.data.length > 0) {
                for (var i = 0; i < data.data.length; i++) {
                    _html += "<option value='" + data.data[i]["CountryID"] + "'>" + data.data[i]["CountryDesc"] + "</option>";
                }
            }
            $(".Js_County").html(_html);
        });
    }
	//填充发薪日
	this.fullFXR=function(){
		var _html="<option value='-1'>请选择发薪日</option>";
		for(var i=1;i<32;i++)
		{
			_html+="<option value="+i+">"+i+"</option>";
		}
		$("#projectFaxingri").html(_html);
	}
	//填充合同期限
	this.fullHTQX=function(){
		var _html="<option value='-1'>请选择合同期限</option>";
		for(var i=1;i<100;i++)
		{
			_html+="<option value="+i+">"+i+"</option>";
		}
		$("#projectHetongqixian").html(_html);
	}
	
});
