﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../Cmn.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="SiteFunc.js" />
var ActiveParm = {
    CorpID: "",
    CityID: "",
    ActivityTypeID: "",
    Sex: "",
    SecurityStaffCardRequire: "",
    ClothesRequire: "",
    WorkTimeStart: "",
    WorkTimeEnd: "",
    PageIndex:1,
    PageCount:"",
    
}
Cmn.App.CurPage.OnInit.Add(function (data) {
	Recruitment.Init();
	
	Recruitment.BindActiveList();
	
    var _html = "";
    //活动类型
    $(".Js_ActiveType").click(function () {
        _html = "";
        _html += '<p class="mui-recruitment-DownList mui-recruitment-color" status="">项目状态</p>';
        _html += '<p class="mui-recruitment-DownList" type="1">临时</p>';
        _html += '<p class="mui-recruitment-DownList" type="2">驻场</p>';
        Recruitment.FillSelect(_html, 1);
    });

    //区域
    $(".Js_City").click(function () {
        _html = _cityList;
        Recruitment.FillSelect(_html, 2);
    });
    var _cityList = '<p class="mui-recruitment-DownList mui-recruitment-color" CityID="">区域</p>';
    //填充已开通城市列表
    CmnAjax.PostData(SiteFunc.AjaxUrl + "GetOpenCity", "", function (data) {
        if (data.data.length > 1) {
            for (var i = 0; i < data.data.length; i++) {
                _cityList += "<p class='mui-recruitment-DownList' CityID='" + data.data[i]["CityID"] + "'>" + data.data[i]["CityDesc"] + "</p>";
            }
        }
    });
    //更多
    $(".Js_More").click(function () {
        if ($(".Js_MoreChose").css("display") == "none") {
            $(".Js_MoreChose").show();
            $(".Js_Select").hide();
        }
        else {
            $(".Js_MoreChose").hide();
        }
    });

    //性别
    $(".Js_Sex span").click(function () {
        $(".Js_Sex span").each(function () {
            $(this).find("img").eq(0).removeClass("sexselect");
            $(this).find("img").eq(1).addClass("sexselect");
        });
        $(this).find("img").eq(1).removeClass("sexselect");
        $(this).find("img").eq(0).addClass("sexselect");
        ActiveParm.Sex = $(this).attr("sex");
    });
    //是否需要上岗证
    $(".Js_Need span").click(function () {
        $(".Js_Need span").each(function () {
            $(this).find("img").eq(0).removeClass("sexselect");
            $(this).find("img").eq(1).addClass("sexselect");
        });
        $(this).find("img").eq(1).removeClass("sexselect");
        $(this).find("img").eq(0).addClass("sexselect");
        ActiveParm.SecurityStaffCardRequire = $(this).attr("need");
    });
    //衣服
    $(".Js_Cloth span").click(function () {
        $(".Js_Cloth span").each(function () {
            $(this).find("img").eq(0).removeClass("sexselect");
            $(this).find("img").eq(1).addClass("sexselect");
        });
        $(this).find("img").eq(1).removeClass("sexselect");
        $(this).find("img").eq(0).addClass("sexselect");
        ActiveParm.ClothesRequire = $(this).attr("cloth");
    });
    //前往
    $(".Js_Sub").click(function () {
        ActiveParm.WorkTimeStart = $(".Js_StartTime").val();
        ActiveParm.WorkTimeEnd = $(".Js_EndTime").val();
        Recruitment.BindActiveList();
        $(".Js_MoreChose").hide();
    });
    //清除筛选
    $(".Js_Off").click(function () {
        ActiveParm.Sex = "";
        ActiveParm.ClothesRequire = "";
        ActiveParm.WorkTimeStart = "";
        ActiveParm.WorkTimeEnd = "";
        ActiveParm.SecurityStaffCardRequire = "";
        $(".Js_StartTime").val("");
        $(".Js_EndTime").val("");
        $(".mui-recr-sex").each(function () {
            $(this).find("img").eq(0).removeClass("sexselect");
            $(this).find("img").eq(1).addClass("sexselect");
        });
        $(".mui-recr-sex2").each(function () {
            $(this).find("img").eq(0).removeClass("sexselect");
            $(this).find("img").eq(1).addClass("sexselect");
        });
    });
});

//每次都执行
Cmn.App.CurPage.BeforeShow.Add(function (data) {
	//返回
    $(".Js_Back").off().click(function () {
    	var a=data.From;
    	if(a=="NewAction.html")
    	{
    		SiteFunc.GoTo("NewAction.html");
    	}
//      if (ActiveParm.CorpID != "") {
//      	
//          //SiteFunc.GoTo("NewAction.html");
//          SiteFunc.GoTo("Collection.html");
//      }
        SiteFunc.GoTo("Main.html?Menu=NewAction.html");
    });
    
    if (data.Back == 1) { return; }
    ActiveParm.CorpID = (data.CorpID == undefined ? "" : data.CorpID);
    ActiveParm.CityID = (data.CityID == undefined ? "" : data.CityID);
    
    if(data.break==1){
    	
    }
    else{
    	$(".Js_ActiveBox").empty();
    	Recruitment.BindActiveList();
    }
    
    
    
//  $(".Js_ActiveBox").empty();
    
});
(Recruitment = new function () {
    var _Self = this;
//  var item=1;
    //初始化
    this.Init = function () {
//     $(window).bind("touchmove", function () {
	        //当滚动条滚动时
//	        var top = $(".mui-DownLoadAction").scrollTop(); //滚动条的高度
//	        var height = $(".Js_ActiveBox").height(); //窗口文本高度
//	        var windowH = $(window).height(); //可视高度
//	        if(ActiveParm.PageIndex!=ActiveParm.PageCount) {
//	        	if (top >= height - windowH) {
//		        	if(!window.isposting) {
//		        		window.isposting = true;
//		        		ActiveParm.PageIndex = parseInt(ActiveParm.PageIndex)  + 1;
//		        		Cmn.App.ShowWaiting("请稍后，程序加载中...");
//		        		Recruitment.BindActiveList();
//		        	}
//	        	}
//	        }
//	        else{
//	        	Cmn.App.SystemToast("没有更多数据可加载！");
//	        }
//	        
//      })
    }
    
     
    
    this.BindActiveList = function () {
        CmnAjax.PostData(SiteFunc.AjaxUrl + "GetActivityList", ActiveParm, function (data) {
        	var _html = $(".Js_ActiveBox").html();
            var data = data.data;
            if (data.length > 0) {
                for (var _i = 0; _i < data.length; _i++) {
                    _html += '<div class="mui-input-row Recr-Content Js_ActiveList '+ (data[_i]['Status'] == 1 ? '' : 'gray')  + '">' + (data[_i]['Status'] == 1 ? '' : '<div class="stamp"></div>') +
                            '<p class="mui-Recuitment-action Js_GotoDtl" ActivityCategory="'+data[_i]["ActivityCategory"]+'" activityID="' + data[_i]["ActivityID"] + '">' +
                            TipImg(data[_i]["ActivityTypeID"], data[_i]["IsRecommend"]) + 
                            '<span class="all_title">'+data[_i]["ActiveDesc"] + '</span>' +
                            '<span class="mui-monuday">' + data[_i]["DaySalary"] + '</span>' +
                            '</p>' +
                            '<p class="mui-Recuitment-time"><span>工作时间：</span><span class="Recuitment-time_">' + data[_i]["WorkTime"] + '</span></p>' +
                            '<p class="mui-Recuitment-address"><span>项目地址：</span><span class="Recuitment">' + data[_i]["ProvinceDesc"] + data[_i]["CityDesc"] +data[_i]["CountryDesc"] + '</span></p>' +
                            '<p class="mui-Recuitment-address"><span>企业名称：</span><span class="Recuitment">' + data[_i]["CompanyName"] + '</span></p>' +
                            '<div class="m10">' +
                            '<label id="mui-rec-Number">计划招募<span>' + data[_i]["RecruitPersonQty"] + '</span>人</label>' +
                            '<label id="mui-rec-Number2">已报名<span>' + data[_i]["ApplyPersonQty"] + '</span>人</label>' +
                            '<label id="mui-rec-Number3">已录取<span>' + data[_i]["RecruitedPersonQty"] + '</span>人</label>' +
                            '</div>' +
                            '</div>'; 
                }
                ActiveParm.PageIndex= data[0]["CurrentPage"];
                ActiveParm.PageCount = data[0]["TotalPage"];
            }
            $(".Js_ActiveBox").html(_html);
            Cmn.App.CloseWaiting();
            //活动详情
            $(".Js_GotoDtl").off().click(function () { 	
            	if(ActiveParm.CorpID != ""){
            		if($(this).attr("ActivityCategory")=="1")
            		{
            			SiteFunc.GoTo("ActionRement.html?ActivityID=" + $(this).attr("activityID")+"&From=Recruitment.html&CorpID="+ActiveParm.CorpID +"&CityID="+ActiveParm.CityID);
            		}
            		else
            		{
            			SiteFunc.GoTo("ActionRement2.html?ActivityID=" + $(this).attr("activityID")+"&From=Recruitment.html&CorpID="+ActiveParm.CorpID +"&CityID="+ActiveParm.CityID );
            		}
                      
            		
            	}
            	else{
            		SiteFunc.GoTo("ActionRement.html?ActivityID=" + $(this).attr("activityID")+"&From=Recruitment.html&CityID="+ActiveParm.CityID + "&CorpID="+ActiveParm.CorpID);
            	}
            });
            window.isposting=false;
        });
    }
    function TipImg(type, isRec) {
        var html = "";
        if (type == 1) {
            html += '<span ><img src="images/Action/picture-xiang.png" /></span>';
        }
        else if (type == 2) {
            html += '<span ><img src="images/Action/picture-zhu.png" /></span>';
        }
        else if (type==3)
        {
            html += '<span ><img src="images/Action/picture-zhi.png" /></span>';
        }
        else if (type==4)
        {
            html += '<span ><img src="images/Action/picture-pai.png" /></span>';
        }
        if (isRec == "True") {
            html += '<span ><img src="images/Action/RecommendIcon.png" style="width: 49px; height: 38px;" /></span>';
        }
        return html;
    }
    this.Index = "";
    //填充选项
    this.FillSelect = function (select, index) {
        $(".Js_MoreChose").hide();
        if ($(".Js_Select").css("display") == "none") {
            //填充
            $(".Js_Select").html(select).show();
        }
        else {
            //填充
            $(".Js_Select").html(select).hide();
        }
     
        //选择
        $(".Js_Select p").off().on("tap",function () {
            var _text = $(this).text();
            if (index == 1) {
                $(".Js_ActiveType").text(_text);
                ActiveParm.ActivityTypeID = $(this).attr("type");
            }
            if (index == 2) {
                $(".Js_City").text(_text);
                ActiveParm.CityID = $(this).attr("CityID");
            }
            _Self.BindActiveList();
        });
        $(".Js_Select").off().on("click", function () {
            $(".Js_Select").hide();
        });
    }
})
