﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../CmnAjax.js" />
/// <reference path="CmnApp.js" />
/// <reference path="SiteFunc.js" />

var _MessageType = 1;
Cmn.App.CurPage.BeforeShow.Add(function (data) {
    _MessageType = data.MessageType;
    if (_MessageType == 1) {
        $("#LoginTitle").text("系统消息");
    }
    else if (_MessageType == 2) {
        $("#LoginTitle").text("项目消息");
    }
    else if (_MessageType == 3) {
        $("#LoginTitle").text("评价消息");
    }
    //填充消息
    ActionInfo.FillMessage();
});
Cmn.App.CurPage.OnInit.Add(function (data) {
    $(".Js_Back").off().click(function () {
        SiteFunc.GoTo("Main.html?Menu=Home.html");
    })
});
(ActionInfo = new function () {
    var _Self = this;
    //填充
    this.FillMessage = function () {
        CmnAjax.FillData(".Js_MessageList", SiteFunc.AjaxUrl + "GetMessageDtlByTypeID", { MessageType: _MessageType }, function (data) {
            if(!data.data || data.data.length == 0) {
                $('form').addClass('emptyContent');
            }
            else {
                $('form').removeClass('emptyContent');
            }
        });
    }
});