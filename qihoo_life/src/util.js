var Tips = require('./tips');
var Config = require('./config');
var DEBUG = false;
var D = Function.prototype.bind.call(console.log, console);
var parseUrl = function(url, id) {
	if (!url) {
		throw 'need param1';
	}
	id = id || '';
	var a = document.createElement('a');
	a.href = url;
	if (location.host == 'test1.baohe.mobilem.360.cn') {
		if (a.hostname == 'openbox.mobilem.360.cn') {
			a.host = 'test1.baohe.mobilem.360.cn';
		}
	}
	if (a.search.length > 0) {
		a.href += '&showShzsCpBar=1&shzsCpId=' + id;
	} else {
		a.href += '?showShzsCpBar=1&shzsCpId=' + id;
	}
	return a.href;
};
var getQuery = function(key) {
	var query = window.location.search.substr(1);
	var parts = query.split('&');

	for (var i = 0, len = parts.length; i < len; i++) {
		var kv = parts[i].split('=');

		if (kv[0] == key) {
			try {
				return decodeURIComponent(kv[1]);
			} catch (e) {
				return kv[1];
			}
		}
	}
	return undefined;
};
var filterService = function(item) {
	//	if (item.stream || item.neednfc || item.needpay) {
	//		DEBUG && D('filter:' + JSON.stringify(item));
	//		return false;
	//	}
	return item;
};
var handleService = function(item) {
	if (!item) {
		return false;
	}
	var flag = true;
	//	try {
	//		// e袋洗下线
	//		if (item.service_id == '202437376') {
	//			flag = false;
	//		}
	//	} catch (e) {
	//		flag = false;
	//	}
	item.dcloud = item.dcloud == '1' ? true : false;
	item.stream = item.dcloud == '2' ? true : false;
	item.needpay = item.needpay == '1' ? true : false;
	item.neednfc = item.neednfc == '1' ? true : false;
	item.needlogin = item.login == '1' ? true : false;

	if (!item.stream) {
		item.service_url = parseUrl(item.service_url, item.service_id);
	}

	if (flag) {
		return filterService(item);
	}
	return false;
};
var div = document.createElement('div');
var handleHotService = function(item) {
	if (!item) {
		return false;
	}
	div.innerHTML = item.brief;
	item.brief = div.textContent;

	item.dcloud = item.dcloud == '1' ? true : false;
	item.stream = item.dcloud == '2' ? true : false;
	item.needpay = item.needpay == '1' ? true : false;
	item.neednfc = item.neednfc == '1' ? true : false;
	item.needlogin = item.login == '1' ? true : false;
	return filterService(item);
};
var handleActivity = function(item) {
	if (!item) {
		return false;
	}
	if (item.activity_url) {
		item.url = item.activity_url;
	} else {
		item.url = 'http://openbox.mobilem.360.cn/html/shzs/detailtest.html?activity_id=' + item.activity_id;
	}
	item.dcloud = item.dcloud == '1' ? true : false;
	item.stream = item.dcloud == '2' ? true : false;
	item.needpay = item.needpay == '1' ? true : false;
	item.neednfc = item.neednfc == '1' ? true : false;
	item.needlogin = item.login == '1' ? true : false;
	return filterService(item);
};
var handleServiceList = function(items) {
	var menu = [];
	var list = [];
	if (items && items.length) {
		items.forEach(function(item) {
			var hasService = false;
			item.child.forEach(function(childItem, index) {
				index === 0 && (childItem.id = item.cate_class.replace(/\s/g, '.'));
				childItem.child_list = childItem.child_list.filter(function(serviceItem) {
					serviceItem.dcloud = !!serviceItem.dcloud;
					serviceItem.stream = !!serviceItem.stream;
					serviceItem.needpay = !!serviceItem.needpay;
					serviceItem.neednfc = !!serviceItem.neednfc;
					serviceItem.needlogin = !!serviceItem.needLogin;
					if (filterService(serviceItem)) {
						hasService = true;
						return serviceItem;
					}
				});
				childItem.child_list.length && list.push(childItem);
			});
			hasService && menu.push({
				cate_class: item.cate_class,
				cate_name: item.cate_name
			});
		});
	}
	return {
		menu: menu,
		list: list
	}
};
var convertFileName = function(url) {
	return url.replace(/[\/|\.|:]/g, '_');
};
var handleImage = function(url, callback, path) {
	path = path || 'img';
	mui.plusReady(function() {
		var filename = path + '/' + convertFileName(url);
		plus.io.resolveLocalFileSystemURL(filename, function(entry) {
			DEBUG && D('CACHE:' + url);
			callback(entry.toLocalURL()); //本地已缓存，返回本地地址
		}, function() {
			DEBUG && D('NO CACHE:' + url);
			plus.downloader.createDownload(url, { //下载图片
				filename: filename
			}, function(download, status) {
				if (DEBUG) {
					D("download.status:" + status);
				}
				if (status == 200) {
					callback(download.filename); //返回本地地址
				} else {
					callback(url); //下载失败，直接返回原始url,或者返回一个默认的error图片?
				}
			}).start();
		});
	});
};
var error = function(msg) {
	if (plus) {
		plus.nativeUI.alert(msg);
	} else {
		alert(msg);
	}
};
var showWaiting = function(modal, title) {
	mui.plusReady(function() {
		plus.nativeUI.showWaiting(title || '', {
			back: 'transmit',
			modal: modal === false ? false : true,
			padlock: true
		});
	});
};
var closeWaiting = function() {
	mui.plusReady(function() {
		plus.nativeUI.closeWaiting();
	});
};
var lastToastMsgs = {};
var toast = function(msg) {
	mui.plusReady(function() {
		if (!lastToastMsgs.hasOwnProperty(msg)) {
			lastToastMsgs[msg] = true;
			plus.nativeUI.toast(msg);
			setTimeout(function() {
				delete lastToastMsgs[msg];
			}, 2000); //toast 默认存在2秒
		}
	});
};
var lastAlertMsgs = {};
var alert = function(msg) {
	mui.plusReady(function() {
		if (!lastAlertMsgs.hasOwnProperty(msg)) {
			lastAlertMsgs[msg] = true;
			plus.nativeUI.alert(msg, function() {
				delete lastAlertMsgs[msg];
			});
		}
	});
};
var isCellular = function() {
	if (!plus) {
		return false;
	}
	var network = plus.networkinfo.getCurrentType();
	//2G|3G|4G
	if (network == plus.networkinfo.CONNECTION_CELL2G || network == plus.networkinfo.CONNECTION_CELL3G || network == plus.networkinfo.CONNECTION_CELL4G) {
		return true;
	}
	return false;
};
var supportFree = function() {
	return !!(mui && mui.os.android);
};
var showTips = function(type, options) {
	var config = Config.TIPS[type];
	options = options || {};
	for (var key in options) {
		config[key] = options[key];
	}
	var show = true;
	if (config.count) {
		var count = localStorage.getItem('TIPS_' + type + '_COUNT');
		if (count) {
			count = parseInt(count) || 0;
			if (count >= config.count) { //已显示数量超出
				show = false;
			} else {
				localStorage.setItem('TIPS_' + type + '_COUNT', count + 1);
			}
		}
	}
	show && Tips.show(config.msg, {
		text: config.btn,
		callback: config.callback
	}, config.timeout || 0);
};
//var showSimCardChangeTips = function() {
//	showTips('SIM_CARD');
//};
var showPauseTips = function(options) {
	showTips('PASUE', options);
};
//var showSimsTips = function() {
//	showTips('SIMS');
//};
var showExhaustTips = function(options) {
	showTips('EXHAUST', options);
};
var redirect = function(url, from) {
	//TODO 是否根据参加体验改进计划禁止CP打点
	//	if (localStorage.getItem('LIFE-EXPERIENCE') === 'FALSE') {
	//		return url;
	//	}
	var query = 'm2=' + plus.device.imei + '&os=' + encodeURIComponent(plus.os.name + ' ' + plus.os.version) + '&vc=' + plus.runtime.version + '&ni=' + getNetwork() + '&from=' + from + '&url=' + encodeURIComponent(url);
	if (DEBUG) {
		return 'http://pre.profile.sj.360.cn/live/service/redirect-to-service?' + query;
	}
	return 'http://profile.sj.360.cn/live/service/redirect-to-service?' + query;
};
var getNetwork = function() {
	var ni = '';
	if (plus) {
		var networkinfo = plus.networkinfo;
		var network = networkinfo.getCurrentType();
		switch (network) {
			case networkinfo.CONNECTION_UNKNOW:
				ni = 'UNKNOW';
				break;
			case networkinfo.CONNECTION_NONE:
				ni = 'NONE';
				break;
			case networkinfo.CONNECTION_ETHERNET:
				ni = 'ETHERNET';
				break;
			case networkinfo.CONNECTION_WIFI:
				ni = 'WIFI';
				break;
			case networkinfo.CONNECTION_CELL2G:
				ni = '2G';
				break;
			case networkinfo.CONNECTION_CELL3G:
				ni = '3G';
				break;
			case networkinfo.CONNECTION_CELL4G:
				ni = '4G';
				break;
		}
	}
	DEBUG && D('network:' + ni);
	return ni;
};
module.exports = {
	DEBUG: DEBUG,
	D: D,
	ERROR: error,
	alert: alert,
	toast: toast,
	redirect: redirect,
	isCellular: isCellular,
	parseUrl: parseUrl,
	getQuery: getQuery,
	supportFree: supportFree,
	showWaiting: showWaiting,
	closeWaiting: closeWaiting,
	handleService: handleService,
	handleActivity: handleActivity,
	handleServiceList: handleServiceList,
	handleHotService: handleHotService,
	handleImage: handleImage,
	getNetwork: getNetwork,
	//	showSimCardChangeTips: showSimCardChangeTips,
	showPauseTips: showPauseTips,
	//	showSimsTips: showSimsTips,
	showExhaustTips: showExhaustTips,
};