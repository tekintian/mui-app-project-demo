module.exports = {
	data: function() {
		return {
			loginActive: false,
			phoneActive: false,
			loginSuccessCallback: false,
			bindSuccessCallback: false
		};
	},
	created: function() {
		this.$on('showLoginPopover', this.showLoginPopover.bind(this));
		this.$on('closeLoginPopover', this.closeLoginPopover.bind(this));
		this.$on('showPhonePopover', this.showPhonePopover.bind(this));
		this.$on('closePhonePopover', this.closePhonePopover.bind(this));
		this.$on('loginSuccess', this.handleLoginSuccess.bind(this));
		this.$on('bindSuccess', this.handleBindSuccess.bind(this));
		var self = this;
		var old_back = mui.back;
		mui.back = function() {
			if (self.loginActive) {
				self.loginActive = false;
			} else if (self.phoneActive) {
				self.phoneActive = false;
			} else {
				old_back();
			}
		}
	},
	components: {
		login: require('../components/loginpopover.vue'),
		bindphone: require('../components/phonepopover.vue')
	},
	methods: {
		showLoginPopover: function(callback) {
			this.loginSuccessCallback = callback || false;
			this.loginActive = true;
		},
		closeLoginPopover: function() {
			this.loginActive = false;
		},
		showPhonePopover: function(callback) {
			this.bindSuccessCallback = callback || false;
			this.phoneActive = true;
		},
		closePhonePopover: function() {
			this.phoneActive = false;
		},
		handleLoginSuccess: function() {
			this.loginSuccessCallback && this.loginSuccessCallback();
		},
		handleBindSuccess: function() {
			this.bindSuccessCallback && this.bindSuccessCallback();
		}
	}
}