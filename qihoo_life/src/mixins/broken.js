module.exports = {
	data: function() {
		return {
			brokenActive: false
		}
	},
	ready: function() {
		this.$on('refresh', this.update.bind(this));
	},
	components: {
		broken: require('../components/broken.vue')
	}
}