var Store = require('../store');
var Logger = require('../logger');
var Util = require('../util');
module.exports = {
	methods: {
		openByBanner: function(item) {
			Logger.logByBanner(item.activity_url); //打点
			this.openByActivity(item, 'top_banner');
		},
		openByIcon: function(item) {
			Logger.logByIcon(item.service_url); //打点
			this.openByRecommend(item, 'icon');
		},
		openByHotServer: function(item) {
			Logger.logByPop(item.url); //打点
			this.open({
				id: item.name,
				url: Util.redirect(item.url, 'pop_ser'),
				appid: item.url,
				title: item.name,
				stream: item.stream,
				needlogin: item.needlogin,
				needpay: item.needpay,
				neednfc: item.neednfc
			});
		},
		openByRecommend: function(item, from) {
			Store.recommend(item.service_id);
			this.open({
				id: item.service_id,
				url: Util.redirect(item.service_url, from),
				appid: item.service_url,
				title: item.service_name,
				stream: item.stream,
				needlogin: item.needlogin,
				needpay: item.needpay,
				neednfc: item.neednfc
			});
		},
		openByActivity: function(item, from) {
			this.open({
				id: item.activity_id,
				url: Util.redirect(item.activity_url, from),
				appid: item.activity_url,
				title: item.name,
				stream: item.stream,
				needlogin: item.needlogin,
				needpay: item.needpay,
				neednfc: item.neednfc
			});
		},
		openByServerList: function(item) {
			Logger.logByList(item.url); //打点
			this.open({
				id: item.name,
				url: Util.redirect(item.url, 'list_ser'),
				appid: item.url,
				title: item.name,
				stream: item.stream,
				needlogin: item.needLogin,
				needpay: item.needpay,
				neednfc: item.neednfc
			});
		},
		openByCpOrder: function(type, item) {
			if (type === 'stream') { //流应用
				var params = JSON.parse(item.orderUrl);
				this.openStream(params.appid, '', params);
			} else { //普通，轻应用
				this.openUrl(item.orderUrl);
			}
		},
		openByCpOrderStream: function(url, money) {
			Logger.logByCpOrder(url, money);
		},
		open: function(options) {
			if (typeof options === 'string') {
				this.openUrl(url);
			} else {
				//TODO needpay neednfc
				if (options.needlogin && !Store.isLogin()) { //登录判定
					this.$dispatch('showLoginPopover', this.open.bind(this, options));
				} else if (options.needlogin && Store.isLogin() && !Store.hasPhone()) { //手机号绑定判定
					this.$dispatch('showPhonePopover', this.open.bind(this, options));
				} else {
					if (options.stream) {
						this.openStream(options.appid, options.title);
					} else {
						this.openUrl(options.url);
					}
				}
			}
		},
		openUrl: function(url) {
			Util.DEBUG && Util.D('CP:' + url);
			plus.webview.getWebviewById('TITLE').evalJS('openServer("' + url + '")');
		},
		openStream: function(appid, title, extras) {
			plus.stream.open({
				appid: appid,
				title: title,
				extras: JSON.stringify(extras)
			}, function() {
				//TODO 打点统计
			}, function(e) {
				mui.alert(e.code + ':' + e.message);
			});
		}
	}
}