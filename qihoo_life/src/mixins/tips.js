var Store = require('../store');
var Util = require('../util');
var Logger = require('../logger');
module.exports = {
	data: function() {
		return {
			tipsActive: false
		};
	},
	created: function() {
		mui.plusReady(function() {
			if (Store.getFreeTrafficTipsFlag() === 1) { //仅当没有提示过的才需要执行下列逻辑
				var self = this;
				var old_back = mui.back;
				mui.back = function() {
					if (self.tipsActive) {
						Logger.logMllByPopAction(1);
						self.tipsActive = false;
					} else {
						old_back();
					}
				};
				//启动时检测是否需要提示免流量
				document.addEventListener("netchange", this.handleNetChange); //监听网络变化
				this.handleNetChange();
			}
//			if (Util.supportFree()) { //流量用尽监听(包括其他状态)
//				plus.stream.onfreetrafficStateChange = function(event) {
//					if (event.state == 814) { //流量用尽
//						Util.showExhaustTips();
//					}
//				};
//			}
		}.bind(this));
	},
	components: {
		tips: require('../components/freetrafficpopover.vue')
	},
	methods: {
		showTipsPopover: function() {
			this.tipsActive = true;
		},
		closeTipsPopover: function() {
			this.tipsActive = false;
		},
		handleNetChange: function() {
			Util.DEBUG && Util.D('network:' + (plus && plus.networkinfo.getCurrentType()));
			if (plus && plus.webview.currentWebview().isVisible()) { //仅显示的窗口执行
				Util.DEBUG && Util.D('webview[' + plus.webview.currentWebview().id + ']');
				if (Store.showFreeTips()) { //免流量提示判定
					this.tipsActive = true;
				}
			}
		}
	}
}