var Vue = require('vue');
var Util = require('./util');
var Logger = require('./logger');

var first = null;
mui.back = function() {
	//首次按键，提示‘再按一次退出应用’
	if (!first) {
		first = mui.now();
		mui.toast('再按一次退出应用');
		setTimeout(function() {
			first = null;
		}, 1000);
	} else {
		if (mui.now() - first < 1000) {
			plus.runtime.quit();
		}
	}
};

var my = new Vue(require('./my.vue'));
mui.plusReady(function() {
	plus.webview.currentWebview().setStyle({
		scrollIndicator: 'none'
	});
});
window.addEventListener('refresh', function() {
	Util.DEBUG && Util.D('my refresh');
	my.update();
});
window.addEventListener('bindSuccess', function(e) {
	Util.closeWaiting();
	Util.DEBUG && Util.D('my bind success:' + JSON.stringify(e.detail));
	Logger.logMllByBindSuccess();
	my.bindTipsActive = true;
	my.bindTotal = e.detail.total;
	my.bindExpire = e.detail.expire
	my.update();
});