var Vue = require('vue');
var Util = require('./util');
var service = new Vue(require('./service.vue'));
var first = true;
window.addEventListener('refresh', function() {
	if (first || !service.loaded) {
		first = false;
		Util.DEBUG && Util.D('service init');
		service.update();
	}
});
