var Store = require('./store');
var Util = require('./util');
var Vue = require('vue');

var index = new Vue(require('./index.vue'));
localStorage.setItem('TIPS_PASUE_COUNT', 0); //启动时清空服务器暂停服务TIPS计数
window.addEventListener('go', function(e) {
	index.go(e.detail);
});
window.addEventListener('bindSuccess', function(e) {
	index.go(2);
});
mui.plusReady(function() {
	if (Util.supportFree()) { //流量用尽监听
		plus.stream.onfreetrafficStateChange = function(event) {
			if (event.state == 811) { //流量用尽
				Util.alert('免费流量已用完，继续浏览将正常消耗运营商流量！');
			}
		};
	}
	if (Util.supportFree() && Util.isCellular()) { //支持免流量，且为2G,3G,4G
		Store.setFreeTrafficTipsFlag(); //递增免流量tips标识
	}
	var lauchFlag = plus.storage.getItem("lauchFlag");
	if (lauchFlag) {
		plus.navigator.closeSplashscreen();
	} else {
		plus.webview.open('guide.html');
	}
});
var first = null;
mui.back = function() {
	//首次按键，提示‘再按一次退出应用’
	if (!first) {
		first = mui.now();
		mui.toast('再按一次退出应用');
		setTimeout(function() {
			first = null;
		}, 1000);
	} else {
		if (mui.now() - first < 1000) {
			plus.runtime.quit();
		}
	}
};