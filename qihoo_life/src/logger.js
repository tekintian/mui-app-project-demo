var queue = []; // 请求队列
var isLoading = false;

var util = {
	param: function(obj) {
		var buffer = [];
		for (var key in obj) {
			buffer.push(key + '=' + encodeURIComponent(obj[key]));
		}
		return buffer.join('&');
	},
	mix: function(target, source, overwrite) {
		for (var i in source) {
			if (target[i] && !overwrite) {
				continue;
			}
			target[i] = source[i];
		}
		return target;
	}
};
var Logger = {
	/**
	 * 获取统计URL
	 *
	 * @param {string} act, 模块名
	 * @param {string} aid 功能名，不同功能之间用,逗号分隔。
	 */
	getLogUrl: function(params) {
		var defaultParam = {
			m2: plus.device.imei,
			os: plus.os.name + ' ' + plus.os.version,
			vc: plus.runtime.version,
			_t: (new Date).getTime()
		};
		var server = 'http://s.360.cn/zhushou/360life.html';
		return server + '?' + util.param(util.mix(params || {}, defaultParam));
	},
	request: function(url) {
		if (!this.hamal) {
			var hamal = this.hamal = new Image();
			hamal.onload = hamal.onerror = function() {
				isLoading = false;
				if (queue.length > 0) {
					Logger.request(queue.shift());
				}
			};
		}
		if (isLoading) {
			queue.push(url);
			return;
		}
		isLoading = true;
		this.hamal.src = url;
	},
	record: function() {
		//同意参加用户体验改进计划
		if (localStorage.getItem('LIFE-EXPERIENCE') !== 'FALSE') {
			var logUrl = this.getLogUrl.apply(this, arguments);
			this.request(logUrl);
		}
	}
};

module.exports = {
	//1.	开启客户端时打点（暂时无用）
	logStart: function(params) { //page_start,location,login,ope
		mui.plusReady(function() {
			var networkinfo = plus.networkinfo;
			var network = networkinfo.getCurrentType();
			var ni = '';

			switch (network) {
				case networkinfo.CONNECTION_UNKNOW:
					ni = 'UNKNOW';
					break;
				case networkinfo.CONNECTION_NONE:
					ni = 'NONE';
					break;
				case networkinfo.CONNECTION_ETHERNET:
					ni = 'ETHERNET';
					break;
				case networkinfo.CONNECTION_WIFI:
					ni = 'WIFI';
					break;
				case networkinfo.CONNECTION_CELL2G:
					ni = '2G';
					break;
				case networkinfo.CONNECTION_CELL3G:
					ni = '3G';
					break;
				case networkinfo.CONNECTION_CELL4G:
					ni = '4G';
					break;
			}
			Logger.record(util.mix({
				md: plus.device.model, //机型
				ni: ni,
			}, params));
		});
	},
	//2.	主页面点击（顶部banner）后打点（暂时无用）
	logByBanner: function(url) {
		this.logService('top_banner', url);
	},
	//2.	主页面点击（服务icon）后打点（暂时无用）
	logByIcon: function(url) {
		this.logService('icon', url);
	},
	//2.	主页面点击（热门服务）后打点（暂时无用）
	logByPop: function(url) {
		this.logService('pop_ser', url);
	},
	//3.	分类页点击服务后（暂时无用）
	logByList: function(url) {
		this.logService('list_ser', url);
	},
	//4.	免流量手机号绑定页面开启时打点(打开时)
	logMllByBind: function() {
		Logger.record({
			act: 'mll_page'
		});
	},
	//4-1.免流量手机号绑定页面动作打点(执行动作)
	logMllByBindAction: function(action) {
		//1为点击返回，2为立即绑定，3为其他
		Logger.record({
			mll_act: action
		});
	},
	//5.	弹出绑定成功弹窗时(打开时)
	logMllByBindSuccess: function() {
		Logger.record({
			act: 'mll_suc'
		});
	},
	//6.	免流量提示弹窗打点(打开时)
	logMllByPop: function() {
		//1为点击取消，2为确认，3为其他
		Logger.record({
			act: 'mll_notice'
		});
	},
	//6-1.免流量提示弹窗执行动作打点(执行动作)
	logMllByPopAction: function(action) {
		//1为点击取消，2为确认，3为其他
		Logger.record({
			mll_notice_act: action
		});
	},
	//7.	进入Cp页打点（暂时无用）
	logByCp: function(url) {
		this.logService('cp', url);
	},
	//8.	进入cp订单页打点（暂时无用）
	logByCpOrder: function(url, money) {
		Logger.record({
			cp_dingdan: url,
			cp_money: money
		});
	},
	//9.	支付选择打点（暂时无用）
	logByPayment: function() {
		Logger.record({
			payment: '5+'
		});
	},
	//10.完成支付（暂时无用）
	logByPaymentFin: function(url) {
		Logger.record({
			pay_fin: url
		});
	},
	logService: function(name, value) {
		var params = {};
		params[name] = value;
		Logger.record(params);
	},
};