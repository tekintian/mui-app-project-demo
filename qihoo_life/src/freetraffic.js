var Util = require('./util');
var Store = require('./store');
var checkNetwork = function(callback) {
	if (Util.getNetwork() === 'NONE') {
		Util.toast('您的网络好像不太给力，请稍后再试');
		callback && callback(false);
		return false;
	}
	return true;
};
var FreeTraffic = {
	//请求验证码
	freetrafficRequest: function(phone, callback) {
		if (!checkNetwork(callback)) {
			return;
		}
		if (typeof plus !== 'undefined') {
			plus.stream.freetrafficRequest({
				phone: phone
			}, function() {
				callback && callback(true);
			}, function(e) {
				Util.DEBUG && Util.D('plus.stream.freetrafficRequest：' + e.code + ',' + e.message);
				callback && callback(false, e);
			});
		} else {
			callback && callback(false);
		}
	},
	//绑定手机
	freetrafficBind: function(phone, code, callback) {
		if (!checkNetwork(callback)) {
			return;
		}
		if (typeof plus !== 'undefined') {
			plus.stream.freetrafficBind({
				phone: phone,
				code: code
			}, function() {
				callback && callback(true);
			}, function(e) {
				Util.DEBUG && Util.D('plus.stream.freetrafficBind：' + e.code + ',' + e.message);
				callback && callback(false, e);
			});
		} else {
			callback && callback(false);
		}
	},
	//解除绑定
	freetrafficRelease: function() {
		if (!checkNetwork(callback)) {
			return;
		}
		//TODO
		plus && plus.stream.freetrafficRelease();
	},
	//获取免流量状态信息
	freetrafficInfo: function(callback) {
		if (!checkNetwork(callback)) {
			return;
		}
		if (typeof plus !== 'undefined') {
			plus.stream.freetrafficInfo(function(response) {
				Util.DEBUG && Util.D('plus.stream.freetrafficInfo：' + JSON.stringify(response));
				//设置提示标识次数，只要有流量信息说明已绑定，则固定设置2(该设置肯定比首页启动设置慢，所以后续启动识别6次的时候肯定不回再提示)
				Store.setFreeTrafficTipsFlag(2);
				Store.setFreeTrafficInfo(response); //存储到本地
				callback && callback(response);
			}, function(e) {
				Util.DEBUG && Util.D('plus.stream.freetrafficInfo：' + e.code + ',' + e.message);
				callback && callback(false, e);
			});
		} else {
			callback && callback(false);
		}
	}
};
module.exports = FreeTraffic;