var Util = require('./util');
var Login = {
	services: {}, //oauth服务列表
	init: function(callback) {
		mui.plusReady(function() {
			//获取终端支持的权登录认证服务列表
			plus.oauth.getServices(function(services) {
				if (services) {
					for (var i = 0, len = services.length; i < len; i++) {
						var service = services[i];
						this.services[service.id] = service;
					}
					Util.DEBUG && Util.D('OAUTH SERVICES:' + JSON.stringify(this.services));
				}
			}.bind(this));
		}.bind(this));
	},
	getUserInfo: function(type, callback) {
		if (mui.isFunction(type)) {
			callback = type;
			type = 'qihoo';
		}
		type = type || 'qihoo';
		var oauth = this.services[type];
		if (oauth) {
			oauth.getUserInfo(function() {
				Util.DEBUG && Util.D('oauth:' + JSON.stringify(oauth));
				Util.DEBUG && Util.D('getUserInfo:' + JSON.stringify(oauth.userInfo));
				callback && callback(oauth.userInfo);
			}, function(e) {
				Util.DEBUG && Util.D('getUserInfo error:' + e.code + ',' + e.message);
				callback && callback(false, 'userInfo', e);
			});
		} else {
			Util.DEBUG && Util.D('暂不支持[' + type + ']获取用户信息');
			callback && callback(false, 'userInfo');
		}
	},
	loginByOAuth: function(type, callback) {
		if (mui.isFunction(type)) {
			callback = type;
			type = 'qihoo';
		}
		type = type || 'qihoo';
		var oauth = this.services[type];
		if (oauth) {
			oauth.login(function() {
				this.getUserInfo(type, callback);
			}.bind(this), function(e) {
				Util.DEBUG && Util.D('oauth[' + type + '] login error:' + e.code + ',' + e.message);
				callback && callback(false, 'login', e);
			});
		} else {
			Util.ERROR('暂不支持[' + type + ']登录');
			callback && callback(false, 'login');
		}
	},
	logoutByOAuth: function(type, callback) {
		if (mui.isFunction(type)) {
			callback = type;
			type = 'qihoo';
		}
		type = type || 'qihoo';
		var oauth = this.services[type];
		if (oauth) {
			//			if (mui.os.ios) {
			oauth.logout(function() {
				callback && callback(true);
			}, function(e) {
				Util.DEBUG && Util.D('oauth.logout:' + e.code + ',' + e.message);
				callback && callback(false);
			});
			//			} else {
			//				callback && callback(true);
			//			}
		} else {
			Util.ERROR('暂不支持[' + type + ']登录');
			callback && callback(false);
		}

	},
	addPhoneNumber: function(callback) {
		var oauth = this.services['qihoo'];
		if (oauth) {
			oauth.addPhoneNumber(function(response) {
				Util.DEBUG && Util.D('addPhoneNumber：' + JSON.stringify(response));
				callback(true);
			}, function(event) {
				Util.DEBUG && Util.D('addPhoneNumber error：' + event.code + '，' + event.message);
				callback(false);
			});
		} else {
			Util.ERROR('暂不支持[qihoo]绑定手机号');
			callback(false);
		}
	}
};
Login.init();
module.exports = Login;