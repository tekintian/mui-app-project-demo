var TIPS = {
	SIM_CARD: {
		msg: '您已更换sim卡，请重新绑定免流量号码',
		btn: '重新绑定',
		timeout: 10000,
		callback: function() {

		}
	},
	PASUE: {
		msg: '由于服务器维护，免流量功能暂停使用，敬请谅解！',
		btn: '我知道了',
		count: 2,
		callback: function(tips) {
			tips.close();
		}
	},
	TIMEOUT: {
		msg: '由于您长时间未使用免流量功能，若要继续使用免流量，请重新绑定手机号',
		btn: '重新绑定',
		timeout: 10000,
		callback: function(tips) {}
	},
	REPEAT: {
		msg: '您使用的手机号在其他设备中绑定了免流量功能，请重新绑定',
		btn: '重新绑定',
		timeout: 10000,
		callback: function(tips) {}
	},
	BALANCE: {
		msg: '免费流量已不足10M',
		btn: '我知道了',
		callback: function(tips) {
			tips.close();
		}
	},
	EXHAUST: {
		msg: '免费流量已用完，继续浏览将正常消耗运营商流量！',
		btn: '我知道了',
		timeout: 10000,
		callback: function(tips) {
			tips.close();
		}
	},
	UNSUPPORT: {
		msg: '该网页内容不支持使用免费流量',
		btn: '我知道了',
		callback: function(tips) {
			tips.close();
		}
	},
	SIMS: {
		msg: '双卡手机仅支持卡槽1号码',
		btn: '我知道了',
		callback: function(tips) {
			tips.close();
		}
	}
};
module.exports = {
	TIPS: TIPS
};