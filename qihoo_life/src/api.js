var Util = require('./util');
var HOST = 'http://openbox.mobilem.360.cn';
var HOST_ENCRYPT = 'https://openbox.mobilem.360.cn';
var HOST_PROFILE = 'http://profile.sj.360.cn';

if (Util.DEBUG) {
	HOST_PROFILE = 'http://pre.profile.sj.360.cn';
	//	HOST = 'http://test1.baohe.mobilem.360.cn';
}
var URL = HOST + '/html/shzs/api/';

var CHECK_UPDATE_URL = 'https://profile.sj.360.cn/live/version/check-update';
if (Util.DEBUG) {
	CHECK_UPDATE_URL = 'http://pre.profile.sj.360.cn/live/version/check-update';
}

var getJSON = function(method, callback) {
	mui.ajax({
		url: URL + method + '.json',
		dataType: 'json',
		cache: false,
		success: function(response) {
			callback && callback(response);
		},
		error: function(xhr, type, error) {
			Util.toast('您的网络好像不太给力，请稍后再试');
			callback && callback(false);
		}
	});
};
var JSONP = (function() {
	var head, window = this,
		config = {};

	function load(url, pfnError) {
		var script = document.createElement('script'),
			done = false;
		script.src = url;
		script.async = true;

		var errorHandler = pfnError || config.error;
		if (typeof errorHandler === 'function') {
			script.onerror = function(ex) {
				errorHandler({
					url: url,
					event: ex
				});
			};
		}

		script.onload = script.onreadystatechange = function() {
			if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
				done = true;
				script.onload = script.onreadystatechange = null;
				if (script && script.parentNode) {
					script.parentNode.removeChild(script);
				}
			}
		};

		if (!head) {
			head = document.getElementsByTagName('head')[0];
		}
		head.appendChild(script);
	}

	function encode(str) {
		return encodeURIComponent(str);
	}

	function jsonp(url, params, callback, callbackName) {
		var query = (url || '').indexOf('?') === -1 ? '?' : '&',
			key;

		var uniqueName = (callbackName || config['callbackName'] || 'callback');
		//		var uniqueName = callbackName + "_json" + (++counter);

		params = params || {};
		for (key in params) {
			if (params.hasOwnProperty(key)) {
				query += encode(key) + "=" + encode(params[key]) + "&";
			}
		}

		window[uniqueName] = function(data) {
			callback(data);
			try {
				delete window[uniqueName];
			} catch (e) {}
			window[uniqueName] = null;
		};

		load(url + query + callbackName + '=' + uniqueName);
		return uniqueName;
	}

	function setDefaults(obj) {
		config = obj;
	}

	return {
		get: jsonp,
		init: setDefaults
	};
}());
module.exports = {
	//获取Location
	getLocation: function(callback) {
		mui.ajax({
			url: 'http://profile.sj.360.cn/live/get-location',
			dataType: 'json',
			cache: false,
			success: function(response) {
				callback && callback(response);
			},
			error: function(xhr, type, error) {
				Util.toast('您的网络好像不太给力，请稍后再试');
				callback && callback(false);
			}
		});
	},
	//获取Banner
	getBanner: function(callback) {
		getJSON('banner', callback);
	},
	//获取所有推荐服务
	getAllServer: function(callback) {
		getJSON('allserver', callback);
	},
	//获取优惠活动
	getActivity: function(callback) {
		getJSON('activity', callback);
	},
	//获取热门服务
	getHotServer: function(callback) {
		getJSON('hotserver', callback);
	},
	//获取所有服务
	getServerList: function(callback) {
		JSONP.init({
			error: function() {
				Util.toast('您的网络好像不太给力，请稍后再试');
				callback && callback(false);
			}
		})
		JSONP.get(HOST + '/html/shzs/api/serverlist.js?v=' + Math.random(), {}, callback, 'serverList');
	},
	//获取TIPS域名列表
	getTipsDomainList: function(callback) {
		mui.getJSON(HOST_ENCRYPT + '/webviewjs/domainlist', callback);
	},
	//获取指定域名的TIPS规则
	getRuleByDomain: function(domain, callback) {
		mui.getJSON(HOST_ENCRYPT + '/webviewjs/rule', {
			domain: domain
		}, callback)
	},
	//反馈
	careUpload: function(params, callback) {
		mui.ajax({
			url: 'http://care.help.360.cn/care/upload',
			type: 'POST',
			dataType: 'json',
			contentType: false,
			processData: false,
			data: params,
			success: function(response) {
				callback && callback(response);
			},
			error: function(response) {
				Util.toast('您的网络好像不太给力，请稍后再试');
				Util.DEBUG && Util.D('care upload error');
				callback && callback(false);
			}
		});
	},
	checkUpdate: function(data, callback) {
		if (mui.isFunction(data)) {
			callback = data;
			data = {};
		}
		data.cli_type = mui.os.android ? 'android' : 'ios';
		data.ver = plus.runtime.version; //该方法取到的版本号虽可用，但API有歧义
		mui.ajax({
			url: CHECK_UPDATE_URL,
			dataType: 'json',
			cache: false,
			data: data,
			success: function(response) {
				callback && callback(response);
			},
			error: function(response) {
				Util.toast('您的网络好像不太给力，请稍后再试');
				Util.DEBUG && Util.D('check update error');
				callback && callback(false);
			}
		});
	},
	getOrderList: function(page, page_size, callback) {
		mui.ajax({
			url: 'http://profile.sj.360.cn/live/userorder/list',
			dataType: 'json',
			cache: false,
			data: {
				page: page || 1,
				page_size: page_size || 10
			},
			success: function(response) {
				callback && callback(response);
			},
			error: function(response) {
				Util.toast('您的网络好像不太给力，请稍后再试');
				Util.DEBUG && Util.D('userorder list error');
				callback && callback(false);
			}
		});
	},
	getOrderDetail: function(appId, orderId, callback) {
		mui.ajax({
			url: 'http://profile.sj.360.cn/live/userorder/detail',
			dataType: 'json',
			cache: false,
			data: {
				appId: appId,
				orderId: orderId
			},
			success: function(response) {
				callback && callback(response);
			},
			error: function(response) {
				Util.toast('您的网络好像不太给力，请稍后再试');
				Util.DEBUG && Util.D('order detail error');
				callback && callback(false);
			}
		});
	},
	/**
	 * 免流量黑名单及号段
	 * @param {Object} callback
	 */
	getFlowStatus: function(callback) {
		mui.getJSON(HOST_PROFILE + '/live/version/flow-status', callback);
	},
	/**
	 * 消息通知接口
	 * @param {Object} callback
	 */
	getNotice: function(callback) {
		mui.getJSON(HOST_PROFILE + '/live/version/notice', callback);
	}
};