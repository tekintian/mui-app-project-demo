var showTips = function(msg, btn, time) {
	if (typeof btn === 'number') {
		time = btn;
		btn = false;
	}
	var tips = document.createElement('div');
	tips.setAttribute('style', 'width:100%;background:#d6f1ff;color:#03A9F4;font-size:14px;padding:5px 8px;position: fixed;left: 0;top: 0;z-index: 2147483647;');
	tips.innerHTML = msg;
	var close = function() {
		tips && tips.parentNode === body && body.removeChild(tips);
	};
	if (btn) {
		var btnElem = document.createElement('div');
		btnElem.setAttribute('style', 'margin-left:10px;background: #03a9f4;border-radius: 5px;color: #fff;text-align: center;padding: 1px 10px;font-size: 13px;display: inline-block;vertical-align: 1px;');
		btnElem.innerText = btn.text;
		btnElem.addEventListener('click', function() {
			btn.callback && btn.callback({
				close: close
			});
		});
		tips.appendChild(btnElem);
	}
	var body = document.body;
	body.appendChild(tips);
	if (time) {
		setTimeout(function() {
			close();
		}, time);
	}
};

module.exports = {
	show: showTips
};