var MD5 = require("blueimp-md5");
var Api = require('./api');
var Util = require('./util');
var CITYCODE = '000';
var Store = function() {};
Store.setItem = function(key, value) {
	return window.localStorage.setItem('LIFE-' + key, value);
};
Store.getItem = function(key) {
	return window.localStorage.getItem('LIFE-' + key);
};
Store.removeItem = function(key) {
	return window.localStorage.removeItem('LIFE-' + key);
};
Store.getVersion = function(callback) {
	mui.plusReady(function() {
		callback && callback(plus.runtime.version); //TODO runtime.version目前可用，但API有歧义
	});
};
Store.getTabbar = function(index) {
	return [{
		className: 'life-home',
		title: '生活助手',
		parentTitle: '生活助手',
		active: index === 0,
		webview: {
			id: 'home',
			url: 'home.html',
			show: true,
			styles: {
				top: mui && mui.supportStatusbarOffset() ? '70px' : '50px',
				bottom: '67px'
			}
		}
	}, {
		className: 'life-service',
		title: '服务列表',
		parentTitle: '服务列表',
		active: index === 1,
		webview: {
			id: 'service',
			url: 'service.html',
			show: false,
			styles: {
				top: mui && mui.supportStatusbarOffset() ? '70px' : '50px',
				bottom: '67px'
			}
		}
	}, {
		className: 'life-my',
		title: '我的生活',
		parentTitle: '',
		active: index === 2,
		webview: {
			id: 'my',
			url: 'my.html',
			show: true,
			preload: true,
			styles: {
				top: '0px',
				bottom: '67px'
			}
		}
	}]
};
Store.getLocation = function(callback) {
	var location = Store.getItem('LOCATION');
	if (location) { //本地上次缓存
		location = JSON.parse(location);
		CITYCODE = location.citycode;
		Api.getLocation(function(response) {
			if (response && !response.errno) {
				CITYCODE = response.data.citycode;
				Store.setItem('LOCATION', JSON.stringify(response.data));
				if (CITYCODE != location.citycode) {
					//TODO 刷新当前页面
				}
			}
			Util.DEBUG && Util.D('LATEST CITYCODE:' + CITYCODE);
		});
		Util.DEBUG && Util.D('LOCAL CITYCODE:' + CITYCODE);
		callback && callback(location);
	} else { //无缓存直接请求
		Api.getLocation(function(response) {
			if (response && !response.errno) {
				CITYCODE = response.data.citycode;
				Store.setItem('LOCATION', JSON.stringify(response.data));
				callback && callback(response.data);
			} else {
				callback && callback(false);
			}
			Util.DEBUG && Util.D('FIRST CITYCODE:' + CITYCODE);
		});
	}
};
Store._getRecommends = function(recommends) {
	return Store.getItem('RECOMMENDS');
};
Store._setRecommends = function(recommends) {
	return Store.setItem('RECOMMENDS', JSON.stringify(recommends));
};
Store.getRecommends = function(count) {
	var recommends = Store._getRecommends();
	var result = [];
	if (recommends) {
		recommends = JSON.parse(recommends);
		for (var id in recommends) {
			if (recommends.hasOwnProperty(id)) {
				result.push(recommends[id]);
			}
		}
		result.sort(function(a, b) {
			return a.hits < b.hits;
		});
		if (count) {
			return result.slice(0, count);
		} else {
			return result;
		}
	} else {
		result = [];
	}
	return recommends;
};
Store.recommend = function(service_id) {
	var recommends = Store._getRecommends();
	if (recommends) {
		recommends = JSON.parse(recommends);
		var service = recommends[service_id];
		if (service) {
			service.hits = service.hits + 1;
			Store._setRecommends(recommends);
		}
	}
};

Store.saveRecommends = function(services) {
	var recommends = Store._getRecommends();
	if (recommends) { //已存在
		recommends = JSON.parse(recommends);
		services.forEach(function(service) {
			if (!recommends[service.service_id]) {
				recommends[service.service_id] = service;
			}
		});
	} else { //不存在
		recommends = {};
		services.forEach(function(service) {
			service.hits = 0;
			recommends[service.service_id] = service;
		});
	}
	Store._setRecommends(recommends);
};
Store.getAllServer = function(callback) {
	Api.getAllServer(function(response) {
		if (response) {
			var globalData = response['000'];
			var data = response[CITYCODE];
			if (!data) {
				data = [];
			}
			data = data.concat(globalData);
			// 去除data中重复数据
			var tempObj = {};
			var tempdata = [];
			data.forEach(function(item, index) {
				if (!tempObj[item['service_id']]) {
					tempdata.push(item);
					tempObj[item['service_id']] = 1;
				}
			})
			data = tempdata;
			var result = [];
			data.forEach(function(item, index) {
				if (Util.handleService(item)) {
					result.push(item);
				}
			});
			result = result.slice(0, 7);
			Store.saveRecommends(result); //储存推荐列表
			result.push({
				openmode: 'more',
				service_icon: 'img/more.png',
				service_sname: '更多服务',
				service_id: 'all',
				service_url: ''
			});
			callback && callback(result);
		} else {
			callback && callback([]);
		}
	});
};

Store.getBanner = function(callback) {
	Api.getBanner(function(response) {
		if (response) {
			var banners = response.filter(function(item) {
				if (Util.handleActivity(item)) {
					if (item.citycode) {
						var allCitys = item.citycode.split(',');
						if (!~allCitys.indexOf(CITYCODE)) {
							return;
						}
					}
					return item;
				}
			});
			callback && callback(banners);
		} else {
			callback && callback([]);
		}
	});
};
Store.getActivity = function(callback) {
	Api.getActivity(function(response) {
		if (response) {
			var activities = response.filter(function(item) {
				return Util.handleActivity(item);
			});
			callback && callback(activities);
		} else {
			callback && callback([]);
		}
	});
};
Store.getHotServer = function(callback) {
	Api.getHotServer(function(response) {
		if (response) {
			response = response.filter(function(item) {
				return Util.handleHotService(item);
			})
		}
		callback && callback(response);
	});
};

Store.getServerList = function(callback) {
	Api.getServerList(function(response) {
		if (response) {
			callback && callback(Util.handleServiceList(response));
		} else {
			callback && callback(false);
		}

	});
};
Store.isLogin = function() {
	return !!Store.getUser();
};
Store.hasPhone = function() {
	var user = Store.getUser();
	if (user && !user.isExistPhone) {
		return false;
	}
	return true;
};
/**
 *area: "",
 *avatar: "http://u1.qhimg.com/qhimg/quc/48_48/22/02/55/220255dq9816.3eceac.jpg?f=08158bf9f09b919790a63f10c381be52",
 *id: "47785038"
 *isExistPhone:false,
 *name: "fxy060608",
 *nick: "",
 *sex: "未知"
 */
Store.getUser = function() {
	var user = Store.getItem('USER');
	if (user) {
		user = JSON.parse(user);
		if (user.accountdata) {
			var account = user.accountdata;
			return {
				area: '',
				avatar: account.key_avatorurl,
				id: '',
				isExistPhone: account.key_secmobile ? true : false,
				name: account.key_username,
				nick: account.key_nickname,
				sex: '未知'
			}
		}
		return user;
	}
	return false;
};
Store.updateUser = function() {
	var user = Store.getItem('USER');
	if (user) {
		user = JSON.parse(user);
		user.isExistPhone = true;
		Store.setUser(user);
		return user;
	}
	return false;
};
Store.setUser = function(user) {
	Store.setItem('USER', JSON.stringify(user));
};
Store.clearUser = function() {
	Store.removeItem('USER');
};

Store.showFreeTips = function() {
	var show = false;
	if (Util.supportFree() && Store.getFreeTrafficTipsFlag() === 1) { //支持免流量且还没有提示过
		if (Util.isCellular()) { //2G,3G,4G
			show = true;
		}
	}
	return show;
};
Store.getMonthTips = function(expire) {
	Util.DEBUG && Util.D('FREETRAFFICINFO_MONTH:' + expire);
	//当前流量过期时间作为标识
	return Store.getItem('FREETRAFFICINFO_MONTH_' + expire);
};
Store.setMonthTips = function(expire) {
	//当前流量过期时间作为标识
	return Store.setItem('FREETRAFFICINFO_MONTH_' + expire, 'TRUE');
};
Store.getFreeTrafficInfo = function() {
	var freeInfo = Store.getItem('FREETRAFFICINFO');
	if (freeInfo) {
		return JSON.parse(freeInfo);
	}
	return false;
};
Store.setFreeTrafficInfo = function(freeInfo) {
	Store.setItem('FREETRAFFICINFO', JSON.stringify(freeInfo));
};
Store.getFreeTrafficTipsFlag = function() {
	var flag = Store.getItem('FREETRAFFICTIPS_FLAG');
	if (!flag) {
		flag = 0;
	}
	flag = parseInt(flag);
	return isNaN(flag) ? 0 : flag;
};
Store.setFreeTrafficTipsFlag = function(flag) {
	if (typeof flag === 'undefined') {
		var flag = Store.getFreeTrafficTipsFlag() + 1;
		if (flag >= 6) { //启动够6次，重置
			flag = 1;
		}
	}
	return Store.setItem('FREETRAFFICTIPS_FLAG', flag);
};
Store.fetchTipsDomainList = function(callback) {
	Api.getTipsDomainList(function(response) {
		Util.DEBUG && Util.D('tips domain list:' + JSON.stringify(response));
		Store.setItem('TIPS_DOMAINLIST', JSON.stringify(response));
		callback && callback(response);
	});
};
Store.getTipsDomainList = function() {
	var domainList = Store.getItem('TIPS_DOMAINLIST');
	if (domainList) {
		return JSON.parse(domainList);
	}
	return false;
};
Store.fetchRuleByDomain = function(domain, callback) {
	Api.getRuleByDomain(domain, function(response) { //读取远程rule
		Util.DEBUG && Util.D('[' + domain + '] rule:' + JSON.stringify(response));
		Store.setItem('TIPS_RULES_' + MD5(domain), JSON.stringify(response));
		callback && callback(response);
	});
};
Store.getRuleByDomain = function(domain, url, callback) {
	var key = 'TIPS_RULES_' + MD5(domain);
	var rules = Store.getItem(key);
	if (rules) { //已存在
		rules = JSON.parse(rules);
		for (var i = 0, len = rules.length; i < len; i++) {
			var rule = rules[i];
			var ruleExp = rule['rule'];
			var js = rule['js_content'];
			if (ruleExp && js) {
				if (new RegExp(ruleExp).test(url)) {
					callback && callback(js);
					return;
				}
			}
		}
		callback && callback(false);
	} else { //不存在
		var domainList = Store.getTipsDomainList();
		if (domainList) {
			if (~domainList.indexOf(domain)) {
				Store.fetchRuleByDomain(domain, function() {
					Store.getRuleByDomain(domain, url, callback);
				});
			} else {
				Util.DEBUG && Util.D('[' + domain + '] is not in the blacklist' + JSON.stringify(domainList));
			}
		}
	}

	return false;
};
Store.fetchFlowStatus = function() {
	Api.getFlowStatus(function(response) {
		if (response && !response.errno) {
			Store.setItem('FLOW_STATUS', JSON.stringify(response.data));
		}
	});
};
Store.getFlowStatus = function() {
	var flowStatus = Store.getItem('FLOW_STATUS');
	if (flowStatus) {
		return JSON.parse(flowStatus);
	}
	return false;
};
Store.getFreeTrafficBalance = function() {
	var balance = Store.getItem('FREETRAFFIC_BALANCE');
	if (balance) {
		return parseInt(balance) || 0;
	}
	return 0;
};
Store.setFreeTrafficInfoBalance = function(balance) {
	return Store.setItem('FREETRAFFIC_BALANCE', balance + '');
};
Store.setFlowTree = function(flowTree) {
	return Store.setItem('FLOW_TREE_STATUS', flowTree + '');
};
Store.getFlowTree = function() {
	return Store.getItem('FLOW_TREE_STATUS');
};
module.exports = Store;