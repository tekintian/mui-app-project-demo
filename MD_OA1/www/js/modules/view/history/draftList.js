define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var currentWindow;
	var nextIndex = 0;
	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		window.setTimeout(function() {
			loadData(function() {
				currentWindow.endPullToRefresh();
			});
		}, 500);
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};
	bindEvent = function() {
		var shareUL = $('.list[dir="draft"]', '.workData');
		if (shareUL) {
			$common.touchSE($('li', shareUL), function(event, startTouch, o) {}, function(event, o) {
				$nativeUIManager.confactionSheetirm('请选择上传方式操作', '取消', [{
						title: '编辑'
					}, {
						title: '删除'
					}],
					function(index) {
						if (index > 0) {
							if (index == 1) {
								var payYn = $userInfo.get('payYn');
								var diffDay = $userInfo.get('diffDay');
								var tryDay = $userInfo.get('tryDay');
								var over = true;
								if (payYn == 'N') {
									if (tryDay) {
										tryDay = parseInt(tryDay);
										if (tryDay >= 0) {
											over = false;
										}
									}
								} else if (payYn == 'Y') {
									if (diffDay) {
										diffDay = parseInt(diffDay);
										if (diffDay >= 0) {
											over = false;
										}
									}
								}
								if (!over) {
									var reqId = $(o).attr('reqId');
									var applyId = $(o).attr('dir');
									if (reqId && applyId) {
										window.setTimeout(function() {
											$windowManager.create('apply_head', '../apply/head.html?applyId=' + applyId + '&draftId=' + reqId + '&reqId=' + reqId, false, true, function(show) {
												show();
											});
										}, 100);
									}
								} else {
									if (payYn == 'Y') {
										$nativeUIManager.alert('提示', '您的明道OA已到期,暂时不能提交新的申请', 'OK', function() {});
									} else if (payYn == 'N') {
										$nativeUIManager.alert('提示', '您的明道OA已试用结束,暂时不能提交新的申请', 'OK', function() {});
									}
								}
							} else if (index == 2) {
								$nativeUIManager.confirm('提示', '你确定删除申请?', ['确定', '取消'], function() {
									var reqId = $(o).attr('reqId');
									if (reqId) {
										$nativeUIManager.watting('请稍后...');
										$common.refreshToken(function(token) {
											$.ajax({
												type: 'POST',
												url: $common.getRestApiURL()+'/wf/req/delete',
												dataType: 'json',
												data: {
													id: reqId,
													'org.guiceside.web.jsp.taglib.Token': token
												},
												success: function(jsonData) {
													if (jsonData) {
														if (jsonData['result'] == '0') {
															$nativeUIManager.wattingTitle('删除成功!');
															window.setTimeout(function(){
																$windowManager.reload();
																$nativeUIManager.wattingClose();
															},1500);
														} else {
															$nativeUIManager.wattingClose();
															$nativeUIManager.alert('提示', '删除失败,请稍后再试!', 'OK', function() {});
														}
													}
												},
												error: function(jsonData) {
													$nativeUIManager.wattingClose();
													$nativeUIManager.alert('提示', '删除失败,请稍后再试!', 'OK', function() {});
												}
											});
										});
									}
								}, function() {});
							}
						}
					});
			});
			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/screen/req/draftData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						var dataList = jsonData['dataList'];
						var sb = new StringBuilder();
						if (dataList && $(dataList).size() > 0) {
							var batchFlag = false;
							if (!append) {
								sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
									typeText: '草稿箱',
									dir: 'draft'
								}));
							}
							sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
								dir: 'draft'
							}));
							$(dataList).each(function(i, o) {
								var desc = '<span class="t_wait">于</span> ' + o['dateTime'] +
									' <span class="t_wait">发起</span> ';
								sb.append(String.formatmodel($templete.getWorkData(batchFlag, 'N'), {
									applyName: o['subject'],
									desc: desc,
									uid: o['id'],
									reqId: o['id'],
									quickPreview: 'N',
									backYn: 'N',
									dir: o['applyId']
								}));
							});
							sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
						} else {
							sb.append(String.formatmodel($templete.getBankData(), {
								img: '../../img/nodata.png'
							}));
						}
						if (append) {
							$('.workData').append(sb.toString());
						} else {
							$('.workData').empty().append(sb.toString());
						}
						nextIndex = 0;
						$('.workData').attr('nextIndex', 0);
						var page = jsonData['page'];
						if (page) {
							if (page['hasNextPage'] == true) {
								$('.workData').attr('nextIndex', page['nextIndex']);
							}
						}
						bindEvent();
						pullToRefreshEvent();
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});