define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $controlWindow = require('manager/controlWindow');
	var $statusbarResetManager = require('manager/statusbarReset');
	var immersed = 0;
	loadData = function() {
		$('#attName').text($userInfo.get('attData_fileName'));
		var type = $userInfo.get('attData_type');
		if (type) {
			if (type == 'img') {
				var topOffset = immersed + 45;
				var imgViewWindow = plus.webview.create('attImgPreview.html', "imgView", {
					top: topOffset + "px",
					bottom: "0px",
					scalable: true
				});
				imgViewWindow.addEventListener("loaded", function() {
					$windowManager.current().append(imgViewWindow);
				}, false);
			} else if (type == 'doc' || type == 'docx' || type == 'xls' || type == 'xlsx' || type == 'ppt' || type == 'pptx' || type == 'pdf') {
				var onlineURL = 'https://docview.mingdao.com/op/view.aspx?src=' + encodeURIComponent($userInfo.get('attData_url'));
				$common.switchOS(function(){
					onlineURL =  $userInfo.get('attData_url');
				},function(){
					onlineURL = 'https://docview.mingdao.com/op/view.aspx?src=' + encodeURIComponent($userInfo.get('attData_url'));
					$nativeUIManager.watting('正在加载附件...');
				});
				var topOffset = immersed + 45;
				var officeViewWindow = plus.webview.create(onlineURL, "officeView", {
					top: topOffset + "px",
					bottom: "0px",
					scrollIndicator: 'vertical'
				});
				officeViewWindow.addEventListener("loaded", function() {
					$nativeUIManager.wattingClose();
					$windowManager.current().append(officeViewWindow);
				}, false);
			} else {
				$nativeUIManager.watting('改附件类型无法在手机上查看', 1000);
			}
		}
	};
	plusReady = function() {
		immersed = $statusbarResetManager.getStatusbarHeight();
		if (immersed) {
			$('header').css({
				paddingTop: immersed + 'px'
			});
			$('.main').css({
				top: 45 + immersed + 'px'
			});
		}
		loadData();
		$common.androidBack(function() {
			$userInfo.removeItem('attData_fileName');
			$userInfo.removeItem('attData_url');
			$userInfo.removeItem('attData_type');
			$controlWindow.attListWindowShow();
			$windowManager.close('slide-out-right');
		});
		$common.touchSE($('#backAction'), function(event, startTouch, o) {
			if (!$(o).hasClass('active')) {
				$(o).addClass('active');
			}
		}, function(event, o) {
			if ($(o).hasClass('active')) {
				$userInfo.removeItem('attData_fileName');
				$userInfo.removeItem('attData_url');
				$userInfo.removeItem('attData_type');
				$controlWindow.attListWindowShow();
				$windowManager.close('slide-out-right');
				$(o).removeClass('active');
			}
		});
		$controlWindow.attListWindowHide();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});