define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $controlWindow = require('manager/controlWindow');
	var hashMap = new HashMap();
	var initPhotoSwipeFromDOM = function(gallerySelector) {

		// parse slide data (url, title, size ...) from DOM elements 
		// (children of gallerySelector)
		var parseThumbnailElements = function(el) {
			var thumbElements = el.childNodes,
				numNodes = thumbElements.length,
				items = [],
				figureEl,
				childElements,
				linkEl,
				size,
				item;

			for (var i = 0; i < numNodes; i++) {

				figureEl = thumbElements[i]; // <figure> element

				// include only element nodes 
				if (figureEl.nodeType !== 1) {
					continue;
				}

				linkEl = figureEl.children[0]; // <a> element

				size = linkEl.getAttribute('data-size').split('x');

				// create slide object
				item = {
					src: linkEl.getAttribute('href'),
					w: parseInt(size[0], 10),
					h: parseInt(size[1], 10)
				};

				if (figureEl.children.length > 1) {
					// <figcaption> content
					item.title = figureEl.children[1].innerHTML;
				}

				if (linkEl.children.length > 0) {
					// <img> thumbnail element, retrieving thumbnail url
					item.msrc = linkEl.children[0].getAttribute('src');
				}

				item.el = figureEl; // save link to element for getThumbBoundsFn
				items.push(item);
			}

			return items;
		};

		// find nearest parent element
		var closest = function closest(el, fn) {
			return el && (fn(el) ? el : closest(el.parentNode, fn));
		};

		// triggers when user clicks on thumbnail
		var onThumbnailsClick = function(e) {
			e = e || window.event;
			e.preventDefault ? e.preventDefault() : e.returnValue = false;

			var eTarget = e.target || e.srcElement;

			var clickedListItem = closest(eTarget, function(el) {
				return el.tagName === 'FIGURE';
			});

			if (!clickedListItem) {
				return;
			}

			// find index of clicked item
			var clickedGallery = clickedListItem.parentNode,
				childNodes = clickedListItem.parentNode.childNodes,
				numChildNodes = childNodes.length,
				nodeIndex = 0,
				index;

			for (var i = 0; i < numChildNodes; i++) {
				if (childNodes[i].nodeType !== 1) {
					continue;
				}

				if (childNodes[i] === clickedListItem) {
					index = nodeIndex;
					break;
				}
				nodeIndex++;
			}

			if (index >= 0) {
				openPhotoSwipe(index, clickedGallery);
			}
			return false;
		};

		// parse picture index and gallery index from URL (#&pid=1&gid=2)
		var photoswipeParseHash = function() {
			var hash = window.location.hash.substring(1),
				params = {};

			if (hash.length < 5) {
				return params;
			}

			var vars = hash.split('&');
			for (var i = 0; i < vars.length; i++) {
				if (!vars[i]) {
					continue;
				}
				var pair = vars[i].split('=');
				if (pair.length < 2) {
					continue;
				}
				params[pair[0]] = pair[1];
			}

			if (params.gid) {
				params.gid = parseInt(params.gid, 10);
			}

			if (!params.hasOwnProperty('pid')) {
				return params;
			}
			params.pid = parseInt(params.pid, 10);
			return params;
		};

		var openPhotoSwipe = function(index, galleryElement, disableAnimation) {
			var pswpElement = document.querySelectorAll('.pswp')[0],
				gallery,
				options,
				items;

			items = parseThumbnailElements(galleryElement);

			// define options (if needed)
			options = {
				index: index,

				// define gallery index (for URL)
				galleryUID: galleryElement.getAttribute('data-pswp-uid'),

				getThumbBoundsFn: function(index) {
					// See Options -> getThumbBoundsFn section of docs for more info
					var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
						pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
						rect = thumbnail.getBoundingClientRect();

					return {
						x: rect.left,
						y: rect.top + pageYScroll,
						w: rect.width
					};
				},

				// history & focus options are disabled on CodePen
				// remove these lines in real life: 
				historyEnabled: false,
				focus: false

			};

			if (disableAnimation) {
				options.showAnimationDuration = 0;
			}

			// Pass data to PhotoSwipe and initialize it
			gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
			gallery.init();
		};

		// loop through all gallery elements and bind events
		var galleryElements = document.querySelectorAll(gallerySelector);

		for (var i = 0, l = galleryElements.length; i < l; i++) {
			galleryElements[i].setAttribute('data-pswp-uid', i + 1);
			galleryElements[i].onclick = onThumbnailsClick;
		}

		// Parse URL and open gallery if it contains #&pid=3&gid=1
		var hashData = photoswipeParseHash();
		if (hashData.pid > 0 && hashData.gid > 0) {
			openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
		}
	};

	loadData = function() {
		$nativeUIManager.watting('加载中...');
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/common/common/buildAtt',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				attData: $userInfo.get('attData'),
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						var att = jsonData['att'];
						if (att) {
							var reqAtt = att['reqAtt'];
							var taskAtt = att['taskAtt'];
							if (reqAtt && $(reqAtt).size() > 0) {
								var imgCount = 0;
								var attCount=0;
								$(reqAtt).each(function(i, o) {
									var icon_postfix = o['postfix'];
									if (icon_postfix == 'jpeg' || icon_postfix == 'jpg' || icon_postfix == 'gif' || icon_postfix == 'bmp' || icon_postfix == 'png') {
										icon_postfix = 'img';
									}
									if (icon_postfix == 'img') {
										attCount+=1;
										var imgOriginal = o['imgOriginalPreview'];
										var imgPreview = o['imgPreview'];
										imgReady(imgOriginal, function() {}, function() {
											var size = this.width + 'x' + this.height;
											hashMap.put(o['fileKey'], size);
											imgCount++;
											console.info('imgOriginal成功' + imgCount);
										}, function() {
											$nativeUIManager.watting('图片加载失败', 1000);
										});
										imgReady(imgPreview, function() {}, function() {
											imgCount++;
											console.info('imgPreview成功' + imgCount);
										}, function() {
											$nativeUIManager.watting('图片加载失败', 1000);
										});
									}
								});
								var timer = window.setInterval(function() {
									if (imgCount == attCount * 2) {
										var sb = new StringBuilder();
										$(reqAtt).each(function(i, o) {
											var icon_postfix = o['postfix'];
											if (icon_postfix == 'jpeg' || icon_postfix == 'jpg' || icon_postfix == 'gif' || icon_postfix == 'bmp' || icon_postfix == 'png') {
												icon_postfix = 'img';
											}
											if (icon_postfix == 'img') {
												sb.append(String.formatmodel($templete.figureDIV(), {
													fileName: o['fileName'] + '.' + o['postfix'],
													dataSize: hashMap.get(o['fileKey']),
													imgOriginal: o['imgOriginalPreview'],
													imgPreview: o['imgPreview']
												}));
											}
										});
										$('.my-simple-gallery').append(sb.toString());
										$nativeUIManager.wattingClose();
										window.clearInterval(timer);
									}
								}, 1000);
							} else if (taskAtt && $(taskAtt).size() > 0) {
								var imgCount = 0;
								var attCount=0;
								$(taskAtt).each(function(i, o) {
									var icon_postfix = o['postfix'];
									if (icon_postfix == 'jpeg' || icon_postfix == 'jpg' || icon_postfix == 'gif' || icon_postfix == 'bmp' || icon_postfix == 'png') {
										icon_postfix = 'img';
									}
									if (icon_postfix == 'img') {
										attCount+=1;
										var imgOriginal = o['imgOriginalPreview'];
										var imgPreview = o['imgPreview'];
										imgReady(imgOriginal, function() {}, function() {
											var size = this.width + 'x' + this.height;
											hashMap.put(o['fileKey'], size);
											imgCount++;
											console.info('imgOriginal成功' + imgCount);
										}, function() {
											$nativeUIManager.watting('图片加载失败', 1000);
										});
										imgReady(imgPreview, function() {}, function() {
											imgCount++;
											console.info('imgPreview成功' + imgCount);
										}, function() {
											$nativeUIManager.watting('图片加载失败', 1000);
										});
									}
								});
								var timer = window.setInterval(function() {
									if (imgCount == attCount * 2) {
										var sb = new StringBuilder();
										$(taskAtt).each(function(i, o) {
											var icon_postfix = o['postfix'];
											if (icon_postfix == 'jpeg' || icon_postfix == 'jpg' || icon_postfix == 'gif' || icon_postfix == 'bmp' || icon_postfix == 'png') {
												icon_postfix = 'img';
											}
											if (icon_postfix == 'img') {
												sb.append(String.formatmodel($templete.figureDIV(), {
													fileName: o['fileName'] + '.' + o['postfix'],
													dataSize: hashMap.get(o['fileKey']),
													imgOriginal: o['imgOriginalPreview'],
													imgPreview: o['imgPreview']
												}));
											}
										});
										$('.my-simple-gallery').append(sb.toString());
										$nativeUIManager.wattingClose();
										window.clearInterval(timer);
									}
								}, 1000);
							} else {
								$nativeUIManager.wattingTitle('没有附件');
								window.setTimeout(function() {
									$nativeUIManager.wattingClose();
								}, 1500);
							}
						} else {
							$nativeUIManager.wattingTitle('没有附件');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1500);
						}
					} else {
						$nativeUIManager.wattingTitle('未知错误');
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$nativeUIManager.wattingTitle('未知错误');
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
				}, 1500);
			}
		});
		// execute above function
		initPhotoSwipeFromDOM('.my-simple-gallery');
	};
	plusReady = function() {
		loadData();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});