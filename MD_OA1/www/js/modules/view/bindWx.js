define(function(require, exports, module) {
	var $common = require('core/common');
	var $windowManager = require('manager/window');
	var $nativeUIManager = require('manager/nativeUI');
	var $authorize = require('core/authorize');
	var $userInfo = require('core/userInfo');
	var $statusbarResetManager = require('manager/statusbarReset');
	var auths = null;
	var queryMap = parseURL();
	var unionId = queryMap.get('unionId');
	var state = queryMap.get('state');
	bindWx = function(account, password, wxID, s) {
		$nativeUIManager.watting('正在验证帐户...', false);
		$authorize.bindWx(account, password, wxID, s, function(jsonObj) {
			if (jsonObj['result'] == '0') {
				$nativeUIManager.wattingTitle('绑定成功');
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					$windowManager.close('slide-out-right');
					$windowManager.getLaunchWindow().evalJS('wxLogin(1)');
				}, 1000);
			} else {
				$nativeUIManager.wattingTitle('绑定失败');
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
				}, 1000);
			}
		}, function(code) {
			if (code) {
				if (code == 'unkown') {
					$nativeUIManager.wattingTitle('身份验证失败');
				} else if (code == 'network') {
					$nativeUIManager.wattingTitle('网络异常,未知的域名');
				} else {
					$nativeUIManager.wattingTitle(code);
				}
			}
			window.setTimeout(function() {
				$nativeUIManager.wattingClose();
			}, 1000);
		});
	};
	init = function() {
		if ($userInfo.isSupport()) {
			if (unionId) {
				$('#unionId').val(unionId);
			}
			if (state) {
				$('#state').val(state);
			}
			if ($('#account').val() == '') {
				$('#account').val($userInfo.get('account'));
			}
			$('input', '.login_box').each(function() {
				if ($(this).val() != '') {
					$(this).next().hide();
				}
			});
			$('input', '.login_box').off("blur").on('blur', function() {
				if (!$(this).val()) {
					$(this).next().show();
				}
			});
			$('input', '.login_box').off("focus").on('focus', function() {
				$(this).next().hide();
			});


			$common.touchSE($('#bindBtn'), function(event, startTouch, o) {}, function(event, o) {
				if ($.trim($('#account').val()).length > 0 && $.trim($('#password').val()).length > 0 && $.trim($('#unionId').val()).length > 0) {
					bindWx($.trim($('#account').val()), $.trim($('#password').val()), $.trim($('#unionId').val()), $.trim($('#state').val()));
				} else {
					$nativeUIManager.watting('请输入用户名称和密码', 1500);
				}
			});
		}
	};

	plusReady = function() {
		var immersed = $statusbarResetManager.getStatusbarHeight();
		if (immersed) {
			$('header').css({
				paddingTop: immersed + 'px'
			});
		}
		init();
		$nativeUIManager.watting('请输入明道帐号进行绑定', 1500);

		$common.androidBack(function() {
			$windowManager.close('slide-out-right');
		});
		$common.touchSE($('#backAction'), function(event, startTouch, o) {
			if (!$(o).hasClass('active')) {
				$(o).addClass('active');
			}
		}, function(event, o) {
			if ($(o).hasClass('active')) {
				$windowManager.close('slide-out-right');
				$(o).removeClass('active');
			}
		});
		plus.navigator.closeSplashscreen();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});