define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $nativeUIManager = require('manager/nativeUI');
	var $pushManager = require('manager/push');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	bindEvent = function() {
		$common.touchSE($('#infoAction'), function(event, startTouch, o) {}, function(event, o) {
			$windowManager.create('logout', '../logout.html', false, true, function(show) {
				show();
			});
		});
	};
	plusReady = function() {
		
		$sideManager.loadDataType(function(loadPage){
			var worklist = plus.webview.create(loadPage, "platform_list", {
				top: "80px",
				bottom: "0px",
				scrollIndicator: 'vertical'
			});
			if (worklist) {
				worklist.addEventListener("loaded", function() {
					$windowManager.current().append(worklist);
				}, false);
			}
		});
		bindEvent();
		$('#userImg').attr('src', $userInfo.get('userImg'));
		$('#userName').text($userInfo.get('userName'));
		$('#companyName').text($userInfo.get('companyName'));
		$pushManager.connect(function(type, id, reqId) {
			$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
				show();
			});
		}, function() {
		});
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});