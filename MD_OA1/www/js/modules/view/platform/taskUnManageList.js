define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var $updateManager = require('manager/update');
	var currentWindow;
	var nextIndex = 0;
	var queryMap = parseURL();
	var orderBy = queryMap.get('orderBy');
	var onRefreshSuccess=false;
	viewCallback = function(type, id) {
		processCallback(type, id);
	};
	remind = function(reqId) {
		if (reqId) {
			$nativeUIManager.watting('正在发送提醒...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL()+'/wf/req/remind',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: reqId,
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									$nativeUIManager.wattingTitle('提醒发送成功!');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('提醒失败,请稍后再试!');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		if(!onRefreshSuccess){
			onRefreshSuccess=true;
			$sideManager.loadDataType(function(loadPage) {
				loadData(function() {
					currentWindow.endPullToRefresh();
					var listCount = $('p.list_tilte', '.workData').size();
					if (listCount == 0) {
						$windowManager.loadOtherWindow('platform_list', loadPage);
					} else {
						$sideManager.builder('taskTypeCount', 'taskUnManageCount');
					}
					onRefreshSuccess=false;
				});
			});
		}
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};
	bindEvent = function() {
		var reqUL = $('.list[dir="task"]', '.workData');
		if (reqUL) {
			$common.touchSME($('li', reqUL),
				function(startX, startY, endX, endY, event, startTouch, o) {},
				function(startX, startY, endX, endY, event, moveTouch, o) {}, function(startX, startY, endX, endY, event, o) {
					if (startX == endX && startY == endY) {
						if (!$(o).hasClass('active')) {
							var reqId = $(o).attr('reqId');
							if (reqId) {
								$(o).addClass('active');
								var remindYn = $(o).attr('remindYn');
								if (remindYn) {
									window.setTimeout(function() {
										$(o).removeClass('active');
										if (remindYn == 'Y') {
											$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
													title: '查看'
												}, {
													title: '提醒'
												}],
												function(index) {
													if (index > 0) {
														if (index == 1) {
															$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
																show();
															});
														} else if (index == 2) {
															remind(reqId);
															$(o).attr('remindYn','N');
														}
													}
												});
										} else if (remindYn == 'N') {
											$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
												show();
											});
										}
									}, 100);
								}
							}
						}
					}
				});
			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	bindData = function(jsonData, append) {
		var sb = new StringBuilder();
		var dataList = jsonData['dataList'];
		if (dataList && $(dataList).size() > 0) {
			var batchFlag = false;
			if (!append) {
				sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
					typeText: $userInfo.get('taskUnManageCount_'),
					dir: 'task'
				}));
			}

			sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
				dir: 'task'
			}));
			$(dataList).each(function(i, o) {
				var desc = '<span class="t_wait">于</span> ' + o['dateTime'] +
					' <span class="t_wait">发起</span> ';
				desc += '等待&nbsp;<span class="name width-48">' + o['manageName'] + '</span>&nbsp;<span class="t_wait">经办</span>';
				sb.append(String.formatmodel($templete.getWorkData(batchFlag, 'N'), {
					applyName: o['subject'] + ' <span class="t_wait">[' + o['reqNo'] + ']</span>',
					desc: desc,
					uid: o['id'],
					reqId: o['reqId'],
					quickPreview: 'N',
					backYn: 'N',
					remindYn: o['remindYn']
				}));
			});
			sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
		} else {
			sb.append(String.formatmodel($templete.getBankData(), {
				img: '../../img/nodata.png'
			}));
		}
		if (append) {
			$('.workData').append(sb.toString());
		} else {
			$('.workData').empty().append(sb.toString());
		}
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		var page = jsonData['page'];
		if (page) {
			if (page['hasNextPage'] == true) {
				$('.workData').attr('nextIndex', page['nextIndex']);
			}
			var totalRecord = page['totalRecord'];
			if (totalRecord || totalRecord == 0) {
				$userInfo.put('taskUnManageCount', totalRecord);
			}
		}
		bindEvent();
		pullToRefreshEvent();
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/taskUnManageListData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				orderBy:orderBy,
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						bindData(jsonData, append);
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
		if(orderBy){
			$userInfo.put('dataOrderBy',orderBy);
		}
		$updateManager.execute();
		$sideManager.builder('taskTypeCount', 'taskUnManageCount');
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});