define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var $updateManager = require('manager/update');
	var currentWindow;
	var nextIndex = 0;
	var queryMap = parseURL();
	var orderBy = queryMap.get('orderBy');
	var onRefreshSuccess=false;
	viewCallback = function(type, id) {
		processCallback(type, id);
	};
	processCallback = function(type, uid, manualCloseWatting) {
		var taskPassCount = $userInfo.get('taskPassCount');
		taskPassCount = parseInt(taskPassCount);
		var listItem = $('.list[dir="' + type + '"]');
		if (listItem) {
			var liObj = $(listItem).find('li[uid="' + uid + '"]');
			if (liObj) {
				if (taskPassCount > 0) {
					taskPassCount -= 1;
				}
				$userInfo.put('taskPassCount', taskPassCount);
				$(liObj).fadeOut(500, function() {
					$(liObj).remove();
					var listTitle = $('.list_tilte[dir="' + type + '"]');
					var count = $('li', listItem).size();
					if (count == 0) {
						$(listTitle).remove();
						$(listItem).remove();
						$('.workData').empty().append(String.formatmodel($templete.getBankData(), {
							img: '../../img/nodata.png'
						}));
					}
					if (!manualCloseWatting) {
						$nativeUIManager.wattingClose();
					}
				});
			}
		}
	};
	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		if(!onRefreshSuccess){
			onRefreshSuccess=true;
			$sideManager.loadDataType(function(loadPage) {
				loadData(function() {
					currentWindow.endPullToRefresh();
					var listCount = $('p.list_tilte', '.workData').size();
					if (listCount == 0) {
						$windowManager.loadOtherWindow('platform_list', loadPage);
					} else {
						$sideManager.builder('taskTypeCount', 'taskPassCount');
					}
					onRefreshSuccess=false;
				});
			});
		}
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};
	bindEvent = function() {
		var reqUL = $('.list[dir="task"]', '.workData');
		if (reqUL) {
			$common.touchSME($('li', reqUL),
				function(startX, startY, endX, endY, event, startTouch, o) {},
				function(startX, startY, endX, endY, event, moveTouch, o) {}, function(startX, startY, endX, endY, event, o) {
					if (startX == endX && startY == endY) {
						if (!$(o).hasClass('active')) {
							var reqId = $(o).attr('reqId');
							var taskId = $(o).attr('uid');
							if (reqId&&taskId) {
								$(o).addClass('active');
								var remindYn = $(o).attr('remindYn');
								if (remindYn) {
									window.setTimeout(function() {
										$(o).removeClass('active');
										processCallback('task', taskId);
										$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&taskId='+taskId+'&from=platform', false, true, function(show) {
											show();
										});
									}, 100);
								}
							}
						}
					}
				});
			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	bindData = function(jsonData, append) {
		var sb = new StringBuilder();
		var dataList = jsonData['dataList'];
		if (dataList && $(dataList).size() > 0) {
			var batchFlag = false;
			if (!append) {
				sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
					typeText: $userInfo.get('taskPassCount_'),
					dir: 'task'
				}));
			}

			sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
				dir: 'task'
			}));
			$(dataList).each(function(i, o) {
				var desc = '<span class="t_wait">于</span> ' + o['dateTime'] +
					' <span class="t_wait">发起</span> ';
				if (o['nodeType'] == "confirm") {
					desc += '已被&nbsp;<span class="name width-48">' + o['name'] + '</span>&nbsp;<span class="t_croose">确认经办结果</span>';
				} else if (o['nodeType'] == "manage") {
					desc += '已被&nbsp;<span class="name width-48">' + o['name'] + '</span>&nbsp;<span class="t_croose">经办完成</span>';
				} else if (o['nodeType'] == "task") {
					desc += '已被&nbsp;<span class="name width-48">' + o['name'] + '</span>&nbsp;<span class="t_croose">审批通过</span>';
				}

				sb.append(String.formatmodel($templete.getWorkData(batchFlag, 'N'), {
					applyName: o['subject'] + ' <span class="t_wait">[' + o['reqNo'] + ']</span>',
					desc: desc,
					uid: o['id'],
					reqId: o['reqId'],
					quickPreview: 'N',
					backYn: 'N',
					remindYn: 'N'
				}));
			});
			sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
		} else {
			sb.append(String.formatmodel($templete.getBankData(), {
				img: '../../img/nodata.png'
			}));
		}
		if (append) {
			$('.workData').append(sb.toString());
		} else {
			$('.workData').empty().append(sb.toString());
		}
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		var page = jsonData['page'];
		if (page) {
			if (page['hasNextPage'] == true) {
				$('.workData').attr('nextIndex', page['nextIndex']);
			}
			var totalRecord = page['totalRecord'];
			if (totalRecord || totalRecord == 0) {
				$userInfo.put('taskPassCount', totalRecord);
			}
		}
		bindEvent();
		pullToRefreshEvent();
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/taskPassListData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				orderBy:orderBy,
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						bindData(jsonData, append);
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
		if(orderBy){
			$userInfo.put('dataOrderBy',orderBy);
		}
		$updateManager.execute();
		$sideManager.builder('taskTypeCount', 'taskPassCount');
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});