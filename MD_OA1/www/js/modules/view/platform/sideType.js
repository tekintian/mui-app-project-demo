define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $windowManager = require('manager/window');
	var queryMap = parseURL();
	var currentTypeId = queryMap.get('typeId');
	var currentItemId = queryMap.get('itemId');

	function loadData() {
		$('ul', '#typeDIV').first().empty();
		var workPlatformTypeCount = $userInfo.get('workPlatformTypeCount');
		workPlatformTypeCount = parseInt(workPlatformTypeCount);
		if (workPlatformTypeCount > 0) {
			var waitingCount = $userInfo.get('waitingCount');
			waitingCount = parseInt(waitingCount);
			var reqTypeCount = $userInfo.get('reqTypeCount');
			reqTypeCount = parseInt(reqTypeCount);
			var taskTypeCount = $userInfo.get('taskTypeCount');
			taskTypeCount = parseInt(taskTypeCount);
			var manageTypeCount = $userInfo.get('manageTypeCount');
			manageTypeCount = parseInt(manageTypeCount);
			var shareTypeCount = $userInfo.get('shareTypeCount');
			shareTypeCount = parseInt(shareTypeCount);
			var itemName = '';
			var typeName = '';
			if (waitingCount > 0) {
				typeName = $userInfo.get('waitingCount_') + '&nbsp;';
				if (currentTypeId == 'waitingCount') {
					itemName = $userInfo.get(currentItemId + '_');
					if (itemName != '') {
						typeName += '&gt;&nbsp;' + itemName;
					}
				}
				$('ul', '#typeDIV').first().append(String.formatmodel($templete.getSideType(), {
					text: typeName,
					dir: 'waitingCount'
				}));
			}
			itemName = '';
			if (reqTypeCount > 0) {
				typeName = $userInfo.get('reqTypeCount_') + '&nbsp;';
				if (currentTypeId == 'reqTypeCount') {
					itemName = $userInfo.get(currentItemId + '_');
					if (itemName != '') {
						typeName += '&gt;&nbsp;' + itemName;
					}
				}

				$('ul', '#typeDIV').first().append(String.formatmodel($templete.getSideType(), {
					text: typeName,
					dir: 'reqTypeCount'
				}));
			}
			itemName = '';
			if (taskTypeCount > 0) {
				typeName = $userInfo.get('taskTypeCount_') + '&nbsp;';
				if (currentTypeId == 'taskTypeCount') {
					itemName = $userInfo.get(currentItemId + '_');
					if (itemName != '') {
						typeName += '&gt;&nbsp;' + itemName;
					}
				}

				$('ul', '#typeDIV').first().append(String.formatmodel($templete.getSideType(), {
					text: typeName,
					dir: 'taskTypeCount'
				}));
			}
			itemName = '';
			if (manageTypeCount > 0) {
				typeName = $userInfo.get('manageTypeCount_') + '&nbsp;';
				if (currentTypeId == 'manageTypeCount') {
					itemName = $userInfo.get(currentItemId + '_');
					if (itemName != '') {
						typeName += '&gt;&nbsp;' + itemName;
					}
				}

				$('ul', '#typeDIV').first().append(String.formatmodel($templete.getSideType(), {
					text: typeName,
					dir: 'manageTypeCount'
				}));
			}
			itemName = '';
			if (shareTypeCount > 0) {
				typeName = $userInfo.get('shareTypeCount_') + '&nbsp;';
				if (currentTypeId == 'shareTypeCount') {
					itemName = $userInfo.get(currentItemId + '_');
					if (itemName != '') {
						typeName += '&gt;&nbsp;' + itemName;
					}
				}

				$('ul', '#typeDIV').first().append(String.formatmodel($templete.getSideType(), {
					text: typeName,
					dir: 'shareTypeCount'
				}));
			}
		}
		bindEvent();
		$('#itemDIV').css({
			'width': $(document).width() + 'px'
		});
		$common.switchOS(function() {
			$('.rightInner').addClass('Ios_scroll');
		}, function() {});
	}

	function bindEvent() {
		$common.touchSE($('#closeSide'), function(event, startTouch, o) {}, function(event, o) {
			$windowManager.current().close();
		});
		$common.touchSE($('ul', '#typeDIV').first().find('li'), function(event, startTouch, o) {}, function(event, o) {
			var dir = $(o).attr('dir');
			if (dir) {
				$('ul', '#itemDIV').empty();
				if (dir == 'waitingCount') {
					var taskCount = $userInfo.get('taskCount');
					taskCount = parseInt(taskCount);
					var manageCount = $userInfo.get('manageCount');
					manageCount = parseInt(manageCount);
					var reqConfirmCount = $userInfo.get('reqConfirmCount');
					reqConfirmCount = parseInt(reqConfirmCount);
					if (taskCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('taskCount_') + '&nbsp;(' + taskCount + ')',
							dir: 'taskCount'
						}));
					}
					if (manageCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('manageCount_') + '&nbsp;(' + manageCount + ')',
							dir: 'manageCount'
						}));
					}
					if (reqConfirmCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('reqConfirmCount_') + '&nbsp;(' + reqConfirmCount + ')',
							dir: 'reqConfirmCount'
						}));
					}
				} else if (dir == 'reqTypeCount') {
					var reqCount = $userInfo.get('reqCount');
					reqCount = parseInt(reqCount);
					var reqPassCount = $userInfo.get('reqPassCount');
					reqPassCount = parseInt(reqPassCount);
					var reqRejectCount = $userInfo.get('reqRejectCount');
					reqRejectCount = parseInt(reqRejectCount);
					if (reqCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('reqCount_') + '&nbsp;(' + reqCount + ')',
							dir: 'reqCount'
						}));
					}
					if (reqPassCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('reqPassCount_') + '&nbsp;(' + reqPassCount + ')',
							dir: 'reqPassCount'
						}));
					}
					if (reqRejectCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('reqRejectCount_') + '&nbsp;(' + reqRejectCount + ')',
							dir: 'reqRejectCount'
						}));
					}
				} else if (dir == 'taskTypeCount') {
					var taskUnCompleteCount = $userInfo.get('taskUnCompleteCount');
					taskUnCompleteCount = parseInt(taskUnCompleteCount);
					var taskUnManageCount = $userInfo.get('taskUnManageCount');
					taskUnManageCount = parseInt(taskUnManageCount);
					var taskUnConfirmCount = $userInfo.get('taskUnConfirmCount');
					taskUnConfirmCount = parseInt(taskUnConfirmCount);
					var taskPassCount = $userInfo.get('taskPassCount');
					taskPassCount = parseInt(taskPassCount);
					var taskRejectCount = $userInfo.get('taskRejectCount');
					taskRejectCount = parseInt(taskRejectCount);
					if (taskUnCompleteCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('taskUnCompleteCount_') + '&nbsp;(' + taskUnCompleteCount + ')',
							dir: 'taskUnCompleteCount'
						}));
					}
					if (taskUnManageCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('taskUnManageCount_') + '&nbsp;(' + taskUnManageCount + ')',
							dir: 'taskUnManageCount'
						}));
					}
					if (taskUnConfirmCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('taskUnConfirmCount_') + '&nbsp;(' + taskUnConfirmCount + ')',
							dir: 'taskUnConfirmCount'
						}));
					}
					if (taskPassCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('taskPassCount_') + '&nbsp;(' + taskPassCount + ')',
							dir: 'taskPassCount'
						}));
					}
					if (taskRejectCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('taskRejectCount_') + '&nbsp;(' + taskRejectCount + ')',
							dir: 'taskRejectCount'
						}));
					}
				} else if (dir == 'manageTypeCount') {
					var manageUnConfirmCount = $userInfo.get('manageUnConfirmCount');
					manageUnConfirmCount = parseInt(manageUnConfirmCount);
					if (manageUnConfirmCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('manageUnConfirmCount_') + '&nbsp;(' + manageUnConfirmCount + ')',
							dir: 'manageUnConfirmCount'
						}));
					}
				} else if (dir == 'shareTypeCount') {
					var shareUnReadCount = $userInfo.get('shareUnReadCount');
					shareUnReadCount = parseInt(shareUnReadCount);
					if (shareUnReadCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('shareUnReadCount_') + '&nbsp;(' + shareUnReadCount + ')',
							dir: 'shareUnReadCount'
						}));
					}
					var shareUnCompleteCount = $userInfo.get('shareUnCompleteCount');
					shareUnCompleteCount = parseInt(shareUnCompleteCount);
					if (shareUnCompleteCount > 0) {
						$('ul', '#itemDIV').append(String.formatmodel($templete.getSideItem(), {
							text: $userInfo.get('shareUnCompleteCount_') + '&nbsp;(' + shareUnCompleteCount + ')',
							dir: 'shareUnCompleteCount'
						}));
					}
				}
				$('#typeDIV').css({
					'margin-left': '-100%'
				});
				$('li[dir="' + currentItemId + '"]', '#itemDIV').addClass('choosed');

				$common.touchSE($('li', '#itemDIV'), function(event, startTouch, o) {}, function(event, o) {
					if (!$(o).hasClass('choosed')) {
						$('li', '#itemDIV').removeClass('choosed');
						$(o).addClass('choosed');
						var dir = $(o).attr('dir');
						if (dir) {
							var loadPage = ''
							if (dir == 'reqCount') {
								loadPage = 'reqList.html';
							} else if (dir == 'reqPassCount') {
								loadPage = 'reqPassList.html';
							} else if (dir == 'reqRejectCount') {
								loadPage = 'reqRejectList.html';
							} else if (dir == 'taskCount') {
								loadPage = 'taskList.html';
							} else if (dir == 'manageCount') {
								loadPage = 'manageList.html';
							} else if (dir == 'reqConfirmCount') {
								loadPage = 'reqConfirmList.html';
							} else if (dir == 'taskUnCompleteCount') {
								loadPage = 'taskUnCompleteList.html';
							} else if (dir == 'taskUnManageCount') {
								loadPage = 'taskUnManageList.html';
							} else if (dir == 'taskUnConfirmCount') {
								loadPage = 'taskUnConfirmList.html';
							} else if (dir == 'taskPassCount') {
								loadPage = 'taskPassList.html';
							} else if (dir == 'taskRejectCount') {
								loadPage = 'taskRejectList.html';
							} else if (dir == 'manageUnConfirmCount') {
								loadPage = 'manageUnConfirmList.html';
							} else if (dir == 'shareUnReadCount') {
								loadPage = 'shareUnReadList.html';
							} else if (dir == 'shareUnCompleteCount') {
								loadPage = 'shareUnCompleteList.html';
							}
							$userInfo.put('platformLoadPage', loadPage);
							$windowManager.loadOtherWindow('platform_list', loadPage, function() {
								var launchWindow = $windowManager.getLaunchWindow();
								launchWindow.setStyle({
									mask: "none"
								});
								var sideWindow = $windowManager.getById('side');
								sideWindow.close();
							});
						}
					}
				});

				$('#closeSide').hide();
				$('#backSide').show();
				$common.touchSE($('#backSide'), function(event, startTouch, o) {}, function(event, o) {
					$('#typeDIV').css({
						'margin-left': '0'
					});
					$('#closeSide').show();
					$('#backSide').hide();
				});
			}
		});
		$common.touchSE($('li', '#orderByUL'), function(event, startTouch, o) {}, function(event, o) {
			if (!$(o).hasClass('choosed')) {
				$('li', '#orderByUL').removeClass('choosed');
				$(o).addClass('choosed');
				var dir = $(o).attr('dir');
				if (dir) {
					var loadPage = $userInfo.get('platformLoadPage');
					if (loadPage) {
						$windowManager.loadOtherWindow('platform_list', loadPage + '?orderBy=' + dir, function() {
							var launchWindow = $windowManager.getLaunchWindow();
							launchWindow.setStyle({
								mask: "none"
							});
							var sideWindow = $windowManager.getById('side');
							sideWindow.close();
						});
					}
				}
			}
		});
	}
	plusReady = function() {
		var dataOrderBy = $userInfo.get('dataOrderBy');
		if (!dataOrderBy) {
			dataOrderBy = 'reqDate';
		}
		$('li', '#orderByUL').removeClass('choosed');
		$('li[dir="' + dataOrderBy + '"]', '#orderByUL').addClass('choosed');
		loadData();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});