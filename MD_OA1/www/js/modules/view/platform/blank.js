define(function(require, exports, module) {
	var $common = require('core/common');
	var $templete = require('core/templete');
	var $updateManager = require('manager/update');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var currentWindow;
	onRefresh = function() {
		window.setTimeout(function() {
			$sideManager.loadDataType(function(loadPage) {
				if(loadPage=='blank.html'){
					currentWindow = $windowManager.current();
					currentWindow.endPullToRefresh();
				}else{
					$windowManager.loadOtherWindow('platform_list', loadPage);
				}
			});
		}, 500);
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};
	
	plusReady = function() {
		$('.workData').append(String.formatmodel($templete.getBankData(), {
			img: '../../img/nodata.png'
		}));
		pullToRefreshEvent();
	};
	if (window.plus) {
		plusReady();
		$updateManager.execute();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});