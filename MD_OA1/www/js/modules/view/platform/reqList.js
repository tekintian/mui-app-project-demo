define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var $updateManager = require('manager/update');
	var currentWindow;
	var nextIndex = 0;
	var queryMap = parseURL();
	var orderBy = queryMap.get('orderBy');
	var onRefreshSuccess=false;
	viewCallback = function(type, id) {
		processCallback(type, id);
	};
	remind = function(reqId) {
		if (reqId) {
			$nativeUIManager.watting('正在发送提醒...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/req/remind',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: reqId,
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									$nativeUIManager.wattingTitle('提醒发送成功!');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('提醒失败,请稍后再试!');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	back = function(reqId) {
		if (reqId) {
			$nativeUIManager.watting('正在撤销申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/req/back',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: reqId,
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									$nativeUIManager.wattingTitle('撤销成功!');
									processCallback('req', reqId);
								} else if (jsonData['result'] == '1') {
									$nativeUIManager.wattingTitle('单据已被审批或处理!');
								} else if (jsonData['result'] == '-1') {
									$nativeUIManager.wattingTitle('撤销失败,请稍后再试!');
									processCallback('req', reqId);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('确认发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	processCallback = function(type, uid, manualCloseWatting) {
		var listItem = $('.list[dir="' + type + '"]');
		if (listItem) {
			var liObj = $(listItem).find('li[uid="' + uid + '"]');
			if (liObj) {
				$(liObj).fadeOut(500, function() {
					$(liObj).remove();
					var listTitle = $('.list_tilte[dir="' + type + '"]');
					var count = $('li', listItem).size();
					if (count == 0) {
						$(listTitle).remove();
						$(listItem).remove();
					} else {
						$('.desc', listTitle).attr('dir', count).find('em').text(count);
						if (count == 1) {
							$('#batchAction').hide();
						}
					}
					if (!manualCloseWatting) {
						$nativeUIManager.wattingClose();
					}
				});
			}
		}
	};
	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		if(!onRefreshSuccess){
			onRefreshSuccess=true;
			$sideManager.loadDataType(function(loadPage) {
				loadData(function() {
					currentWindow.endPullToRefresh();
					var listCount=$('p.list_tilte','.workData').size();
					if(listCount==0){
						$windowManager.loadOtherWindow('platform_list', loadPage);
					}else{
						$sideManager.builder('reqTypeCount', 'reqCount');
					}
					onRefreshSuccess=false;
				});
			});
		}
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};
	bindEvent = function() {
		var reqUL = $('.list[dir="req"]', '.workData');
		if (reqUL) {
			$common.touchSME($('li', reqUL),
				function(startX, startY, endX, endY, event, startTouch, o) {},
				function(startX, startY, endX, endY, event, moveTouch, o) {}, function(startX, startY, endX, endY, event, o) {
					if (startX == endX && startY == endY) {
						if (!$(o).hasClass('active')) {
							var reqId = $(o).attr('reqId');
							if (reqId) {
								$(o).addClass('active');
								window.setTimeout(function() {
									$(o).removeClass('active');
									var backYn = $(o).attr('backYn');
									var remindYn = $(o).attr('remindYn');
									if (backYn && remindYn) {
										if (backYn == 'N' && remindYn == 'N') {
											$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
												show();
											});
										} else {
											if (backYn == 'Y' && remindYn == 'Y') {
												$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
														title: '查看'
													}, {
														title: '提醒'
													}, {
														title: '撤销'
													}],
													function(index) {
														if (index > 0) {
															if (index == 1) {
																$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
																	show();
																});
															} else if (index == 2) {
																remind(reqId);
																$(o).attr('remindYn', 'N');
															} else if (index == 3) {
																back(reqId);
															}
														}
													});
											} else if (backYn == 'Y' && remindYn == 'N') {
												$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
														title: '查看'
													}, {
														title: '撤销'
													}],
													function(index) {
														if (index > 0) {
															if (index == 1) {
																$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
																	show();
																});
															} else if (index == 2) {
																back(reqId);
															}
														}
													});
											} else if (backYn == 'N' && remindYn == 'Y') {
												$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
														title: '查看'
													}, {
														title: '提醒'
													}],
													function(index) {
														if (index > 0) {
															if (index == 1) {
																$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
																	show();
																});
															} else if (index == 2) {
																remind(reqId);
																$(o).attr('remindYn', 'N');
															}
														}
													});
											}
										}
									}
								}, 100);
							}
						}
					}
				});
			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	bindData = function(jsonData, append) {
		var sb = new StringBuilder();
		var dataList = jsonData['dataList'];
		if (dataList && $(dataList).size() > 0) {
			var batchFlag = false;
			if (!append) {
				sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
					typeText: $userInfo.get('reqCount_'),
					dir: 'req'
				}));
			}
			sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
				dir: 'req'
			}));
			$(dataList).each(function(i, o) {
				var desc = '<span class="t_wait">于</span> ' + o['dateTime'] +
					' <span class="t_wait">发起</span> ';
				desc += '正在等待 <span class="name width-48">' + o['userName'] + '</span>';
				if (o['nodeType'] == "task") {
					desc += ' <span class="t_wait">审批</span>';
				} else if (o['nodeType'] == "manage") {
					desc += ' <span class="t_wait">执行</span>';
				}
				sb.append(String.formatmodel($templete.getWorkData(batchFlag, 'N'), {
					applyName: o['subject'] + ' <span class="t_wait">[' + o['reqNo'] + ']</span>',
					desc: desc,
					uid: o['id'],
					reqId: o['id'],
					quickPreview: 'N',
					backYn: o['backYn'],
					remindYn: o['remindYn']
				}));
			});
			sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
		} else {
			sb.append(String.formatmodel($templete.getBankData(), {
				img: '../../img/nodata.png'
			}));
		}
		if (append) {
			$('.workData').append(sb.toString());
		} else {
			$('.workData').empty().append(sb.toString());
		}
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		var page = jsonData['page'];
		if (page) {
			if (page['hasNextPage'] == true) {
				$('.workData').attr('nextIndex', page['nextIndex']);
			}
			var totalRecord = page['totalRecord'];
			if (totalRecord || totalRecord == 0) {
				$userInfo.put('reqCount', totalRecord);
			}
		}
		bindEvent();
		pullToRefreshEvent();
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/reqListData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				orderBy: orderBy,
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						bindData(jsonData, append);
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
		if (orderBy) {
			$userInfo.put('dataOrderBy', orderBy);
		}
		$updateManager.execute();
		$sideManager.builder('reqTypeCount', 'reqCount');
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});