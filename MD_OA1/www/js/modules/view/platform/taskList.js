define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var $updateManager = require('manager/update');
	var currentWindow;
	var nextIndex = 0;
	var queryMap = parseURL();
	var orderBy = queryMap.get('orderBy');
	var onRefreshSuccess = false;
	viewCallback = function(type, id) {
		processCallback(type, id);
	};
	rejectBatch = function(taskIds, callback) {
		if (taskIds) {
			$nativeUIManager.watting('正在批量否决申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqTask/overruleBatch',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							ids: taskIds,
							reason: $('#reasonBatch').val(),
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									var successArray = jsonData['successArray'];
									if (successArray && $(successArray).size() > 0) {
										$(successArray).each(function(i, o) {
											processCallback('task', o, true);
										});
									}
									$nativeUIManager.wattingTitle('批量否决完成!共计否决了' + jsonData['passCount'] + '个申请');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
										$common.switchOS(function() {
											var badgeNumber = $userInfo.get('userBadgeNumber');
											var batchCount=jsonData['passCount'];
											badgeNumber = parseInt(badgeNumber);
											batchCount=parseInt(batchCount);
											badgeNumber = badgeNumber - batchCount;
											if (badgeNumber < 0) {
												badgeNumber = 0;
											}
											$userInfo.put('userBadgeNumber', badgeNumber);
											plus.runtime.setBadgeNumber(badgeNumber);
										}, function() {});
									}, 1000);
								} else {
									$nativeUIManager.wattingTitle('批量否决发生错误,请稍后重试');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('批量否决发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
								if (typeof callback == 'function') {
									callback();
								}
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};

	reject = function(taskId) {
		if (taskId) {
			$nativeUIManager.watting('正在否决申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqTask/overrule',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: taskId,
							reason: '',
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									processCallback('task', taskId);
									$nativeUIManager.wattingTitle('审批完成');
									$common.switchOS(function() {
										var badgeNumber = $userInfo.get('userBadgeNumber');
										badgeNumber = parseInt(badgeNumber);
										badgeNumber = badgeNumber - 1;
										if (badgeNumber < 0) {
											badgeNumber = 0;
										}
										$userInfo.put('userBadgeNumber', badgeNumber);
										plus.runtime.setBadgeNumber(badgeNumber);
									}, function() {});
								} else if (jsonData['result'] == '1') {
									processCallback('task', taskId);
									$nativeUIManager.wattingTitle('审批失败,不能重复审批!');
								} else if (jsonData['result'] == '2') {
									processCallback('task', taskId);
									$nativeUIManager.wattingTitle('审批失败,单据已被撤销!');
								} else if (jsonData['result'] == '-1') {
									$nativeUIManager.wattingTitle('审批失败,请稍后重试!');
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('审批发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};

	passBatch = function(taskIds, callback) {
		if (taskIds) {
			$nativeUIManager.watting('正在批量通过申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqTask/passBatch',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							ids: taskIds,
							reason: $('#reasonBatch').val(),
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									var successArray = jsonData['successArray'];
									if (successArray && $(successArray).size() > 0) {
										$(successArray).each(function(i, o) {
											processCallback('task', o, true);
										});
									}
									$nativeUIManager.wattingTitle('批量通过完成!共计通过了' + jsonData['passCount'] + '个申请');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										$common.switchOS(function() {
											var badgeNumber = $userInfo.get('userBadgeNumber');
											var batchCount=jsonData['passCount'];
											badgeNumber = parseInt(badgeNumber);
											batchCount=parseInt(batchCount);
											badgeNumber = badgeNumber - batchCount;
											if (badgeNumber < 0) {
												badgeNumber = 0;
											}
											$userInfo.put('userBadgeNumber', badgeNumber);
											plus.runtime.setBadgeNumber(badgeNumber);
										}, function() {});
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								} else {
									$nativeUIManager.wattingTitle('批量通过发生错误,请稍后重试');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('批量通过发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
								if (typeof callback == 'function') {
									callback();
								}
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};

	pass = function(taskId) {
		if (taskId) {
			$nativeUIManager.watting('正在通过申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqTask/pass',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: taskId,
							reason: '',
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									processCallback('task', taskId);
									$nativeUIManager.wattingTitle('审批完成');
									$common.switchOS(function() {
										var badgeNumber = $userInfo.get('userBadgeNumber');
										badgeNumber = parseInt(badgeNumber);
										badgeNumber = badgeNumber - 1;
										if (badgeNumber < 0) {
											badgeNumber = 0;
										}
										$userInfo.put('userBadgeNumber', badgeNumber);
										plus.runtime.setBadgeNumber(badgeNumber);
									}, function() {});
								} else if (jsonData['result'] == '1') {
									processCallback('task', taskId);
									$nativeUIManager.wattingTitle('审批失败,不能重复审批!');
								} else if (jsonData['result'] == '2') {
									processCallback('task', taskId);
									$nativeUIManager.wattingTitle('审批失败,单据已被撤销!');
								} else if (jsonData['result'] == '-1') {
									$nativeUIManager.wattingTitle('审批失败,请稍后重试!');
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('审批发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	processCallback = function(type, uid, manualCloseWatting) {
		var taskCount = $userInfo.get('taskCount');
		taskCount = parseInt(taskCount);
		var listItem = $('.list[dir="' + type + '"]');
		if (listItem) {
			var liObj = $(listItem).find('li[uid="' + uid + '"]');
			if (liObj) {
				if (taskCount > 0) {
					taskCount -= 1;
				}
				$userInfo.put('taskCount', taskCount);
				$(liObj).fadeOut(500, function() {
					$(liObj).remove();
					var listTitle = $('.list_tilte[dir="' + type + '"]');
					var count = $('li', listItem).size();
					if (count == 0) {
						$(listTitle).remove();
						$(listItem).remove();
						$('.workData').empty().append(String.formatmodel($templete.getBankData(), {
							img: '../../img/nodata.png'
						}));
					} else {
						if (count == 1) {
							$('#batchAction').hide();
						}
					}
					if (!manualCloseWatting) {
						$nativeUIManager.wattingClose();
					}
				});
			}
		}
	};
	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		if (!onRefreshSuccess) {
			onRefreshSuccess = true;
			$sideManager.loadDataType(function(loadPage) {
				loadData(function() {
					currentWindow.endPullToRefresh();
					var listCount = $('p.list_tilte', '.workData').size();
					if (listCount == 0) {
						$windowManager.loadOtherWindow('platform_list', loadPage);
					} else {
						$sideManager.builder('waitingCount', 'taskCount');
					}
					onRefreshSuccess = false;
				});
			});
		}
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};
	bindQuickViewEvent = function(obj) {
		var preViewObj = $('.preview', obj);
		var li = $(obj).closest('li');
		if (preViewObj && li) {
			$common.touchSME($('.approveAction', preViewObj).find('span'),
				function(startX, startY, endX, endY, event, startTouch, o) {
					event.stopPropagation();
				},
				function(startX, startY, endX, endY, event, moveTouch, o) {
					event.stopPropagation();
				}, function(startX, startY, endX, endY, event, o) {
					event.stopPropagation();
					if (startX == endX && startY == endY) {
						if (!$(o).hasClass('active')) {
							var reqId = $(li).attr('reqId');
							var taskId = $(li).attr('uid');
							var dir = $(o).attr('dir');
							if (reqId && taskId && dir) {
								$(o).addClass('active');
								window.setTimeout(function() {
									$(o).removeClass('active');
									if (dir == 'view') {
										$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
											show();
										});
									} else if (dir == 'pass') {
										pass(taskId);
									} else if (dir == 'reject') {
										reject(taskId);
									}
								}, 100);
							}
						}
					}
				});
		}
	};
	taskProccessEvent = function(o, taskUL) {
		if (!$(o).hasClass('current')) {
			$('li', taskUL).removeClass('current');
			$('li', taskUL).find('.preview').hide();
			$(o).addClass('current');
			var quickPreview = $(o).attr('quickPreview');
			if (quickPreview) {
				if (quickPreview == 'Y') {
					if ($('.preview', o).size() == 0) {
						var reqId = $(o).attr('reqId');
						if (reqId) {
							$nativeUIManager.watting('请稍等...');
							$('.list_li', o).after(String.formatmodel($templete.getWorkDataView(), {}));
							var table = $('.preview', o).find('table');
							if (table) {
								$.ajax({
									type: 'POST',
									url: $common.getRestApiURL() + '/platform/desktop/quickPreview',
									dataType: 'json',
									data: {
										reqId: reqId,
										oaToken: $userInfo.get('token')
									},
									success: function(jsonData) {
										if (jsonData) {
											if (jsonData['result'] == '0') {
												var dataArray = jsonData['dataArray'];
												if (dataArray && $(dataArray).size() > 0) {
													var tableSb = new StringBuilder();
													$(dataArray).each(function(i, trDataArray) {
														$(trDataArray).each(function(j, o) {
															tableSb.append('<tr>\n');
															tableSb.append('<th style="width:120px;">' + o['label'] + ':</th>\n');
															tableSb.append('<td>' + o['value'] + '</td>\n');
															tableSb.append('</tr>\n');
														});
													});
													$('tbody', table).empty().append(tableSb.toString());
													$('.preview', o).show();
													$nativeUIManager.wattingClose();
													bindQuickViewEvent(o);
												}
											} else {
												$nativeUIManager.wattingTitle('错误,请稍后再试');
												window.setTimeout(function() {
													$nativeUIManager.wattingClose();
												}, 1500);
											}
										}
									},
									error: function(jsonData) {
										$nativeUIManager.wattingTitle('错误,请稍后再试');
										window.setTimeout(function() {
											$nativeUIManager.wattingClose();
										}, 1500);
									}
								});
							}
						}
					} else {
						$('.preview', o).show();
						bindQuickViewEvent(o);
					}
				} else if (quickPreview == 'N') {
					$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
							title: '通过'
						}, {
							title: '否决'
						}, {
							title: '更多'
						}],
						function(index) {
							if (index > 0) {
								var taskId = $(o).attr('uid');
								var reqId = $(o).attr('reqId');
								if (reqId && taskId) {
									if (index == 1) {
										pass(taskId);
									} else if (index == 2) {
										reject(taskId);
									} else if (index == 3) {
										$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
											show();
										});
									}
								}
							}
							$(o).removeClass('current');
						});
				}
			}
		} else {
			$(o).removeClass('current');
			$('.preview', o).hide();
		}
	};
	taskBatchEvent = function(o, taskUL) {
		if (!$(o).hasClass('current')) {
			$(o).addClass('current');
			$('.radio_ico', o).addClass('current');
		} else {
			$(o).removeClass('current');
			$('.radio_ico', o).removeClass('current');
		}
	};
	bindEvent = function() {
		var taskUL = $('.list[dir="task"]', '.workData');
		if (taskUL) {
			$common.touchSME($('li', taskUL),
				function(startX, startY, endX, endY, event, startTouch, o) {},
				function(startX, startY, endX, endY, event, moveTouch, o) {

				}, function(startX, startY, endX, endY, event, o) {
					if (startX == endX && startY == endY) {
						$(o).addClass('active');
						window.setTimeout(function() {
							$(o).removeClass('active');
							var ul = $(o).closest('ul');
							if (!$(ul).hasClass('choose')) {
								taskProccessEvent(o, taskUL);
							} else {
								taskBatchEvent(o, taskUL)
							}
						}, 100);
					}
				});

			$common.touchSE($('.batchAction', '.workData'), function(event, startTouch, o) {
				var obj = o;
				var p = $(obj).parent();
				if (p) {
					var dir = $(p).attr('dir');
					var ul = $(p).next();
					if (ul && dir) {
						if (!$(ul).hasClass('choose')) {
							$(ul).addClass('choose');
							$(obj).text('完成');
							$('.desc', p).hide();
							$('.batchCancelAction', p).show();
							$common.touchSE($('.batchCancelAction', p), function(event, startTouch, o) {
									$(ul).removeClass('choose');
									$('.radio_ico', ul).removeClass('current');
									$(obj).text('批量操作');
									$('.desc', p).show();
									$('.batchCancelAction', p).hide();
								},
								function(event, o) {

								});
						} else {
							var batchIds = '';
							var batchCount = 0;
							$('.radio_ico', ul).each(function(i, o) {
								if ($(o).hasClass('current')) {
									var li = $(o).parent();
									if (li) {
										var batchId = $(li).attr('uid');
										if (batchId) {
											batchIds += batchId + ',';
											batchCount++;
										}
									}
								}
							});
							if (batchCount == 0) {
								$(ul).removeClass('choose');
								$('.desc', p).show();
								$('.batchCancelAction', p).hide();
								$(obj).text('批量操作');
							} else {
								if (dir == 'task') {
									$nativeUIManager.confactionSheetirm('你将批量审批' + batchCount + '个申请', '取消', [{
											title: '通过'
										}, {
											title: '否决'
										}],
										function(index) {
											if (index > 0) {
												if (batchIds) {
													if (index == 1) {
														passBatch(batchIds, function() {
															$(ul).removeClass('choose');
															$('.desc', p).show();
															$('.batchCancelAction', p).hide();
															$(obj).text('批量操作');
														});
													} else {
														rejectBatch(batchIds, function() {
															$(ul).removeClass('choose');
															$('.desc', p).show();
															$('.batchCancelAction', p).hide();
															$(obj).text('批量操作');
														});
													}
												}
											}
										});
								}
							}
						}
					}
				}
			}, function(event, o) {

			});

			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	bindData = function(jsonData, append) {
		var sb = new StringBuilder();
		var dataList = jsonData['dataList'];
		if (dataList && $(dataList).size() > 0) {
			var batchFlag = false;
			if ($(dataList).size() > 1) {
				batchFlag = true;
			}
			if (!append) {
				sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
					typeText: $userInfo.get('taskCount_'),
					dir: 'task'
				}));
			}
			sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
				dir: 'task'
			}));
			$(dataList).each(function(i, o) {
				var desc = '<span class="name width-48">' + o['userName'] + '</span> <span class="t_wait">于</span> ' + o['dateTime'] +
					' <span class="t_wait">发起</span> ';
				if (o['nodeSeq'] != "1") {
					desc += '已被 <span class="name width-48">' + o['prevName'] + '</span>';
					if (o['prevIdea'] == "1") {
						desc += ' <span class="t_croose">通过</span>';
					} else if (o['prevIdea'] == "3") {
						desc += ' <span class="t_wait">转审</span>';
					}
				}
				var quickPreview = o['quickPreview'];
				sb.append(String.formatmodel($templete.getWorkData(batchFlag, quickPreview), {
					applyName: o['subject'] + ' <span class="t_wait">[' + o['reqNo'] + ']</span>',
					desc: desc,
					uid: o['id'],
					reqId: o['reqId'],
					quickPreview: o['quickPreview'],
					backYn: 'N'
				}));
			});
			sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
		} else {
			sb.append(String.formatmodel($templete.getBankData(), {
				img: '../../img/nodata.png'
			}));
		}
		if (append) {
			$('.workData').append(sb.toString());
		} else {
			$('.workData').empty().append(sb.toString());
		}
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		var page = jsonData['page'];
		if (page) {
			if (page['hasNextPage'] == true) {
				$('.workData').attr('nextIndex', page['nextIndex']);
			}
			var totalRecord = page['totalRecord'];
			if (totalRecord || totalRecord == 0) {
				$userInfo.put('taskCount', totalRecord);
			}
		}
		bindEvent();
		pullToRefreshEvent();
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/taskListData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				orderBy: orderBy,
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						bindData(jsonData, append);
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
		if (orderBy) {
			$userInfo.put('dataOrderBy', orderBy);
		}
		$updateManager.execute();
		$sideManager.builder('waitingCount', 'taskCount');
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});