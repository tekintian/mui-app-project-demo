define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var $updateManager = require('manager/update');
	var currentWindow;
	var nextIndex = 0;
	var queryMap = parseURL();
	var orderBy = queryMap.get('orderBy');
	var onRefreshSuccess=false;
	viewCallback = function(type, id) {
		processCallback(type, id);
	};
	processCallback = function(type, uid, manualCloseWatting) {
		var reqConfirmCount = $userInfo.get('reqConfirmCount');
		reqConfirmCount = parseInt(reqConfirmCount);
		var listItem = $('.list[dir="' + type + '"]');
		if (listItem) {
			var liObj = $(listItem).find('li[uid="' + uid + '"]');
			if (liObj) {
				if (reqConfirmCount > 0) {
					reqConfirmCount -= 1;
				}
				$userInfo.put('reqConfirmCount', reqConfirmCount);
				$(liObj).fadeOut(500, function() {
					$(liObj).remove();
					var listTitle = $('.list_tilte[dir="' + type + '"]');
					var count = $('li', listItem).size();
					if (count == 0) {
						$(listTitle).remove();
						$(listItem).remove();
						$('.workData').empty().append(String.formatmodel($templete.getBankData(), {
							img: '../../img/nodata.png'
						}));
					}
					if (!manualCloseWatting) {
						$nativeUIManager.wattingClose();
					}
				});
			}
		}
	};

	confirmProccessEvent = function(o, confirmUL) {
		if (!$(o).hasClass('current')) {
			$('li', confirmUL).removeClass('current');
			$(o).addClass('current');
			$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
					title: '确认'
				}, {
					title: '查看'
				}],
				function(index) {
					if (index > 0) {
						var reqId = $(o).attr('reqId');
						if (reqId) {
							if (index == 1) {
								confirm(reqId);
							} else if (index == 2) {
								$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
									show();
								});
							}
						}
					}
					$(o).removeClass('current');
				});
		} else {
			$(o).removeClass('current');
			$('.preview', o).hide();
		}
	};
	confirmBatchEvent = function(o, manageUL) {
		if (!$(o).hasClass('current')) {
			$(o).addClass('current');
			$('.radio_ico', o).addClass('current');
		} else {
			$(o).removeClass('current');
			$('.radio_ico', o).removeClass('current');
		}
	};
	confirmBatch = function(reqIds, callback) {
		if (reqIds) {
			$nativeUIManager.watting('正在批量确认申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/req/confirmBatch',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							ids: reqIds,
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									var successArray = jsonData['successArray'];
									if (successArray && $(successArray).size() > 0) {
										$(successArray).each(function(i, o) {
											processCallback('req', o, true);
										});
									}
									$nativeUIManager.wattingTitle('批量确认完成!共计确认了' + jsonData['passCount'] + '个申请');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								} else {
									$nativeUIManager.wattingTitle('批量确认发生错误,请稍后重试');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('批量确认发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
								if (typeof callback == 'function') {
									callback();
								}
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	confirm = function(reqId) {
		if (reqId) {
			$nativeUIManager.watting('正在确认申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/req/confirm',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: reqId,
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									$nativeUIManager.wattingTitle('确认完成!');
									processCallback('req', reqId);
								} else if (jsonData['result'] == '-1') {
									$nativeUIManager.wattingTitle('确认失败,请稍后再试!');
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('确认发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};

	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		if(!onRefreshSuccess){
			onRefreshSuccess=true;
			$sideManager.loadDataType(function(loadPage) {
				loadData(function() {
					currentWindow.endPullToRefresh();
					var listCount = $('p.list_tilte', '.workData').size();
					if (listCount == 0) {
						$windowManager.loadOtherWindow('platform_list', loadPage);
					} else {
						$sideManager.builder('waitingCount', 'reqConfirmCount');
					}
					onRefreshSuccess=false;
				});
			});
		}
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};

	bindEvent = function() {
		var reqUL = $('.list[dir="req"]', '.workData');
		if (reqUL) {
			$common.touchSME($('li', reqUL),
				function(startX, startY, endX, endY, event, startTouch, o) {},
				function(startX, startY, endX, endY, event, moveTouch, o) {}, function(startX, startY, endX, endY, event, o) {
					if (startX == endX && startY == endY) {
						$(o).addClass('active');
						window.setTimeout(function() {
							$(o).removeClass('active');
							var ul = $(o).closest('ul');
							if (!$(ul).hasClass('choose')) {
								confirmProccessEvent(o, reqUL);
							} else {
								confirmBatchEvent(o, reqUL)
							}
						}, 100);
					}
				});

			$common.touchSE($('.batchAction', '.workData'), function(event, startTouch, o) {
				var obj = o;
				var p = $(obj).parent();
				if (p) {
					var dir = $(p).attr('dir');
					var ul = $(p).next();
					if (ul && dir) {
						if (!$(ul).hasClass('choose')) {
							$(ul).addClass('choose');
							$(obj).text('完成');
							$('.desc', p).hide();
							$('.batchCancelAction', p).show();
							$common.touchSE($('.batchCancelAction', p), function(event, startTouch, o) {
									$(ul).removeClass('choose');
									$('.radio_ico', ul).removeClass('current');
									$(obj).text('批量操作');
									$('.desc', p).show();
									$('.batchCancelAction', p).hide();
								},
								function(event, o) {

								});
						} else {
							var batchIds = '';
							var batchCount = 0;
							$('.radio_ico', ul).each(function(i, o) {
								if ($(o).hasClass('current')) {
									var li = $(o).parent();
									if (li) {
										var batchId = $(li).attr('uid');
										if (batchId) {
											batchIds += batchId + ',';
											batchCount++;
										}
									}
								}
							});
							if (batchCount == 0) {
								$(ul).removeClass('choose');
								$('.desc', p).show();
								$('.batchCancelAction', p).hide();
								$(obj).text('批量操作');
							} else {
								if (dir == 'req') {
									$nativeUIManager.confactionSheetirm('你将批量确认' + batchCount + '个申请', '取消', [{
											title: '确认完成'
										}],
										function(index) {
											if (index > 0) {
												if (batchIds) {
													confirmBatch(batchIds, function() {
														$(ul).removeClass('choose');
														$('.desc', p).show();
														$('.batchCancelAction', p).hide();
														$(obj).text('批量操作');
													});
												}
											}
										});
								}
							}
						}
					}
				}
			}, function(event, o) {

			});

			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	bindData = function(jsonData, append) {
		var sb = new StringBuilder();
		var dataList = jsonData['dataList'];
		if (dataList && $(dataList).size() > 0) {
			var batchFlag = false;
			if ($(dataList).size() > 1) {
				batchFlag = true;
			}
			if (!append) {
				sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
					typeText: $userInfo.get('reqConfirmCount_'),
					dir: 'req'
				}));
			}
			sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
				dir: 'req'
			}));
			$(dataList).each(function(i, o) {
				var desc = '<span class="name width-48">' + o['userName'] + '</span> <span class="t_wait">于</span> ' + o['dateTime'] +
					' <span class="t_wait">发起</span> ';
				if (o['taskYn'] == "Y") {
					desc += '已被&nbsp;<span class="name width-48">' + o['userName'] + '</span>&nbsp;<span class="t_croose">处理</span>';
				}
				desc += '等待您 &nbsp;<span class="t_wait">确认结果</span>';
				var quickPreview = o['quickPreview'];
				sb.append(String.formatmodel($templete.getWorkData(batchFlag, quickPreview), {
					applyName: o['subject'] + ' <span class="t_wait">[' + o['reqNo'] + ']</span>',
					desc: desc,
					uid: o['id'],
					reqId: o['id'],
					quickPreview: o['quickPreview'],
					backYn: 'N'
				}));
			});
			sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
		} else {
			sb.append(String.formatmodel($templete.getBankData(), {
				img: '../../img/nodata.png'
			}));
		}
		if (append) {
			$('.workData').append(sb.toString());
		} else {
			$('.workData').empty().append(sb.toString());
		}
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		var page = jsonData['page'];
		if (page) {
			if (page['hasNextPage'] == true) {
				$('.workData').attr('nextIndex', page['nextIndex']);
			}
			var totalRecord = page['totalRecord'];
			if (totalRecord || totalRecord == 0) {
				$userInfo.put('reqConfirmCount', totalRecord);
			}
		}
		bindEvent();
		pullToRefreshEvent();
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/reqConfirmListData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				orderBy:orderBy,
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						bindData(jsonData, append);
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
		if(orderBy){
			$userInfo.put('dataOrderBy',orderBy);
		}
		$updateManager.execute();
		$sideManager.builder('waitingCount', 'reqConfirmCount');
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});