define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $sideManager = require('manager/side');
	var $updateManager = require('manager/update');
	var currentWindow;
	var nextIndex = 0;
	var queryMap = parseURL();
	var orderBy = queryMap.get('orderBy');
	var onRefreshSuccess=false;
	viewCallback = function(type, id) {
		processCallback(type, id);
	};
	processCallback = function(type, uid, manualCloseWatting) {
		var manageCount = $userInfo.get('manageCount');
		manageCount = parseInt(manageCount);
		var listItem = $('.list[dir="' + type + '"]');
		if (listItem) {
			var liObj = $(listItem).find('li[uid="' + uid + '"]');
			if (liObj) {
				if (manageCount > 0) {
					manageCount -= 1;
				}
				$userInfo.put('manageCount', manageCount);
				$(liObj).fadeOut(500, function() {
					$(liObj).remove();
					var listTitle = $('.list_tilte[dir="' + type + '"]');
					var count = $('li', listItem).size();
					if (count == 0) {
						$(listTitle).remove();
						$(listItem).remove();
						$('.workData').empty().append(String.formatmodel($templete.getBankData(), {
							img: '../../img/nodata.png'
						}));
					} else {
						if (count == 1) {
							$('#batchAction').hide();
						}
					}
					if (!manualCloseWatting) {
						$nativeUIManager.wattingClose();
					}
				});
			}
		}
	};
	workBatch = function(manageIds, callback) {
		if (manageIds) {
			$nativeUIManager.watting('正在批量处理...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqManage/workBatch',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							ids: manageIds,
							reason: $('#reasonBatch').val(),
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									var successArray = jsonData['successArray'];
									if (successArray && $(successArray).size() > 0) {
										$(successArray).each(function(i, o) {
											processCallback('manage', o, true);
										});
									}
									$nativeUIManager.wattingTitle('批量处理完成!共计处理了' + jsonData['passCount'] + '个申请');
									window.setTimeout(function() {
										$common.switchOS(function() {
											var badgeNumber = $userInfo.get('userBadgeNumber');
											var batchCount=jsonData['passCount'];
											badgeNumber = parseInt(badgeNumber);
											batchCount=parseInt(batchCount);
											badgeNumber = badgeNumber - batchCount;
											if (badgeNumber < 0) {
												badgeNumber = 0;
											}
											$userInfo.put('userBadgeNumber', badgeNumber);
											plus.runtime.setBadgeNumber(badgeNumber);
										}, function() {});
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								} else {
									$nativeUIManager.wattingTitle('批量处理发生错误,请稍后重试');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('批量处理发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
								if (typeof callback == 'function') {
									callback();
								}
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	noTreatmentBatch = function(manageIds, callback) {
		if (manageIds) {
			$nativeUIManager.watting('正在批量处理...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqManage/noTreatmentBatch',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							ids: manageIds,
							reason: $('#reasonBatch').val(),
							attToken: '',
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									var successArray = jsonData['successArray'];
									if (successArray && $(successArray).size() > 0) {
										$(successArray).each(function(i, o) {
											processCallback('manage', o, true);
										});
									}
									$nativeUIManager.wattingTitle('批量处理完成!共计拒绝处理了' + jsonData['passCount'] + '个申请');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										$common.switchOS(function() {
											var badgeNumber = $userInfo.get('userBadgeNumber');
											var batchCount=jsonData['passCount'];
											badgeNumber = parseInt(badgeNumber);
											batchCount=parseInt(batchCount);
											badgeNumber = badgeNumber - batchCount;
											if (badgeNumber < 0) {
												badgeNumber = 0;
											}
											$userInfo.put('userBadgeNumber', badgeNumber);
											plus.runtime.setBadgeNumber(badgeNumber);
										}, function() {});
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								} else {
									$nativeUIManager.wattingTitle('批量处理发生错误,请稍后重试');
									window.setTimeout(function() {
										$nativeUIManager.wattingClose();
										if (typeof callback == 'function') {
											callback();
										}
									}, 1000);
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('批量处理发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
								if (typeof callback == 'function') {
									callback();
								}
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	work = function(manageId) {
		if (manageId) {
			$nativeUIManager.watting('正在处理申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqManage/work',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: manageId,
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									$nativeUIManager.wattingTitle('完成!');
									processCallback('manage', manageId);
									$common.switchOS(function() {
										var badgeNumber = $userInfo.get('userBadgeNumber');
										badgeNumber = parseInt(badgeNumber);
										badgeNumber = badgeNumber - 1;
										if (badgeNumber < 0) {
											badgeNumber = 0;
										}
										$userInfo.put('userBadgeNumber', badgeNumber);
										plus.runtime.setBadgeNumber(badgeNumber);
									}, function() {});
								} else if (jsonData['result'] == '2') {
									processCallback('manage', manageId);
									$nativeUIManager.wattingTitle('失败,单据已被撤销!');
								} else if (jsonData['result'] == '1') {
									processCallback('manage', manageId);
									$nativeUIManager.wattingTitle('失败,请勿重复处理!');
								} else if (jsonData['result'] == '-1') {
									$nativeUIManager.wattingTitle('失败,请稍后再试!');
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	noTreatment = function(manageId) {
		if (manageId) {
			$nativeUIManager.watting('正在拒绝处理申请...');
			window.setTimeout(function() {
				$common.refreshToken(function(token) {
					$.ajax({
						type: 'POST',
						url: $common.getRestApiURL() + '/wf/reqManage/noTreatment',
						dataType: 'json',
						data: {
							oaToken: $userInfo.get('token'),
							id: manageId,
							'org.guiceside.web.jsp.taglib.Token': token
						},
						success: function(jsonData) {
							if (jsonData) {
								if (jsonData['result'] == '0') {
									$nativeUIManager.wattingTitle('完成!');
									processCallback('manage', manageId);
									$common.switchOS(function() {
										var badgeNumber = $userInfo.get('userBadgeNumber');
										badgeNumber = parseInt(badgeNumber);
										badgeNumber = badgeNumber - 1;
										if (badgeNumber < 0) {
											badgeNumber = 0;
										}
										$userInfo.put('userBadgeNumber', badgeNumber);
										plus.runtime.setBadgeNumber(badgeNumber);
									}, function() {});
								} else if (jsonData['result'] == '2') {
									processCallback('manage', manageId);
									$nativeUIManager.wattingTitle('失败,单据已被撤销!');
								} else if (jsonData['result'] == '1') {
									processCallback('manage', manageId);
									$nativeUIManager.wattingTitle('失败,请勿重复处理!');
								} else if (jsonData['result'] == '-1') {
									$nativeUIManager.wattingTitle('失败,请稍后再试!');
								}
							}
						},
						error: function(jsonData) {
							$nativeUIManager.wattingTitle('发生错误,请稍后重试');
							window.setTimeout(function() {
								$nativeUIManager.wattingClose();
							}, 1000);
						}
					});
				});
			}, 500);
		}
	};
	manageProccessEvent = function(o, manageUL) {
		if (!$(o).hasClass('current')) {
			$('li', manageUL).removeClass('current');
			$('li', manageUL).find('.preview').hide();
			$(o).addClass('current');
			$nativeUIManager.confactionSheetirm('请选择操作', '取消', [{
					title: '处理'
				}, {
					title: '不予处理'
				}, {
					title: '查看'
				}],
				function(index) {
					if (index > 0) {
						var manageId = $(o).attr('uid');
						var reqId = $(o).attr('reqId');
						if (reqId && manageId) {
							if (index == 1) {
								work(manageId);
							} else if (index == 2) {
								noTreatment(manageId);
							} else if (index == 3) {
								$windowManager.create('req_viewHead', '../req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
									show();
								});
							}
						}
					}
					$(o).removeClass('current');
				});
		} else {
			$(o).removeClass('current');
			$('.preview', o).hide();
		}
	};
	manageBatchEvent = function(o, manageUL) {
		if (!$(o).hasClass('current')) {
			$(o).addClass('current');
			$('.radio_ico', o).addClass('current');
		} else {
			$(o).removeClass('current');
			$('.radio_ico', o).removeClass('current');
		}
	};
	onRefresh = function() {
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		if(!onRefreshSuccess){
			onRefreshSuccess=true;
			$sideManager.loadDataType(function(loadPage) {
			loadData(function() {
				currentWindow.endPullToRefresh();
				var listCount = $('p.list_tilte', '.workData').size();
				if (listCount == 0) {
					$windowManager.loadOtherWindow('platform_list', loadPage);
				} else {
					$sideManager.builder('waitingCount', 'manageCount');
				}
				onRefreshSuccess=false;
			});
		});
		}
		
	};
	pullToRefreshEvent = function() {
		currentWindow = $windowManager.current();
		currentWindow.setPullToRefresh({
			support: true,
			height: "50px",
			range: "200px",
			contentdown: {
				caption: "下拉可以刷新"
			},
			contentover: {
				caption: "释放立即刷新"
			},
			contentrefresh: {
				caption: "正在刷新..."
			}
		}, onRefresh);
	};

	bindEvent = function() {
		var manageUL = $('.list[dir="manage"]', '.workData');
		if (manageUL) {
			$common.touchSME($('li', manageUL),
				function(startX, startY, endX, endY, event, startTouch, o) {},
				function(startX, startY, endX, endY, event, moveTouch, o) {}, function(startX, startY, endX, endY, event, o) {
					if (startX == endX && startY == endY) {
						$(o).addClass('active');
						window.setTimeout(function() {
							$(o).removeClass('active');
							var ul = $(o).closest('ul');
							if (!$(ul).hasClass('choose')) {
								manageProccessEvent(o, manageUL);
							} else {
								manageBatchEvent(o, manageUL)
							}
						}, 100);
					}
				});

			$common.touchSE($('.batchAction', '.workData'), function(event, startTouch, o) {
				var obj = o;
				var p = $(obj).parent();
				if (p) {
					var dir = $(p).attr('dir');
					var ul = $(p).next();
					if (ul && dir) {
						if (!$(ul).hasClass('choose')) {
							$(ul).addClass('choose');
							$(obj).text('完成');
							$('.desc', p).hide();
							$('.batchCancelAction', p).show();
							$common.touchSE($('.batchCancelAction', p), function(event, startTouch, o) {
									$(ul).removeClass('choose');
									$('.radio_ico', ul).removeClass('current');
									$(obj).text('批量操作');
									$('.desc', p).show();
									$('.batchCancelAction', p).hide();
								},
								function(event, o) {

								});
						} else {
							var batchIds = '';
							var batchCount = 0;
							$('.radio_ico', ul).each(function(i, o) {
								if ($(o).hasClass('current')) {
									var li = $(o).parent();
									if (li) {
										var batchId = $(li).attr('uid');
										if (batchId) {
											batchIds += batchId + ',';
											batchCount++;
										}
									}
								}
							});
							if (batchCount == 0) {
								$(ul).removeClass('choose');
								$('.desc', p).show();
								$('.batchCancelAction', p).hide();
								$(obj).text('批量操作');
							} else {
								if (dir == 'manage') {
									$nativeUIManager.confactionSheetirm('你将批量经办' + batchCount + '个申请', '取消', [{
											title: '处理'
										}, {
											title: '不予处理'
										}],
										function(index) {
											if (index > 0) {
												if (batchIds) {
													if (index == 1) {
														workBatch(batchIds, function() {
															$(ul).removeClass('choose');
															$('.desc', p).show();
															$('.batchCancelAction', p).hide();
															$(obj).text('批量操作');
														});
													} else if (index == 2) {
														noTreatmentBatch(batchIds, function() {
															$(ul).removeClass('choose');
															$('.desc', p).show();
															$('.batchCancelAction', p).hide();
															$(obj).text('批量操作');
														});
													}
												}
											}
										});
								}
							}
						}
					}
				}
			}, function(event, o) {

			});
			document.addEventListener("plusscrollbottom", function() {
				var next = $('.workData').attr('nextIndex');
				if (next) {
					if (next > 0) {
						nextIndex = next;
						$nativeUIManager.watting('正在加载更多...');
						$('.workData').attr('nextIndex', 0);
						window.setTimeout(function() {
							loadData(function() {
								$nativeUIManager.wattingClose();
							}, true);
						}, 500);
					}
				}
			});
		}
	};
	bindData = function(jsonData, append) {
		var sb = new StringBuilder();
		var dataList = jsonData['dataList'];
		if (dataList && $(dataList).size() > 0) {
			var batchFlag = false;
			if ($(dataList).size() > 1) {
				batchFlag = true;
			}
			if (!append) {
				sb.append(String.formatmodel($templete.getWorkType(batchFlag), {
					typeText: $userInfo.get('manageCount_'),
					dir: 'manage'
				}));
			}
			sb.append(String.formatmodel($templete.getWorkDataCardStart(), {
				dir: 'manage'
			}));
			$(dataList).each(function(i, o) {
				var desc = '<span class="name width-48">' + o['userName'] + '</span> <span class="t_wait">于</span> ' + o['dateTime'] +
					' <span class="t_wait">发起</span> ';
				if (o['taskYn'] == "Y") {
					desc += '已被&nbsp;<span class="name width-48">' + o['taskName'] + '</span>&nbsp;<span class="t_croose">通过</span>';
				}
				desc += '等待您 &nbsp;<span class="t_wait">处理</span>';
				var quickPreview = o['quickPreview'];
				sb.append(String.formatmodel($templete.getWorkData(batchFlag, quickPreview), {
					applyName: o['subject'] + ' <span class="t_wait">[' + o['reqNo'] + ']</span>',
					desc: desc,
					uid: o['id'],
					reqId: o['reqId'],
					quickPreview: o['quickPreview'],
					backYn: 'N'
				}));
			});
			sb.append(String.formatmodel($templete.getWorkDataCardEnd(), {}));
		} else {
			sb.append(String.formatmodel($templete.getBankData(), {
				img: '../../img/nodata.png'
			}));
		}
		if (append) {
			$('.workData').append(sb.toString());
		} else {
			$('.workData').empty().append(sb.toString());
		}
		nextIndex = 0;
		$('.workData').attr('nextIndex', 0);
		var page = jsonData['page'];
		if (page) {
			if (page['hasNextPage'] == true) {
				$('.workData').attr('nextIndex', page['nextIndex']);
			}
			var totalRecord = page['totalRecord'];
			if (totalRecord || totalRecord == 0) {
				$userInfo.put('manageCount', totalRecord);
			}
		}
		bindEvent();
		pullToRefreshEvent();
	};
	loadData = function(callback, append) {
		if (!callback) {
			$nativeUIManager.watting('加载中...');
		}
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/manageListData',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				orderBy: orderBy,
				start: nextIndex > 0 ? nextIndex : ''
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						bindData(jsonData, append);
						if (!callback) {
							$nativeUIManager.wattingClose();
						}
						if (typeof callback == 'function') {
							callback();
						}
					} else {
						if (!callback) {
							$nativeUIManager.wattingTitle('未知错误');
						} else {
							$nativeUIManager.watting('未知错误');
						}
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
							if (typeof callback == 'function') {
								callback();
							}
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if (!callback) {
					$nativeUIManager.wattingTitle('未知错误');
				} else {
					$nativeUIManager.watting('未知错误');
				}
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
					if (typeof callback == 'function') {
						callback();
					}
				}, 1500);
			}
		});
	};
	plusReady = function() {
		loadData();
		if (orderBy) {
			$userInfo.put('dataOrderBy', orderBy);
		}
		$updateManager.execute();
		$sideManager.builder('waitingCount', 'manageCount');
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});