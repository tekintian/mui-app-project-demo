define(function(require, exports, module) {
	var $common = require('core/common');
	var $windowManager = require('manager/window');
	var queryMap = parseURL();
	var src = queryMap.get('src');
	plusReady = function() {
		$('#pic').attr('src',src);
		$common.touchSE($('#pic'), function(event, startTouch, o) {}, function(event, o) {
			$windowManager.close();
		});
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});