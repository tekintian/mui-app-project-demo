define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var queryMap = parseURL();
	var provinceId = queryMap.get('provinceId');
	var cityId = queryMap.get('cityId');
	var uuid = queryMap.get('uuid');
	var $dbDataManager = require('manager/dbData');
	var $statusbarResetManager = require('manager/statusbarReset');
	var immersed = 0;
	loadData = function() {
		$nativeUIManager.watting('正在加载省份', false);
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/common/common/provinceList',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token')
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						var sb = new StringBuilder();
						var provinceList = jsonData['provinceList'];
						if (provinceList && $(provinceList).size() > 0) {
							$(provinceList).each(function(i, provinceObj) {
								sb.append(String.formatmodel($templete.getSelectItemItem(true), {
									itemValue: provinceObj['id'],
									itemName: provinceObj['name']
								}));
							});
							$('#itemUL').empty().append(sb.toString());
							if (provinceId) {
								$('li[value="' + provinceId + '"]', '#itemUL').addClass('choosed');
							}
							bindEvent();
						}
						$nativeUIManager.wattingClose();
					} else {
						$nativeUIManager.wattingTitle('未知错误');
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
						}, 1500);
					}
				}
			},
			error: function(jsonData) {
				$nativeUIManager.wattingTitle('未知错误');
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
				}, 1500);
			}
		});
	};
	bindEvent = function() {
		$common.touchSME($('li', '#itemUL'),
			function(startX, startY, endX, endY, event, startTouch, o) {},
			function(startX, startY, endX, endY, event, moveTouch, o) {},
			function(startX, startY, endX, endY, event, o) {
				if (startX == endX && startY == endY) {
					if (!$(o).hasClass('active')) {
						$('li', '#itemUL').removeClass('choosed');
						$(o).addClass('choosed');
						var pId = $(o).attr('value');
						var pName = $(o).attr('text');
						var url = 'provinceCity_cityItem.html?provinceId=' + pId + '&provinceName=' + encodeURIComponent(pName) + '&cityId=' + cityId + '&uuid=' + uuid;
						immersed = $statusbarResetManager.getStatusbarHeight();
						var topOffset = immersed + 45;
						var city = plus.webview.create(url, 'provinceCity_cityItem', {
							top: topOffset + "px",
							bottom: "0px",
							scrollIndicator: 'vertical'
						});
						city.addEventListener("loaded", function() {
							$windowManager.opener().append(city);
						}, false);
						window.setTimeout(function() {
							$(o).removeClass('active');
						}, 100);
					}
				}
			});
	};
	plusReady = function() {
		loadData();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});