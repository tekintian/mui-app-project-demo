define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $controlWindow = require('manager/controlWindow');
	var $statusbarResetManager = require('manager/statusbarReset');
	var queryMap = parseURL();
	var currentId = queryMap.get('id');
	var title = queryMap.get('title');
	var uuid = queryMap.get('uuid');
	var immersed = 0;
	loadWebview = function() {
		var topOffset=immersed+45;
		var item = plus.webview.create('province_item.html?id='+currentId+'&uuid='+uuid, 'province_item', {
			top: topOffset+"px",
			bottom: "0px",
			scrollIndicator: 'vertical'
		});
		item.addEventListener("loaded", function() {
			$windowManager.current().append(item);
		}, false);
	};
	plusReady = function() {
		immersed = $statusbarResetManager.getStatusbarHeight();
		if (immersed) {
			$('header').css({
				paddingTop: immersed + 'px'
			});
		}
		loadWebview();
		title=decodeURIComponent(title);
		$('#selectName').text(title);
		$common.androidBack(function() {
			$userInfo.removeItem('itemJsonData');
			$windowManager.close('slide-out-right');
		});
		$common.touchSE($('#backAction'), function(event, startTouch, o) {
			if (!$(o).hasClass('active')) {
				$(o).addClass('active');
			}
		}, function(event, o) {
			if ($(o).hasClass('active')) {
				$userInfo.removeItem('itemJsonData');
				$windowManager.close('slide-out-right');
				$(o).removeClass('active');
			}
		});
		$common.touchSE($('#okBtn'), function(event, startTouch, o) {
			if (!$(o).hasClass('active')) {
				$(o).addClass('active');
			}
		}, function(event, o) {
			if ($(o).hasClass('active')) {
				var itemWindow=$windowManager.getById('province_item');
				if(itemWindow){
					itemWindow.evalJS('getCurrentId()');
				}
				$(o).removeClass('active');
			}
		});
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});