define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $templete = require('core/templete');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var queryMap = parseURL();
	var provinceId = queryMap.get('provinceId');
	var provinceName = queryMap.get('provinceName');
	var cityId = queryMap.get('cityId');
	var uuid = queryMap.get('uuid');
	var $dbDataManager = require('manager/dbData');
	getCurrentId = function() {
		var current = '';
		var currentText = '';
		if ($('li.choosed', '#itemUL').size() > 0) {
			var li = $('li.choosed', '#itemUL');
			if (li) {
				current = $(li).attr('value');
				currentText = $(li).attr('text');
				var applyEdit = $windowManager.getById('apply_edit');
				if (applyEdit) {
					applyEdit.evalJS('setProvinceCityItem("' + uuid + '","' + provinceName + '","' + provinceId + '","' + currentText + '","' + current + '")');
				}
			}
		} else {
			var applyEdit = $windowManager.getById('apply_edit');
			if (applyEdit) {
				applyEdit.evalJS('setProvinceCityItem("' + uuid + '","","","","")');
			}
		}
		var selectHead = $windowManager.opener().opener();
		if (selectHead) {
			selectHead.close('slide-out-right');
		}
	};
	loadData = function() {
		$nativeUIManager.watting('正在加载城市', false);
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/common/common/cityList',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token'),
				id: provinceId
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						var sb = new StringBuilder();
						var cityList = jsonData['cityList'];
						if (cityList && $(cityList).size() > 0) {
							$(cityList).each(function(i, cityObj) {
								sb.append(String.formatmodel($templete.getSelectItemItem(false), {
									itemValue: cityObj['id'],
									itemName: cityObj['name']
								}));
							});
							$('#itemUL').empty().append(sb.toString());
							if (cityId) {
								$('li[value="' + cityId + '"]', '#itemUL').addClass('choosed');
							}
							bindEvent();
						}
						$nativeUIManager.wattingClose();
					} else {
						$nativeUIManager.wattingTitle('未知错误');
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
						}, 1500);
					}
				}
			},
			error: function(jsonData) {
				$nativeUIManager.wattingTitle('未知错误');
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
				}, 1500);
			}
		});
	};
	bindEvent = function() {
		$windowManager.opener().opener().evalJS('showOkBtn()');
		$common.touchSME($('li', '#itemUL'),
			function(startX, startY, endX, endY, event, startTouch, o) {},
			function(startX, startY, endX, endY, event, moveTouch, o) {},
			function(startX, startY, endX, endY, event, o) {
				if (startX == endX && startY == endY) {
					if (!$(o).hasClass('active')) {
						if (!$(o).hasClass('choosed')) {
							$('li', '#itemUL').removeClass('choosed');
							$(o).addClass('choosed');
						} else {
							$(o).removeClass('choosed');
						}
						window.setTimeout(function() {
							$(o).removeClass('active');
						}, 100);
					}
				}
			});
	};
	plusReady = function() {
		loadData();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});