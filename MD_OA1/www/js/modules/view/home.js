define(function(require, exports, module) {
	var $common = require('core/common');
	var $userInfo = require('core/userInfo');
	var $authorize = require('core/authorize');
	var $nativeUIManager = require('manager/nativeUI');
	var $windowManager = require('manager/window');
	var $controlWindow = require('manager/controlWindow');
	var $statusbarResetManager = require('manager/statusbarReset');
	var immersed = 0;
	bindEvent = function() {
		$common.androidBack(function() {
			$nativeUIManager.confirm('提示', '你确定登出明道OA?', ['确定', '取消'], function() {
				plus.runtime.quit();
			}, function() {});
		});
		$common.touchSE($('span', '#bottomTab'), function(event, startTouch, o) {}, function(event, o) {
			if (!$(o).hasClass('current')) {
				var dir = $(o).attr('dir');
				if (dir) {
					var oldDir = $('span.current', '#bottomTab').attr('dir');
					$('span', '#bottomTab').removeClass('current');

					var lang = $('span[dir="' + dir + '"]', '#bottomTab').attr('lang');
					if (lang) {
						if (lang == "1") {
							if (dir == 'platform') {
								$controlWindow.platformWindowShow();
								var draftYn = $userInfo.get('draftYn');
								if (draftYn == 'Y') {
									var platformLoadPage = $userInfo.get('platformLoadPage');
									if (platformLoadPage) {
										$windowManager.loadOtherWindow('platform_list', 'platform/' + platformLoadPage, function() {});
									}
									$userInfo.put('draftYn', 'N');
								}
							} else if (dir == 'history') {
								$controlWindow.historyWindowShow($userInfo.get('historyType'));
							} else if (dir == 'send') {
								$controlWindow.sendWindowShow();
							} else if (dir == 'search') {
								$controlWindow.searchWindowShow();
							} else if (dir == 'draft') {
								$userInfo.put('draftYn', 'Y');
								$controlWindow.platformWindowShow();
								$windowManager.loadOtherWindow('platform_list', 'history/draftList.html', function() {});
							}
						} else if (lang == "0") {
							if (dir == 'platform') {
								//todo
							} else if (dir == 'history') {
								var historyHead = plus.webview.create("history/head.html", "history_head", {
									top: immersed + "px",
									bottom: "50px"
								});
								historyHead.addEventListener("loaded", function() { //叶面加载完成后才显示
									$windowManager.current().append(historyHead);
									$('span[dir="history"]', '#bottomTab').attr('lang', '1');
								}, false);
							} else if (dir == 'send') {
								var sendHead = plus.webview.create("send/head.html", "send_head", {
									top: immersed + "px",
									bottom: "50px"
								});
								sendHead.addEventListener("loaded", function() { //叶面加载完成后才显示
									$windowManager.current().append(sendHead);
									$('span[dir="send"]', '#bottomTab').attr('lang', '1');
								}, false);
							} else if (dir == 'search') {
								var searchHead = plus.webview.create("search/head.html", "search_head", {
									top: immersed + "px",
									bottom: "50px"
								});
								searchHead.addEventListener("loaded", function() { //叶面加载完成后才显示
									$windowManager.current().append(searchHead);
									$('span[dir="search"]', '#bottomTab').attr('lang', '1');
								}, false);
							} else if (dir == 'draft') {
								$userInfo.put('draftYn', 'Y');
								$controlWindow.platformWindowShow();
								$windowManager.loadOtherWindow('platform_list', 'history/draftList.html', function() {});
								$('span[dir="draft"]', '#bottomTab').attr('lang', '1');
							}
						}
						$(o).addClass('current');
					}
					window.setTimeout(function() {
						if (oldDir) {
							if (oldDir == 'platform') {
								if (dir != 'draft') {
									$controlWindow.platformWindowHide();
								}
							} else if (oldDir == 'history') {
								$controlWindow.historyWindowHide($userInfo.get('historyType'));
							} else if (oldDir == 'send') {
								$controlWindow.sendWindowHide();
								$userInfo.removeItem('applyJsonData');
							} else if (oldDir == 'search') {
								$controlWindow.searchWindowHide();
								$userInfo.removeItem('resultJsonData');
							} else if (oldDir == 'draft') {}
						}
					}, 500);
				}
			}
		});
	};
	loadWebview = function() {
		$userInfo.put('updateTip', 0);
		var workHead = plus.webview.create("platform/head.html", "platform_head", {
			top: immersed + "px",
			bottom: "50px",
			scrollIndicator: 'vertical'
		});
		workHead.addEventListener("loaded", function() { //叶面加载完成后才显示
			$windowManager.current().append(workHead);
			$('span[dir="platform"]', '#bottomTab').attr('lang', '1');
			$userInfo.put('draftYn', 'N');
		}, false);
	};
	callOpen = function(args) {
		var queryMap = parseURL(args);
		if (queryMap && queryMap.size() > 0) {
			$nativeUIManager.watting('正在进行跳转', false);
			window.setTimeout(function() {
				$nativeUIManager.wattingClose();
				var reqId = queryMap.get('id');
				var companyId = queryMap.get('cId');
				companyId = parseFloat(companyId);
				var userId = queryMap.get('uId');
				userId = parseFloat(userId);
				var userUID = $userInfo.get('userUID');
				userUID = parseFloat(userUID);
				var companyUID = $userInfo.get('companyUID');
				companyUID = parseFloat(companyUID);
				if (companyId == companyUID) {
					if (userId == userUID) {
						reqId = parseFloat(reqId);
						if (reqId) {
							$windowManager.create('req_viewHead', 'req/viewHead.html?reqId=' + reqId + '&from=platform', false, true, function(show) {
								show();
								plus.runtime.arguments='';
							});
						}
					} else {
						$nativeUIManager.watting('跳转失败 身份错误', 1000);
					}
				} else {
					$nativeUIManager.watting('跳转失败 网络错误', 1000);
				}
			}, 1000);
		}
	};
	plusReady = function() {
		immersed = $statusbarResetManager.getStatusbarHeight();
		loadWebview();
		bindEvent();
		document.addEventListener("resume", function() {
			$common.switchOS(function() {}, function() {
				var args = plus.runtime.arguments;
				if (args) {
					callOpen(args);
				}
			});
			$authorize.timeout();
		}, false);
		plus.navigator.closeSplashscreen();
	};
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener("plusready", plusReady, false);
	}
});