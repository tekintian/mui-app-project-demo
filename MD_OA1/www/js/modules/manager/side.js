define(function(require, exports, module) {
	var $common = require('core/common');
	var $windowManager = require('manager/window');
	var $nativeUIManager = require('manager/nativeUI');
	var $userInfo = require('core/userInfo');

	function bindEvent(typeId, itemId) {
		$common.touchSE($('.humBtn'), function(event, startTouch, o) {}, function(event, o) {
			var launchWindow = $windowManager.getLaunchWindow();
			// 开启遮罩
			launchWindow.setStyle({
				mask: "rgba(0,0,0,0.5)"
			});
			// 创建侧滑页面
			var sideWindow = plus.webview.create("sideType.html?typeId=" + typeId + '&itemId=' + itemId, "side", {
				left: "30%",
				width: "70%",
				popGesture: 'none',
				scrollIndicator: 'vertical'
			});
			// 侧滑页面关闭后关闭遮罩
			sideWindow.addEventListener('close', function() {
				launchWindow.setStyle({
					mask: "none"
				});
				sideWindow = null;
			}, false);
			launchWindow.addEventListener('maskClick', function() {
				if (sideWindow) {
					sideWindow.close();
				}
				launchWindow.setStyle({
					mask: "none"
				});
			}, false);

			// 侧滑页面加载后显示（避免白屏）
			sideWindow.addEventListener("loaded", function() {
				sideWindow.show("slide-in-right", 200);
			}, false);
		});
	}
	loadWebview = function(jsonData) {
		var dataTypeCountKey = jsonData['dataTypeCountKey'];
		if (dataTypeCountKey && $(dataTypeCountKey).size() > 0) {
			$(dataTypeCountKey).each(function(i, o) {
				$userInfo.put(o, jsonData[o]);
			});
			var dataTypeDesc = jsonData['dataTypeDesc'];
			if (dataTypeDesc) {
				$(dataTypeCountKey).each(function(i, o) {
					$userInfo.put(o + '_', dataTypeDesc[o + '_']);
				});
			}
		}
		var workPlatformTypeCount = $userInfo.get('workPlatformTypeCount');
		workPlatformTypeCount = parseInt(workPlatformTypeCount);
		var loadPage = false;
		var badgeNumber = 0;
		if (workPlatformTypeCount > 0) {
			var waitingCount = $userInfo.get('waitingCount');
			waitingCount = parseInt(waitingCount);
			var reqTypeCount = $userInfo.get('reqTypeCount');
			reqTypeCount = parseInt(reqTypeCount);
			var taskTypeCount = $userInfo.get('taskTypeCount');
			taskTypeCount = parseInt(taskTypeCount);
			var manageTypeCount = $userInfo.get('manageTypeCount');
			manageTypeCount = parseInt(manageTypeCount);
			var shareTypeCount = $userInfo.get('shareTypeCount');
			shareTypeCount = parseInt(shareTypeCount);
			if (waitingCount > 0) {
				var taskCount = $userInfo.get('taskCount');
				taskCount = parseInt(taskCount);
				var manageCount = $userInfo.get('manageCount');
				manageCount = parseInt(manageCount);
				var reqConfirmCount = $userInfo.get('reqConfirmCount');
				reqConfirmCount = parseInt(reqConfirmCount);
				if (taskCount > 0) {
					loadPage = 'taskList.html';
				} else if (manageCount > 0) {
					loadPage = 'manageList.html';
				} else if (reqConfirmCount > 0) {
					loadPage = 'reqConfirmList.html';
				}
			}
			if (!loadPage) {
				if (reqTypeCount > 0) {
					var reqCount = $userInfo.get('reqCount');
					reqCount = parseInt(reqCount);
					var reqPassCount = $userInfo.get('reqPassCount');
					reqPassCount = parseInt(reqPassCount);
					var reqRejectCount = $userInfo.get('reqRejectCount');
					reqRejectCount = parseInt(reqRejectCount);
					if (reqCount > 0) {
						loadPage = 'reqList.html';
					} else if (reqPassCount > 0) {
						loadPage = 'reqPassList.html';
					} else if (reqRejectCount > 0) {
						loadPage = 'reqRejectList.html';
					}
				}
			}

			if (!loadPage) {
				if (taskTypeCount > 0) {
					var taskUnCompleteCount = $userInfo.get('taskUnCompleteCount');
					taskUnCompleteCount = parseInt(taskUnCompleteCount);
					var taskUnManageCount = $userInfo.get('taskUnManageCount');
					taskUnManageCount = parseInt(taskUnManageCount);
					var taskUnConfirmCount = $userInfo.get('taskUnConfirmCount');
					taskUnConfirmCount = parseInt(taskUnConfirmCount);
					var taskPassCount = $userInfo.get('taskPassCount');
					taskPassCount = parseInt(taskPassCount);
					var taskRejectCount = $userInfo.get('taskRejectCount');
					taskRejectCount = parseInt(taskRejectCount);
					if (taskUnCompleteCount > 0) {
						loadPage = 'taskUnCompleteList.html';
					} else if (taskUnManageCount > 0) {
						loadPage = 'taskUnManageList.html';
					} else if (taskUnConfirmCount > 0) {
						loadPage = 'taskUnConfirmList.html';
					} else if (taskPassCount > 0) {
						loadPage = 'taskPassList.html';
					} else if (taskRejectCount > 0) {
						loadPage = 'taskRejectList.html';
					}
				}
			}
			if (!loadPage) {
				if (manageTypeCount > 0) {
					var manageUnConfirmCount = $userInfo.get('manageUnConfirmCount');
					manageUnConfirmCount = parseInt(manageUnConfirmCount);
					if (manageUnConfirmCount > 0) {
						loadPage = 'manageUnConfirmList.html';
					}
				}
			}
			if (!loadPage) {
				if (shareTypeCount > 0) {
					var shareUnReadCount = $userInfo.get('shareUnReadCount');
					shareUnReadCount = parseInt(shareUnReadCount);
					var shareUnCompleteCount = $userInfo.get('shareUnCompleteCount');
					shareUnCompleteCount = parseInt(shareUnCompleteCount);
					if (shareUnReadCount > 0) {
						loadPage = 'shareUnReadList.html';
					} else if (shareUnCompleteCount > 0) {
						loadPage = 'shareUnCompleteList.html';
					}
				}
			}
			if (!loadPage) {
				loadPage = 'blank.html';
			}
			badgeNumber = waitingCount;
			if (shareTypeCount > 0) {
				var shareUnReadCount = $userInfo.get('shareUnReadCount');
				shareUnReadCount = parseInt(shareUnReadCount);
				badgeNumber += shareUnReadCount;
			}
		} else {
			loadPage = 'blank.html';
		}
		if (loadPage) {
			$userInfo.put('platformLoadPage', loadPage);
		}

		$common.switchOS(function() {
			$userInfo.put('userBadgeNumber', badgeNumber);
			plus.runtime.setBadgeNumber(badgeNumber);
		}, function() {});
	};
	exports.loadDataType = function(callback) {
		$.ajax({
			type: 'POST',
			url: $common.getRestApiURL() + '/platform/desktop/dataTypeCount',
			dataType: 'json',
			data: {
				oaToken: $userInfo.get('token')
			},
			success: function(jsonData) {
				if (jsonData) {
					if (jsonData['result'] == '0') {
						loadWebview(jsonData);
						if (typeof callback == 'function') {
							callback($userInfo.get('platformLoadPage'));
						}
					} else {
						$nativeUIManager.watting('未知错误');
						window.setTimeout(function() {
							$nativeUIManager.wattingClose();
						}, 1500);
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$nativeUIManager.watting('未知错误');
				window.setTimeout(function() {
					$nativeUIManager.wattingClose();
				}, 1500);
			}
		});
	};
	exports.builder = function(typeId, itemId) {
		var workPlatformTypeCount = $userInfo.get('workPlatformTypeCount');
		workPlatformTypeCount = parseInt(workPlatformTypeCount);
		var humCount = 0;
		if (workPlatformTypeCount > 0) {
			var waitingCount = $userInfo.get('waitingCount');
			waitingCount = parseInt(waitingCount);
			var reqTypeCount = $userInfo.get('reqTypeCount');
			reqTypeCount = parseInt(reqTypeCount);
			var taskTypeCount = $userInfo.get('taskTypeCount');
			taskTypeCount = parseInt(taskTypeCount);
			var manageTypeCount = $userInfo.get('manageTypeCount');
			manageTypeCount = parseInt(manageTypeCount);
			var shareTypeCount = $userInfo.get('shareTypeCount');
			shareTypeCount = parseInt(shareTypeCount);
			if (waitingCount > 0) {
				var taskCount = $userInfo.get('taskCount');
				taskCount = parseInt(taskCount);
				var manageCount = $userInfo.get('manageCount');
				manageCount = parseInt(manageCount);
				var reqConfirmCount = $userInfo.get('reqConfirmCount');
				reqConfirmCount = parseInt(reqConfirmCount);
				if (taskCount > 0) {
					humCount += 1;
				}
				if (manageCount > 0) {
					humCount += 1;
				}
				if (reqConfirmCount > 0) {
					humCount += 1;
				}
			}
			if (reqTypeCount > 0) {
				var reqCount = $userInfo.get('reqCount');
				reqCount = parseInt(reqCount);
				var reqPassCount = $userInfo.get('reqPassCount');
				reqPassCount = parseInt(reqPassCount);
				var reqRejectCount = $userInfo.get('reqRejectCount');
				reqRejectCount = parseInt(reqRejectCount);
				if (reqCount > 0) {
					humCount += 1;
				}
				if (reqPassCount > 0) {
					humCount += 1;
				}
				if (reqRejectCount > 0) {
					humCount += 1;
				}
			}
			if (taskTypeCount > 0) {
				var taskUnCompleteCount = $userInfo.get('taskUnCompleteCount');
				taskUnCompleteCount = parseInt(taskUnCompleteCount);
				var taskUnManageCount = $userInfo.get('taskUnManageCount');
				taskUnManageCount = parseInt(taskUnManageCount);
				var taskUnConfirmCount = $userInfo.get('taskUnConfirmCount');
				taskUnConfirmCount = parseInt(taskUnConfirmCount);
				var taskPassCount = $userInfo.get('taskPassCount');
				taskPassCount = parseInt(taskPassCount);
				var taskRejectCount = $userInfo.get('taskRejectCount');
				taskRejectCount = parseInt(taskRejectCount);
				if (taskUnCompleteCount > 0) {
					humCount += 1;
				}
				if (taskUnManageCount > 0) {
					humCount += 1;
				}
				if (taskUnConfirmCount > 0) {
					humCount += 1;
				}
				if (taskPassCount > 0) {
					humCount += 1;
				}
				if (taskRejectCount > 0) {
					humCount += 1;
				}
			}
			if (manageTypeCount > 0) {
				var manageUnConfirmCount = $userInfo.get('manageUnConfirmCount');
				manageUnConfirmCount = parseInt(manageUnConfirmCount);
				if (manageUnConfirmCount > 0) {
					humCount += 1;
				}
			}
			if (shareTypeCount > 0) {
				var shareUnReadCount = $userInfo.get('shareUnReadCount');
				shareUnReadCount = parseInt(shareUnReadCount);
				if (shareUnReadCount > 0) {
					humCount += 1;
				}
				var shareUnCompleteCount = $userInfo.get('shareUnCompleteCount');
				shareUnCompleteCount = parseInt(shareUnCompleteCount);
				if (shareUnCompleteCount > 0) {
					humCount += 1;
				}
			}
		}
		var currentTypeCount = -1;
		if (typeId == 'waitingCount') {
			currentTypeCount = waitingCount;
		} else if (typeId == 'reqTypeCount') {
			currentTypeCount = reqTypeCount;
		} else if (typeId == 'taskTypeCount') {
			currentTypeCount = taskTypeCount;
		} else if (typeId == 'manageTypeCount') {
			currentTypeCount = manageTypeCount;
		} else if (typeId == 'shareTypeCount') {
			currentTypeCount = shareTypeCount;
		}
		if (humCount == 1 && currentTypeCount == 0) {
			humCount += 1;
		}
		if (humCount > 1) {
			if ($('.humBtn', '.list_main').size() == 0) {
				$('.list_main').append('<span class="humBtn font22"><i class="icon-menu"></i></span>\n');
			}
			bindEvent(typeId, itemId);
		}
	};
});