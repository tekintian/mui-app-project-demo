var shares=null;

function outLine(msg){
	console.log(msg);
}

function updateSerivces(){
	plus.share.getServices( function(s){
		shares={};
		for(var i in s){
			var t=s[i];
			shares[t.id]=t;
		}
	}, function(e){
		outSet( "获取分享服务列表失败："+e.message );
	} );
}

function shareAction(id,ex,body) {
	var s=null;
	outLine( "分享操作：" );
	if(!id||!(s=shares[id])){
		outLine( "无效的分享服务！" );
		return;
	}
	if ( s.authenticated ) {
		outLine( "---已授权---" );
		shareMessage(s,ex,body);
	} else {
		outLine( "---未授权---" );
		s.authorize( function(){
				shareMessage(s,ex,body);
			},function(e){
			outLine( "认证授权失败："+e.code+" - "+e.message );
		});
	}
}


/**
   * 发送分享消息
   * @param {plus.share.ShareService} s
   */
function shareMessage(s,ex,body){
	var msg={extra:{scene:ex}};
	for (var i = 0; i < Object.keys(body).length; i++) {
		msg[Object.keys(body)[i]] = body[Object.keys(body)[i]];
	}

	s.send( msg, function(){
		plus.nativeUI.alert( "分享到\""+s.description+"\"成功！ " );
	}, function(e){
		plus.nativeUI.alert( "分享到\""+s.description+"\"失败"+e.message);
	} );

}

mui.plusReady(function(){
	updateSerivces();
})