function saveKey(key, item) {
	console.log("Savekey"+key);
	plus.storage.setItem(key, item.toString());

}

function getKey(key) {
	value = plus.storage.getItem(key);
	if(value){
		return value;
	}else{
		return '';
	}
}

function setLocation(position) {
	var codns = position.coords; //获取地理坐标信息；
	var lat = codns.latitude; //获取到当前位置的纬度；
	var longt = codns.longitude; //获取到当前位置的经度
	console.log('lat->' + lat.toString() + ' longt->' + longt.toString());
	saveKey('latitude', lat);
	saveKey('longtitude', longt);

}

function getLocation() {
	console.log('getLocation');
	plus.geolocation.getCurrentPosition(setLocation, function(e) {
		mui.toast( "Geolocation error: " + e.message );
	}, {
		provider: 'baidu',
	});
}

function GBody(id, html) {
	console.log(id);
	G(id).innerHTML = html;
}
function GAppend(id, html) {
	G(id).innerHTML += html;
}

function W(id) {
	return plus.webview.getWebviewById(id);
}

function getUserStatus() {
	mui.ajax(IFBase + 'getUserStatus', {
		data: {
			username: getKey('username'),
			password: getKey('encryptPassword'),
			uuid: plus.device.uuid,
		},
		dataType: 'script', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		success: function(data) {


		},
		error: function(xhr, type, errorThrown) {
		}
	})
}


//导航到
function navigateTo(srcLat,srcLong,desLat,desLong,des){
		// 设置目标位置坐标点和其实位置坐标点
	var src = new plus.maps.Point(srcLong,srcLat);
	var dst = new plus.maps.Point(desLong,desLat);

	plus.maps.openSysMap(  dst, des, src );
}


//拨打电话

function dial(number){
	location.href='tel:'+number;

}

function phoneClick(number){
		plus.device.dial(number);
}
//调用浏览器打开网址;
function openUrl(url){
	plus.runtime.openURL(url);
}

function openWindow(url,id){
	mui.openWindow({
					url: url,
					id: id,
					extras: {

					}
				});
}
function G(id) {
	return document.getElementById(id);
}


function Update() {
	mui.ajax(IFBase + '/update.php?', {
		data: {
			uuid: plus.device.uuid,
			os: plus.device.os,
		},
		dataType: 'html', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		success: function(data) {
			try{

			//	eval(data);
			}catch(e){
				//TODO handle the exception
			}

		},
		error: function(xhr, type, errorThrown) {
		}
	})
}


function ifLogin(){
	username = getKey('username');
	if(username == '' || isNaN(username)){
		return false;
	}else{
		return true;
	}

}

function openLogin(){
	openWindow('./user/user_login.html','login');
}
function getExtention(file_name){
var result =/\.[^\.]+/.exec(file_name);
return result;
}
