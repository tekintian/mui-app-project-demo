//确认收货
function confirmReceiptOrder(memberInfo) {
    $('.receiptOrder').on('click', function (e) {
        var orderId = $(this).data('id');
        $.confirm('您确认收到了商品?', '温馨提醒',
            function() {
                $.ajax({
                    url: ApiUrl+"/api/order/confirmGoodsOrder",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        memberId: memberInfo.memberId,
                        token: memberInfo.token,
                        orderId: orderId
                    },
                    timeout: 30000,
                    success: function(res) {
                        console.log(JSON.stringify(res));
                        if(res.status == 200) {
                            $.toast("确认收货成功");
                            document.location.reload();
                        } else {
                            $.toast("确认收货失败");
                        }
                    },
                    error: function(xhr, errorType, error) {
                        $.toast("网络错误");
                    }
                });
            },
            function() {return false;}
        );
    });
}

function checkLogin() {
	var memberInfo = store.get('memberInfo');
	if(memberInfo == null || memberInfo.memberId == null || memberInfo.memberId == 'undefined') {
		$.confirm('您尚未登录,是否立即登录？', '温馨提醒',
			function() {
				$.router.load("Login_index.html");
			},
			function() {
				$.router.back();
			}
		);
		return false;
	} else {
		return true;
	}
}

function confirmLoginOut() {
	var buttons1 = [{
		text: '退出后不会删除任何历史数据，下次登录依然可以使用本账号。',
		label: true
	}, {
		text: '退出登录',
		color: 'danger',
		onClick: function() {
			store.remove('memberInfo');
			$.router.load("Index_index.html");
		}
	}];
	var buttons2 = [{
		text: '取消',
	}];
	var groups = [buttons1, buttons2];
	$.actions(groups);
}
//取消订单
function confirmCancelOrder (memberInfo) {
    $('.cancelOrder').on('click', function (e) {
        var orderId = $(this).data('id');
        $.confirm('您确定取消此订单吗?', '温馨提醒',
            function() {
                $.ajax({
                    url: ApiUrl + "/api/order/cancelOrder",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        memberId: memberInfo.memberId,
                        token: memberInfo.token,
                        orderId: orderId
                    },
                    timeout: 30000,
                    success: function(res) {
                        if(res.status == 314) {
                            $.alert(res.msg, function() {
                                store.remove('memberInfo');
                                checkLogin();
                            });
                            return;
                        }
                        if(res.status == '200') {
                            $.toast(res.msg);
                            document.location.reload();
                            return;
                        } else {
                            $.toast(res.msg);
                            document.location.reload();
                            return;
                        }
                    },error: function(xhr, errorType, error) {
                        $.toast("网络错误");
                    }
                });
            },
            function() {return false;}
        );
    });
}

function remindShip(memberInfo) {
    $('.remindShip').on('click',function (e) {
        $.showPreloader();
        setTimeout(function () {
            $.hidePreloader();
            $.toast('提醒成功');
        }, 2000);
    });
}

function picLazyLoadInit() {
	picLazyLoad({
		parent: 'content',
		className: "lazyload",
		picError: 'images/loading-800.png',
		callback: function(me) {
			me.classList.add('fadeIn');
		}
	});
}

function goTopInit() {
	if(store.get('immersed')) {
		$('body').css("margin-top", store.get('topoffset'));
	}
	if($("#go-top").length > 0) {
		$("#go-top").remove();
	}
	$(".page-group").append("<i id='go-top'></i>");
	var o = 0.8 * document.body.clientWidth;
	$(".content").on('scroll', function() {
		$("#go-top").css($(".content").scrollTop() > o ? {
			bottom: "16%",
			opacity: 1
		} : {
			bottom: "-16%",
			opacity: 0
		});
	});
	$("#go-top").on("click", function(e) {
		//$('.content').scrollTop(0);
		btn = $(this)[0];
		this.timer = setInterval(function(e) {
				$(".content").scrollTop(Math.floor($(".content").scrollTop() * .8));
				if($(".content").scrollTop() === 0) {
					clearInterval(btn.timer, e);
				}
			},
			10);
	});
}

function appInit() {
	picLazyLoadInit();
	goTopInit();
	var _cartNum = store.get('TotalPro');
	if(_cartNum != null && _cartNum != "0" && _cartNum != "undefined") {
		if($(".badge").length > 0) {
			$(".badge").show();
			$(".badge").html(_cartNum);
		}
	} else {
		if($(".badge").length > 0) {
			$(".badge").hide();
		}
	}
	if($.device.isWeixin){
		console.log("微信浏览器");
	}
	//document.oncontextmenu=shield;
}

//历史足迹
//参数：数组形式
//goodsId, goodsLogo, goodsName, goodsPrice, saleCount
function addHistory(result) {
	if(store.get("userHistory") == undefined) {
		store.set("userHistory", [result]);
	} else {
		var arr1 = store.get("userHistory");
		addData("userHistory", arr1, result);
	}
}

//本地存储写入判断
function addData(stoName, arr1, result) {
	var flag = false;
	if(result == null) {
		//alert('你好，不能添加空数据');
		return arr1;
	}
	for(var i = 0; i < arr1.length; i++) {
		var temp = arr1[i];
		//console.log(JSON.stringify(temp));
		if(temp == null) {
			/*如果当前数据为null结束本次循环*/
			continue;
		} else {
			if(temp.goodsId == result.goodsId) {
				//console.log("重复了");
				//temp.num +1;  如果重复了，就改变值
				break;
			} else {
				if(i == arr1.length - 1) {
					flag = true;
					continue;
				} else {
					continue;
				}
			}
		}
	}
	if(flag) {
		arr1[arr1.length + 1] = result;
		store.set(stoName, arr1);
		//$.toast('历史记录数据添加成功！');
	} else {
		//$.toast('你好，不能重复添加数据');
		return;
	}
}
function getParameterByName(name) {
	var sURL = window.location.search
	var re = new RegExp("" + name + "=([^&?]+)", "ig");
	var result = re.exec(sURL);
	if(result) {
		var temp = result[0].split('=');
		return temp[1];
	} else {
		return "";
	}
}
function havingParameter(){
	if(window.location.search.length > 0) {
		return true;
	}
	else{
		return false;
	}
}
function getPayChannel(bc_channel,channels) {
	var dc_channel_id = '';
	switch (bc_channel) {
		case 'ALI_APP':
			dc_channel_id = 'alipay';
			break;
		case 'WX_APP':
			dc_channel_id = 'wxpay';
			break;
		default:
			break;
	} 

	for (var i in channels) {
		if (channels[i].id == dc_channel_id) {
			return channels[i];
		}
	}
	return null;
}
function getRandomHost() {
	var hosts = ['https://apibj.beecloud.cn',
		'https://apihz.beecloud.cn',
		'https://apisz.beecloud.cn',
		'https://apiqd.beecloud.cn'
	];
	return "" + hosts[parseInt(3 * Math.random())] + "/2/rest/app/bill";
}
//更新本地个人信息
function updateUserInfo() {
    var loginMember = store.get('memberInfo');
    $.ajax({
        url: ApiUrl + "/api/mcenter/userInfo ",
        type: 'post',
        dataType: 'json',
        data: {
            memberId: loginMember.memberId,
            token: loginMember.token
        },
        async: false, //阻塞请求
        timeout: 10000,
        success: function(res) {
            if(res.status == '200') {
                store.remove('memberInfo');
                store.set('memberInfo', res);
                return true;
            } else {
                return false;
            }
        },
        error: function(xhr, errorType, error) {
            return false;
        }
    });
}

function paySubmit(orderInfo,payType,channels){
    if(orderInfo.totalPrice == null || orderInfo.totalPrice == '' || orderInfo.totalPrice == 'undefined'
        || orderInfo.orderCode == null || orderInfo.orderCode == '' || orderInfo.orderCode == 'undefined'){
        $.toast("支付参数不完整");
        $.router.back();
        return;
    }
    if(orderInfo.title == null || orderInfo.title == '' || orderInfo.title == 'undefined'){
    	orderInfo.title = '我订花订单';
    }
    var payData = {
        app_id: BeeCloud_APPID,
        channel: payType,
        title: orderInfo.title,
        total_fee: orderInfo.totalPrice * 100,
        bill_no: orderInfo.orderCode,
        optional: {
            'orderId': orderInfo.orderCode
        },
        bill_timeout: 360,
        return_url: ApiUrl + '/api/pay/callback'
    };

    $.showIndicator();
    $.ajax({
        url: getRandomHost(),
        data: JSON.stringify(payData),
        type: 'post',
        dataType: 'json',
        contentType: "application/json",
        timeout: 30000,
        success: function(data) {
            $.hideIndicator();
            var paySrc = '';
            if(data.result_code == 0) {
                var payChannel = getPayChannel(payType,channels);
                if (payChannel) {
                    if (payChannel.id === 'alipay') {
                        paySrc = data.order_string;
                    } else if (payChannel.id === 'wxpay') {
                        var statement = {};
                        statement.appid = data.app_id;
                        statement.noncestr = data.nonce_str;
                        statement.package = data.package;
                        statement.partnerid = data.partner_id;
                        statement.prepayid = data.prepay_id;
                        statement.timestamp = parseInt(data.timestamp);
                        statement.sign = data.pay_sign;
                        paySrc = JSON.stringify(statement);
                    }
                    plus.payment.request(payChannel,paySrc,
                        function(result) {
                            updateUserInfo();
                            $.alert("恭喜您,支付成功");
                            document.location.reload();

                            // if(result.status == 200) { 不支持接受回调接口返回的参数
                            //     $.alert("恭喜您,支付成功");
                            //     document.location.reload();
                            //     return true;
                            // }else{
                            //     $.toast(result.msg);
                            //     return false;
                            // }
                        }, function(error) {
                            if(error.code=='-100'){
                                $.toast("您已取消支付");
                            }else if(error.code=='-8'){
                                $.toast("手机未安装此客户端");
                            }else{
                                $.toast("支付异常");
                            }
                            return false;
                        }
                    );
                }else{
                    $.toast("暂不支持此类支付方式");
                }
            } else {
                var bcError = {};
                bcError.code = data.result_code;
                bcError.message = data.result_msg + ":" + data.err_detail;
                console.log(bcError);
            }
        },error: function(xhr, errorType, error) {
            $.hideIndicator();
            $.toast("网络错误");
        }
    });
}
//短信接口
function sendSMS(phone, code) {
	var res = null;
	$.ajax({
		url: ApiUrl + "/api/member/code",
		type: 'post',
		dataType: 'json',
		data: {
			mobile: phone,
			code: code
		},
		async: false, //阻塞请求
		success: function(result) {
			res = result.res;
		}
	});
	return res;
}
//输出指定位数的随机数的随机整数
function RndNum(n) {
	var rnd = "";
	for(var i = 0; i < n; i++)
		rnd += Math.floor(Math.random() * 10);
	return rnd;
}

function hasOne(pid) {
	var carList = store.get("carList");
	if(carList.lastIndexOf("&") != -1) {
		var arr = carList.split("&");
		for(i = 0; i < arr.length; i++) {
			if(arr[i].substr(0, arr[i].indexOf("=")) == pid) {
				return false;
			}
		}
	} else if(carList != "null" && carList != "" && carList != "undefined") {
		if(carList.substr(0, carList.indexOf("=")) == pid)
			return false;
	}
	return true;
}

function insertCar(proid, quantity, proname, imgurl, price, procode, des, shopId) {
	if(proid != "" && proname != "") {
		var carList = store.get("carList");
		if(carList != null && carList != "" && carList != "undefined" && carList != "null") {
			if(hasOne(proid)) {
				carList += "&" + proid + "=" + proid + "|" + quantity + "|" + encodeURI(proname) + "|" + encodeURI(imgurl) + "|" + price + "|" + procode + "|" + encodeURI(des) + "|" + shopId;
				store.set("carList", carList);
				var TotalPro = parseInt(store.get("TotalPro"));
				TotalPro += parseInt(quantity);
				store.set("TotalPro", TotalPro);
				var TotalPrice = parseFloat(store.get("TotalPrice"));
				TotalPrice += parseFloat(price) * parseFloat(quantity);
				store.set("TotalPrice", TotalPrice);
			} else {

			}
		} else {
			carList = proid + "=" + proid + "|" + quantity + "|" + encodeURI(proname) + "|" + encodeURI(imgurl) + "|" + price + "|" + procode + "|" + encodeURI(des) + "|" + shopId;
			store.set("carList", carList);
			store.set("TotalPro", quantity);
			store.set("TotalPrice", parseFloat(price) * parseFloat(quantity));
		}
	}
}

function createHtmlAddress() {
	var address = store.get('address');
	var html = '';
	if(address == '' || address == null || address == 'undefined') {
		html += '<a href="#" class="item-link item-content">' +
			'<div class="item-inner">' +
			'<div class="item-text" style="text-align: right;color:#999999;margin-right:15px;">未选择，点击选择地址</div>' +
			'</div>' +
			'</a>';
	} else {
		html += '<a href="#" class="item-link item-content">' +
			'   <div class="item-inner">' +
			'       <div class="item-subtitle">' + address.accepterName + ' - ' + address.accepterMobile + '</div>' +
			'       <div class="item-text">' + address.accepterAddress + '</div>' +
			'    </div>' +
			'</a>';
	}
	$('#address').html(html);
}

function createHtmlCart() {
	var t = store.get("TotalPro");
	if(t != "" && t != "null" && t > 0) {
		var html = "";
		if(store.get("carList").lastIndexOf("&") != -1) {
			var arr = store.get("carList").split("&");
			for(i = 0; i < arr.length; i++) {
				var arr2 = arr[i].split("|");
				html += '<div class="item selected" id="se_div_' + arr2[0].split("=")[0] + '" data-acid="' + arr2[0].split("=")[0] + '"  name="checkgroup">' +
					'<i class="icon_select" data-acid="' + arr2[0].split("=")[0] + '" id="icon_' + arr2[0].split("=")[0] + '"></i>' +
					'<a href="Detail_index.html?goodsId=' + arr2[0].split("=")[0] + '" class="link">' +
					'<img class="image" src="' + decodeURI(arr2[3]) + '" width="80" height="80" alt="">' +
					'</a> ' +
					'<div class="content">' +
					'<input type="hidden" value="' + decodeURI(arr2[7]) + '" id="shopId_' + arr2[0].split("=")[0] + '"/>' +
					'<input type="hidden" value="' + decodeURI(arr2[5]) + '" id="goodsCode_' + arr2[0].split("=")[0] + '"/>' +
					'<input type="hidden" value="' + decodeURI(arr2[3]) + '" id="goodsLogo_' + arr2[0].split("=")[0] + '"/>' +
					'<div class="name"><b id="name_' + arr2[0].split("=")[0] + '">' + decodeURI(arr2[2]) + ' ' + decodeURI(arr2[6]) + '</b></div>' +
					'<p class="price" id="price_' + arr2[0].split("=")[0] + '">' + decodeURI(arr2[4]) + '</p>' +
					'<div class="sku_num">' +
					'<em>数量：</em>' +
					'<div class="num_wrap">' +
					'<span class="minus minus_disabled" id="minus_' + arr2[0].split("=")[0] + '" onclick="OptFlowerNum(' + arr2[0].split("=")[0] + ',0, ' + arr2[4] + ')"></span>' +
					'<input class="num" type="tel" id="num_' + arr2[0].split("=")[0] + '" value="' + decodeURI(arr2[1]) + '" data-max="200" data-prevalue="1">' +
					'<span class="plus"  id="plus_' + arr2[0].split("=")[0] + '" onclick="OptFlowerNum(' + arr2[0].split("=")[0] + ',1, ' + arr2[4] + ')"></span>' +
					'</div> ' +
					'<span class="text"></span>' +
					'</div>' +
					'</div> ' +
					'</div> ';
			}
		} else {
			var arr2 = store.get("carList").split("|");
			html += '<div class="item selected" id="se_div_' + arr2[0].split("=")[0] + '" data-acid="' + arr2[0].split("=")[0] + '"  name="checkgroup">' +
				'<i class="icon_select" data-acid="' + arr2[0].split("=")[0] + '" id="icon_' + arr2[0].split("=")[0] + '"></i>' +
				'<a href="Detail_index.html?goodsId=' + arr2[0].split("=")[0] + '" class="link">' +
				'<img class="image" src="' + decodeURI(arr2[3]) + '" width="80" height="80" alt="">' +
				'</a> ' +
				'<div class="content">' +
				'<input type="hidden" value="' + decodeURI(arr2[7]) + '" id="shopId_' + arr2[0].split("=")[0] + '"/>' +
				'<input type="hidden" value="' + decodeURI(arr2[5]) + '" id="goodsCode_' + arr2[0].split("=")[0] + '"/>' +
				'<input type="hidden" value="' + decodeURI(arr2[3]) + '" id="goodsLogo_' + arr2[0].split("=")[0] + '"/>' +
				'<div class="name"><b  id="name_' + arr2[0].split("=")[0] + '">' + decodeURI(arr2[2]) + ' ' + decodeURI(arr2[6]) + '</b></div>' +
				'<p class="price" id="price_' + arr2[0].split("=")[0] + '">' + decodeURI(arr2[4]) + '</p>' +
				'<div class="sku_num">' +
				'<em>数量：</em>' +
				'<div class="num_wrap">' +
				'<span class="minus minus_disabled" id="minus_' + arr2[0].split("=")[0] + '" onclick="OptFlowerNum(' + arr2[0].split("=")[0] + ',0, ' + arr2[4] + ')"></span>' +
				'<input class="num" type="tel" id="num_' + arr2[0].split("=")[0] + '" value="' + decodeURI(arr2[1]) + '" data-max="200" data-prevalue="1">' +
				'<span class="plus"  id="plus_' + arr2[0].split("=")[0] + '" onclick="OptFlowerNum(' + arr2[0].split("=")[0] + ',1, ' + arr2[4] + ')"></span>' +
				'</div> ' +
				'<span class="text"></span>' +
				'</div>' +
				'</div> ' +
				'</div> ';
		}
		html += '<div class="foot"><p>应付：<span class="price attr-suit-totalprice" id="fact_price">￥' + parseFloat(store.get("TotalPrice")).toFixed(2) + '</span></p></div> ';
		$("#cartinfo").html(html);
		var _ptext = '总计： <b id="totalPrice">￥' + parseFloat(store.get("TotalPrice")).toFixed(2) + '</b><em>（共<span id="totalNum">' + store.get("TotalPro") + '</span>件）</em> ';
		$("#cartTotalPrice").html(_ptext);
	} else {
		$("#cartinfo").html("");
		$("#cartTotalPrice").html("");
		$("#emptyContent").css('display', 'block');
		$("#deleteBtn").css('display', 'none');
		$("#checkAllBtn").css('display', 'none');
	}
	$("#checkAllBtn").addClass("selected");
	$("i[id^=icon_]").on("click", function(e) {
		var Id = $(this).attr("data-acid");
		if($("#se_div_" + Id).hasClass("item selected")) {
			$("#se_div_" + Id).removeClass("selected");
		} else {
			$("#se_div_" + Id).addClass("selected");
		}
		$(".icon-remove").css('display', '');
		$("#shopCartConfirm").css('display', '');
		doAllOrNot();
	});
	var _cartNum = store.get('TotalPro');
	if(_cartNum == null || _cartNum == "0" || _cartNum == "undefined") {
		$(".icon-remove").css('display', 'none');
	}
}

function createHtmlOrder() {
	var t = store.get("TotalPro");
	if(t != "" && t != "null" && t > 0) {
		var html = '<li><div class="item-link item-content item-subtitle">商品信息</div></li>';
		if(store.get("carList").lastIndexOf("&") != -1) {
			var arr = store.get("carList").split("&");
			for(i = 0; i < arr.length; i++) {
				var arr2 = arr[i].split("|");
				html += '<li>' +
					'<a class="item-link item-content" href="Detail_index.html?goodsId=' + arr2[0].split("=")[0] + '">' +
					'<div class="item-media"><img width="80" src="' + decodeURI(arr2[3]) + '"></div>' +
					'<div class="item-inner">' +
					'<div class="item-title-row"><div class="item-title goods">' + decodeURI(arr2[2]) + '</b>-' + decodeURI(arr2[6]) + '</div></div>' +
					'<div class="item-text goods">' +
					'<div><span class="txt_color_red price">￥：' + arr2[4] +
					'</span> × ' + arr2[1] + '</div>' +
					'</div></div></a></li>';
			}
		} else {
			var arr = store.get("carList").split("|");
			html += '<li>' +
				'<a class="item-link item-content" href="Detail_index.html?goodsId=' + arr[0].split("=")[0] + '">' +
				'<div class="item-media"><img width="80" src="' + decodeURI(arr[3]) + '"></div>' +
				'<div class="item-inner">' +
				'<div class="item-title-row"><div class="item-title goods">' + decodeURI(arr[2]) + '</b>-' + decodeURI(arr[6]) + '</div></div>' +
				'<div class="item-text goods">' +
				'<div><span class="txt_color_red price">￥：' + arr[4] +
				'</span> × ' + arr[1] + '</div>' +
				'</div></div></a></li>';
		}
		html += '<div class="foot"><p>应付：<span class="price attr-suit-totalprice" id="fact_price">￥' + parseFloat(store.get("TotalPrice")).toFixed(2) + '</span></p></div> ';
		$("#order_carts_info").html(html);
		// $("#pageTotalPrice").html("¥"+parseFloat(store.get("TotalPrice")).toFixed(2)+"");
		// $("#TotalPrice").val(parseFloat(store.get("TotalPrice")).toFixed(2));
	}
}

function OptFlowerNum(proid, optType, _price) {
	var crtNum = parseInt($("#num_" + proid + "").val());
	if(optType == 0 && crtNum <= 1) {
		return;
	}
	if(optType == "0") {
		crtNum--;
		$("#num_" + proid + "").val(crtNum);
		updateQuantity(proid, crtNum, _price);
		SelectCount();
	}
	if(optType == "1") {
		crtNum++;
		$("#num_" + proid + "").val(crtNum);
		updateQuantity(proid, crtNum, _price);
		SelectCount();
	}
}

function updateQuantity(proid, quantity, _price) {
	var ProIDList = store.get("carList");
	if(ProIDList.lastIndexOf("&") != -1) {
		var arr = ProIDList.split("&");
		var sub = getSubPlace(ProIDList, proid);
		var arr2 = arr[sub].split("|");
		var oQuantity = parseInt(arr2[1]);
		var oPrice = parseFloat(oQuantity) * parseFloat(arr2[4]);
		arr2[1] = quantity;
		var tempStr = arr2.join("|");
		arr[sub] = tempStr;
		var newProList = arr.join("&");
		store.set("carList", newProList);
		var t = parseInt(store.get("TotalPro"));
		store.set("TotalPro", t - oQuantity + quantity);
		var pp = parseFloat(store.get("TotalPrice"));
		store.set("TotalPrice", pp - oPrice + parseFloat(_price) * parseFloat(quantity));
	} else {
		var arr = ProIDList.split("|");
		arr[1] = quantity;
		var newProList = arr.join("|");
		store.set("carList", newProList);
		store.set("TotalPro", quantity);
		store.set("TotalPrice", parseFloat(_price) * parseFloat(quantity));
	}
}

function reMoveOne(proid, quantity, _price) {
	var ProIDList = store.get("carList");
	if(!hasOne(proid)) {
		if(ProIDList.lastIndexOf("&") != -1) {
			var arr = ProIDList.split("&");
			for(i = 0; i < arr.length; i++) {
				if(arr[i].substr(0, arr[i].indexOf("=")) == proid) {
					var arr2 = delArr(arr, i);
					var tempStr = arr2.join("&");
					store.set("carList", tempStr, 2, "/");
					var t = parseInt(store.get("TotalPro"));
					store.set("TotalPro", t - quantity, 2, "/");
					var pp = parseFloat(store.get("TotalPrice"));
					store.set("TotalPrice", pp - parseFloat(_price) * parseFloat(quantity), 2, "/");
					return;
				}
			}
		} else {
			store.set("carList", "null", 2, "/");
			store.set("TotalPro", 0, 2, "/");
			store.set("TotalPrice", 0, 2, "/");
		}
	}
}

function delArr(ar, n) {
	if(n < 0)
		return ar;
	else
		return ar.slice(0, n).concat(ar.slice(n + 1, ar.length));
}

/**
 *计算选择项
 */
function SelectCount() {
	var _s = $("div[name^=checkgroup]");
	var count = 0;
	var _Tq = 0;
	var _Tp = 0;
	for(var i = 0; i < _s.length; i++) {
		if($(_s[i]).hasClass("item selected")) {
			count++;
			var Id = $(_s[i]).attr("data-acid");
			var _quantity = parseInt($("#num_" + Id + "").val());
			var _price = parseFloat($("#price_" + Id + "").text());
			if(_Tq == 0) {
				_Tq = _quantity;
				_Tp = parseFloat(_price) * parseFloat(_quantity);
			} else {
				_Tq = _Tq + _quantity;
				_Tp = parseFloat(_Tp) + parseFloat(_price) * parseFloat(_quantity);
			}
		}
	}
	var _ptext = '总计： <b id="totalPrice">￥' + _Tp.toFixed(2) + '</b><em>（共<span id="totalNum">' + _Tq + '</span>件）</em> ';
	$("#cartTotalPrice").html(_ptext);
	$("#fact_price").html('￥' + _Tp.toFixed(2) + '');
	return count;
}

function doAllOrNot() {
	var _s = $("div[name^=checkgroup]");
	var _count = SelectCount();
	if(_count == _s.length) {
		$("#checkAllBtn").removeClass('pay_bar');
		$("#checkAllBtn").addClass("pay_bar selected");
	} else if(_count == 0) {
		$(".icon-remove").css('display', 'none');
		$("#shopCartConfirm").css('display', 'none');
		$("#checkAllBtn").removeClass("pay_bar selected");
		$("#checkAllBtn").addClass("pay_bar");
	} else {
		$("#checkAllBtn").removeClass("pay_bar selected");
		$("#checkAllBtn").addClass("pay_bar");
		$(".icon-remove").css('display', '');
		$("#shopCartConfirm").css('display', '');
	}
}

//判断是否有选择
function IsSelected() {
	var _s = $("div[name^=checkgroup]");
	for(var i = 0; i < _s.length; i++) {
		if($(_s[i]).hasClass("item selected")) {
			return true;
		}
	}
	return false;
}

function getSubPlace(list, proid) {
	var arr = list.split("&");
	for(i = 0; i < arr.length; i++) {
		if(arr[i].substr(0, arr[i].indexOf("=")) == proid) {
			return i;
		}
	}
}

function deleteAll() {
	var _s = $("div[name^=checkgroup]");
	for(var i = 0; i < _s.length; i++) {
		if($(_s[i]).hasClass("item selected")) {
			var Id = $(_s[i]).attr("data-acid");
			var _quantity = $("#num_" + Id + "").val();
			var _price = $("#price_" + Id + "").text();
			reMoveOne(Id, _quantity, parseFloat(_price));
		}
	}
	createHtmlCart();
}