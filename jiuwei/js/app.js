var ApiUrl = 'http://app.wodinghua.com'; //接口地址
var myPhotoBrowserStandalone = null; //
//var BeeCloud_APPID = '44f01a13-965f-4b27-ba9f-da678b47f3f5'; //测试环境
var BeeCloud_APPID = '7fb135de-d1e6-4cd6-8748-6842dc50d388'; //正式环境
$(function() {
	if(store.get('immersed')) {
		$('body').css("margin-top", store.get('topoffset'));
	}
	//console.log(JSON.stringify($.device.isWeixin));
	$(document).on("pageInit", "#index_index", function(e, id, page) {
		var $nav = $(page).find('#nav_index');
		$nav.addClass("active");
		var indexRes = null;
		$.ajax({
			url: ApiUrl + "/api/index/index",
			type: 'get',
			dataType: 'json',
			timeout: 30000,
			success: function(res) {
				indexRes = res;
				//console.log(JSON.stringify(res));
				if($(".swiper-wrapper").children().length < 1) {
					$('#swiper').html(template('ad13ListTemplate', res));
					$(".swiper-container").swiper({
						speed: 1000,
						autoplay: 3000
					});
				}
				if($("#recommend").children().length < 1) {
					$('#recommend').html(template('ad14ListTemplate', res));
				}
				if($("#home-cate").children().length < 2) {
					$('#home-cate').html(template('ad15ListTemplate', res));
				}
				if($("#indexArticleListTemplateTab1 .tab-list").children().length < 1) {
					$('#tab1').html(template('indexArticleListTemplateTab1', res));
				}
				if($("#indexArticleListTemplateTab2 .tab-list").children().length < 1) {
					$('#tab2').html(template('indexArticleListTemplateTab2', res));
				}
				if($("#indexArticleListTemplateTab3 .tab-list").children().length < 1) {
					$('#tab3').html(template('indexArticleListTemplateTab3', res));
				}
				if($("#indexArticleListTemplateTab4 .tab-list").children().length < 1) {
					$('#tab4').html(template('indexArticleListTemplateTab4', res));
				}
				picLazyLoadInit();

			}
		});
		var obj = $(".panel-item");
		obj.each(function() {
			var x = 6;
			var y = 1;
			var rand = parseInt(Math.random() * (x - y + 1) + y);
			$(this).addClass("tags" + rand);
		});
		$(".panel-item").on("click", function() {
			var kw = $(this).attr("data");
			$.router.load("Search_index.html?keyword=" + kw);
		});
		$("#bt_wx_share").on("click", function(e) {
			shareWeixinMessage();
		});
		if(!$.plus) {
			$("#bt_wx_share").hide();
		}
		var msg = {};
		msg.href = "http://app.wodinghua.com/dl.php";
		msg.thumbs = ["_www/images/logo.png"];
		msg.pictures = ["_www/images/logo.png"];

		function shareWeixinMessage() {
			var buttons1 = [{
				text: '分享',
				label: true
			}, {
				text: '微信朋友圈',
				onClick: function() {
					msg.content = "我订花-10年专注鲜花速递，中国鲜花速递领先品牌。";
					msg.extra = {
						scene: "WXSceneTimeline"
					};
					sharewx.send(msg,
						function() {
							$.toast("分享成功");
						},
						function(e) {
							//console.log("分享失败：" + e.message);
						});
				}
			}, {
				text: '微信好友',
				onClick: function() {
					msg.title = "我订花";
					msg.content = "我订花-10年专注鲜花速递，中国鲜花速递领先品牌。";
					msg.extra = {
						scene: "WXSceneSession"
					};
					sharewx.send(msg,
						function() {
							$.toast("分享成功");
						},
						function(e) {
							//console.log("分享失败：" + e.message);
						});
				}
			}];
			var buttons2 = [{
				text: '取消',
				color: 'danger',
			}];
			var groups = [buttons1, buttons2];
			$.actions(groups);
		}
		//扩展API准备完成后要执行的操作
		function plusReady() {
			plus.share.getServices(function(s) {
				shares = s;
				for(var i in s) {
					if('weixin' == s[i].id) {
						sharewx = s[i];
					}
				}
			}, function(e) {
				alert("获取分享服务列表失败：" + e.message);
			});
			//console.log("Index_index.html页面扩展API准备完毕。");
		}
		//扩展API是否准备，如果没有则监听“plusready"事件
		if(window.plus) {
			plusReady();
		} else {
			document.addEventListener('plusready', plusReady, false);
		}
		$(".page").on('refresh', '.pull-to-refresh-content', function(e) {
			$.ajax({
			url: ApiUrl + "/api/index/index",
			type: 'get',
			dataType: 'json',
			timeout: 30000,
			success: function(res) {
				indexRes = res;
				//console.log(JSON.stringify(res));
				$('#swiper').html(template('ad13ListTemplate', res));
					$(".swiper-container").swiper({
						speed: 1000,
						autoplay: 3000
					});
					$('#recommend').html(template('ad14ListTemplate', res));
					$('#home-cate').html(template('ad15ListTemplate', res));
					$('#tab1').html(template('indexArticleListTemplateTab1', res));
					$('#tab2').html(template('indexArticleListTemplateTab2', res));
					$('#tab3').html(template('indexArticleListTemplateTab3', res));
					$('#tab4').html(template('indexArticleListTemplateTab4', res));
				
				picLazyLoadInit();
				$.toast("刷新成功");
				$.pullToRefreshDone('.pull-to-refresh-content');

			},
				error: function(xhr, errorType, error) {
					$.toast("网络加载失败");
					$.pullToRefreshDone('.pull-to-refresh-content');
				}
		});
		});
		appInit();
	});

	$(document).on("pageInit", "#article_list", function(e, id, page) {

		var loading = false;
		var type = 58;
		var p = 1;
		var pageall = 2;

		function loadArticleList(type, p) {
			p = ($("#article_list li").length / 5) + 1;
			//console.log("type参数是：" + type);
			//console.log("p参数是：" + p);
			$('.infinite-scroll-preloader').show();
			$.ajax({
				url: ApiUrl + "/api/article/index",
				type: 'get',
				dataType: 'json',
				timeout: 30000,
				data: {
					type: type,
					p: p
				},
				success: function(res) {
					//console.log(JSON.stringify(res));
					$("#article_list_Dom").append(template('articleListTemplate', res));
					$('.infinite-scroll-preloader').hide();
					pageall = res.page.total;
					$("#mark").css("display", "none");
					pageall = res.page.total;
					if(p >= pageall) {
						$('.infinite-scroll-preloader').hide();
					}
					picLazyLoadInit();
				},
				error: function() {
					$.toast("网络错误");
				}
			});
		}
		if($("#article_list_Dom").children().length > 0) {

		} else {
			loadArticleList(type, p);
		}

		$("#article_list .buttons-tab a").on("click", function() {
			loading = true;
			$("#mark").css("display", "table");
			$("#article_list_Dom").html("");
			$('.infinite-scroll-preloader').show();
			p = 1;
			$(this).addClass("active").siblings().removeClass("active");
			loadArticleList($(this).attr("data-num"), p);
		});

		$(page).on('infinite', function(e) {
			// 如果正在加载，则退出
			if(loading)
				return;
			loading = true;
			if(p >= pageall) {
				$.toast("没有更多");
				$('.infinite-scroll-preloader').hide();
				return;
			}
			p = p + 1;
			loadArticleList(type, p);
		});

		appInit();
	});

	$(document).on("pageInit", "#article_detail", function(e, id, page) {
		var articleId = "";
		if(havingParameter()) {
			if(getParameterByName("articleId").length > 0) {
				articleId = getParameterByName("articleId");
			}
		}

		$('.infinite-scroll-preloader').show();
		$.ajax({
			url: ApiUrl + "/api/article/detail",
			type: 'get',
			dataType: 'json',
			timeout: 30000,
			data: {
				articleId: articleId
			},
			success: function(res) {
				//console.log(JSON.stringify(res));

				$("#article-Detail-Dom").html(template('articleDetailDomSpt', res));
				$("#header-articleTitle").html(res.detail.articleTitle);
				$('.infinite-scroll-preloader').hide();
			},
			error: function() {
				$('.infinite-scroll-preloader').hide();
			}
		});
	});

	$(document).on("pageInit", "#search_index", function(e, id, page) {
		//初始化参数
		var loading = false;
		var ot = 0; //初始化关键字搜索
		var pageno = 1; //初始化当前页数
		var pageall = 2; //初始化总页数
		var _keyWords = "";
		//$("#goodsList").html("");
		var $nav = $(page).find('#nav_search');
		$nav.addClass("active");

		if(havingParameter()) {
			if(getParameterByName("ot").length > 0) {
				ot = getParameterByName("ot");
			}
			if(getParameterByName("pageno").length > 0) {
				pageno = getParameterByName("pageno")
			}
			if(getParameterByName("pageall").length > 0) {
				pageall = getParameterByName("pageall");
			}
			if(getParameterByName("keyword").length > 0) {
				_keyWords = getParameterByName("keyword");
				$("#search").val(decodeURIComponent(_keyWords));
			}
		}

		$(".searchkey").on("click", function() {
			_keyWords = $(this).attr("data");
			$.router.load("Search_index.html?keyword=" + _keyWords);
		});

		var obj = $(".panel-item");
		obj.each(function() {
			var x = 6;
			var y = 1;
			var rand = parseInt(Math.random() * (x - y + 1) + y);
			$(this).addClass("tags" + rand);
		});
		if($("#goodsList").children().length > 0) {

		} else {
			addItems(ot, pageno, _keyWords);
		}

		$("#search").on("search", function(e) {
			pageno = 1;
			pageall = 2;
			$('.infinite-scroll-preloader').show();
			$.attachInfiniteScroll($('.infinite-scroll'));
			if($("#mark").length > 0) {
				$("#mark").css("display", "table");
			}
			_keyWords = $("#search").val();
			//console.log("关键词：" + _keyWords);
			$("#goodsList").html("");
			addItems('', 1, _keyWords);
		});
		$("#search").on("blur", function(e) {
			pageno = 1;
			pageall = 2;
			$('.infinite-scroll-preloader').show();
			$.attachInfiniteScroll($('.infinite-scroll'));
			if($("#mark").length > 0) {
				$("#mark").css("display", "table");
			}
			_keyWords = $("#search").val();
			//console.log("关键词：" + _keyWords);
			$("#goodsList").html("");
			addItems('', 1, _keyWords);
		});

		$("#gridIcon").on("click", function(e) {
			if($("#gridIcon").hasClass("state_switch")) {
				$("#gridIcon").removeClass("state_switch");
				$("#goodsList_content").removeClass().addClass("grid");
				$('.grid .item-media img').css('height', $('.grid .item-media img').css('width'));
			} else {
				$("#gridIcon").addClass("state_switch");
				$("#goodsList_content").removeClass().addClass("list-block media-list");
				$('.item-media img').css('height', '80px');
			}
		});

		$(".my_menu_s li").on("click", function() {

			var txt = $.trim($(this).text());
			var i = $(this).index();
			if(txt == "") {
				return;
			}
			if(txt == "评价") {
				ot = 4;
			}
			if(txt == "销量") {
				ot = 3;
			}
			if(txt == "价格") {

				if($(this).children('a').hasClass('state_switch_d')) {
					ot = 2;
				} else {
					ot = 5;
				}
			}
			pageno = 1;
			pageall = 2;
			$("#goodsList").html("");
			if($("#mark").length > 0) {
				$("#mark").css("display", "table");
			}
			addItems(ot, pageno, _keyWords);
		});

		function addItems(ot, pageno, keyword) {
			//console.log('------------------------API请求参数------------------------');
			pageno = ($("#goodsList li").length / 16) + 1;
			//console.log("ot:" + ot);
			//console.log("pageno:" + pageno);
			//console.log("keyword:" + keyword);

			$.ajax({
				url: ApiUrl + "/api/product/index",
				type: 'get',
				dataType: 'json',
				//async: false, //阻塞请求
				data: {
					ot: ot,
					p: pageno,
					keyword: keyword
				},
				timeout: 30000,
				success: function(res) {
					//console.log(JSON.stringify(res));
					pageall = res.page.total;
					if(pageno >= pageall) {
						$('.infinite-scroll-preloader').hide();
					}
					if(res.ot == 3) {
						$("#orderBySale").addClass("select");
						$("#orderByComments").removeClass("select");
						$("#orderByPrice").removeClass("select");

					}
					if(res.ot == 4) {
						$("#orderByComments").addClass("select");
						$("#orderBySale").removeClass("select");
						$("#orderByPrice").removeClass("select");
					}
					if(res.ot == 5) {
						$("#orderByPrice").removeClass().addClass("select state_switch_d");
						$("#orderBySale").removeClass("select");
						$("#orderByComments").removeClass("select");
					}
					if(res.ot == 2) {
						$("#orderByPrice").removeClass().addClass("select state_switch_u");
						$("#orderBySale").removeClass("select");
						$("#orderByComments").removeClass("select");
					}
					if(res != null) {
						var html = template('goodsListTemplate', res);
						$('#goodsList').append(html);
						$('.grid .item-media img').css('height', $('.grid .item-media img').css('width')); //图片尺寸有细微偏差
						if($("#mark").length > 0) {
							$("#mark").hide();
						}
						picLazyLoadInit();
						//console.log("li的数量是： "+$("#goodsList li").length);
					}
					loading = false;
				}
			});

		}

		$(page).on('infinite', function() {
			// 如果正在加载，则退出
			if(loading)
				return;
			loading = true;
			if(pageno >= pageall) {
				//$.detachInfiniteScroll($('.infinite-scroll'));
				$('.infinite-scroll-preloader').hide();
				return;
			}
			pageno = ++pageno;
			//console.log(pageno);
			addItems(ot, pageno, _keyWords);
			$.refreshScroller();
		});

		//扩展API准备完成后要执行的操作
		function plusReady() {
			//console.log("Search_index.html页面扩展API准备完毕。");
		}
		//扩展API是否准备，如果没有则监听“plusready"事件
		if(window.plus) {
			plusReady();
		} else {
			document.addEventListener('plusready', plusReady, false);
		}
		appInit();
	});

	$(document).on("pageInit", "#category_index", function(e, id, page) {

		var searchEle = $("#category_index .today-hot-search");
		var listEle = $("#category_index .category-list-box");
		$("#search").on("focus", function() {
			searchEle.css("display", "block");
			listEle.css("display", "none");
		});
		$(".category-list ul li").on("click", function() {
			var keywords = $(this).text();
			var url = "Search_index.html?keyWords=" + encodeURIComponent(keywords);
			$.router.load(url);
		});
		$("#search").keyup(function(e) {
			if(e.which == 13) {
				var searchVal = $("#search").val();
				if(searchVal == "") {
					alert("搜索关键字不能为空");
				} else {
					alert("关键字：" + searchVal + ", 开始搜索中......");
				}
			}
		});
		$("#category_index .searchbar-cancel").on("click", function() {
			var searchVal = $("#search").val("");
			searchEle.css("display", "none");
			listEle.css("display", "block");
		});
		var hotSearchKeyWords = {
			"hotKWs": [{
				"color": "#BF8DA8",
				"keywords": "圣诞节"
			}, {
				"color": "#E29C94",
				"keywords": "送爱人"
			}, {
				"color": "#CCB0AD",
				"keywords": "爱情"
			}, {
				"color": "#BC8EA8",
				"keywords": "绝世好花"
			}, {
				"color": "#92BAC2",
				"keywords": "19朵玫瑰"
			}, {
				"color": "#B194C0",
				"keywords": "情人节"
			}, {
				"color": "#A2BE98",
				"keywords": "全城热恋"
			}, {
				"color": "#92BAC2",
				"keywords": "蓝玫瑰"
			}, {
				"color": "#C3AB93",
				"keywords": "玫瑰花"
			}, {
				"color": "#CCB0AD",
				"keywords": "闺蜜"
			}, {
				"color": "#92BAC2",
				"keywords": "礼盒"
			}, {
				"color": "#C3AB93",
				"keywords": "甜蜜礼物"
			}]
		};
		var _html = template('todayHotSearchDom', hotSearchKeyWords);
		$('#todayHotSearch').append(_html);

		$("#category_index .today-hot-search ul li").on("click", function() {
			alert($(this).text());
		});
		appInit();
	});

	$(document).on("pageInit", "#user_index", function(e, id, page) {
		if(!checkLogin()) return false;
		var $nav = $(page).find('#nav_user');
		$nav.addClass("active");
		var loginMember = store.get('memberInfo');
		//console.log(JSON.stringify(loginMember));
		if(loginMember != undefined) {
			$(".member-name").text(loginMember.memberName);
			if(loginMember.headImgUrl != 'null' && loginMember.headImgUrl != undefined && loginMember.headImgUrl != "") {
				$("#member_logo").attr("src", loginMember.headImgUrl);
			}
			$("#my_turnover").text(loginMember.turnover);
			$("#my_integral").text(loginMember.integral);
		}
		$(".page").on('refresh', '.pull-to-refresh-content', function(e) {
			$.ajax({
				url: ApiUrl + "/api/mcenter/userInfo",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: loginMember.memberId,
					token: loginMember.token
				},
				timeout: 10000,
				success: function(res) {
					if(res.status == '200') {
						//console.log(res);
						store.remove('memberInfo');
						store.set('memberInfo', res);
						loginMember = store.get('memberInfo');
						if(loginMember != undefined) {
							$(".member-name").text(loginMember.memberName);
							if(loginMember.headImgUrl != 'null' && loginMember.headImgUrl != undefined && loginMember.headImgUrl != "") {
								$("#member_logo").attr("src", loginMember.headImgUrl);
							}
							$("#my_turnover").text(loginMember.turnover);
							$("#my_integral").text(loginMember.integral);
						}
						$.toast("刷新成功");
						$.pullToRefreshDone('.pull-to-refresh-content');

					} else {
						$.toast("网络加载失败");
						$.pullToRefreshDone('.pull-to-refresh-content');
					}
				},
				error: function(xhr, errorType, error) {
					$.toast("网络加载失败");
					$.pullToRefreshDone('.pull-to-refresh-content');
				}
			});

		});
		appInit();
	});

	$(document).on("pageInit", "#user_favorite", function(e, id, page) {
		if(!checkLogin()) return false;
		var loading = false;
		var p = 1;
		var pageall = "2";

		$('#user_favorite_list').html("");
		initFavorite(p);

		function initFavorite(p) {
			var loginMember = store.get('memberInfo');
			var memberId = loginMember.memberId;
			var token = loginMember.token;
			$('.infinite-scroll-preloader').show();
			$.ajax({
				url: ApiUrl + "/api/mcenter/favorite",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: loginMember.memberId,
					token: loginMember.token,
					p: p
				},
				success: function(res) {
					$('.infinite-scroll-preloader').hide();
					//console.log(JSON.stringify(res));
					if(res.status == 314) {
						$.alert(res.msg, function() {
							store.remove('memberInfo');
							$.router.load("Login_index.html");
						});
						return;
					}
					if(res.dataList == null) {
						$("#mark").hide();
						$("#favoriteEmptyContent").css("display", "block");
					} else {
						//console.log(JSON.stringify(res));
						var _html = template('user_favorite_content', res);
						$('#user_favorite_list').append(_html);
						picLazyLoadInit();
						pageall = res.page.total;
						$("#mark").css("display", "none");
					};
					loading = false;
				},
				error: function() {
					$.toast("网络加载失败");
				}
			});
		}

		$(page).on('infinite', function() {
			// 如果正在加载，则退出
			if(loading)
				return;
			loading = true;
			if(p >= pageall) {
				$.toast("没有更多");
				$('.infinite-scroll-preloader').hide();
				return;
			}
			p = ++p;
			initFavorite(p);
			$.refreshScroller();
		});

		appInit();
	});

	$(document).on("pageInit", "#user_signed", function(e, id, page) {
		var signed = {
			"timeList": [{
				"today": "1",
				"time": "1",
				"signed": "0",
				"int": ""
			}, {
				"today": "0",
				"time": "2",
				"signed": "1",
				"int": ""
			}, {
				"today": "0",
				"time": "3",
				"signed": "2",
				"int": ""
			}, {
				"today": "0",
				"time": "4",
				"signed": "3",
				"int": ""
			}, {
				"today": "0",
				"time": "5",
				"signed": "4",
				"int": ""
			}, {
				"today": "0",
				"time": "6",
				"signed": "5",
				"int": ""
			}, {
				"today": "0",
				"time": "7",
				"signed": "6",
				"int": ""
			}, {
				"today": "0",
				"time": "8",
				"signed": "7",
				"int": "100积分"
			}, {
				"today": "0",
				"time": "9",
				"signed": "8",
				"int": ""
			}, {
				"today": "0",
				"time": "10",
				"signed": "9",
				"int": ""
			}, {
				"today": "0",
				"time": "11",
				"signed": "0",
				"int": ""
			}, {
				"today": "0",
				"time": "12",
				"signed": "1",
				"int": ""
			}, {
				"today": "0",
				"time": "13",
				"signed": "2",
				"int": ""
			}, {
				"today": "0",
				"time": "14",
				"signed": "3",
				"int": ""
			}, {
				"today": "0",
				"time": "15",
				"signed": "4",
				"int": ""
			}, {
				"today": "0",
				"time": "16",
				"signed": "5",
				"int": ""
			}, {
				"today": "0",
				"time": "17",
				"signed": "6",
				"int": ""
			}, {
				"today": "0",
				"time": "18",
				"signed": "7",
				"int": "100积分"
			}, {
				"today": "0",
				"time": "19",
				"signed": "8",
				"int": ""
			}, {
				"today": "0",
				"time": "20",
				"signed": "9",
				"int": ""
			}, {
				"today": "0",
				"time": "21",
				"signed": "10",
				"int": ""
			}, {
				"today": "0",
				"time": "22",
				"signed": "11",
				"int": ""
			}, {
				"today": "0",
				"time": "23",
				"signed": "12",
				"int": ""
			}, {
				"today": "0",
				"time": "24",
				"signed": "13",
				"int": ""
			}, {
				"today": "0",
				"time": "25",
				"signed": "14",
				"int": ""
			}, {
				"today": "0",
				"time": "26",
				"signed": "15",
				"int": "300积分"
			}, {
				"today": "0",
				"time": "27",
				"signed": "0",
				"int": ""
			}, {
				"today": "0",
				"time": "28",
				"signed": "1",
				"int": ""
			}, {
				"today": "0",
				"time": "29",
				"signed": "2",
				"int": ""
			}, {
				"today": "0",
				"time": "30",
				"signed": "3",
				"int": ""
			}, {
				"today": "0",
				"time": "31",
				"signed": "4",
				"int": ""
			}]
		};
		var _html = template('signedList', signed);
		$('#userSignedList').append(_html);
	});
	$(document).on("pageInit", "#login_index", function(e, id, page) {
		var auths = null;
		// 登录操作，获取登录用户信息操作
		if(!$.plus) {
			$("#regLink").show();
			$("#otherLogin").hide();
		}

		function wxPostLogin(userJson) {
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/member/wechatLogin",
				type: 'post',
				dataType: 'json',
				data: {
					token: userJson
				},
				success: function(res) {
                    $.hideIndicator();
					if(res.status != 202) {
						$.alert(res.msg);
						return false;
					} else {
						$.toast(res.msg);
						store.remove('memberInfo');
						store.set('memberInfo', res);
						$.router.back();
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		}

		function authLogin() {
			$.showIndicator();
			var s = null;
			for(var i = 0; i < auths.length; i++) {
				if(auths[i].id == 'weixin') {
					s = auths[i];
				}
			}
			if(!s.authResult) {
				s.login(function(e) {
					s.getUserInfo(function(e) {
						$.hideIndicator();
						wxPostLogin(JSON.stringify(s.userInfo))
							// alert("获取用户信息成功：" + JSON.stringify(s.userInfo));
					}, function(e) {
						$.hideIndicator();
						$.toast("获取用户信息失败");
					});
				}, function(e) {
					$.hideIndicator();
					$.toast("登录认证失败");
				});
			} else {
				s.getUserInfo(function(e) {
					$.hideIndicator();
					wxPostLogin(JSON.stringify(s.userInfo))
						// alert("获取用户信息成功：" + JSON.stringify(s.userInfo));
				}, function(e) {
					$.hideIndicator();
					$.toast("获取用户信息失败");
				});
			}
		}
		//登录
		$("#btn_login").on("click", function(e) {
			var userName = $('#userName').val();
			var passWord = $('#passWord').val();
			if($.trim(userName) == '' || $.trim(passWord) == '') {
				$.toast("请输入用户名和密码");
				return false;
			}
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/member/login",
				type: 'post',
				dataType: 'json',
				data: {
					userName: 　userName　,
					passWord: 　passWord
				},
				success: function(res) {
                    $.hideIndicator();
					if(res.status != 202) {
						$.alert(res.msg);
						return false;
					} else {
						$.toast(res.msg);
						store.set('memberInfo', res);
						$.router.back();
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		});
		$("#btn_wx_login").on("click", function(e) {
			authLogin();
		});
		//扩展API准备完成后要执行的操作
		function plusReady() {
			plus.oauth.getServices(function(services) {
				auths = services;
			}, function(e) {
				alert("获取分享服务列表失败：" + e.message + " - " + e.code);
			});
			//console.log("Login_index.html页面扩展API准备完毕。");
		}
		//扩展API是否准备，如果没有则监听“plusready"事件
		if(window.plus) {
			plusReady();
		} else {
			document.addEventListener('plusready', plusReady, false);
		}
		appInit();
	});
	$(document).on("pageInit", '#login_findpwd', function(e, id, page) {
		var memberInfo = store.get('memberInfo');
		if(memberInfo != null && memberInfo.memberId != null && memberInfo.memberId != 'undefined') {
			$.router.load("User_index.html");
			return;
		}
		var code = null;
		var wait = 90;

		function time(o) {
			if(wait == 0) {
				o.innerHTML = "获取验证码";
				wait = 90;
			} else {
				o.innerHTML = "重新发送(" + wait + ")";
				wait--;
				setTimeout(function() {
						time(o);
					},
					1000);
			}
		}
		$("#bt_get_code").on("click", function(e) {
			var _mobile = $("#mobile").val();
			if(wait < 90) {
				$.toast("请勿频繁操作");
			} else {
				if(!/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/.test(_mobile)) {
					$.toast("手机号码格式错误");
				} else {
					code = RndNum(4);
					var res = sendSMS(_mobile, code);
					if(res == '0') {
						time(document.getElementById("bt_get_code"));
						$.toast("验证码发送成功");
					} else {
						$.toast("验证码发送失败");
					}
				}
			}
		});
		$("#bt_register").on("click", function(e) {
			var _mobile = $("#mobile").val();
			if(!/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/.test(_mobile)) {
				$.toast("手机号码格式错误");
				return false;
			}
			var _code = $("#code").val();
			if(_code != code) {
				$.toast("验证码错误");
				return false;
			}
			var _pwd = $("#pwd").val();
			if(!/\S{6,}/.test(_pwd)) {
				$.toast("密码不能低于6位");
				return false;
			}
            $.showIndicator();
			//注册请求
			$.ajax({
				url: ApiUrl + "/api/member/updatePwd",
				type: 'post',
				dataType: 'json',
				data: {
					userName: _mobile,
					passWord: _pwd,
					code: _code,
				},
				timeout: 30000,
				success: function(res) {
                    $.hideIndicator();
					if(res.status != '200') {
						$.toast(res.msg);
					} else {
						$.toast("修改成功");
						$.router.load('Login_index.html', true);
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		});
	})
	$(document).on("pageInit", "#register_index", function(e, id, page) {
		var memberInfo = store.get('memberInfo');
		if(memberInfo != null && memberInfo.memberId != null && memberInfo.memberId != 'undefined') {
			$.router.load("User_index.html");
		}

		var code = null;
		var wait = 90;

		function time(o) {
			if(wait == 0) {
				o.innerHTML = "获取验证码";
				wait = 90;
			} else {
				o.innerHTML = "重新发送(" + wait + ")";
				wait--;
				setTimeout(function() {
						time(o);
					},
					1000);
			}
		}
		$("#bt_get_code").on("click", function(e) {
			var _mobile = $("#mobile").val();
			if(wait < 90) {
				$.toast("请勿频繁操作");
			} else {
				if(!/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/.test(_mobile)) {
					$.toast("手机号码格式错误");
				} else {
					code = RndNum(4);
					var res = sendSMS(_mobile, code);
					if(res == '0') {
						time(document.getElementById("bt_get_code"));
						$.toast("验证码发送成功");
					} else {
						$.toast("验证码发送失败");
					}
				}
			}
		});
		$("#bt_register").on("click", function(e) {
			var _mobile = $("#mobile").val();
			if(!/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/.test(_mobile)) {
				$.toast("手机号码格式错误");
				return false;
			}
			var _code = $("#code").val();
			if(_code != code) {
				$.toast("验证码错误");
				return false;
			}
			var _pwd = $("#pwd").val();
			if(!/\S{6,}/.test(_pwd)) {
				$.toast("密码不能低于6位");
				return false;
			}
            $.showIndicator();
			//注册请求
			$.ajax({
				url: ApiUrl + "/api/member/register",
				type: 'post',
				dataType: 'json',
				data: {
					userName: _mobile,
					passWord: _pwd,
					code: _code,
				},
				timeout: 30000,
				success: function(res) {
                    $.hideIndicator();
					if(res.status != '200') {
						$.toast(res.msg);
					} else {
						$.toast("注册成功");
						$.router.load('Login_index.html', true);
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		});
		appInit();
	});
	$(document).on("pageInit", "#user_settings", function(e, id, page) {

		if(!checkLogin()) return false;
		var loginMember = store.get('memberInfo');
		if(loginMember != undefined) {
			if(loginMember.memberMobile != 'null' && loginMember.memberMobile != undefined && loginMember.memberMobile != "") {
				$("#mobile_span").html(loginMember.memberMobile);
			}
		}
		$("#btn_loginout").on("click", function(e) {
			confirmLoginOut();
		});

		$("#link_checkMobile").on("click", function(e) {
			if(loginMember.memberMobile == 'null' || loginMember.memberMobile == undefined || loginMember.memberMobile == "") {
				$.confirm('您尚未设置手机号码,是否立即设置?', '温馨提示', function() {
					$.router.load("User_userInfo.html");
				});
			} else {
				code = RndNum(4);
				var res = sendSMS(loginMember.memberMobile, code);
				if(res == '0') {
					$.prompt('验证码已通过短信的方式发送到您的手机.', '请输入验证码', function(value) {
						if(value == code) {
							$.toast("验证成功");
						} else {
							$.toast("验证失败");
						}
					});
				} else {
					$.toast("验证码发送失败");
				}

			}
		});
		$("#link-updatePwd").on("click", function(e) {
			$.prompt('为保障您的账号安全,修改前请填写原密码.', '验证原密码', function(value) {
				$.showIndicator();
				$.ajax({
					url: ApiUrl + "/api/mcenter/checkPwd",
					type: 'post',
					dataType: 'json',
					data: {
						memberId: loginMember.memberId,
						token: loginMember.token,
						password: 　value
					},
					timeout: 30000,
					success: function(res) {
						$.hideIndicator();
						if(res.status == 314) {
							$.alert(res.msg, function() {
								store.remove('memberInfo');
								$.router.load("Login_index.html");
							});
							return;
						}
						if(res.status == '200') {
							//$.toast(res.msg);
							$.prompt('为保障您的账号安全,请勿向他人透露此密码.', '请输入新密码', function(value) {
								if(value.length < 6) {
									$.toast("修改失败:密码不能少于6位");
									return false;
								}
								$.showIndicator();
								$.ajax({
									url: ApiUrl + "/api/mcenter/updatePwd",
									type: 'post',
									dataType: 'json',
									data: {
										memberId: loginMember.memberId,
										token: loginMember.token,
										password: 　value
									},
									timeout: 30000,
									success: function(res) {
										$.hideIndicator();
										if(res.status == '200') {
											$.toast(res.msg);
										} else {
											$.toast(res.msg);
										}
									},
									error: function(xhr, errorType, error) {
										$.hideIndicator();
										$.toast("网络错误");
									}
								});
							});
						} else {
							$.toast(res.msg);
						}
					},
					error: function(xhr, errorType, error) {
						$.hideIndicator();
						$.toast("网络错误");
					}
				});
			});
		});
		appInit();
	});
	$(document).on("pageInit", "#user_orderlist", function(e, id, page) {
		if(!checkLogin()) return false;
		var t = 1;
		var loading = false;
		var p = 1;
		var pageall = 2;

		var memberInfo = store.get('memberInfo');
		var memberId = memberInfo.memberId;
		var token = memberInfo.token;

		if(havingParameter()) {
			if(getParameterByName("t").length > 0) {
				t = getParameterByName("t");
				var obj = $("#user_orderlist .buttons-tab a");
				obj.each(function() {
					if($(this).attr("data-num") == t) {
						$(this).addClass("active").siblings().removeClass("active");
					}
				});
			}
		}

		if($("#orderListDom").children().length > 0) {

		} else {
			addItems(t, p);
		}

		function addItems(t,p) {
			p = ($("#orderListDom ul").length / 5) + 1;
			//console.log("t的参数是: " + t);
			//console.log("p的参数是: " + p);
			$('.infinite-scroll-preloader').show();
			$.ajax({
				url: ApiUrl + "/api/order/index",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: memberId,
					token: token,
					t: t,
					p: p
				},
				timeout: 30000,
				success: function(res) {
					$('.infinite-scroll-preloader').hide();
					if(res.status == 314) {
						$.alert(res.msg, function() {
							store.remove('memberInfo');
							$.router.load("Login_index.html");
						});
						return;
					}
					console.log(JSON.stringify(res.dataList));
					if(res.status == '200') {
						if(res.dataList == null) {
							$("#orderListDom ul").css("display", "none");
							$("#orderListyEmptyContent").css("display", "block");
							$("#mark").css("display", "none");
						} else {
							//$.router.load('User_orderDetail.html?orderId='+res.orderId);
							//$("#orderListDom ul").css("display", "block");
							$("#orderListyEmptyContent").css("display", "none");
							var _html = template('orderListSpt', res);
							$('#orderListDom').append(_html);
							$("#mark").css("display", "none");
							pageall = res.page.total;
							if(p >= pageall) {
								$('.infinite-scroll-preloader').hide();
							}

							confirmReceiptOrder(memberInfo);
							confirmCancelOrder(memberInfo);
							remindShip(memberInfo);
						}
					} else {
						$.toast(res.msg);
					};
					loading = false;
				},
				error: function(xhr, errorType, error) {
					$.toast("网络错误");
				}
			});
		}

		$("#user_orderlist .buttons-tab a").on("click", function() {
			loading = true;
			$('#orderListDom').html("");
			p = 1;
			t = $(this).attr("data-num");
			$(this).addClass("active").siblings().removeClass("active");
			$("#mark").css("display", "table");
			$('.infinite-scroll-preloader').show();
			addItems(t,p);
		});

		$(page).on('infinite', function() {
			// 如果正在加载，则退出
			if(loading)
				return;
			loading = true;
			if(p >= pageall) {
				$.toast("没有更多");
				$('.infinite-scroll-preloader').hide();
				return;
			}
			p = ++p;
			addItems(1, p);
			$.refreshScroller();
		});

		appInit();
	});
	$(document).on("pageInit", "#cart_index", function(e, id, page) {
		createHtmlCart();
		$("#CheckAll").on("click", function(e) {
			var _s = $("div[name^=checkgroup]");
			if($("#checkAllBtn").hasClass('pay_bar selected')) {
				$("#checkAllBtn").removeClass("selected");
				for(var i = 0; i < _s.length; i++) {
					$(_s[i]).removeClass("item selected").addClass("item");
				}
				$(".icon-remove").css('display', 'none');
				$("#shopCartConfirm").css('display', 'none');
			} else {
				$("#checkAllBtn").addClass("selected");
				for(var i = 0; i < _s.length; i++) {
					$(_s[i]).removeClass("item").addClass("item selected")
				}
				$(".icon-remove").css('display', '');
				$("#shopCartConfirm").css('display', '');
			}
			SelectCount();
		});

		$('#removeCart').on('click', function(e) {
			$.confirm('你确定删除选中的商品吗?', '温馨提醒',
				function() {
					deleteAll();
				}
			);
		})

		appInit();
	});
	$(document).on("pageInit", "#order_index", function(e, id, page) {
		if(!checkLogin()) return false;
		var cartList = store.get("carList");
		var totalPrice = store.get("TotalPrice");
		if(cartList == '' || cartList == null || cartList == 'undefined') {
			$.toast("您还没有添加任何商品");
			$.router.load('Search_index.html');
		}
		createHtmlAddress();
		createHtmlOrder();
		$('#address').on('click', function(e) {
			$.router.load('User_addressList.html?type=c');
		});

		var oDate = new Date();
		var oDateMonth = oDate.getMonth() + 1;
		var oDateDay = oDate.getDate() < 10 ? '0' + oDate.getDate() : oDate.getDate();
		var oDateMinutes = oDate.getMinutes();
		oDateMonth = oDateMonth < 10 ? '0' + oDateMonth : oDateMonth;
		oDateMinutes = oDateMinutes < 10 ? '0' + oDateMinutes : oDateMinutes;
		oDate.setHours(oDate.getHours() + 2);
		$("#datetime-picker").datetimePicker({
			value: [oDate.getFullYear(), oDateMonth, oDateDay, oDate.getHours(), oDateMinutes]
		});

		$('#order_sub').on('click', function(e) {
			checkLogin();
			var memberInfo = store.get('memberInfo');
			var arrivedTime = $('#datetime-picker').val();
			var orderRemarks = $('#orderRemarks').val();
			var address = store.get('address');
			if(address == undefined) {
				$.alert("您还未选择收货地址哦!");
				return;
			}
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/order/add",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: memberInfo.memberId,
					token: memberInfo.token,
					cartList: cartList,
					totalPrice: totalPrice,
					arrivedTime: arrivedTime,
					orderRemarks: orderRemarks,
					accepterName: address.accepterName,
					accepterMobile: address.accepterMobile,
					provinceName: address.accepterProvinceName,
					cityName: address.accepterCityName,
					districtName: address.accepterDistrictName,
					address: address.accepterAddress
				},
				timeout: 30000,
				success: function(res) {
                    $.hideIndicator();
					if(res.status == 314) {
						$.alert(res.msg, function() {
							store.remove('memberInfo');
							$.router.load("Login_index.html");
						});
						return;
					}
					//console.log(res);
					if(res.status == '200') {
						store.remove("carList");
						store.remove("TotalPro");
						store.remove("TotalPrice");
						updateUserInfo();
						$.toast("订单提交成功");
						$.router.load('User_orderDetail.html?path=cart&orderId=' + res.orderId);
					} else {
						$.toast(res.msg);
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		})

		appInit();
	});
	$(document).on("pageInit", "#user_orderdetail", function(e, id, page) {
		var orderInfo = null;
		var channels = null;
		if(!checkLogin()) return false;
		var _orderId = parseInt(getParameterByName("orderId"));
		if(!/^[1-9]+/.test(_orderId)) {
			$.router.load('User_orderList.html');
			return false;
		}
		if(getParameterByName("path").length > 0) {
				var backUrl = "User_orderList.html?t=1";
				$("#backHref").removeClass("back");
				$("#backHref").attr("href", backUrl);
			}
		var memberInfo = store.get('memberInfo');
		$.ajax({
			url: ApiUrl + "/api/order/detail",
			type: 'post',
			dataType: 'json',
			data: {
				memberId: memberInfo.memberId,
				token: memberInfo.token,
				orderId: _orderId
			},
			timeout: 30000,
			success: function(res) {
				if(res.status == 314) {
					$.alert(res.msg, function() {
						store.remove('memberInfo');
						$.router.load("Login_index.html");
					});
					return;
				}
				orderInfo = res.order;
				if(res.status == '200') {
					var orderDetailHtml = template('orderDetailTemplate', res);
					var orderDetailNavHtml = template('orderDetailNavTemplate', res);
					$('#orderDetail').html(orderDetailHtml);
					$('#orderDetailNav').html(orderDetailNavHtml);
					$('#mark').hide();
					confirmReceiptOrder(memberInfo);
					confirmCancelOrder(memberInfo);
					remindShip(memberInfo);

					$('.button-success').on('click', function() {
						var payButtons = [{
							text: '请选择支付方式',
							label: true
						}, {
							text: '微信支付',
							onClick: function() {
								paySubmit(orderInfo, 'WX_APP', channels);
							}
						}, {
                            text: '支付宝支付',
                            onClick: function() {
                                paySubmit(orderInfo, 'ALI_APP', channels);
                            }
                        }, {
                            text: '余额支付',
                            onClick: function() {
                                $.showIndicator();
                                $.ajax({
                                    url: ApiUrl + "/api/order/overagePay",
                                    data: {
                                        memberId: memberInfo.memberId,
                                        token: memberInfo.token,
                                        orderId: _orderId
                                    },
                                    type: 'post',
                                    dataType: 'json',
                                    timeout: 30000,
                                    success: function(result) {
                                        $.hideIndicator();
                                        if(result.status == '200') {
                                            updateUserInfo();
                                            $.toast("恭喜您,支付成功");
                                            document.location.reload();
                                        }else{
                                            $.hideIndicator();
                                            $.toast(result.msg);
                                            return;
                                        }
                                    },error: function(xhr, errorType, error) {
                                        $.hideIndicator();
                                        $.toast("网络错误");
                                        return;
                                    }
                                });
                            }
                        }];
						var canlButtons = [{
							text: '取消',
							bg: 'danger'
						}];
						var groups = [payButtons, canlButtons];
						$.actions(groups);
					});
				} else {
					$.toast(res.msg);
					$.router.load('User_orderList.html');
				}
			},
			error: function(xhr, errorType, error) {
				$.toast("网络错误");
			}
		});

		function plusReady() {
			plus.payment.getChannels(function(s) {
				channels = s;
			});
		}
		//扩展API是否准备，如果没有则监听“plusready"事件
		if(window.plus) {
			plusReady();
		} else {
			document.addEventListener('plusready', plusReady, false);
		}

		appInit();
	});
	$(document).on("pageInit", "#user_orderevaluate", function(e, id, page) {
		var orderInfo = null;
		if(!checkLogin()) return false;
		var _orderId = parseInt(getParameterByName("orderId"));
		if(!/^[1-9]+/.test(_orderId)) {
			$.router.load('User_orderList.html');
			return false;
		}
		var memberInfo = store.get('memberInfo');
		$.ajax({
			url: ApiUrl + "/api/order/detail",
			type: 'post',
			dataType: 'json',
			data: {
				memberId: memberInfo.memberId,
				token: memberInfo.token,
				orderId: _orderId
			},
			timeout: 30000,
			success: function(res) {
				orderInfo = res.order;
				if(res.status == '200') {
					var orderDetailHtml = template('orderDetailTemplatee', res);
					$('#orderDetail').html(orderDetailHtml);
					$('#mark').hide();
				}else if(res.status == '314') {
                    $.alert(res.msg, function() {
                        store.remove('memberInfo');
                        $.router.load("Login_index.html");
                    });
                    return;
                } else {
					$.toast(res.msg);
					$.router.back();
				}
			},
			error: function(xhr, errorType, error) {
				$.toast("网络错误");
			}
		});

        $('#comment-submit').on('click',function (e) {
        	var _level=$("input[name='commentLevel']:checked").val();
            var content = $.trim($("#content").val());
            if(content == "") {
                $.toast("请输入评价内容");
                return false;
            }
            $.showIndicator();
            $.ajax({
                url: ApiUrl + "/api/mcenter/addComment",
                type: 'post',
                dataType: 'json',
                data: {
                    memberId: memberInfo.memberId,
                    token: memberInfo.token,
                    orderId:_orderId,
                    level:_level,
                    commentContent: content
                },
                timeout: 30000,
                success: function(res) {
                    $.hideIndicator();
                    if(res.status == '200') {
                        $("#content").val('');
                        $.toast('评论成功,非常感谢您对我们的支持');
                        $.router.load('User_orderList.html?t=1');
                        return;
                    } else {
                        $.toast(res.msg);
                    }
                },
                error: function(xhr, errorType, error) {
                    $.hideIndicator();
                    $.toast("网络错误");
                }
            });
        })
		appInit();
	});
	$(document).on("pageInit", "#user_userinfo", function(e, id, page) {
		if(!checkLogin()) return false;
		var loginMember = store.get('memberInfo');
		if(loginMember != undefined) {
			//console.log(JSON.stringify(loginMember))
			$("#realName").val(loginMember.realName);
			$("#memberMobile").val(loginMember.memberMobile);
			$("#memberEmail").val(loginMember.memberEmail);
			$("#memberSex option").eq(loginMember.memberSex - 1).attr("selected", "selected");
			document.getElementById("city-picker").value = loginMember.provinceName + " " + loginMember.cityName + " " + loginMember.districtName;
		}
		$("#city-picker").cityPicker({
			toolbarTemplate: '<header class="bar bar-nav">\<button class="button button-link pull-right close-picker">确定</button>\<h1 class="title">选择省市县</h1>\</header>'
		});
		$("#btn_userinfo_update").on("click", function() {
			var memberId = loginMember.memberId;
			var token = loginMember.token;
			var realName = $("#realName").val();
			var memberMobile = $("#memberMobile").val();
			var memberEmail = $("#memberEmail").val();
			var memberSex = $("#memberSex").val();
			var newCity = $("#city-picker").val().split(" ");
			var provinceName = newCity[0];
			var cityName = newCity[1];
			var districtName = newCity[2];
			var mobileVierfy = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/;
			var emailVierfy = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(realName == "" || realName.length < 2) {
				$.toast("姓名输入有误");
				return false;
			}
			if(!mobileVierfy.test(memberMobile)) {
				$.toast("手机号码输入有误");
				return false;
			}
			if(!emailVierfy.test(memberEmail)) {
				$.toast("邮箱输入有误");
				return false;
			}
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/mcenter/updateUserInfo",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: memberId,
					token: token,
					realName: 　realName　,
					memberMobile: 　memberMobile,
					memberEmail: 　memberEmail　,
					memberSex: 　memberSex　,
					provinceName: 　provinceName　,
					cityName: 　cityName　,
					districtName: districtName　
				},
				success: function(res) {
                    $.hideIndicator();
					if(res.status == 314) {
						$.alert(res.msg, function() {
							store.remove('memberInfo');
							$.router.load("Login_index.html");
						});
						return;
					}
					if(res.status == 200) {
						store.set('memberInfo', res);
						$.alert(res.msg, function() {
							document.location.reload();
						});
					} else {
						$.toast(res.msg + ", 错误代码: WDH205");
					}
				},
				error: function() {
                    $.hideIndicator();
					$.toast("错误代码: WDH206");
				}
			});
		});
		appInit();
	});
	$(document).on("pageInit", "#user_addressedit", function(e, id, page) {
        if(!checkLogin()) return false;
        var loginMember = store.get('memberInfo');
        var _accepterId = parseInt(getParameterByName("accepterId"));
        if(!/^[1-9]+/.test(_accepterId)) {
            $.router.back();
            return false;
        }

        $.ajax({
            url: ApiUrl + "/api/mcenter/detailAddress",
            type: 'post',
            dataType: 'json',
            data: {
                memberId: loginMember.memberId,
                token: loginMember.token,
                accepterId: _accepterId
            },
            success: function(res) {
                if(res.status == 200) {
                    $('#addressEdit').html(template('addressEditTemplate', res));

                    $("#edit-city-picker").cityPicker({
                        toolbarTemplate: '<header class="bar bar-nav">\<button class="button button-link pull-right close-picker">确定</button>\<h1 class="title">选择省市县</h1>\</header>'
                    });
                } else {
                    $.toast(res.msg);
                }
            },
            error: function(xhr, errorType, error) {
                $.toast("网络错误");
            }
        });

        $("#btn_address_edit").on("click", function() {
            var cityStr = $("#edit-city-picker").val();
            if($.trim($("#name").val()).length < 2) {
                $.toast("姓名长度太短");
                return false;
            } else if(!/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/.test($("#mobile").val())) {
                $.toast("手机号码格式不正确");
                return false;
            } else if(cityStr.length < 1) {
                $.toast("请选择城市");
                return false;
            } else if($.trim($("#street").val()).length < 1) {
                $.toast("请填写街道");
                return false;
            }

            $.showIndicator();
            $.ajax({
                url: ApiUrl + "/api/mcenter/editAddress",
                type: 'post',
                dataType: 'json',
                data: {
                    memberId: loginMember.memberId,
                    token: loginMember.token,
                    accepterId: _accepterId,
                    accepterName: $.trim($("#name").val()),
                    accepterMobile: $.trim($("#mobile").val()),
                    accepterProvinceName: cityStr.split(' ')[0],
                    accepterCityName: cityStr.split(' ')[1],
                    accepterDistrictName: cityStr.split(' ')[2],
                    street: $.trim($("#street").val())
                },
                success: function(res) {
                    $.hideIndicator();
                    if(res.status == 200) {
                        store.remove('address');
                        $.alert('修改成功', function() {
                            $.router.back();
                        });
                    } else {
                        $.toast(res.msg);
                    }
                },error: function(xhr, errorType, error) {
                    $.hideIndicator();
                    $.toast("网络错误");
                }
            });
        });
	});
	$(document).on("pageInit", "#user_addressadd", function(e, id, page) {
		if(!checkLogin()) return false;
		var loginMember = store.get('memberInfo');

		$("#city-picker").cityPicker({
			toolbarTemplate: '<header class="bar bar-nav">\<button class="button button-link pull-right close-picker">确定</button>\<h1 class="title">选择省市县</h1>\</header>'
		});
		$("#btn_address_add").on("click", function() {
			var cityStr = $("#city-picker").val();
			if($.trim($("#name").val()).length < 2) {
				$.toast("姓名长度太短");
				return false;
			} else if(!/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/.test($("#mobile").val())) {
				$.toast("手机号码格式不正确");
				return false;
			} else if(cityStr.length < 1) {
				$.toast("请选择城市");
				return false;
			} else if($.trim($("#street").val()).length < 1) {
				$.toast("请填写街道");
				return false;
			}

            $.showIndicator();
            $.ajax({
                url: ApiUrl + "/api/mcenter/insertAddress",
                type: 'post',
                dataType: 'json',
                data: {
                    memberId: loginMember.memberId,
                    token: loginMember.token,
                    accepterName: $.trim($("#name").val()),
                    accepterMobile: $.trim($("#mobile").val()),
                    accepterProvinceName: cityStr.split(' ')[0],
                    accepterCityName: cityStr.split(' ')[1],
                    accepterDistrictName: cityStr.split(' ')[2],
                    street: $.trim($("#street").val())
                },
                success: function(res) {
                    $.hideIndicator();
                    if(res.status == 200) {
                        $.alert('添加成功', function() {
                            $.router.back();
                        });
                    } else {
                        $.toast(res.msg);
                    }
                },
                error: function(xhr, errorType, error) {
                    $.hideIndicator();
                    $.toast("网络错误");
                }
            });
		});

		appInit();
	});
	$(document).on("pageInit", "#user_addresslist", function(e, id, page) {
		if(!checkLogin()) return false;
		var _listType = getParameterByName("type");
		var loginMember = store.get('memberInfo');
		var html = "";

		function listInit() {
			$.ajax({
				url: ApiUrl + "/api/mcenter/address",
				type: 'post',
				dataType: 'json',
				async: false, //阻塞请求
				data: {
					memberId: loginMember.memberId,
					token: loginMember.token
				},
				success: function(res) {
					if(res.status == 314) {
						$.alert(res.msg, function() {
							store.remove('memberInfo');
							$.router.load("Login_index.html");
						});
						return;
					}
					if(res.status == 200) {
						if(_listType == 'c') {
							html = template('addresslist_c', res);
						} else {
							html = template('addresslist_m', res);
						}
						$('#addresslist').html(html);
					} else if(res.status == 314) {
						$.alert(res.msg, function() {
							store.remove('memberInfo');
							$.router.load("Login_index.html");
						});
						return;
					} else {
						$.toast(res.msg);
					}
				},
				error: function(xhr, errorType, error) {
					$.toast("网络错误");
				}
			});
			$(".del").on("click", function() {
				var acid = $(this).data("id");
				$.confirm('确认要删除该收货地址吗?', '温馨提示', function() {
                    $.showIndicator();
					$.ajax({
						url: ApiUrl + "/api/mcenter/delAddress",
						type: 'post',
						dataType: 'json',
						data: {
							accepterId: acid,
							memberId: loginMember.memberId,
							token: loginMember.token
						},
						success: function(res) {
                            $.hideIndicator();
							if(res.status == 314) {
								$.alert(res.msg, function() {
									store.remove('memberInfo');
									$.router.load("Login_index.html");
								});
								return;
							}
							if(res.status == 200) {
								$.toast("删除成功");
                                store.remove('address');
                                document.location.reload();
                                return;
							} else {
								$.toast(res.msg);
							}
						},
						error: function(xhr, errorType, error) {
                            $.hideIndicator();
							$.toast("网络错误");
						}
					});
				},
                function () {
                    $.hideIndicator();
                });
			});
		}
		listInit();

		//选中收货地址
		$("label[id^=orderAddressScl]").on('click', function(e) {
			var $this = $(this);
			var accepterName = $this.data('name');
			var accepterMobile = $this.data('mobile');
			var accepterProvinceName = $this.data('province');
			var accepterCityName = $this.data('city');
			var accepterDistrictName = $this.data('district');
			var street = $this.data('street');
			var accepterAddress = $this.data('address');
			var address = {
				"accepterName": "" + accepterName + "",
				"accepterMobile": "" + accepterMobile + "",
				"accepterProvinceName": "" + accepterProvinceName + "",
				"accepterCityName": "" + accepterCityName + "",
				"accepterDistrictName": "" + accepterDistrictName + "",
				"street": "" + street + "",
				"accepterAddress": "" + accepterAddress + ""
			};
			store.set('address', address);
			$.router.load('Order_index.html');
			return false;
		})

		appInit();
	});
	$(document).on("pageInit", "#detail_index", function(e, id, page) {
		var msg = {};
		var _goodsId = getParameterByName("goodsId");
		var loginMember = store.get('memberInfo');
		var _memberId = 0;
		var _goodsInfo = null;

		if(loginMember != undefined) {
			_memberId = loginMember.memberId;
		}
		var _cartNum = store.get('TotalPro');
		if(_cartNum != null && _cartNum != "0" && _cartNum != "undefined") {
			$('#cartNum').attr('num', _cartNum);
		} else {
			$('#cartNum').removeAttr('num');
		}
		$.ajax({
			url: ApiUrl + "/api/product/detail",
			type: 'get',
			dataType: 'json',
			data: {
				goodsId: _goodsId,
				memberId: _memberId
			},
			timeout: 30000,
			success: function(res) {
				if(res.status == 321) {
					$.alert("该产品已下架", function() {
						$.router.back();
					});
					return;
				}
				_goodsInfo = res.detail;
				h5plus();
				var html = template('goodsDetailTemplate', res);
				$('#goodsDetail').html(html);

				if($("#mark").length > 0) {
					$("#mark").hide();
				}
				picLazyLoadInit();
				//判断是否已收藏
				if(res.isFavorite == 1) {
					$("#fav").removeClass("btn_fav").addClass("btn_fav_checked");
				}
				$("#header-goodsName").html(res.detail.goodsName);
				$("#favoriteId").val(res.detail.goodsId);
				var result = {
						"goodsId": res.detail.goodsId,
						"goodsLogo": res.apiUrl + res.detail.goodsLogo,
						"goodsName": res.detail.goodsName,
						"goodsSub": res.detail.goodsSub,
						"goodsPrice": res.detail.goodsPrice,
						"saleCount": res.detail.saleCount
					}
					//console.log(JSON.stringify(result));
				addHistory(result);
				$("#addCart").on("click", function(e) {
					var _goodsId = $("#goodsId").val();
					var _buyNum = $("#buyNum").val();
					var _goodsName = $("#goodsName").val();
					var _goodsLogo = ApiUrl + $("#goodsLogo").val();
					var _price = $("#goodsPrice").val();
					var _goodsCode = $("#goodsCode").val();
					var _goodsSub = $("#goodsSub").val();
					var _shopId = $("#shopId").val();

					insertCar(_goodsId, _buyNum, _goodsName, _goodsLogo, _price, _goodsCode, _goodsSub, _shopId);
					var _cartNum = store.get('TotalPro');
					if(_cartNum != null && _cartNum != "0") {
						$('#cartNum').attr('num', _cartNum);
					}
					$.modal({
						title: '恭喜',
						text: '成功加入购物车',
						buttons: [{
							text: '去结算',
							onClick: function() {
								$.router.load('Cart_index.html', true);
								return false;
							}
						}, {
							text: '再逛逛',
							onClick: function() {
								$.router.load('Search_index.html', true);
								return false;
							}
						}, ]
					});
				});
				$("#buyBtn").on("click", function(e) {
					var _goodsId = $("#goodsId").val();
					var _buyNum = $("#buyNum").val();
					var _goodsName = $("#goodsName").val();
					var _goodsLogo = ApiUrl + $("#goodsLogo").val();
					var _price = $("#goodsPrice").val();
					var _goodsCode = $("#goodsCode").val();
					var _goodsSub = $("#goodsSub").val();
					var _shopId = $("#shopId").val();

					insertCar(_goodsId, _buyNum, _goodsName, _goodsLogo, _price, _goodsCode, _goodsSub, _shopId);
					var _cartNum = store.get('TotalPro');
					if(_cartNum != null && _cartNum != "0") {
						$('#cartNum').attr('num', _cartNum);
					}
					$.router.load('Cart_index.html', true);
				});
			}
		});
		//新增收藏及取消收藏
		$("#fav").on("click", function() {
			if(checkLogin() != false) {
				var loginMember = store.get('memberInfo');
				var goodsId = $("#favoriteId").val();
                $.showIndicator();
				if($(this).hasClass("btn_fav_checked")) {
					$.ajax({
						url: ApiUrl + "/api/mcenter/delFavorite",
						type: 'post',
						dataType: 'json',
						data: {
							memberId: loginMember.memberId,
							token: loginMember.token,
							ids: goodsId
						},
						success: function(res) {
                            $.hideIndicator();
							if(res.status == 314) {
								$.alert(res.msg, function() {
									store.remove('memberInfo');
									$.router.load("Login_index.html");
								});
								return;
							}
							if(res.status == 200) {
								$.toast(res.msg);
								$("#fav").removeClass("btn_fav_checked").addClass("btn_fav");
							}
						},
                        error: function(xhr, errorType, error) {
                            $.hideIndicator();
                            $.toast("网络错误");
                        }
					});
				} else {
					$.ajax({
						url: ApiUrl + "/api/mcenter/addFavorite",
						type: 'post',
						dataType: 'json',
						data: {
							memberId: loginMember.memberId,
							token: loginMember.token,
							goodsId: goodsId
						},
						success: function(res) {
                            $.hideIndicator();
							if(res.status == 314) {
								$.alert(res.msg, function() {
									store.remove('memberInfo');
									$.router.load("Login_index.html");
								});
								return;
							}
							if(res.status == 200) {
								$.toast(res.msg);
								$("#fav").removeClass("btn_fav").addClass("btn_fav_checked");
							}
						},
                        error: function(xhr, errorType, error) {
                            $.hideIndicator();
                            $.toast("网络错误");
                        }
					});
				}
			}
		});
		$("#bt_wx_share").on("click", function(e) {
			shareWeixinMessage();
		});

		function shareWeixinMessage() {
			var buttons1 = [{
				text: '分享',
				label: true
			}, {
				text: '微信朋友圈',
				onClick: function() {
					msg.extra = {
						scene: "WXSceneTimeline"
					};
					sharewx.send(msg,
						function() {
							$.toast("分享成功");
						},
						function(e) {
							//console.log("分享失败：" + e.message);
						});
				}
			}, {
				text: '微信好友',
				onClick: function() {
					msg.title = "我订花";
					msg.extra = {
						scene: "WXSceneSession"
					};
					sharewx.send(msg,
						function() {
							$.toast("分享成功");
						},
						function(e) {
							//console.log("分享失败：" + e.message);
						});
				}
			}];
			var buttons2 = [{
				text: '取消',
				color: 'danger',
			}];
			var groups = [buttons1, buttons2];
			$.actions(groups);
		}

		function h5plus() {
			//扩展API准备完成后要执行的操作
			function plusReady() {
				plus.share.getServices(function(s) {
					shares = s;
					for(var i in s) {
						if('weixin' == s[i].id) {
							sharewx = s[i];
						}
					}
				}, function(e) {
					alert("获取分享服务列表失败：" + e.message);
				});
				//console.log(_goodsInfo.goodsLogo);
				//下载logo
				var dtask = plus.downloader.createDownload(ApiUrl + _goodsInfo.goodsLogoM, {}, function(d, status) {
					// 下载完成
					if(status == 200) {
						//console.log("文件下载成功: " + d.filename);
						msg.href = "http://wap.wodinghua.com/Detail_index.html?goodsId=" + _goodsInfo.goodsId;
						msg.content = _goodsInfo.goodsName + _goodsInfo.goodsSub;
						msg.thumbs = [d.filename];
						msg.pictures = [d.filename];
						$("#bt_wx_share").show();
					} else {
						//console.log("缩略图下载失败: " + status);
					}
				});
				dtask.start();

				//console.log("Detail_index.html页面扩展API准备完毕。");
			}
			//扩展API是否准备，如果没有则监听“plusready"事件
			if(window.plus) {
				plusReady();
			} else {
				document.addEventListener('plusready', plusReady, false);
			}
		}
		appInit();
	});

	$(document).on("pageInit", "#user_browsinghistory", function(e, id, page) {
		var array = store.get("userHistory");
		if(array == undefined) {
			$("#historyEmptyContent").css("display", "block");
			$(".del-history").css("display", "none");
			return;
		}

		$("#historyEmptyContent").css("display", "none");
		$(".del-history").css("display", "block");

		var arr = [];
		for(var i = 0; i < array.length; i++) {
			if(array[i] == null) {
				continue;
			} else {
				arr.unshift(array[i]);
			}
		}
		var res = {
			"userHistory": arr
		};
		var _html = template('user_history_con', res);
		$('#user_history').html(_html);

		$(".del-history").on("click", function() {
			$.confirm('您是否清空历史记录？', '温馨提醒',
				function() {
					store.remove("userHistory");
					$.router.load("User_index.html");
				},
				function() {
					$.toast("您已取消");
				}
			);
		});
	});

	$(document).on("pageInit", "#user_redpackets", function(e, id, page) {
		if(!checkLogin()) return false;

		var loginMember = store.get('memberInfo');

		$.ajax({
			url: ApiUrl + "/api/mcenter/userInfo",
			type: 'post',
			dataType: 'json',
			data: {
				memberId: loginMember.memberId,
				token: loginMember.token
			},
			async: false, //阻塞请求
			timeout: 10000,
			success: function(res) {
				$("#mark").hide();
				if(res.status == '200') {
					//console.log(res);
					store.remove('memberInfo');
					store.set('memberInfo', res);
					loginMember = store.get('memberInfo');
					//console.log(loginMember.hongbao);
					if(loginMember.hongbao > 0) {
						$("#redpacketsPrice").html(loginMember.hongbao);
						$.ajax({
							url: ApiUrl + "/api/mcenter/getHongBao",
							type: 'post',
							dataType: 'json',
							data: {
								memberId: loginMember.memberId,
								token: loginMember.token
							},
							async: false, //阻塞请求
							timeout: 10000,
							success: function(res) {
								if(res.status == '200') {
									//console.log(res);
									store.remove('memberInfo');
									store.set('memberInfo', res);
									$("#container1").css("display", "block");
								} else {
									$.alert(res.msg, function() {
										store.remove('memberInfo');
										$.router.load("Login_index.html");
									});
								}
							},
							error: function(xhr, errorType, error) {
								$.modal({
									title: '提示',
									text: '网络错误',
									buttons: [{
										text: '返回首页',
										onClick: function() {
											$.router.load('Index_index.html', true);
											return false;
										}
									}, {
										text: '重新加载',
										onClick: function() {
											document.location.reload();
											return false;
										}
									}, ]
								});
							}
						});
					} else {
						$("#container2").css("display", "block");
					}
				} else {
					$.alert(res.msg, function() {
						store.remove('memberInfo');
						$.router.load("Login_index.html");
					});
				}
			},
			error: function(xhr, errorType, error) {
				$.modal({
					title: '提示',
					text: '网络错误',
					buttons: [{
						text: '返回首页',
						onClick: function() {
							$.router.load('Index_index.html', true);
							return false;
						}
					}, {
						text: '重新加载',
						onClick: function() {
							document.location.reload();
							return false;
						}
					}, ]
				});
			}
		});

		var j = document.getElementById('user_redpackets').lastChild;
		if(j.nodeName != "SCRIPT") {
			$.getScript("js/mathlib-min.js", 'user_redpackets', function() {
				$.getScript("js/k3d-min.js", 'user_redpackets', function() {
					$.getScript("js/radiation.js", 'user_redpackets', function() {
						onloadHandler();
					});
				});
			});
		} else {
			onloadHandler();
		}
		$(".goToWallet").on("click", function() {
			$("#container1").css("display", "none");
			$("#container2").css("display", "none");
			$.router.load("User_wallet.html?path=redpacket");
		});
		appInit();
		if(store.get('immersed')) {
			$('body').css("margin-top", 0);
		}
	});

	$(document).on("pageInit", "#user_wallet", function(e, id, page) {
		if(!checkLogin()) return false;

		if(havingParameter()) {
			if(getParameterByName("path").length > 0) {
				var backUrl = "Index_index.html";
				$("#backHref").removeClass("back");
				$("#backIcon").removeClass("icon-left").addClass("icon-home");
				$("#backHref").attr("href", backUrl);
				$("#userIndexHref").css("display", "block");
			}
		}

		var memberInfo = store.get('memberInfo');
		$.ajax({
			url: ApiUrl + "/api/mcenter/wallet",
			type: 'post',
			dataType: 'json',
			data: {
				memberId: memberInfo.memberId,
				token: memberInfo.token,
			},
			timeout: 30000,
			success: function(res) {
				if(res.status == '200') {
					$('#my_turnover').html(res.detail.turnover);
					$('#my_integral').html(res.detail.integral);
				} else {
					$.alert(res.msg, function() {
						store.remove('memberInfo');
						$.router.load("Login_index.html");
					});
				}
			},
			error: function(xhr, errorType, error) {
				$.toast("网络错误");
			}
		});

		$("#wallet-btn").on("click", function() {
			var buttons1 = [{
				text: '充值方式',
				label: true
			}, {
				text: '在线充值',
				onClick: function() {
					$.router.load("User_recharge.html");
				}
			}, {
				text: '充值卡充值',
				onClick: function() {
					$.router.load("User_cardRecharge.html");
				}
			}];
			var buttons2 = [{
				text: '取消',
				color: 'danger',
			}];
			var groups = [buttons1, buttons2];
			$.actions(groups);
		});

		appInit();
	});

	$(document).on("pageInit", "#user_recharge", function(e, id, page) {
		if(!checkLogin()) return false;

		var memberInfo = store.get('memberInfo');
		$("#rechargeUserName").val(memberInfo.realName);
		$("#rechargeMobile").val(memberInfo.memberMobile);

		$("#recharge-submit").on("click", function() {
			var rechargeUserName = $.trim($("#rechargeUserName").val());
			var rechargeMobile = $.trim($("#rechargeMobile").val());
			var rechargeAmount = $.trim($("#rechargeAmount").val());
			var mobileVierfy = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57]|17[0-9])[0-9]{8}$/;
			if(rechargeUserName.length < 2 || rechargeUserName == "") {
				$.toast("用户姓名输入有误");
				return;
			}
			if(!mobileVierfy.test(rechargeMobile)) {
				$.toast("手机号码输入有误");
				return;
			}
			if(isNaN(rechargeAmount) || rechargeAmount <= 0 || !(/^\d+$/.test(rechargeAmount))) {
				$.toast("充值金额必须是正整数");
				return;
			}

			if(rechargeAmount < 1 || rechargeAmount == "") {
				$.toast("最少充值金额不能低于1元");
				return;
			}
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/mcenter/addChargeRecodPrice",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: memberInfo.memberId,
					token: memberInfo.token,
					realName: rechargeUserName,
					mobile: rechargeMobile,
					price: rechargeAmount,
				},
				timeout: 30000,
				success: function(res) {
                    $.hideIndicator();
					if(res.status == '200') {
						$.router.load("User_chargeRecodDetail.html?id=" + res.detail.id);
					} else {
						$.alert(res.msg, function() {
							$.router.back();
						});
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		});

		appInit();
	});

	$(document).on("pageInit", "#user_cardrecharge", function(e, id, page) {
		if(!checkLogin()) return false;

		$("#cardrecharge-submit").on("click", function() {
			var cardrechargeNumber = $.trim($("#cardrechargeNumber").val());
			var cardrechargePassword = $.trim($("#cardrechargePassword").val());
			if(cardrechargeNumber == "") {
				$.toast("请输入充值卡卡号");
				return false;
			}
			if(cardrechargePassword == "") {
				$.toast("请输入充值卡密码");
				return false;
			}
			var memberInfo = store.get('memberInfo');
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/mcenter/addChargeRecodCards",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: memberInfo.memberId,
					token: memberInfo.token,
					checkCode: cardrechargeNumber,
					checkPwd: cardrechargePassword
				},
				timeout: 30000,
				success: function(res) {
                    $.hideIndicator();
					if(res.status == '200') {
						updateUserInfo();
						$.toast('充值成功,成功充值 ' + res.detail.price);
						$.router.load("User_chargeRecodDetail.html?id=" + res.detail.id);
						return;
					} else {
						$.toast(res.msg);
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		});

		appInit();
	});

	$(document).on("pageInit", "#user_chargerecod", function(e, id, page) {
		if(!checkLogin()) return false;
		var memberInfo = store.get('memberInfo');

		$.ajax({
			url: ApiUrl + "/api/mcenter/chargeRecod",
			type: 'post',
			dataType: 'json',
			data: {
				memberId: memberInfo.memberId,
				token: memberInfo.token
			},
			timeout: 30000,
			success: function(res) {
				if(res.status == 200) {
					if(res.dataList == null) {
						$("#chargeRecodDom").html("<li style='padding: .75rem;text-align:center'>您还没有充值记录</li>");
					} else {
						$("#chargeRecodDom").html(template('chargeRecodSpt', res));
					}
				}
			}
		});
	});

	$(document).on("pageInit", "#user_chargerecoddetail", function(e, id, page) {
		if(!checkLogin()) return false;
		var channels = null;
		var memberInfo = store.get('memberInfo');
		var orderInfo = {
			title: '我订花会员充值',
			totalPrice: '',
			orderCode: ''
		};
		var _id = parseInt(getParameterByName("id"));
		if(!/^[1-9]+/.test(_id)) {
			$.router.load('User_wallet.html');
			return;
		}

		$.ajax({
			url: ApiUrl + "/api/mcenter/chargeRecodDetail",
			type: 'post',
			dataType: 'json',
			data: {
				memberId: memberInfo.memberId,
				token: memberInfo.token,
				id: _id,
			},
			timeout: 30000,
			success: function(res) {
				if(res.status == '200') {
					orderInfo.orderCode = res.detail.tradeCode;
					orderInfo.totalPrice = res.detail.totalPrice;
					$('#chargeRecodDetail').html(template('chargeRecodDetailTemplate', res));

                    $('#charge-Recod-Detail a').on('click', function(e) {
                        var payButtons = [{
                            text: '请选择支付方式',
                            label: true
                        }, {
                            text: '微信支付',
                            onClick: function() {
                                paySubmit(orderInfo, 'WX_APP', channels);
                            }
                        }, {
                            text: '支付宝支付',
                            onClick: function() {
                                paySubmit(orderInfo, 'ALI_APP', channels);
                            }
                        }];
                        var canlButtons = [{
                            text: '取消',
                            bg: 'danger'
                        }];
                        var groups = [payButtons, canlButtons];
                        $.actions(groups);
                    });
				} else {
					$.alert(res.msg, function() {
						store.remove('memberInfo');
						$.router.load("Login_index.html");
					});
				}
			},
			error: function(xhr, errorType, error) {
				$.toast("网络错误");
			}
		});

		function plusReady() {
			plus.payment.getChannels(function(s) {
				channels = s;
			});
		}
		//扩展API是否准备，如果没有则监听“plusready"事件
		if(window.plus) {
			plusReady();
		} else {
			document.addEventListener('plusready', plusReady, false);
		}

		appInit();
	});

	$(document).on("pageInit", "#info_tickling", function(e, id, page) {
		if(!checkLogin()) return false;

		$("#content-submit").on("click", function() {
			var content = $.trim($("#content").val());
			if(content == "") {
				$.toast("请输入反馈内容");
				return false;
			}
			var memberInfo = store.get('memberInfo');
            $.showIndicator();
			$.ajax({
				url: ApiUrl + "/api/mcenter/addProposal",
				type: 'post',
				dataType: 'json',
				data: {
					memberId: memberInfo.memberId,
					token: memberInfo.token,
					content: content
				},
				timeout: 30000,
				success: function(res) {
                    $.hideIndicator();
					if(res.status == '200') {
						$("#content").val('');
						$.toast('反馈成功,非常感谢您对我们的支持');
						$.router.back();
						return;
					} else {
						$.toast(res.msg);
					}
				},
				error: function(xhr, errorType, error) {
                    $.hideIndicator();
					$.toast("网络错误");
				}
			});
		});

		appInit();
	});

	$.init();
});