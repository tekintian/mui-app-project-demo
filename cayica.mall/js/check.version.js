!function(e, s) {
    s.checkServerVersion = function(s, i, t) {
        sui.post("Passport/Upgrade", {
            appId:plus.runtime.appid,
            version:s,
            os:plus.os.name
        }, function(s) {
            if (s) {
                if (s.IsPass) {
                    var n = {
                        DownloadUrl:s.ReturnObject.DownloadUrl,
                        HasVersion:s.ReturnObject.HasVersion,
                        UpdateType:s.ReturnObject.UpdateType,
                        PackageSize:s.ReturnObject.PackageSize,
                        Version:s.ReturnObject.Version,
                        NewFunction:s.ReturnObject.NewFunction
                    };
                    if (n.HasVersion) {
                        var r = document.createElement("div"), o = decodeURIComponent("%E6%9B%B4%E6%96%B0"), d = "masked";
                        e.os.android && parseFloat(e.os.version) < 4.4 && (d = "");
                        var a = [ '<div class="mui-backdrop" id="backdrops"></div><div class="mui-version-tips" id="version_box" >', '<div class="title ' + d + '">' + decodeURIComponent("%E6%A3%80%E6%B5%8B%E5%88%B0%E6%96%B0%E7%89%88%E6%9C%AC") + "</div>", '<div class="version">' + decodeURIComponent("%E7%89%88%E6%9C%AC%E5%8F%B7") + "：<span>" + n.Version + "</span></div>", '<div class="size">文件大小：<span id="fileSize">' + n.PackageSize + '</span></div><button type="button" class="mui-btn mui-btn-warning"  id="updateVersion" data-type="' + n.UpdateType + '" data-url="' + n.DownloadUrl + '">立即{{keys}}</button>', '<div class="tips">温馨提示：请您先{{keys}}再进行操作，若不及时{{keys}}可能导致部分功能无法正常使用。</div><div class="newfunction ">' + n.NewFunction + "</div></div>", '<div class="mui-version-box mui-hidden" id="Progress_box"><div class="tip">' + decodeURIComponent("%E5%8D%87%E7%BA%A7") + "中,请稍后...</div>", '<div class="bar_l"><div class="bar_lw" id="updateProgress"></div><div class="bar_lr"></div></div><div class="bar_lr" id="numProcess">0%</div></div>' ];
                        if ("index" == t) {
                            var u = plus.webview.getLaunchWebview();
                            u && u.evalJS("openbackdrop();");
                        }
                        r.innerHTML = a.join("").replaceAll("{{keys}}", o), document.body.appendChild(r), 
                        i(!0);
                    } else i(!1);
                } else i(!1);
            } else i(!1);
        });
    }, s.updateApp = function(e) {
        var s = {
            updateProgress:document.querySelector("#updateProgress"),
            numProcess:document.querySelector("#numProcess"),
            Progress_box:document.querySelector("#Progress_box"),
            backdrops:document.querySelector("#backdrops"),
            keys:decodeURIComponent("%E6%9B%B4%E6%96%B0")
        };
        s.updateProgress.style.width = "0%", s.numProcess.innerText = "0%", s.Progress_box.classList.remove("mui-hidden");
        var i = plus.downloader.createDownload(e, {
            method:"GET"
        }, function(e, i) {
            if (200 == i) plus.runtime.install(e.filename, {
                force:!0
            }, function() {
                s.updateProgress.style.width = "100%", s.numProcess.innerText = "100%", s.Progress_box.classList.add("mui-hidden"), 
                s.backdrops.classList.add("mui-hidden");
                var e = plus.webview.getLaunchWebview();
                e && e.evalJS("closebackdrop();"), plus.nativeUI.alert(decodeURIComponent("%E5%8D%87%E7%BA%A7") + "成功, 立即重启应用程序!", function() {
                    plus.runtime.restart();
                });
            }, function(e) {
                s.Progress_box.classList.add("mui-hidden"), s.backdrops.classList.add("mui-hidden");
                var i = plus.webview.getLaunchWebview();
                i && i.evalJS("closebackdrop();"), plus.nativeUI.alert(s.keys + "失败，请退出应用程序稍后重新尝试！", function() {
                    plus.runtime.restart();
                });
            }); else {
                s.Progress_box.classList.add("mui-hidden"), s.backdrops.classList.add("mui-hidden");
                var t = plus.webview.getLaunchWebview();
                t && t.evalJS("closebackdrop();"), plus.nativeUI.alert(s.keys + "失败，请退出应用程序稍后重新尝试！", function() {
                    plus.runtime.restart();
                });
            }
        });
        i.addEventListener("statechanged", function(e, i) {
            if (null != i && 200 == i && null != e.state) {
                var t = parseInt(100 * e.downloadedSize / e.totalSize);
                t ? (t = t > 100 ? 100 :t, s.updateProgress.style.width = t + "%", s.numProcess.innerText = t + "%") :(s.updateProgress.style.width = "0%", 
                s.numProcess.innerText = "0%");
            }
        }), i.start();
    }, s.checkVersion = function(e, i) {
        plus.runtime.getProperty(plus.runtime.appid, function(t) {
            var n = t.version, r = document.querySelector("#NewVersion");
            r ? r.innerText = "擦一擦 V" + n :statistic.request(n), s.checkServerVersion(n, e, i);
        });
    }, e("body").on("tap", "button[id=updateVersion]", function() {
        var e = this.getAttribute("data-type"), i = this.getAttribute("data-url");
        if (sui.IsNullOrEmpty(e) || sui.IsNullOrEmpty(i)) {
            var t = decodeURIComponent("%E6%9B%B4%E6%96%B0");
            plus.nativeUI.alert(t + "出错，请退出应用程序稍后重新尝试！", function() {
                plus.runtime.restart();
            });
        } else 1 == e ? plus.runtime.openURL(i) :(document.querySelector("#version_box").classList.add("mui-hidden"), 
        s.updateApp(i));
    });
}(mui, window.wg = {});