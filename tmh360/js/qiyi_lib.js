/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Bruce & Jaden & Tom
 * @version     qiyi_libs.js 15/8/5 09:09
 * @link        http://www.fs71.com.cn
 */

// 倒计时
var addCountDown = function() {
    var list = [];
    var interval;
    var template;
    
    return function(id, time, template) {
        type = template;
        if(!interval) {
            interval = setInterval(go, 1000);
        }
        list.push({
            ele: document.getElementById(id),
            time: time
        });
    }
    
    function go() {
        for(var i = 0; i < list.length; i++) {
            list[i].ele.innerHTML = getTimerString(list[i].time ? list[i].time -= 1 : 0);
            if(!list[i].time) {
                list.splice(i--, 1);
                list.length == 0 ? clearInterval(interval) : true;
            }
        }
    }
    
    function getTimerString(time) {
        var note = !!time,
        day = Math.floor(time / 86400),
        hour = Math.floor((time %= 86400) / 3600),
        minute = Math.floor((time %= 3600) / 60),
        second = time % 60;
        
        if(note) {
            if(hour < 10) {
                hour = '0' + hour;
            }
            if(minute < 10) {
                minute = '0' + minute;
            }
            if(second < 10) {
                second = '0' + second;
            }
            
            switch(type) {
                case 'promotion':
                    return '<span class="time">' + day + '天</span>\n<span class="time">' + hour + '</span><span class="division">:</span><span class="time">' + minute + '</span><span class="division">:</span><span class="time">' + second + '</span>';
                    break;
                case 'soldin':
                    return '<span class="qiyifont icon-alarmclock"></span> 距开售' + day + '天' + hour + '时' + minute + '分' + second + '秒';
                    break;
            }
        } else {
            return '<span class="time">0天</span>\n<span class="time">00</span><span class="division">:</span><span class="time">00</span><span class="division">:</span><span class="time">00</span>';
        }
    }
}();

/**
 * 60 秒倒计时
 */
var wait = 60;
function countDown(obj) {
    if (wait == 0) {
        obj.removeAttribute('disabled');
        obj.value = '重新获取';
        document.getElementById('smsBtn').style.color = '#3e3e3e';
        wait = 60;
    } else {
        obj.setAttribute('disabled', 'disabled');
        obj.value = wait + ' 秒后重新获取';
        wait--;
        setTimeout(function() {
            countDown(obj);
        }, 1000);
    }
}

/**
 * 重新获取短信验证码
 */
function refreshSMS(type) {
    switch(type) {
        case 'signUp':
            var url = 'http://app.tmh360.cn/api/user/reCode/type/1';
            break;
        case 'resetPassword':
            var url = 'http://app.tmh360.cn/api/user/reCode/type/2';
            break;
        case 'bindPhone':
            var url = 'http://app.tmh360.cn/api/user/reCode/type/3';
            break;
    }
    
    mui.ajax(url, {
        type: 'post',
        data: 0,
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            var obj = eval(data);
            if(obj.code != 0) {
                mui.toast(obj.message);
            } else {
                mui.toast('短信验证码已下发，请注意查收');
                countDown(document.getElementById('smsBtn'));
            }
        }
    });
}

/**
 * 对象 CSS 类操作
 * 
 * @param {Object} obj      要改变 CSS 类的对象
 * @param {String} cls      要操作的 CSS 类名
 */
function hasClass(obj, cls) {
    return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}
function addClass(obj, cls) {
    if(!this.hasClass(obj, cls)) obj.className += ' ' + cls;
}
function removeClass(obj, cls) {
    if(hasClass(obj, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        obj.className = obj.className.replace(reg, ' ');
    }
}
function toggleClass(obj, cls) {
    if(hasClass(obj, cls)) {
        removeClass(obj, cls);
    } else {
        addClass(obj, cls);
    }
}

/**
 * 遍历数组或对象元素
 * 
 * @param {Object} obj      需要遍历的数组或对象
 * @param {Function} fn     遍历的回调函数
 */
function each(obj, fn) {
    if(!fn) {
        return;
    }
    
    if(obj instanceof Array) {
        // 数组
        for(var i = 0; i < obj.length; i++) {
            if(fn.call(obj[i], i) == false) {
                // 函数中的this指向obj[i], i为索引
                break;
            }
        }
    } else if(typeof obj === 'object') {
        // 对象
        var j = null;
        for(j in obj) {
            if(fn.call(obj[j], j) == false) {
                // 函数中的this指向obj[j], j为属性名称
                break;
            }
        }
    }
}

/**
 * 格式化数字
 * 
 * @param {Number} num              需要格式化的数字
 * @param {Number} precision        需要保留小数点的位数
 * @param {String} separator        分隔符
 */
function formatNumber(num, precision, separator) {
    var parts;
    // 判断是否为数字
    if (!isNaN(parseFloat(num)) && isFinite(num)) {
        // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
        // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(num))
        // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
        // 的值变成了 12312312.123456713
        num = Number(num);
        // 处理小数点位数
        num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
        // 分离数字的小数部分和整数部分
        parts = num.split('.');
        // 整数部分加[separator]分隔, 借用一个著名的正则表达式
        parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

        return parts.join('.');
    }
    return '0.00';
}

function closeAllWebView(exclude) {
    var views = plus.webview.all();
    for(var i in views) {
        if(views[i]['id'] != plus.runtime.appid && views[i]['id'] != 'leftMenu' && views[i]['id'] != exclude) {
            plus.webview.getWebviewById(views[i]['id']).close();
        }
    }
}

function showIndex() {
    plus.webview.getWebviewById(plus.runtime.appid).show();
    setTimeout('closeAllWebView(plus.runtime.appid);', 1000);
}
