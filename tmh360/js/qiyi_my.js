/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Bruce
 * @version     qiyi_my.js 15/8/12 15:14
 * @link        http://www.fs71.com.cn
 */

/**
 * 消费者注册
 * 
 * @param {Number} step     注册步骤
 */
function ajaxConsumerSignup(step) {
    if(step == 1) {
        var frmPhone = document.getElementById('frmPhone').value;
    
        if(frmPhone == '') {
            mui.toast('请输入手机号码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/signup/step/1', {
                type: 'post',
                data: {
                    "frmPhone": frmPhone,
                    "frmUserType": "3",
                    "frmPlatform": "1"
                },
                timeout: 10000,
                dataType: 'json',
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.openWindow({
                            id: 'signupConsumerStep2',
                            url: 'signupConsumerStep2.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            }
                        });
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
    
    if(step == 2) {
        var frmCode = document.getElementById('frmCode').value;
        var frmPassword = document.getElementById('frmPassword').value;

        if(frmCode == '') {
            mui.toast('请输入短信验证码');
        } else if(frmPassword == '') {
            mui.toast('请设置6-16位的登录密码');
        } else {
            var screenWidth = plus.screen.resolutionWidth * plus.screen.scale;
            var screenHeight = plus.screen.resolutionHeight * plus.screen.scale;
            var os = plus.os.name + ' ' + plus.os.version + ' ' + plus.os.language;
            mui.ajax('http://app.tmh360.cn/api/user/signup/step/2', {
                type: 'post',
                data: {
                    "frmCode": frmCode,
                    "frmPassword": frmPassword,
                    "mfr": plus.device.vendor,
                    "model": plus.device.model,
                    "uuid": plus.device.uuid,
                    "screen": screenWidth + 'x' + screenHeight,
                    "os": os,
                    "network": plus.networkinfo.getCurrentType()
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.fire(plus.webview.getWebviewById('homeConsumer'), 'reload', {
                            id: 'signupConsumer'
                        });
                        mui.toast('注册成功');
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
}

/**
 * 获取管理员级别及保证金
 */
function ajaxDeposit() {
    mui.ajax('http://app.tmh360.cn/api/user/getconfigdata', {
        type: 'post',
        data: {
            "item": "deposit"
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            var obj = document.getElementById('levelPicker');
            obj.setAttribute('data-lva', data.deposit_level_a);
            obj.setAttribute('data-lvb', data.deposit_level_b);
            obj.setAttribute('data-lvc', data.deposit_level_c);
            obj.setAttribute('data-deposit', data.deposit_level_c);
            document.getElementById('deposit').innerText = '￥' + data.deposit_level_c + '.00';
            
            plus.storage.setItem('depositLevelC', data.deposit_level_c);
        },
        error: function(type) {
//              alert(1);
        }
    });
}

/**
 * 管理员注册
 * 
 * @param {Number} step     注册步骤
 */
function ajaxDistributorSignup(step) {
    if(step == 1) {
        var frmPhone = document.getElementById('frmPhone').value;
        var frmInvitation = document.getElementById('frmInvitation').value;
    
        if(frmPhone == '') {
            mui.toast('请输入手机号码');
        } else if(frmInvitation == '') {
            mui.toast('请输入邀请码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/signup/step/1', {
                type: 'post',
                data: {
                    "frmPhone": frmPhone,
                    "frmInvitation": frmInvitation,
                    "frmUserType": "2",
                    "frmPlatform": "1"
                },
                timeout: 10000,
                dataType: 'json',
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.openWindow({
                            id: 'signupDistributorStep2',
                            url: 'signupDistributorStep2.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            }
                        });
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
    
    if(step == 2) {
        var level = document.getElementById('levelPicker');
        var frmCode = document.getElementById('frmCode').value;
        var frmPassword = document.getElementById('frmPassword').value;
        var frmLevel = level.getAttribute('data-level');
        var frmDeposit = level.getAttribute('data-deposit');
        var frmArea = document.getElementById('frmArea').innerText;

        if(frmCode == '') {
            mui.toast('请输入短信验证码');
        } else if(frmPassword == '') {
            mui.toast('请设置6-16位的登录密码');
        } else if(frmArea == '选择所在地区') {
            mui.toast('请选择所在地区');
        } else {
            var screenWidth = plus.screen.resolutionWidth * plus.screen.scale;
            var screenHeight = plus.screen.resolutionHeight * plus.screen.scale;
            var os = plus.os.name + ' ' + plus.os.version + ' ' + plus.os.language;
            var depositLevelC = plus.storage.getItem('depositLevelC');
            plus.storage.removeItem('depositLevelC');
            mui.ajax('http://app.tmh360.cn/api/user/signup/step/2', {
                type: 'post',
                data: {
                    "frmCode": frmCode,
                    "frmPassword": frmPassword,
                    "frmLevel": frmLevel,
                    "frmDeposit": frmDeposit,
                    "frmArea": frmArea,
                    "mfr": plus.device.vendor,
                    "model": plus.device.model,
                    "uuid": plus.device.uuid,
                    "screen": screenWidth + 'x' + screenHeight,
                    "os": os,
                    "network": plus.networkinfo.getCurrentType(),
                    "depositLevelC": depositLevelC
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        if(frmLevel == 3) {
                            if(depositLevelC == 0) {
                                plus.nativeUI.alert('注册成功!', showIndex(), '跨境特卖会');
                            } else {
                                mui.openWindow({
                                    id: 'depositCharge',
                                    url: 'depositCharge.html',
                                    styles: {
                                        scrollIndicator: 'none'
                                    },
                                    show: {
                                        duration: 200
                                    },
                                    extras: {
                                        orderID: data.order_id,
                                        deposit: frmDeposit,
                                    }
                                });
                            }
                        } else {
                            mui.openWindow({
                                id: 'depositTransfer',
                                url: 'depositTransfer.html',
                                styles: {
                                    scrollIndicator: 'none'
                                },
                                show: {
                                    duration: 200
                                },
                                extras: {
                                    level: frmLevel,
                                    deposit: frmDeposit
                                }
                            });
                        }
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
}

/**
 * 检测用是否登录
 * 
 * @param {String} type     用户类型
 */
function checkUserStatus(type) {
    if(type == 'consumer' && plus.storage.getItem('uid') != null) {
        removeClass(document.getElementById('settings'), 'mui-hidden');
        addClass(document.getElementById('login').parentElement.parentElement, 'mui-hidden');
        removeClass(document.getElementById('phone').parentElement, 'mui-hidden');
        addClass(document.getElementById('about').parentElement, 'mui-hidden');
        addClass(document.getElementById('terms').parentElement, 'mui-hidden');
        removeClass(document.getElementById('follow').parentElement, 'mui-hidden');
        removeClass(document.getElementById('certification').parentElement, 'mui-hidden');
        
        var phone = plus.storage.getItem('phone');
        
        document.getElementById('phone').innerText = phone.substr(0,3) + '****' + phone.substr(7,4);
    }
    
    if(type == 'distributor' && plus.storage.getItem('uid') != null) {
        var phone = plus.storage.getItem('phone');
        
        document.getElementById('phone').innerText = phone.substr(0,3) + '****' + phone.substr(7,4);
    }
}

/**
 * 管理员及消费者登录 
 */
function ajaxLogin() {
    var frmPhone = document.getElementById('frmPhone').value;
    var frmPassword = document.getElementById('frmPassword').value;
    
    if(frmPhone == '') {
        mui.toast('请输入手机号码');
    } else if(frmPassword == '') {
        mui.toast('请输入登录密码');
    } else {
        mui.ajax('http://app.tmh360.cn/api/user/login', {
            type: 'post',
            data: {
                "frmPhone": frmPhone,
                "frmPassword": frmPassword
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    if(data.status == 3) {
                        mui.openWindow({
                            id: 'depositCharge',
                            url: 'depositCharge.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            },
                            extras: {
                                orderID: data.order_id,
                                deposit: data.deposit,
                            }
                        });
                        
                        mui.toast('请先缴纳保证金，以激活账号');
                    } else {
                        plus.storage.clear();
                        plus.storage.setItem('uid', data.uid);
                        plus.storage.setItem('phone', data.phone);
                        plus.storage.setItem('userType', data.userType);
                        
                        if(plus.webview.currentWebview().from == 'cart') {
                            mui.back();
                            mui.fire(plus.webview.getWebviewById('cart'), 'checkDelivery');
                        } else if(plus.webview.currentWebview().from == 'goods') {
                            mui.back();
                        } else {
                            mui.fire(plus.webview.getWebviewById('homeConsumer'), 'reload', {
                                id: 'login'
                            });
                        }
                        
                        mui.toast('登录成功');
                    }
                }
            },
            error: function(type) {
//              alert(1);
            }
        });
    }
}

/**
 * 检测用户实名认证状态或获取用户实名认证信息
 * 
 * @param {String} type     操作类型
 */
function checkUserCertification(type) {
    var uid = plus.storage.getItem('uid');
    
    if(uid != null) {
        mui.ajax('http://app.tmh360.cn/api/user/certification/action/read', {
            type: 'post',
            data: {
                "frmUID": uid,
                "frmUserType": plus.storage.getItem('userType')
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(type == 'status') {
                    if(data.truename == '' || data.idnum == '') {
                        document.getElementById('certStatus').innerText = '未认证';
                    } else {
                        document.getElementById('certStatus').innerText = '已认证';
                    }
                } else if(type == 'collection') {
                    if(data.truename != '' && data.idnum != '') {
                        document.getElementById('collectionName').value = data.truename;
                        document.getElementById('collectionName').readOnly = true;
                        document.getElementById('collectionID').value = data.idnum;
                        document.getElementById('collectionID').readOnly = true;
                    }
                } else {
                    if(data.truename != '' && data.idnum != '') {
                        document.getElementById('frmTrueName').value = data.truename;
                        document.getElementById('frmTrueName').setAttribute('disabled', true);
                        document.getElementById('frmID').value = data.idnum;
                        document.getElementById('frmID').setAttribute('disabled', true);
                        document.getElementById('btnSubmit').parentElement.setAttribute('class', 'mui-hidden');
                    }
                }
            },
            error: function(type) {
//              alert(1);
            }
        });
    } else {
        document.getElementById('certStatus').innerText = '未认证';
    }
}

/**
 * 检测用户收货地址状态或获取用户收货地址信息
 * 
 * @param {String} type     操作类型
 */
function checkUserDelivery(type) {
    var uid = plus.storage.getItem('uid');
    
    if(uid != null) {
        mui.ajax('http://app.tmh360.cn/api/user/delivery/action/read', {
            type: 'post',
            data: {
                "frmUID": uid,
                "frmUserType": plus.storage.getItem('userType')
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(type == 'status') {
                    if(data == null) {
                        document.getElementById('deliStatus').innerText = '未设置';
                    } else {
                        document.getElementById('deliStatus').innerText = '已设置';
                    }
                } else {
                    if(data != null) {
                        document.getElementById('frmTrueName').value = data.frmTrueName;
                        document.getElementById('frmPhone').value = data.frmPhone;
                        document.getElementById('frmCity').innerText = data.frmCity;
                        document.getElementById('frmAddress').value = data.frmAddress;
                        document.getElementById('frmZipCode').value = data.frmZipCode;
                        document.getElementById('btnSubmit').setAttribute('data-type', 'update');
                    }
                }
            },
            error: function(type) {
//              alert(1);
            }
        });
    } else {
        document.getElementById('deliStatus').innerText = '未设置';
    }
}

function checkUserCollection(type) {
    mui.ajax('http://app.tmh360.cn/api/user/collection/action/read', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(type == 'status') {
                if(data == '') {
                    document.getElementById('collStatus').innerText = '未设置';
                } else {
                    document.getElementById('collStatus').innerText = '已设置';
                }
            } else {
                if(data != '') {
                    document.getElementById('collectionBank').value = data[0];
                    document.getElementById('collectionAccount').value = data[1];
                    document.getElementById('collectionArea').value = data[2];
                }
            }
        }
    });
}

function ajaxUserCollection() {
    var collectionName = document.getElementById('collectionName').value;
    var collectionID = document.getElementById('collectionID').value;
    var collectionAccount = document.getElementById('collectionAccount').value;
    var collectionBank = document.getElementById('collectionBank').value;
    var collectionArea = document.getElementById('collectionArea').value;
    var status = document.getElementById('collectionName').readOnly;

    if(collectionName == '') {
        mui.toast('请输入真实姓名');
    } else if(collectionID == '') {
        mui.toast('请输入18位的身份证号码');
    } else if(collectionAccount == '') {
        mui.toast('请输入收款银行账号');
    } else if(collectionBank == '') {
        mui.toast('请选择开户银行名称');
    } else if(collectionArea == '') {
        mui.toast('请选择开户银行所在地');
    } else {
        mui.ajax('http://app.tmh360.cn/api/user/collection/action/update', {
            type: 'post',
            data: {
                "frmUID": plus.storage.getItem('uid'),
                "frmCollectionAccount": collectionAccount,
                "frmCollectionBank": collectionBank,
                "frmCollectionArea": collectionArea,
                "frmTrueName": collectionName,
                "frmID": collectionID,
                "frmCertStatus": status
            },
            dataType: 'json',
            timeout: 10000,
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    mui.back();
                    plus.webview.getWebviewById('homeDistributor').reload();
                    mui.toast('设置提现账户成功');
                }
            }
        });
    }
}

/**
 * 管理员及消费者重置密码
 * 
 * @param {Number} step     重置密码步骤
 */
function ajaxResetPassword(step) {
    if(step == 1) {
        var frmPhone = document.getElementById('frmPhone').value;
        
        if(frmPhone == '') {
            mui.toast('请输入注册手机号码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/resetpassword/step/1', {
                type: 'post',
                data: {
                    "frmPhone": frmPhone
                },
                timeout: 10000,
                dataType: 'json',
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.openWindow({
                            id: 'resetPasswordStep2',
                            url: 'resetPasswordStep2.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            }
                        });
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
    
    if(step == 2) {
        var frmCode = document.getElementById('frmCode').value;
        var frmPassword = document.getElementById('frmPassword').value;

        if(frmCode == '') {
            mui.toast('请输入短信验证码');
        } else if(frmPassword == '') {
            mui.toast('请设置6-16位新登录密码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/resetpassword/step/2', {
                type: 'post',
                data: {
                    "frmCode": frmCode,
                    "frmPassword": frmPassword
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        plus.webview.getWebviewById('resetPasswordStep1').close('none');
                        plus.webview.getWebviewById('resetPasswordStep2').close('none');
                        mui.toast('重置登录密码成功');
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
}

/**
 * 管理员及消费者绑定手机
 * 
 * @param {Number} step         绑定步骤
 */
function ajaxBindPhone(step) {
    if(step == 1) {
        var frmPassword = document.getElementById('frmPassword').value;
        var frmNewPhone = document.getElementById('frmNewPhone').value;
        
        if(frmPassword == '') {
            mui.toast('请输入当前登录密码');
        } else if(frmNewPhone == '' || frmNewPhone == plus.storage.getItem('phone')) {
            mui.toast('请设置新的手机号码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/bindphone/step/1', {
                type: 'post',
                data: {
                    "frmUID": plus.storage.getItem('uid'),
                    "frmPassword": frmPassword,
                    "frmPhone": frmNewPhone
                },
                timeout: 10000,
                dataType: 'json',
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.openWindow({
                            id: 'bindPhoneStep2',
                            url: 'bindPhoneStep2.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            }
                        });
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
    
    if(step == 2) {
        var frmCode = document.getElementById('frmCode').value;

        if(frmCode == '') {
            mui.toast('请输入短信验证码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/bindphone/step/2', {
                type: 'post',
                data: {
                    "frmCode": frmCode
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        plus.storage.clear();
                        mui.fire(plus.webview.getWebviewById('homeConsumer'), 'reload', {
                            id: 'settings'
                        });
                        plus.webview.getWebviewById('bindPhoneStep1').close('none');
                        plus.webview.getWebviewById('bindPhoneStep2').close('none');
                        mui.toast('绑定手机成功，请使用新手机号码重新登录');
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
}

/**
 * 管理员及消费者修改密码 
 */
function ajaxChangePassword() {
    var frmOldPassword = document.getElementById('frmOldPassword').value;
    var frmNewPassword = document.getElementById('frmNewPassword').value;
    
    if(frmOldPassword == '') {
        mui.toast('请输入当前登录密码');
    } else if(frmNewPassword == '') {
        mui.toast('请设置6-16位新密码');
    } else if(frmNewPassword === frmOldPassword) {
        mui.toast('新旧登录密码不允许一致');
    } else {
        mui.ajax('http://app.tmh360.cn/api/user/changepassword', {
            type: 'post',
            data: {
                "frmUID": plus.storage.getItem('uid'),
                "frmOldPassword": frmOldPassword,
                "frmNewPassword": frmNewPassword
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    plus.storage.clear();
                    mui.fire(plus.webview.getWebviewById('homeConsumer'), 'reload', {
                        id: 'settings'
                    });
                    plus.webview.getWebviewById('changePassword').close('none');
                    mui.toast('修改密码成功，请使用新密码重新登录');
                }
            },
            error: function(type) {
//              alert(1);
            }
        });
    }
}

/**
 * 管理员及消费者实名认证 
 */
function ajaxCertification() {
    var frmTrueName = document.getElementById('frmTrueName').value;
    var frmID = document.getElementById('frmID').value;
    
    if(frmTrueName == '') {
        mui.toast('请输入真实姓名');
    } else if(frmID == '') {
        mui.toast('请输入18位身份证号码');
    } else {
        mui.ajax('http://app.tmh360.cn/api/user/certification/action/create', {
            type: 'post',
            data: {
                "frmUID": plus.storage.getItem('uid'),
                "frmUserType": plus.storage.getItem('userType'),
                "frmTrueName": frmTrueName,
                "frmID": frmID
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    if(plus.webview.currentWebview().from == 'cart') {
                        mui.back();
                        mui.fire(plus.webview.getWebviewById('cart'), 'createOrder');
                    } else {
                        mui.fire(plus.webview.getWebviewById('homeConsumer'), 'reload', {
                            id: 'certification'
                        });
                    }
                    mui.toast('实名认证成功');
                }
            },
            error: function(type) {
//              alert(1);
            }
        });
    }
}

/**
 * 管理员及消费者收货地址
 */
function ajaxDelivery() {
    var frmTrueName = document.getElementById('frmTrueName').value;
    var frmPhone = document.getElementById('frmPhone').value;
    var frmCity = document.getElementById('frmCity').innerText;
    var frmAddress = document.getElementById('frmAddress').value;
    var frmZipCode = document.getElementById('frmZipCode').value;

    if(frmTrueName == '') {
        mui.toast('请输入收货人真实姓名');
    } else if(frmPhone == '') {
        mui.toast('请输入收货人手机号码');
    } else if(frmCity == '选择收货人所在地区') {
        mui.toast('请选择收货人所在地区');
    } else if(frmAddress == '') {
        mui.toast('请输入收货人详细地址');
    } else if(frmZipCode == '') {
        mui.toast('请输入收货人所在地区邮政编码');
    } else {
        if(plus.webview.currentWebview().from == 'checkout') {
            mui.ajax('http://app.tmh360.cn/api/trade/order/action/updateShipInfo', {
                type: 'post',
                data: {
                    "frmOrderID": plus.webview.currentWebview().orderID,
                    "frmTrueName": frmTrueName,
                    "frmPhone": frmPhone,
                    "frmCity": frmCity,
                    "frmAddress": frmAddress,
                    "frmZipCode": frmZipCode
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.back();
                        mui.fire(plus.webview.getWebviewById('checkout'), 'refresh');
                        mui.toast('收货地址设置成功');
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/delivery/action/' + document.getElementById('btnSubmit').getAttribute('data-type'), {
                type: 'post',
                data: {
                    "frmUID": plus.storage.getItem('uid'),
                    "frmUserType": plus.storage.getItem('userType'),
                    "frmTrueName": frmTrueName,
                    "frmPhone": frmPhone,
                    "frmCity": frmCity,
                    "frmAddress": frmAddress,
                    "frmZipCode": frmZipCode
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        if(plus.webview.currentWebview().from == 'cart') {
                            mui.back();
                            mui.fire(plus.webview.getWebviewById('cart'), 'checkOrderType');
                        } else {
                            mui.fire(plus.webview.getWebviewById('homeConsumer'), 'reload', {
                                id: 'delivery'
                            });
                        }
                        mui.toast('收货地址设置成功');
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
    }
}

/**
 * 验证管理员身份
 */
function ajaxCheckDistributor() {
    var frmPassword = document.getElementById('frmPassword').value;

        if(frmPassword == '') {
            mui.toast('请输入登录密码');
        } else {
            mui.ajax('http://app.tmh360.cn/api/user/checkdistributor', {
                type: 'post',
                data: {
                    "frmUID": plus.storage.getItem('uid'),
                    "frmPassword": frmPassword
                },
                dataType: 'json',
                timeout: 10000,
                success: function(data) {
                    if(data.code != 0) {
                        mui.toast(data.message);
                    } else {
                        mui.openWindow({
                            id: 'homeDistributor',
                            url: 'homeDistributor.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            }
                        });
                    }
                },
                error: function(type) {
    //              alert(1);
                }
            });
        }
}

function ajaxChargeDeposit(orderID, pay, amount) {
    var url = 'http://app.tmh360.cn/api/trade/charge/action/create/orderID/'+ orderID + '/channel/' + pay + '/amount/' + amount; 
    waiting = plus.nativeUI.showWaiting();

    // 请求支付订单
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.send();
    
    xhr.onreadystatechange = function() {
        switch(xhr.readyState) {
            case 4:
                waiting.close();
                waiting = null;
                if(xhr.status == 200) {
//                  var obj = JSON.parse(xhr.responseText);
                    plus.payment.request(channel, xhr.responseText, function(result) {
                        var charge = JSON.parse(result.description);
                        switch(charge.result) {
                            case 'success':
                                plus.nativeUI.alert("支付成功!", showIndex(), "跨境特卖会");
                                break;
                            case 'cancel':
                                plus.nativeUI.alert('支付取消，请联系客服：400-110-5876！', showIndex(), '跨境特卖会');
                                break;
                            case 'fail':
                                plus.nativeUI.alert('支付失败，请联系客服：400-110-5876！', showIndex(), '跨境特卖会');
                                break;
                        }
                    }, function(e) {
                        plus.nativeUI.alert('支付失败，请联系客服：400-110-5876！', showIndex(), '跨境特卖会');
                    });
                } else {
                    plus.nativeUI.alert('获取支付信息失败，请再试一次！', null, '跨境特卖会');
                }
                break;
            default:
                break;
        }
    }
}

function checkUserDetails() {
    mui.ajax('http://app.tmh360.cn/api/user/details/action/read', {
            type: 'post',
            data: {
                "frmUID": plus.storage.getItem('uid'),
                "frmUserType": plus.storage.getItem('userType')
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(data.cert.truename != '') {
                    var frmTureName = document.getElementById('frmTrueName');
                    frmTureName.value = data.cert.truename;
                    frmTureName.readOnly = true;
                }

                if(data.cert.idnum != '') {
                    var frmID = document.getElementById('frmID');
                    frmID.value = data.cert.idnum;
                    frmID.readOnly = true;
                }
                
                if(data.delivery != null) {
                    document.getElementById('frmCity').innerText = data.delivery.frmCity;
                    document.getElementById('frmAddress').value = data.delivery.frmAddress;
                    document.getElementById('frmZipCode').value = data.delivery.frmZipCode;
                }
                
                if(data.cert.ic[0] != null) {
                    var frmICFront = document.getElementById('frmICFront');
                    frmICFront.innerHTML = '';
                    frmICFront.setAttribute('data-img', data.cert.ic[0].substr(-22));
                    frmICFront.style.backgroundImage = 'url(' + data.cert.ic[0] + ')';
                }
                
                if(data.cert.ic[1] != null) {
                    var frmICBack = document.getElementById('frmICBack');
                    frmICBack.innerHTML = '';
                    frmICBack.setAttribute('data-img', data.cert.ic[1].substr(-22));
                    frmICBack.style.backgroundImage = 'url(' + data.cert.ic[1] + ')';
                }
            }
    });
}

function details() {
    var frmTrueName = document.getElementById('frmTrueName').value;
    var frmID = document.getElementById('frmID').value;
    var frmCity = document.getElementById('frmCity').innerText;
    var frmAddress = document.getElementById('frmAddress').value;
    var frmZipCode = document.getElementById('frmZipCode').value;
    var frmICFront = document.getElementById('frmICFront').getAttribute('data-img');
    var frmICBack = document.getElementById('frmICBack').getAttribute('data-img');

    if(frmTrueName == '') {
        mui.toast('请输入真实姓名');
    } else if(frmID == '') {
        mui.toast('请输入身份证号码');
    } else if(frmCity == '选择收货人所在地区') {
        mui.toast('请选择收货人所在地区');
    } else if(frmAddress == '') {
        mui.toast('请输入收货人详细地址');
    } else if(frmZipCode == '') {
        mui.toast('请输入收货人所在地区邮政编码');
    } else if(frmICFront == null) {
        mui.toast('请上传身份证正面照片');
    } else if(frmICBack == null) {
        mui.toast('请上传身份证背面照片');
    } else {
        mui.ajax('http://app.tmh360.cn/api/user/details/action/create', {
            type: 'post',
            data: {
                "frmTrueName": frmTrueName,
                "frmID": frmID,
                "frmCity": frmCity,
                "frmAddress": frmAddress,
                "frmZipCode": frmZipCode,
                "frmICFront": frmICFront,
                'frmICBack': frmICBack
            },
            dataType: 'json',
            timeout: 10000,
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    mui.back();
                    mui.fire(plus.webview.getWebviewById('cart'), 'createOrder');
                    mui.toast('完善资料成功');
                }
            }
        });
    }
}

function choose(obj) {
    var a = [{
        title: '拍照'
    }, {
        title: '从手机相册选择'
    }];
    plus.nativeUI.actionSheet({
        cancel: '取消',
        buttons: a
    }, function(b) {
        switch (b.index) {
            case 0:
                break;
            case 1:
                getImage(obj);
                break;
            case 2:
                galleryImg(obj);
                break;
            default:
                break;
        }
    })
}

function getImage(obj) {
    var nowTime = new Date(); //实例一个时间对象；
    //获取系统时间，用来命名
    var time = new Array();
    time[0] = nowTime.getFullYear();//获取系统的年；
    time[1] = nowTime.getMonth() + 1;//获取系统月份，由于月份是从0开始计算，所以要加1
    time[2] = nowTime.getDate(); // 获取系统日，
    time[3] = nowTime.getHours(); //获取系统时，
    time[4] = nowTime.getMinutes(); //分
    time[5] =  nowTime.getSeconds(); //秒
    for(var i = 1; i < 6; i++) {
        if(time[i] < 10) {
            time[i] = '0' + time[i];
        }
    }

    plus.camera.getCamera().captureImage(function(e) {
        //保存图片到相册
        plus.gallery.save(e);

        //转换路径
        plus.io.resolveLocalFileSystemURL(e, function(entry) {
            //获取文件的系统内的绝对路径
            var path = entry.toLocalURL();
            compressImage(path, obj);
        });
    }, function(s) {
        console.log('error' + s);
    }, {
        //filename拍照或摄像文件保存的路径可设置具体文件名（如"_doc/camera/a.jpg"）；也可只设置路径，以"/"结尾则表明是路径（如"_doc/camera/"）。如未设置文件名称或设置的文件名冲突则文件名由程序程序自动生成（文件名为new Date().getTime() + '.jpg'）。 
        filename: '_doc/' + 'IMG_' + time[0] + time[1] + time[2] + '_' + time[3] + time[4] + time[5] + '.jpg'
    });
}

function galleryImg(obj) {
    // 从相册中选择图片
    plus.gallery.pick(function(path) {
        compressImage(path, obj);
    }, function (e) {
        
    }, {
        filter: 'image'
    });
}

// 压缩图片
function compressImage(path, obj){
    plus.zip.compressImage({
        src: path,
        dst: path,
        overwrite: true
    },function() {
        appendFile(path);
        upload(obj, 'ic');
    });
}

// 添加文件
function appendFile(path) {
    files.push({
        name: 0,
        path: path
    });
}

function upload(obj, type) {
    var wt = plus.nativeUI.showWaiting();
    var task = plus.uploader.createUpload('http://app.tmh360.cn/mobile/user/ajaxUpload/type/' + type, {method: 'POST'}, function(ret, status) {
        if(status == 200){
            var data = JSON.parse(ret.responseText);
            mui.toast(data.message);
            setUploadImage(obj, type, data.image);
            files = [];
            wt.close();
        } else {
            plus.nativeUI.alert('上传失败！');
            wt.close();
        }
    });
    
    for(var i = 0; i < files.length; i++) {
        var f = files[i];
        task.addFile(f.path, {key: String(f.name)});
    }
    task.start();
}

/**
 * 设置上传后的图片预览
 *
 * @param obj       设置的对象
 * @param type      图片的类型
 * @param image     图片的文件名
 */
function setUploadImage(obj, type, image) {
    var path = null;

    switch(type) {
        case 'ic':
            path = 'user/ic/';
            break;
    }

    document.getElementById(obj.id).setAttribute('data-img', image);
    document.getElementById(obj.id).style.backgroundImage = 'url("http://img.tmh360.cn/' + path + image + '")';
    document.getElementById(obj.id).style.backgroundSize = 'cover';
}

function checkUserSubordinate() {
    mui.ajax('http://app.tmh360.cn/api/user/subordinate/action/count', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data >= 0) {
                document.getElementById('subordinate').lastChild.previousSibling.innerText = data;
            }
        }
    });
}

function ajaxUserSubordinateList() {
    mui.ajax('http://app.tmh360.cn/api/user/subordinate/action/list', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                var obj = document.getElementById('list');
                mui.each(data, function(index, item) {
                    var ele = document.createElement('tr');
                    ele.innerHTML = '<td>' + item.phone + '</td><td>' + item.count + '</td>';
                    obj.appendChild(ele);
                });
            }
        }
    });
}

function checkOrderDistributor() {
    mui.ajax('http://app.tmh360.cn/api/user/orderDistributor/action/count', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data >= 0) {
                document.getElementById('orderDistributor').lastChild.previousSibling.innerText = data;
            }
        }
    });
}

function ajaxOrderDistributorList(type) {
    mui.ajax('http://app.tmh360.cn/api/user/orderDistributor/action/list', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid'),
            "frmStatus": type
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                if(type == 0) {
                    var obj = document.getElementById('listFinish');
                } else {
                    var obj = document.getElementById('listActive');
                }
                mui.each(data, function(index, item) {
                    var ele = document.createElement('tr');
                    ele.innerHTML = '<td>' + item.order_id + '</td><td>' + item.commission + '</td><td>' + item.status + '</td>';
                    obj.appendChild(ele);
                });
            }
        }
    });
}

function checkUserDeposit() {
    mui.ajax('http://app.tmh360.cn/api/user/deposit/action/read', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                document.getElementById('depositLog').lastChild.previousSibling.innerText = data;
            }
        }
    });
}

function ajaxUserDepositList() {
    mui.ajax('http://app.tmh360.cn/api/user/deposit/action/list', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                var obj = document.getElementById('list');
                mui.each(data, function(index, item) {
                    var ele = document.createElement('tr');
                    ele.innerHTML = '<td>' + item.create_time + '</td><td>' + item.deposit + '</td>';
                    obj.appendChild(ele);
                });
            }
        }
    });
}

function checkUserLevel() {
    mui.ajax('http://app.tmh360.cn/api/user/level/action/read', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                document.getElementById('levelLog').lastChild.previousSibling.innerText = data;
            }
        }
    });
}

function ajaxUserLevelList() {
    mui.ajax('http://app.tmh360.cn/api/user/level/action/list', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                var obj = document.getElementById('list');
                mui.each(data, function(index, item) {
                    var ele = document.createElement('tr');
                    ele.innerHTML = '<td>' + item.create_time + '</td><td>' + item.level + '</td>';
                    obj.appendChild(ele);
                });
            }
        }
    });
}

function checkUserPoint() {
    mui.ajax('http://app.tmh360.cn/api/user/point/action/read', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data >= 0) {
                document.getElementById('pointLog').lastChild.previousSibling.innerText = data;
            }
        }
    });
}

function ajaxUserPointList() {
    mui.ajax('http://app.tmh360.cn/api/user/point/action/list', {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                var obj = document.getElementById('list');
                mui.each(data, function(index, item) {
                    var ele = document.createElement('tr');
                    ele.innerHTML = '<td>' + item.create_time + '</td><td>' + item.point + '</td><td>' + item.remark + '</td>';
                    obj.appendChild(ele);
                });
            }
        }
    });
}

function checkUserCommission(type) {
    mui.ajax('http://app.tmh360.cn/api/user/commission/action/' + type, {
        type: 'post',
        data: {
            "frmUID": plus.storage.getItem('uid')
        },
        dataType: 'json',
        timeout: 10000,
        success: function(data) {
            if(data != '') {
                switch(type) {
                    case 'finish':
                        if(plus.webview.currentWebview().id == 'homeDistributor') {
                            document.getElementById('commission').lastChild.previousSibling.innerText = data;
                        }
                        
                        if(plus.webview.currentWebview().id == 'commission') {
                            document.getElementById('finishCommission').innerText = data;
                        }
                        break;
                    case 'active':
                        document.getElementById('activeCommission').innerText = data;
                        break;
                }
            }
        }
    });
}
