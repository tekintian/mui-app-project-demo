/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Bruce
 * @version     qiyi_store.js 15/8/6 20:56
 * @link        http://www.fs71.com.cn
 */

/**
 * Ajax 加载首页
 * 
 * @param {Number} bid  品牌 ID
 */
function ajaxBrandDetail(bid) {
    mui.ajax('http://app.tmh360.cn/api/store/getBrandDetail', {
        type: 'post',
        data: {
            "bid": bid
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var length = data['slider'].length;
            var loop = document.getElementById('sliderLoop');
            var indicator = document.getElementById('sliderIndicator');
            
            for(var i = 0; i < length + 2; i++) {
                var eleLoop = document.createElement('div');
                if(i == 0 || i > length) {
                    var img = data['slider'][0];
                } else {
                    var img = data['slider'][i - 1];
                }
                eleLoop.innerHTML = '<a><img src="' + img + '"></a>';
                loop.appendChild(eleLoop);
                loop.lastChild.setAttribute('class', 'mui-slider-item');
                
                var eleIndicator = document.createElement('div');
                indicator.appendChild(eleIndicator);
                indicator.lastChild.setAttribute('class', 'mui-indicator');
            }
            
            loop.firstChild.setAttribute('class', 'mui-slider-item mui-slider-item-duplicate');
            loop.lastChild.setAttribute('class', 'mui-slider-item mui-slider-item-duplicate');
            loop.firstChild.firstChild.firstChild.setAttribute('src', data['slider'][length - 1]);
            loop.lastChild.firstChild.firstChild.setAttribute('src', data['slider'][0]);
            
            indicator.removeChild(indicator.firstChild);
            indicator.removeChild(indicator.lastChild);
            indicator.firstChild.setAttribute('class', 'mui-indicator mui-active');
            
            document.getElementById('logo').setAttribute('src', data['logo']);
            document.getElementById('intro').innerText = data['intro'];
            document.getElementById('detail').innerText = data['detail'];
        },
        error: function(type) {
//          alert(1);
        }
    });
}

/**
 * Ajax 加载商品列表
 * 
 * @param {String} type     商品列表类型
 * @param {Number} id       商品分类 ID
 * @param {String} keyword  搜索商品的关键词
 */
function ajaxGoodsList(type, id, keyword) {
    mui.ajax('http://app.tmh360.cn/api/store/getGoodsList', {
        type: 'post',
        data: {
            "type": type,
            "id": id,
            "keyword": keyword
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data.length > 0) {
                var goodsList = document.getElementById('goodsList');
            
                each(data, function() {
                    var ele = document.createElement('div');
                    ele.innerHTML = '<div class="box-flex-1"><img class="thumb" src="' + this['thumb'] + '"></div><div class="info"><h4>' + this['title'].split('|')[0] + '</h4><div>' + this['title'].split('|')[1] + '</div><div>特卖价：<span class="price">' + this['price'] + '</span></div><div class="mui-text-right box"><span class="box-flex-1">销量：' + this['sale'] + '</span><span class="qiyifont icon-share share"></span><button class="mui-btn mui-hidden">抢光</button></div></div>';
                    goodsList.appendChild(ele);
                    
                    goodsList.lastChild.setAttribute('class', 'item box');
                    goodsList.lastChild.setAttribute('id', this['pid']);
                    
                    if(this['stock'] == 0) {
                        var nodes = goodsList.lastChild.lastChild.lastChild;
                        addClass(nodes.childNodes[1], 'mui-hidden');
                        removeClass(nodes.childNodes[2], 'mui-hidden');
                    }
                    
                    var soldinTime = this['soldin_time'] === undefined ? 0 : this['soldin_time'];
                    var nowTime = Math.floor(new Date().getTime() / 1000);
                    if(soldinTime > nowTime) {
                        var nodes = goodsList.lastChild.lastChild.lastChild;
                        var eleCountDown = document.createElement('div');
                        var pid = this['pid'];
                        nodes.replaceChild(eleCountDown, nodes.firstChild);
                        nodes.firstChild.setAttribute('class', 'box-flex-1');
                        nodes.firstChild.setAttribute('id', 'countDown' + pid);
                        addCountDown('countDown' + pid, soldinTime - nowTime, 'soldin');
                    }
                });
            } else {
                addClass(document.getElementById('goodsList'), 'mui-hidden');
                removeClass(document.getElementById('null'), 'mui-hidden');
            }
        },
        error: function(type) {
//          alert(1);
        }
    });
}

/**
 * Ajax 加载商品详情
 * 
 * @param {Number} pid      商品 ID
 */
function ajaxGoodsDetail(pid) {
    mui.ajax('http://app.tmh360.cn/api/store/getGoodsDetail', {
        type: 'post',
        data: {
            "pid": pid
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var length = data['slider'].length;
            var loop = document.getElementById('sliderLoop');
            var indicator = document.getElementById('sliderIndicator');
            
            for(var i = 0; i < length + 2; i++) {
                var eleLoop = document.createElement('div');
                if(i == 0 || i > length) {
                    var img = data['slider'][0];
                } else {
                    var img = data['slider'][i - 1];
                }
                eleLoop.innerHTML = '<a><img src="' + img + '"></a>';
                loop.appendChild(eleLoop);
                loop.lastChild.setAttribute('class', 'mui-slider-item');
                
                var eleIndicator = document.createElement('div');
                indicator.appendChild(eleIndicator);
                indicator.lastChild.setAttribute('class', 'mui-indicator');
            }
            
            loop.firstChild.setAttribute('class', 'mui-slider-item mui-slider-item-duplicate');
            loop.lastChild.setAttribute('class', 'mui-slider-item mui-slider-item-duplicate');
            loop.firstChild.firstChild.firstChild.setAttribute('src', data['slider'][length - 1]);
            loop.lastChild.firstChild.firstChild.setAttribute('src', data['slider'][0]);
            
            indicator.removeChild(indicator.firstChild);
            indicator.removeChild(indicator.lastChild);
            indicator.firstChild.setAttribute('class', 'mui-indicator mui-active');
            
            document.getElementById('brand').innerHTML = data['title'].split('|')[0] + '<span class="qiyifont icon-likes mui-pull-right" id="likes"> ' + data['likes'] + '</span>';
            document.getElementById('title').innerText = data['title'].split('|')[1];
            document.getElementById('price').innerText = data['price'];
            document.getElementById('marketPrice').innerText = data['market_price'];
            
            if(data['stock'] == '0') {
                document.getElementById('cart').innerText = '抢光';
                document.getElementById('cart').setAttribute('disabled', true);
            }
            
            var soldinTime = data['soldin_time'] === undefined ? 0 : data['soldin_time'];
            var nowTime = Math.floor(new Date().getTime() / 1000);
            if(soldinTime > nowTime) {
                document.getElementById('cart').innerText = '即将上线';
                document.getElementById('cart').setAttribute('disabled', true);
                var countDown = document.getElementById('soldinTime');
                countDown.setAttribute('id', 'countDown');
                removeClass(countDown, 'mui-hidden');
                addCountDown('countDown', soldinTime - nowTime, 'soldin');
            }
            
            document.getElementById('freight').innerText = data['freight'];
            document.getElementById('tips').innerText = data['tips'];
            
            var img = data['detail'];
            var detail = document.getElementById('detail');
            for(var i = 0; i < img.length; i++) {
                var ele = document.createElement('img');
                ele.setAttribute('src', img[i]);
                detail.appendChild(ele);
            }
            
            var item = data['params'];
            var params = document.getElementById('params');
            for(var i = 0; i < item.length; i++) {
                var ele = document.createElement('div');
                ele.innerHTML = item[i]['item'] + '： ' + item[i]['value'];
                params.appendChild(ele);
                params.lastChild.setAttribute('class', 'item');
            }
            
//          document.getElementById('shareCorner').setAttribute('data-pid', data['pid']);
            document.getElementById('follow').setAttribute('data-pid', data['pid']);
            document.getElementById('cart').setAttribute('data-pid', data['pid']);
            document.getElementById('share').setAttribute('data-pid', data['pid']);
            if(this['stock'] == 0) {
                addClass(document.getElementById('cart'), 'not');
            }
            
            document.getElementById('likes').addEventListener('tap', addLikes);
        },
        error: function(type) {
//          alert(1);
        }
    });
}

/**
 * 点赞 
 */
function addLikes() {
    mui.ajax('http://app.tmh360.cn/api/store/addlikes', {
        type: 'post',
        data: {
            "pid": document.getElementById('cart').getAttribute('data-pid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            document.getElementById('likes').innerText = ' ' + data.likes;
        },
        error: function(type) {
//          alert(1);
        }
    });
}

function addFollow() {
    mui.ajax('http://app.tmh360.cn/api/store/addFollow', {
        type: 'post',
        data: {
            "uid": plus.storage.getItem('uid'),
            "pid": document.getElementById('follow').getAttribute('data-pid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            mui.toast(data.message);
        }
    });
}

/**
 * 获取购物车内商品数量 
 */
function countCart() {
    mui.ajax('http://app.tmh360.cn/api/trade/cart/action/count', {
        type: 'post',
        data: '',
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data != null) {
                document.getElementById('quantity').firstChild.innerText = data;
            }
        },
        error: function(type) {
//          alert(1);
        }
    });
}

/**
 * 加入购物车
 */
function addCart() {
    mui.ajax('http://app.tmh360.cn/api/trade/cart/action/add', {
        type: 'post',
        data: {
            "pid": document.getElementById('cart').getAttribute('data-pid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            document.getElementById('quantity').firstChild.innerText = data;
        },
        error: function(type) {
//          alert(1);
        }
    });
}

// 显示分享菜单
function showShare(pid) {
    mui.fire(share, 'shareReload', {
        id: page.id,
        pid: pid
    });
    
    if(!shareStatus) {
        share.show('none', 0, function() {
            share.setStyle({
                bottom: '0',
                transition: {
                    duration: 150
                }
            });
        });
    };
    //显示遮罩
    mask.show();
    shareStatus = true;
}

/**
 * Android 返回键方法重写 
 */
function back() {
    if(shareStatus) {
        //菜单处于显示状态，返回键应该先关闭菜单,阻止主窗口执行mui.back逻辑
        hideShare();
        return false;
    } else {
        //菜单处于隐藏状态，执行返回时，要先close菜单页面，然后继续执行mui.back逻辑关闭主窗口
//      share.close('none');
        return true;
    }
}

/**
 * 用户点击蒙版时的回调 
 */
function closeMask() {
    //主窗体开始侧滑
    if(shareStatus) {
        share.setStyle({
            bottom: 0,
            transition: {
                duration: 150
            }
        });
    }
    
    //等窗体动画结束后，隐藏菜单webview，节省资源
    setTimeout(function() {
        share.hide();
    }, 200);
    //改变标志位
    shareStatus = false;
}

/**
 * 隐藏分享菜单
 */
function hideShare() {
    closeMask();
    //关闭遮罩
    mask.close();
}

function ajaxFollowList() {
    mui.ajax('http://app.tmh360.cn/api/store/getFollowList', {
        type: 'post',
        data: {
            "uid": plus.storage.getItem('uid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data != null) {
                var goodsList = document.getElementById('goodsList');
            
                each(data, function() {
                    var ele = document.createElement('div');
                    ele.innerHTML = '<div class="box-flex-1"><img class="thumb" src="' + this['thumb'] + '"></div><div class="info"><h4>' + this['title'].split('|')[0] + '</h4><div>' + this['title'].split('|')[1] + '</div><div>特卖价：<span class="price">' + this['price'] + '</span></div><div class="mui-text-right box"><span class="box-flex-1">销量：' + this['sale'] + '</span><button class="mui-btn" id="follow" data-pid="' + this['pid'] + '">取消关注</button></div></div>';
                    goodsList.appendChild(ele);
                    
                    goodsList.lastChild.setAttribute('class', 'item box');
                    goodsList.lastChild.setAttribute('id', this['pid']);
                    
                    if(this['stock'] == 0) {
                        var nodes = goodsList.lastChild.lastChild.lastChild;
                        addClass(nodes.childNodes[1], 'mui-hidden');
                        removeClass(nodes.childNodes[2], 'mui-hidden');
                    }
                    
                    var soldinTime = this['soldin_time'] === undefined ? 0 : this['soldin_time'];
                    var nowTime = Math.floor(new Date().getTime() / 1000);
                    if(soldinTime > nowTime) {
                        var nodes = goodsList.lastChild.lastChild.lastChild;
                        var eleCountDown = document.createElement('div');
                        var pid = this['pid'];
                        nodes.replaceChild(eleCountDown, nodes.firstChild);
                        nodes.firstChild.setAttribute('class', 'box-flex-1');
                        nodes.firstChild.setAttribute('id', 'countDown' + pid);
                        addCountDown('countDown' + pid, soldinTime - nowTime, 'soldin');
                    }
                });
            } else {
                addClass(document.getElementById('goodsList'), 'mui-hidden');
                removeClass(document.getElementById('null'), 'mui-hidden');
            }
        }
    });
}