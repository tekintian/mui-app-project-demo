/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Bruce
 * @version     qiyi_share.js 15/8/24 20:11
 * @link        http://www.fs71.com.cn
 */

/**
 * 更新分享服务
 */
function updateSerivces(){
    plus.share.getServices(function(service){
        shares = {};
        for(var i in service){
            var item = service[i];
            shares[item.id] = item;
        }
    }, function(e){
        console.log('获取分享服务列表失败：' + e.message);
    });
}

/**
* 分享操作
* 
* @param {String} id
*/
function shareAction(id, ex, pid) {
    var service = null;
    
    if(!id || !(service = shares[id])) {
        console.log('无效的分享服务！');
        return;
    }
    
    if(service.authenticated) {
        console.log('---已授权---');
        shareMessage(service, ex, pid);
    } else {
        console.log('---未授权---');
        service.authorize(function() {
            shareMessage(service, ex, pid);
        }, function(e) {
            console.log('认证授权失败：' + e.code + ' - ' + e.message);
        });
    }
}

/**
* 发送分享消息
* 
* @param {plus.share.ShareService} s
*/
function shareMessage(service, ex, pid) {
    var msg = {
        "content": "跨境特卖会，不一样的海淘体验！http://www.tmh360.com",
        "thumbs": ["_www/img/logo.jpg"],
        "pictures": ["_www/img/logo.jpg"],
        "href": "http://www.tmh360.com",
        "title": "跨境特卖会",
        "extra": {
            "scene": ex
        }
    };
    
    mui.ajax('http://app.tmh360.cn/api/store/getGoodsShare', {
        type: 'post',
        data: {
            "pid": pid
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var shareLink = 'http://m.tmh360.cn/store/goods/show/' + data.pid + '/dist/' + plus.storage.getItem('uid');
            
            msg.content = data.title + '，仅售 ' + data.price + ' 元，已热卖 ' + data.sale + ' 件，亲朋好友们快一起来抢吧。跨境特卖会，不一样的海淘体验！' + shareLink;
            msg.href = shareLink;
            
            if(service.id != 'sinaweibo' && plus.os.name != 'iOS') {
                msg.thumbs = [data.thumb];
                msg.pictures = [data.thumb];
            }
            
            if(ex == 'WXSceneTimeline') {
                msg.title = msg.content;
            }
            
            service.send(msg, function() {
                if(plus.os.name == 'Android') {
                    cancel();
                }
                mui.toast('分享到 ' + service.description + ' 成功！');
            }, function(e) {
                if(e.message.split(',')[0].split(']')[1] == 'User canceled') {
                    if(plus.os.name == 'Android') {
                        cancel();
                    }
                } else {
                    console.log('分享到\"' + service.description + '\"失败: ' + e.code + ' - ' + e.message);
                }
            });
        }
    });
}

function systemShare(pid, type) {
    ArrayList = plus.android.importClass('java.util.ArrayList');
    Uri = plus.android.importClass('android.net.Uri');
    File = plus.android.importClass('java.io.File');
    
    directoryEntry = null;
    localArrayList = new ArrayList();
    
    createDirectory('tmh360');
    
    mui.ajax('http://app.tmh360.cn/api/store/getGoodsShare', {
        type: 'post',
        data: {
            "pid": pid
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var shareLink = 'http://m.tmh360.cn/store/goods/show/' + data.pid + '/dist/' + plus.storage.getItem('uid');
            var content = data.title + '，仅售 ' + data.price + ' 元，已热卖 ' + data.sale + ' 件，亲朋好友们快一起来抢吧。跨境特卖会，不一样的海淘体验！' + shareLink;
            
            if(type == 'multiple') {
                downloadImg(data.slider);
            } else {
                var imgs = new Array();
                imgs[0] = data.slider[0];
                downloadImg(imgs);
            }

            copyToClip(content);
            cancel();
        }
    });
    
    
}

function createDirectory(directory) {
    plus.io.resolveLocalFileSystemURL('/storage/emulated/0/', function(entry) {
        //创建获取目录对象
        entry.getDirectory(directory, {create: true, exclusive: false}, function(dir) {
//          console.log(dir);
            directoryEntry = dir;
        }, function () {
//          console.log(e.message);
        });
    }, function (e) {
//      console.log("Resolve file URL failed: " + e.message);
    });
}

function downloadImg(loadUrl) {
    var imgsLength = 0;
    
    for(var i = 0; i < loadUrl.length; i++) {
        var task = plus.downloader.createDownload(loadUrl[i], null, function(download, status) {
            if(status == 200) {
                var img = plus.io.convertLocalFileSystemURL(download.filename);
                localArrayList.add(Uri.fromFile(new File(img)));

                imgsLength++;
                if(imgsLength == loadUrl.length) {
                    setTimeout('wechatShare();', 1000);
                }
            }
        });
        task.start();
    }
}

function copyToClip(content) {
    var Context = plus.android.importClass('android.content.Context');
    var main1 = plus.android.runtimeMainActivity();
    clip = main1.getSystemService(Context.CLIPBOARD_SERVICE);
    plus.android.invoke(clip, 'setText', content);
}

function wechatShare() {
    alert('图片下载成功，文案已复制到剪切板，请在微信中长按并粘贴文案到文本输入框。');
    
    var Intent = plus.android.importClass('android.content.Intent');
    var ComponentName = plus.android.importClass('android.content.ComponentName');
    var intent = new Intent(Intent.ACTION_SEND);
    var localComponentName = new ComponentName('com.tencent.mm', 'com.tencent.mm.ui.tools.ShareToTimeLineUI');
    
    intent.setComponent(localComponentName);
    intent.setAction('android.intent.action.SEND_MULTIPLE');
    intent.setType('image/*');
    
    //发送图片
    intent.putParcelableArrayListExtra('android.intent.extra.STREAM', localArrayList);

    var act = plus.android.runtimeMainActivity();
    act.startActivity(intent);
}

/*
function copyImg(direcotryEntry, item) {
    var imgName = item.substr(11, item.length);

    plus.io.resolveLocalFileSystemURL(item, function(entry) {
        entry.copyTo(direcotryEntry, imgName, function(dirEntry) {
            console.log("New Path: " + dirEntry.fullPath);
        }, function(e) {
            console.log(e.message);
        });
    }, function (e) {
        console.log( "Resolve file URL failed: " + e.message );
    });
}
*/
