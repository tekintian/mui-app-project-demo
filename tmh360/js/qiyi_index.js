/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Bruce
 * @version     qiyi_index.js 15/8/3 20:14
 * @link        http://www.fs71.com.cn
 */

// Ajax 加载首页轮播图
function ajaxSlider() {
    mui.ajax('http://app.tmh360.cn/api/index/getConfigData', {
        type: 'post',
        data: {
            "item": "slider"
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var length = data.length;
            var loop = document.getElementById('sliderLoop');
            var indicator = document.getElementById('sliderIndicator');
            loop.removeChild(loop.lastChild);
            
            for(var i = 0; i < length + 1; i++) {
                loop.appendChild(loop.firstChild.cloneNode(true));
                var img = loop.lastChild.firstChild.firstChild;
                img.setAttribute('src', data[i]);
                img.setAttribute('alt', data[i]);

                if (i < length - 1) {
                    indicator.appendChild(indicator.firstChild.cloneNode(true));
                }
            }
            
            loop.firstChild.setAttribute('class', 'mui-slider-item mui-slider-item-duplicate');
            loop.lastChild.setAttribute('class', 'mui-slider-item mui-slider-item-duplicate');
            loop.firstChild.firstChild.firstChild.setAttribute('src', data[length - 1]);
            loop.lastChild.firstChild.firstChild.setAttribute('src', data[1]);
            
            indicator.firstChild.setAttribute('class', 'mui-indicator mui-active');
        }
    });
}

// Ajax 加载首页频道
function ajaxChannel() {
    mui.ajax('http://app.tmh360.cn/api/index/getConfigData', {
        type: 'post',
        data: {
            "item": "channel"
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var channel = document.getElementById('channel');
            
            for(var i = 0; i < data.length; i++) {
                var ele = document.createElement('li');
                ele.innerHTML = '<span class="qiyifont ' + data[i]['icon'] + '" style="background-color: ' + data[i]['color'] + ';"></span><a>' + data[i]['name'] + '</a>';
                channel.appendChild(ele);
                channel.lastChild.setAttribute('id', data[i]['cid']);
            }
        },
        error: function(type) {
//          alert(1);
        }
    });
}

// Ajax 加载首页专区
function ajaxSpecial() {
    mui.ajax('http://app.tmh360.cn/api/index/getConfigData', {
        type: 'post',
        data: {
            "item": "special"
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            document.getElementById('speicalLeft').setAttribute('src', data[0]['img']);
            document.getElementById('speicalLeft').setAttribute('data-id', data[0]['cid']);
            document.getElementById('speicalLeft').setAttribute('data-name', data[0]['name']);
            document.getElementById('speicalRightTop').setAttribute('src', data[1]['img']);
            document.getElementById('speicalRightTop').setAttribute('data-id', data[1]['cid']);
            document.getElementById('speicalRightTop').setAttribute('data-name', data[1]['name']);
            document.getElementById('speicalRightBottom').setAttribute('src', data[2]['img']);
            document.getElementById('speicalRightBottom').setAttribute('data-id', data[2]['cid']);
            document.getElementById('speicalRightBottom').setAttribute('data-name', data[2]['name']);
        },
        error: function(type) {
//          alert(1);
        }
    });
}

// Ajax 加载首页推广
function ajaxPromotion() {
    mui.ajax('http://app.tmh360.cn/api/index/getPromotionList', {
        type: 'post',
        data: {
            "period": "40"
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var promotion = document.getElementById('promotion');
            
            for(var i = 1; i <= data.length; i++) {
                promotion.appendChild(promotion.firstChild.cloneNode(true));
                var item = promotion.lastChild;
                
                item.setAttribute('data-gid', data[i - 1]['pid']);
                item.firstChild.childNodes[0].innerText = data[i - 1]['price'] + '元';
                item.firstChild.childNodes[1].innerText = data[i - 1]['title'].replace('|', '');
                item.firstChild.childNodes[2].setAttribute('id', 'countDown' + i);
                item.lastChild.firstChild.setAttribute('src', data[i - 1]['poster']);
                var nowTime = Math.floor(new Date().getTime() / 1000);
                addCountDown('countDown' + i, data[i - 1]['end_time'] - nowTime, 'promotion');
            }
            
            promotion.removeChild(promotion.firstChild);
        },
        error: function(type) {
//          alert(1);
        }
    });
}

// Ajax 加载左菜单
function ajaxLeftMenu() {
    mui.ajax('http://app.tmh360.cn/api/index/getConfigData', {
        type: 'post',
        data: {
            "item": "leftMenu"
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var leftMenu = document.getElementById('leftMenu');
            
            for(var i = 0; i < data.length; i++) {
                var ele = document.createElement('li');
                ele.innerText = data[i]['name'];
                leftMenu.appendChild(ele);
                leftMenu.lastChild.setAttribute('style', 'background-image: url(' + data[i]['flag'] + ');');
                leftMenu.lastChild.setAttribute('id', data[i]['oid']);
            }
        },
        error: function(type) {
//          alert(1);
        }
    });
}

// Ajax 加载右菜单分类
function ajaxCategory() {
    mui.ajax('http://app.tmh360.cn/api/index/getConfigData', {
        type: 'post',
        data: {
            "item": "rightMenu"
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var category = document.getElementById('categoryList');
            var j = 1;
            var cid;
            
            for(var i = 0; i < data.length; i++) {
                if(data[i]['cid'] === undefined) {
                    cid = 'channel' + j;
                    j++;
                } else {
                    cid = data[i]['cid'];
                }
                var ele = document.createElement('li');
                ele.innerHTML = '<a><span class="qiyifont ' + data[i]['icon'] + '"></span><div class="mui-media-boday" id="'+ cid +'">' + data[i]['name'] + '</div></a>';
                category.appendChild(ele);
                category.lastChild.setAttribute('class', 'mui-table-view-cell mui-media mui-col-xs-4');
            }
        },
        error: function(type) {
//          alert(1);
        }
    });
}

// Ajax 加载右菜单品牌
function ajaxBrand() {
    mui.ajax('http://app.tmh360.cn/api/index/getBrandList', {
        type: 'post',
        data: "",
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var brandList = document.getElementById('brandList');
            
            each(data, function(key) {
                var ele = document.createElement('li');
                ele.innerText = key;
                brandList.appendChild(ele);
                brandList.lastChild.setAttribute('class', 'mui-table-view-divider mui-indexed-list-group group');
                brandList.lastChild.setAttribute('data-group', key);
                
                for(var i = 0; i < this.length; i++) {
                    var ele = document.createElement('li');
                    ele.innerText = this[i]['name_cn'] + ' ' + this[i]['name_en'];
                    brandList.appendChild(ele);
                    brandList.lastChild.setAttribute('class', 'mui-table-view-cell mui-indexed-list-item item');
                    brandList.lastChild.setAttribute('id', this[i]['bid']);
                }
            });
            // 计算列表高度
            document.getElementById('list').style.height = document.body.offsetHeight + 'px';
            // 创建列表对象
            window.indexedList = new mui.IndexedList(list);
            highlight();
        },
        error: function(type) {
//          alert(1);
        }
    });
}

/**
 * Android 返回键方法重写 
 */
function back() {
    if(menuStatus) {
        //菜单处于显示状态，返回键应该先关闭菜单,阻止主窗口执行mui.back逻辑
        hideLeftMenu();
        return false;
    } else {
        //菜单处于隐藏状态，执行返回时，要先close菜单页面，然后继续执行mui.back逻辑关闭主窗口
        leftMenu.close('none');
        return true;
    }
}

/**
 * 用户点击蒙版时的回调 
 */
function closeMask() {
    //主窗体开始侧滑
    if(menuStatus) {
        page.setStyle({
            left: 0,
            transition: {
                duration: 150
            }
        });
    }
    
    //等窗体动画结束后，隐藏菜单webview，节省资源
    setTimeout(function() {
        leftMenu.hide();
    }, 200);
    //改变标志位
    menuStatus = false;
}

/**
 * 显示左菜单
 */
function showLeftMenu() {
    //侧滑菜单处于隐藏状态，则立即显示出来
    if(!menuStatus) {
        //显示完毕后，移动窗体
        leftMenu.show('none', 0, function() {
            //主窗体开始侧滑
            page.setStyle({
                left: '45%',
                transition: {
                    duration: 150
                }
            });
        });
    }
    
    //显示遮罩
    mask.show();
    menuStatus = true;
}

/**
 * 隐藏左菜单 
 */
function hideLeftMenu() {
    closeMask();
    //关闭遮罩
    mask.close();
}

/**
 * 左菜单触发首页监听器 
 */
function hideSelf() {
    mui.fire(index, 'leftMenu:swipeleft');
}

/**
 * 右菜单选项卡切换 
 */
function switchTab() {
    switch(this.id) {
        case 'category':
            void plus.webview.hide('brandList');
            break;
        case 'brand':
            void plus.webview.show('brandList');
            mui.init({
                swipeBack: true,
                gestureConfig: {
                    doubletap: true
                },
                subpages: [{
                    id: 'brandList',//子页面标志
                    url: 'brandList.html',//子页面HTML地址，支持本地地址和网络地址
                    styles: {
                        top: 104,//子页面顶部位置
                        bottom: 0,//子页面底部位置
                        bounce: 'all'
                    }
                    //extras: {}//额外扩展参数
                }]
            });
            break;
    }
}

/**
 * 高亮存在的组类 
 */
function highlight() {
    var letter = document.getElementsByTagName('li');
    
    for(var i = 0; i < letter.length; i++) {
        if(letter[i].getAttribute('data-group') != null) {
            var groupID = 'group' + letter[i].getAttribute('data-group');
            removeClass(document.getElementById(groupID), 'highlight');
        }
    };
}
