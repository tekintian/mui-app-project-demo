/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Tom
 * @version     qiyi_trade.js 15/8/18 16:07
 * @link        http://www.fs71.com.cn
 */

function ajaxCart() {
    mui.ajax('http://app.tmh360.cn/api/trade/cart/action/read', {
        type: 'post',
        data: '',
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data == null) {
                addClass(document.getElementById('cart'), 'mui-hidden');
                removeClass(document.getElementById('null'), 'mui-hidden');
            } else {
                if(data['2'] != null) {
                    addClass(document.getElementById('null'), 'mui-hidden');
                    removeClass(document.getElementById('cart'), 'mui-hidden');
                    
                    var category = document.getElementById('bonded');
                    for(var i = 0; i < data['2'].length; i++) {
                        removeClass(category, 'mui-hidden');                    
                        var ele = document.createElement('section');
                        category.appendChild(ele);
                        category.lastChild.setAttribute('class', 'goodscard mui-clearfix');
                        category.lastChild.setAttribute('id', 'goods-' + data['2'][i].pid);
                        category.lastChild.setAttribute('data-pid', data['2'][i].pid);
                        category.lastChild.innerHTML = '<input class="cardcheck" type="checkbox" onclick="clickCheckBox(this, \'one\');"><img><h4></h4><span></span><h5></h5><div class="mui-numbox" data-numbox-min="1"><button class="mui-btn mui-numbox-btn-minus" type="button">-</button><input class="mui-numbox-input" type="number" readonly onchange="setPrice(this);"><button class="mui-btn mui-numbox-btn-plus" type="button">+</button></div><span class="mui-icon mui-icon-clear delete" onclick="removeGoods(this);"></span>';
                        
                        var obj = category.lastChild;
                        obj.querySelector('img').setAttribute('src', data['2'][i].thumb);
                        obj.querySelector('h4').innerText = data['2'][i].title.split('|')[0];
                        obj.querySelector('h5').innerText = data['2'][i].title.split('|')[1];
                        obj.querySelector('span').innerText = data['2'][i].price;
                        obj.querySelector('input[type=number]').value = data['2'][i].quantity;
                    }
                }
                    
                if(data['3'] != null) {
                    addClass(document.getElementById('null'), 'mui-hidden');
                    removeClass(document.getElementById('cart'), 'mui-hidden');
                    
                    var category = document.getElementById('oversea');
                    for(var i = 0; i < data['3'].length; i++) {
                        removeClass(category, 'mui-hidden');                    
                        var ele = document.createElement('section');
                        category.appendChild(ele);
                        category.lastChild.setAttribute('class', 'goodscard mui-clearfix');
                        category.lastChild.setAttribute('id', 'goods-' + data['3'][i].pid);
                        category.lastChild.setAttribute('data-pid', data['3'][i].pid);
                        category.lastChild.innerHTML = '<input class="cardcheck" type="checkbox" onclick="clickCheckBox(this, \'one\');"><img><h4></h4><span></span><h5></h5><div class="mui-numbox" data-numbox-min="1"><button class="mui-btn mui-numbox-btn-minus" type="button">-</button><input class="mui-numbox-input" type="number" readonly onchange="setPrice(this);"><button class="mui-btn mui-numbox-btn-plus" type="button">+</button></div><span class="mui-icon mui-icon-clear delete" onclick="removeGoods(this);"></span>';
                        
                        var obj = category.lastChild;
                        obj.querySelector('img').setAttribute('src', data['3'][i].thumb);
                        obj.querySelector('h4').innerText = data['3'][i].title.split('|')[0];
                        obj.querySelector('h5').innerText = data['3'][i].title.split('|')[1];
                        obj.querySelector('span').innerText = data['3'][i].price;
                        obj.querySelector('input[type=number]').value = data['3'][i].quantity;
                    }
                }
            }
            
            mui('.mui-numbox').numbox();
        },
        error: function(type) {
//              alert(1);
        }
    });
}

function clickCheckBox(objInput, type) {
    var category = objInput.parentNode.parentNode;
    var obj = document.querySelectorAll('article:not(#' + category.id + ') input[type=checkbox]');
    
    for(var i = 0; i < obj.length; i++) {
        obj[i].checked = false;
    }
    
    if(objInput.checked == false && type == 'one') {
        document.querySelector('#' + category.id + ' h4 input').checked = false;
    }
    
    if(type == 'all') {
        var obj = document.querySelectorAll('#' + category.id + ' input[type=checkbox]');
        for(var i = 0; i < obj.length; i++) {
            obj[i].checked = objInput.checked;
        }
    }
    
    setPrice();
}

function setPrice(objInput) {
    var obj = document.querySelectorAll('article :not(h4) input:checked');
    var price = 0;
    for(var i = 0; i < obj.length; i++) {
        price += obj[i].parentNode.querySelector('span').innerText.replace(',', '') * obj[i].parentNode.querySelector('input[type=number]').value;
    }
    if(price == 0) {
        document.getElementById('checkout').setAttribute('disabled', true);
    } else if(price > 500) {
        document.getElementById('checkout').setAttribute('disabled', true);
        mui.toast('单笔订单不允许超过500元');
    } else {
        document.getElementById('checkout').removeAttribute('disabled');
    }
    document.getElementById('total').innerText = formatNumber(price, 2);
    
    if(objInput != null) {
        mui.ajax('http://app.tmh360.cn/api/trade/cart/action/update', {
            type: 'post',
            data: {
                "pid": objInput.parentNode.parentNode.getAttribute('data-pid'),
                "qty": objInput.parentNode.querySelector('input[type=number]').value
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                mui.fire(plus.webview.getWebviewById('goodsDetail'), 'refreshCart');
            },
            error: function(type) {
    //          alert(1);
            }
        });
    } else {
        mui.fire(plus.webview.getWebviewById('goodsDetail'), 'refreshCart');
    }
}

function removeGoods(obj) {
    mui.ajax('http://app.tmh360.cn/api/trade/cart/action/remove', {
        type: 'post',
        data: {
            "pid": obj.parentNode.getAttribute('data-pid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            
        },
        error: function(type) {
//          alert(1);
        }
    });
    
    obj.parentNode.parentNode.removeChild(obj.parentNode);
    setPrice();
}

function ajaxCreateOrder() {
    var obj = document.querySelectorAll('article :not(h4) input:checked');
    var goodsList = new Array();
    
    for(var i = 0; i < obj.length; i++) {
        goodsList.push(obj[i].parentNode.getAttribute('data-pid') + '|' + obj[i].parentNode.querySelector('input[type=number]').value);
    }
    
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/create', {
        type: 'post',
        data: {
            "uid": plus.storage.getItem('uid'),
            "userType": plus.storage.getItem('userType'),
            "type": obj[0].parentNode.parentNode.id,
            "goods": goodsList,
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data.code != 0) {
                mui.toast(data.message);
            } else {
                obj[0].parentNode.parentNode.querySelector('h4 input').checked = false;
                for(var i = 0; i < obj.length; i++) {
                    removeGoods(obj[i]);
                }
                mui.openWindow({
                    id: 'checkout',
                    url: 'checkout.html',
                    styles: {
                        scrollIndicator: 'none'
                    },
                    show: {
                        duration: 200
                    },
                    extras: {
                        orderID: data.orderID
                    }
                });
            }
        },
        error: function(type) {
//          alert(1);
        }
    });
}

function ajaxCheckout(orderID) {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/checkout', {
        type: 'post',
        data: {
            "orderID": orderID
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var shipInfo = document.getElementById('shipInfo');
            if(data.ship_info != '') {
                var name = data.ship_info.split(',')[0];
                var phone = data.ship_info.split(',')[1].substr(0, 3) + '****' + data.ship_info.split(',')[1].substr(7, 4);
                var address = data.ship_info.split(',')[2] + data.ship_info.split(',')[3];
                
                shipInfo.innerHTML = '<span>' + name + '</span><span>' + phone + '</span><p>' + address + '</p><span class="mui-icon mui-icon-forward"></span>';
            } else {
                shipInfo.innerHTML = '<span>请设置收货地址以确保商品顺利送达</span><span class="mui-icon mui-icon-forward"></span>';
            }
            
            switch(data.order_type) {
                case '1':
                    document.getElementById('type').innerText = '保税海淘';
                    break;
                case '2':
                    document.getElementById('type').innerText = '海外直邮';
                    break;
            }
            
            document.getElementById('amount').innerText = data.amount_format;
            document.getElementById('total').innerText = data.amount_format;
            document.getElementById('freight').innerText = data.freight;
            document.getElementById('btnSubmit').setAttribute('data-amount', data.amount);
        },
        error: function(type) {
//              alert(1);
        }
    });
}

function ajaxChargeOrder(orderID, pay, amount) {
    var url = 'http://app.tmh360.cn/api/trade/charge/action/create/orderID/'+ orderID + '/channel/' + pay + '/amount/' + amount; 
    waiting = plus.nativeUI.showWaiting();

    // 请求支付订单
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.send();
    
    xhr.onreadystatechange = function() {
        switch(xhr.readyState) {
            case 4:
                waiting.close();
                waiting = null;
                if(xhr.status == 200) {
//                  var obj = JSON.parse(xhr.responseText);
                    plus.payment.request(channel, xhr.responseText, function(result) {
                        var charge = JSON.parse(result.description);
//                      console.log(result);
//                      console.log(obj);
//                      console.log(charge);
//                      console.log(charge.result);
                        switch(charge.result) {
                            case 'success':
                                plus.nativeUI.alert("支付成功!", chargeSuccess(), "跨境特卖会");
                                break;
                            case 'cancel':
                                console.log(charge.error_code);
                                console.log(charge.error_msg);
                                plus.nativeUI.alert('支付取消，您可稍后再次尝试!', chargeFailed(), '跨境特卖会');
                                break;
                            case 'fail':
                                console.log(charge.error_code);
                                console.log(charge.error_msg);
                                plus.nativeUI.alert('支付失败，请稍后再次尝试!', chargeFailed(), '跨境特卖会');
                                break;
                        }
                    }, function(e) {
                        console.log(e.code);
//                      console.log(obj);
                        plus.nativeUI.alert('支付失败，请稍后再次尝试!', chargeFailed(), '跨境特卖会');
                    });
                } else {
                    plus.nativeUI.alert('获取订单信息失败，请再试一次！', null, '跨境特卖会');
                }
                break;
            default:
                break;
        }
    }
}

function retrieveCharge(chargeID) {
    mui.ajax('http://app.tmh360.cn/api/trade/charge/action/retrieve', {
        type: 'post',
        data: {
            "chargeID": chargeID
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            console.log(data.order_no);
            console.log(data.paid);
            if(data.paid != true) {
                updateOrderStatus(data.order_no, 3);
            } else {
                chargeSuccess();
            }
        }
    });
}

function updateOrderStatus(orderID, status) {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/updateStatus', {
        type: 'post',
        data: {
            "orderID": orderID,
            "status": status
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
//          plus.nativeUI.alert("支付成功!", chargeSuccess(), "跨境特卖会");
            mui.toast('支付成功，感谢您的选购！');
            chargeSuccess();
        }
    });
}

function chargeSuccess() {
    mui.openWindow({
        id: 'orderList',
        url: 'orderList.html',
        styles: {
            scrollIndicator: 'none'
        },
        show: {
            duration: 200
        },
        extras: {
            page: 'icon-truck'
        },
        waiting: {
            autoShow: false
        }
    });
    
    setTimeout('closeAllWebView("orderList");', 1000);
}

function chargeFailed() {
    mui.openWindow({
        id: 'orderList',
        url: 'orderList.html',
        styles: {
            scrollIndicator: 'none'
        },
        show: {
            duration: 200
        },
        extras: {
            page: 'icon-bankcard'
        },
        waiting: {
            autoShow: false
        }
    });
    
    setTimeout('closeAllWebView("orderList");', 1000);
}

function ajaxOrderList() {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/list', {
        type: 'post',
        data: {
            "uid": plus.storage.getItem('uid'),
            "type": document.getElementById('order').getAttribute('data-type')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data.length == 0) {
                removeClass(document.getElementById('null'), 'mui-hidden');
                addClass(document.getElementById('order'), 'mui-hidden');
            } else {
                addClass(document.getElementById('null'), 'mui-hidden');
                removeClass(document.getElementById('order'), 'mui-hidden');
                
                var obj = document.getElementById('order');
                mui.each(data, function(key, value) {
                    var ele = document.createElement('article');
                    switch (value.status) {
                        case '待支付':
                            var btn = '<a class="mui-btn mui-btn-danger mui-btn-outlined" data-order="' + value.order_id + '" data-amount="' + value.amount +'" onclick="reCharge(this);">支付</a>'
                            break;
                        case '待退货':
                            var btn = '<button class="mui-btn mui-btn-danger mui-btn-outlined">退货</button>'
                            break;
                        default:
                            var btn = '<button class="mui-btn mui-btn-danger mui-btn-outlined">查看</button>'
                            break;
                    }
                    ele.innerHTML = '<h5>下单时间：<span>' + value.create_time + '</span></h5><section class="order-body-all"><div>订单编号：' + value.order_id + '</div><div>订单状态：' + value.status + '</div><span class="mui-icon mui-icon-arrowright"></span><div>订单金额：' + value.amount_format + '</div></section>' + btn;
                    obj.appendChild(ele);
                    obj.lastChild.className = 'order-block-all mui-clearfix';
                    obj.lastChild.setAttribute('data-order', value.order_id);
                });
            }
        },
        error: function(type) {
//              alert(1);
        }
    });
}

function ajaxOrderDetail(orderID) {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/show', {
        type: 'post',
        data: {
            "orderID": orderID
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            var objRecharge = document.getElementById('btnReCharge');
            var objCancelOrder = document.getElementById('btnCancelOrder');
            var objReship = document.getElementById('btnReShip');
            var objCancelReship = document.getElementById('btnCancelReship');
            var objBack = document.getElementById('btnBack');
            
            objRecharge.setAttribute('data-order', data.order_id);
            objRecharge.setAttribute('data-amount', data.amount);
            objCancelOrder.setAttribute('data-order', data.order_id);
            objReship.setAttribute('data-order', data.order_id);
            objCancelReship.setAttribute('data-order', data.order_id);
            
            switch(data.status) {
                case '1':
                    objRecharge.parentNode.removeChild(objRecharge);
                    objCancelOrder.parentNode.removeChild(objCancelOrder);
                    objCancelReship.parentNode.removeChild(objCancelReship);
                    break;
                case '2':
                    objReship.parentNode.removeChild(objReship);
                    objCancelReship.parentNode.removeChild(objCancelReship);
                    objBack.parentNode.removeChild(objBack);
                    break;
                case '3':
                case '4':
                    objRecharge.parentNode.removeChild(objRecharge);
                    objReship.parentNode.removeChild(objReship);
                    objCancelReship.parentNode.removeChild(objCancelReship);
                    objBack.parentNode.removeChild(objBack);
                    break;
                case '6':
                    objRecharge.parentNode.removeChild(objRecharge);
                    objCancelOrder.parentNode.removeChild(objCancelOrder);
                    objReship.parentNode.removeChild(objReship);
                    objBack.parentNode.removeChild(objBack);
                    break;
                case '7':
                    objRecharge.parentNode.removeChild(objRecharge);
                    objCancelOrder.parentNode.removeChild(objCancelOrder);
                    objBack.parentNode.removeChild(objBack);
                    break;
                default:
                    objRecharge.parentNode.removeChild(objRecharge);
                    objCancelOrder.parentNode.removeChild(objCancelOrder);
                    objReship.parentNode.removeChild(objReship);
                    objCancelReship.parentNode.removeChild(objCancelReship);
                    break;
            }
            
            document.getElementById('order_id').innerText = data.order_id;
            document.getElementById('create_time').innerText = data.create_time;
            document.getElementById('charge_channel').innerText = data.channel == null ? '未支付' : data.channel;
            document.getElementById('status').innerText = data.status_text;
            if(data.ship_id == '') {
                document.getElementById('ship').innerText = '未发货';
            } else {
                document.getElementById('ship').innerText = data.company + ' ' + data.ship_id;
            }
            
            document.getElementById('shipName').innerText = data.shipName;
            document.getElementById('shipPhone').innerText = data.shipPhone;
            document.getElementById('shipAddress').innerText = data.shipAddress;
            document.getElementById('orderTotal').innerText = data.amount_format;
            document.getElementById('orderAmount').innerText = data.amount_format;
            
            var obj = document.getElementById('order_content');
            mui.each(data.content, function(key, value) {
                if(!isNaN(parseInt(key))) {
                    var ele = document.createElement('section');
                    ele.innerHTML = '<div class="pictureod1"><img src="'+ value.thumb +'"></div><div class="nameod1">' + value.brand + '</div><div class="name1od1">' + value.price + '</div><div class="name2od1">' + value.name + '</div><div class="name2od1">X ' + value.quantity + '</div>';
                    obj.appendChild(ele);
                    obj.lastChild.className = 'mui-clearfix';
                }
            });
        }
    });
}

function reCharge(obj) {
    var actionSheet = document.getElementById('actionSheet');
    actionSheet.setAttribute('data-order', obj.getAttribute('data-order'));
    actionSheet.setAttribute('data-amount', obj.getAttribute('data-amount'));
    
    mui('#actionSheet').popover('toggle');
}

function ajaxCancelOrder(orderID) {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/updateStatus', {
        type: 'post',
        data: {
            "orderID": orderID,
            "status": 0
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data.code != 0) {
                mui.toast(data.message);
            } else {
                mui.back();
                plus.webview.getWebviewById('orderList').reload();
            }
        }
    });
}

function ajaxReship(orderID) {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/reship', {
        type: 'post',
        data: {
            "orderID": orderID
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data.code != 0) {
                mui.toast(data.message);
            } else {
                switch(data.status) {
                    case '1':
                        mui.openWindow({
                            id: 'reshipRequest',
                            url: 'reshipRequest.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            },
                            extras: {
                                orderID: orderID
                            }
                        });
                        break;
                    case '7':
                        mui.openWindow({
                            id: 'reshipDelivery',
                            url: 'reshipDelivery.html',
                            styles: {
                                scrollIndicator: 'none'
                            },
                            show: {
                                duration: 200
                            },
                            extras: {
                                orderID: orderID
                            }
                        });
                        break;
                }
            }
        }
    });
}

function ajaxReshipRequest(orderID) {
    var reason = document.getElementById('reason').getAttribute('data-value');
    var package = document.getElementById('package').getAttribute('data-value');
    var appearance = document.getElementById('appearance').getAttribute('data-value');
    var invoice = document.getElementById('invoice').getAttribute('data-value');

    if(orderID == null) {
        mui.toast('无法获取订单编号，请返回重试');
    } else if(reason == null) {
        mui.toast('请选择退货原因');
    } else if(package == null) {
        mui.toast('请选择包装外观');
    } else if(appearance == null) {
        mui.toast('请选择商品外观');
    } else if(invoice == null) {
        mui.toast('请选择单据情况');
    } else {
        mui.ajax('http://app.tmh360.cn/api/trade/order/action/reshipRequest', {
            type: 'post',
            data: {
                "orderID": orderID,
                "reason": reason,
                "package": package,
                "appearance": appearance,
                "invoice": invoice,
                "message": document.getElementById('message').value
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    mui.back();
                    plus.webview.getWebviewById('orderDetail').reload();
                    plus.nativeUI.toast('提交退货申请成功，请耐心等待审核', {
                        duration: 'long'
                    });
                }
            }
        });
    }
}

function ajaxReshipDelivery(orderID) {
    var express = document.getElementById('express').getAttribute('data-value');
    var shipID = document.getElementById('shipID').value;
    var freight = document.getElementById('freight').value;

    if(orderID == null) {
        mui.toast('无法获取订单编号，请返回重试');
    } else if(express == null) {
        mui.toast('请选择快递公司');
    } else if(shipID == '') {
        mui.toast('请填写运单号码');
    } else if(freight == '') {
        mui.toast('请填写运费');
    } else {
        mui.ajax('http://app.tmh360.cn/api/trade/order/action/reshipDelivery', {
            type: 'post',
            data: {
                "orderID": orderID,
                "express": express,
                "shipID": shipID,
                "freight": freight,
                "message": document.getElementById('message').value
            },
            timeout: 10000,
            dataType: 'json',
            success: function(data) {
                if(data.code != 0) {
                    mui.toast(data.message);
                } else {
                    webviewID = plus.webview.currentWebview().opener().id;
                    
                    mui.back();
                    plus.webview.getWebviewById(webviewID).reload(true);
                    plus.nativeUI.toast('提交寄回详情成功，我们将在收到退货后为您处理退款', {
                        duration: 'long'
                    });
                }
            }
        });
    }
}

function ajaxCancelReship(orderID) {
    mui.ajax('http://app.tmh360.cn/api/trade/order/action/updateStatus', {
        type: 'post',
        data: {
            "orderID": orderID,
            "status": 1
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data.code != 0) {
                mui.toast(data.message);
            } else {
                plus.webview.getWebviewById('orderDetail').reload();
            }
        }
    });
}
