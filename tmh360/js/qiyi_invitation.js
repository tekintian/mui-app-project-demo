/**
 * @copyright   Copyright © 2013-2015 佛山齐易网络科技有限公司（Foshan Qiyi Network Technology Co., Ltd.）
 * @license     佛山齐易网络科技有限公司软件授权协议（Qiyi Software License Agreement）
 * @project     tmh360
 * @author      Bruce
 * @version     qiyi_invitation.js 15/10/01 14:31
 * @link        http://www.fs71.com.cn
 */

/**
 * 更新分享服务
 */
function updateSerivces(){
    plus.share.getServices(function(service){
        shares = {};
        for(var i in service){
            var item = service[i];
            shares[item.id] = item;
        }
    }, function(e){
        console.log('获取分享服务列表失败：' + e.message);
    });
}

/**
* 分享操作
* 
* @param {String} id
*/
function shareAction(id, ex) {
    var service = null;
    
    if(!id || !(service = shares[id])) {
        console.log('无效的分享服务！');
        return;
    }
    
    if(service.authenticated) {
        console.log('---已授权---');
        shareMessage(service, ex);
    } else {
        console.log('---未授权---');
        service.authorize(function() {
            shareMessage(service, ex);
        }, function(e) {
            console.log('认证授权失败：' + e.code + ' - ' + e.message);
        });
    }
}

/**
* 发送分享消息
* 
* @param {plus.share.ShareService} s
*/
function shareMessage(service, ex) {
    var msg = {
        "content": "亲们，快和我一起来跨境特卖会做管理员赚钱吧！",
        "thumbs": ["_www/img/logo.jpg"],
        "pictures": ["_www/img/logo.jpg"],
        "href": "http://app.tmh360.com/release/latest.apk",
        "title": "跨境特卖会管理员邀请",
        "extra": {
            "scene": ex
        }
    };
    
    mui.ajax('http://app.tmh360.cn/api/user/getInvitation', {
        type: 'post',
        data: {
            "uid": plus.storage.getItem('uid')
        },
        timeout: 10000,
        dataType: 'json',
        success: function(data) {
            if(data == 0) {
                mui.toast('您尚未获得邀请码，请支付保证金或联系客服！')
            } else {
                msg.content += '邀请码：' + data + '。http://app.tmh360.com/release/latest.apk';
            
                if(ex == 'WXSceneTimeline') {
                    msg.title = msg.content;
                }
                
                service.send(msg, function() {
                    mui.toast('分享到 ' + service.description + ' 成功！');
                }, function(e) {
                    if(e.message.split(',')[0].split(']')[1] == 'User canceled') {
                        
                    } else {
                        console.log('分享到\"' + service.description + '\"失败: ' + e.code + ' - ' + e.message);
                    }
                });
            }
        }
    });
}
