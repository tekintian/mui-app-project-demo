
/**
 * APP升级通用模块
 * yangshengfei
 * V2.0
 * tips:V1.1,新增启用/停止更新功能;V1.2,新增对更新配置前处理回调;V2.0,全新升级更新策略;
 */  

(function(M,U){
	
	/**
	 * 获取更新内容信息
	 * @param {Object} noteArray
	 * @param {Object} curVer
	 */
	function getNoteInfo(noteArray,curVer){
		var retStr='';
		
		for (var i = 0; i < noteArray.length; i++) {
			var note=noteArray[i];
			
			if(M.CompareVersion(curVer,note.version)){
				retStr+=note.updateDetial;
				//console.log('retStr:'+retStr);
			}
			
		}
		
		return retStr;
	}
	
	/**
	 * 检查升级信息
	 * @param {Object} 执行回调处理插件更新，回调参数插件列表
	 * @params:{String} app版本，如市级、县级等
	 * @param {Object} 执行准备更新前回调，回调参数当前版本信息
	 */
	M.CheckUpdate=function(){
		var e=this;
		e.pluginCall=arguments[0]?arguments[0]:function(plugins){};
		e.appVersion=arguments[1]?arguments[1]:"all";
		e.befUpdateCall=arguments[2]?arguments[2]:function(){};
		
		//mui.alert("appid:"+plus.runtime.appid);
		plus.runtime.getProperty(plus.runtime.appid,function(inf){
			e.version=inf.version;
			e.sysType=plus.os.name;
			e.appId=plus.runtime.appid;
			
			M.web_query("/CheckAppUpdate!APP",{"appId":e.appId,"sysType":e.sysType,"appVersion":e.appVersion},
				function(data){
					//console.log("data:"+JSON.stringify(data));
					if(!data) return e.befUpdateCall("检测更新异常");
					
					//执行回调处理插件更新
					e.pluginCall(data["plugins"]);
					
					// 判断升级检测是否过期
					e.checkInterval=data["checkInterval"];
					var lastcheck = plus.storage.getItem("updateCheck");
					//console.log("lastcheck:"+lastcheck+",checkInterval:"+e.checkInterval);
					if (lastcheck) {
						var dc = parseInt(lastcheck);
						var dn = (new Date()).getTime();
						var bw=parseInt(dn-dc);
						//console.log("当前时间间隔："+bw+",设定的时间间隔："+e.checkInterval);
						if (bw < e.checkInterval) {	// 未超过上次升级检测间隔，不需要进行升级检查
							return e.befUpdateCall("未超过上次升级检测间隔，不需要进行升级检查");
						}
						// 取消已过期，删除取消标记
						plus.storage.removeItem("updateCheck");
					}
					
					if(!data["updateDisbled"] || data["updateDisbled"]!=1) return e.befUpdateCall("当前版本不可升级，请联系管理员");//是否可升级
					
					//是否存在最新版本
					e.NewVersion=data["version"];
					if(M.CompareVersion(e.version,e.NewVersion)){
						//判断是否存在忽略版本号
						var vabort = plus.storage.getItem("updateAbort");
						if (vabort && e.NewVersion==vabort ) return e.befUpdateCall("最新版本已被您忽略更新，请期待更新的版本吧");
						
						e.noticeTitle=data["noticeTitle"];
						e.userPackOrWgt=data["userPackOrWgt"];
						e.updateURL=e.userPackOrWgt==1?data["updateURL"]:data["wgtURL"];
						
						//获取升级详情
						M.web_query("/GetAppUdateDetial!APP",{"appId":e.appId,"sysType":e.sysType,"appVersion":e.appVersion},
						function(d){
							e.befUpdateCall();//准备升级前回调
							
							if(d) e.notestr=getNoteInfo(d,e.version);							
							e.notestr=e.notestr?e.notestr:"升级到最新版本吧";
							
							plus.nativeUI.confirm(e.notestr, function(i){
								if (0==i.index ) {//安装升级
									if(e.userPackOrWgt==1){//安装包升级
										if(mui.os.android){//android
											M.DownUpdatePack(e.updateURL,"installPack");
										}else if(mui.os.ios){//ios
											plus.runtime.openURL(e.updateURL);
										}
									}else{//差量包升级
										M.DownUpdatePack(e.updateURL,"installWgt");
									}
								} else if (1==i.index ) {
									plus.storage.setItem("updateAbort",e.NewVersion.toString());
									plus.storage.setItem("updateCheck",(new Date()).getTime().toString());
								} else {
									plus.storage.setItem("updateCheck",(new Date()).getTime().toString());
									//console.log("updateCheck存储后:"+plus.storage.getItem("updateCheck"));
								}
							},e.noticeTitle,["立即更新","跳过此版本","取　　消"]);
						
						},
						function(c,m){
							e.befUpdateCall("更新检测失败，请检查网络");
						});
					}else{
						e.befUpdateCall("当前版本已是最新版本了");
					}
					
				},function(code,msg){
					e.befUpdateCall("更新检测失败，请检查网络");
				}
			);
		});
		
	};
	
	/**
	 * 比较版本大小，如果新版本nv大于旧版本ov则返回true，否则返回false
	 * @param {String} ov
	 * @param {String} nv
	 * @return {Boolean} 
	*/
	M.CompareVersion=function(ov,nv){
		if ( !ov || !nv || ov=="" || nv=="" ){
			return false;
		}
		
		var b=false,
		ova = ov.split(".",4),
		nva = nv.split(".",4);
		
		for ( var i=0; i<ova.length&&i<nva.length; i++ ) {
			var so=ova[i],no=parseInt(so),sn=nva[i],nn=parseInt(sn);
			if ( nn>no || sn.length>so.length  ) {
				return true;
			} else if ( nn<no ) {
				return false;
			}
		}
		if ( nva.length>ova.length && 0==nv.indexOf(ov) ) {
			return true;
		}
	};
	
	/**
	 * 根据ID获取相应的插件更新信息
	 * @param {Object} plugins
	 * @param {Object} id
	 */
	M.GetPluginByID=function(plugins,id){
		//if(plugins typeof !="object") return null;
		for (var i = 0; i < plugins.length; i++) {
			var v=plugins[i];
			if(id==v["id"]){
				return v;
			}
		}
		
		return null;

	};
	
	/**
	 * 下载并安装升级
	 * @param {Object} url
	 * @param {Object} type
	 */
	M.DownUpdatePack=function(url,type){
		var e=this;
        plus.io.requestFileSystem(plus.io.PUBLIC_DOWNLOADS,
          	function(f){
                e.fileSystemAPI=f;
                e.UpdateDtask=plus.downloader.createDownload(url,
                {
                    filename: "_doc/update"+new Date().getTime()+"/",
                    timeout: 600,
                    priority: 9
                },function(h,g){
                	e.showWaiting.close();
                    if(g==200){
                        h.abort();
                        if(type=="installPack"){                       	
                        	plus.runtime.openFile(h.filename);
                        }else if(type=="installWgt"){ 
                        	plus.runtime.install(h.filename,{force:true},
                        		function(){
                        			//console.log("wgt安装更新成功");
                        			if(mui.os.ios){
                        				plus.runtime.restart();
                        			}else{//安卓系统重启接口存在bug
                        				mui.alert("更新资源已下载完成，手动重启应用完成更新吧");
                        			}
                        		},
                        		function(e){          
                        			M.alert("抱歉！安装更新包失败了");
                        		}
                        	);
                        }
                    }else{                   	
                        M.alert("抱歉！下载更新失败了");
                    }
                });
                               
                e.UpdateDtask.addEventListener("statechanged",
                function(h,g){
                    if(!e.UpdateDtask){
                        return;
                    }switch(h.state){
                        case 1: 
	                        e.showWaiting.setTitle("开始下载");
	                        break;
                        case 2: 
	                        e.showWaiting.setTitle("已连接到服务器");
	                        e.showWaiting.setTitle("更新已下载0%...");
	                        break;
                        case 3:
                        	var dsize=h.downloadedSize;
                        	var progress=Math.round(dsize*100/h.totalSize);
                        	if(progress){
                        		if(progress%5==0)
	                        	e.showWaiting.setTitle("更新已下载"+progress+"%...");
                        	}else{
                        		e.showWaiting.setTitle("更新正在下载...");
                        	}
                        	
	                        if(dsize==h.totalSize){
	                        	e.isUpdate=false;
	                        	e.showWaiting.close();
	                        }
	                        break;
                        case 4: 
	                        e.isUpdate=false;
	                        e.showWaiting.close();
	                        break;
                    }
                });
                
                e.UpdateDtask.start();
                e.showWaiting=plus.nativeUI.showWaiting("开始下载",
                {
                    background: "rgba(0,0,0,0.8)",
                    modal: false
                });
                
                e.showWaiting.onclose=function(){
                    if(e.isUpdate){
                        e.UpdateDtask.pause();
                        e.UpdateDtask.abort();
                        e.UpdateDtask=null;
                        e.isUpdate=false;              
                    }
                }
        	}
        );
	};
	
})(mui,utils)
