
/**
 * APP升级通用模块
 * yangshengfei
 * V1.2
 * tips:V1.1,新增启用/停止更新功能;V1.2,新增对更新配置前处理回调
 */

/*
 * 正式升级描述文件服务器地址：http://htwater.net:8080/SmartWaterWeb/update.json
 * 正式增量包服务器地址：http://htwater.net:8080/SmartWaterWeb/H559D7DDA.wgt
 * 
 * 测试升级描述文件服务器地址：http://htwater.net:8080/testAPPupdate/update.json
 * 测试增量包服务器地址：http://htwater.net:8080/testAPPupdate/H55BDF6BE.wgt
 */

var update=(function(w){
//全市版本
//var server="http://htwater.net:8080/SmartWaterWeb/update.json",//获取升级描述文件服务器地址（需配置）
//wgtUrl="http://htwater.net:8080/SmartWaterWeb/H559D7DDA.wgt",//获取增量包服务器地址（需配置）
//北仑区版本
var server="http://htwater.net:8080/SmartWaterWeb/update_BL.json",//获取升级描述文件服务器地址（需配置）
wgtUrl="http://htwater.net:8080/SmartWaterWeb/H559D7DDA_BL.wgt",//获取增量包服务器地址（需配置）
appName='智慧水利',//应用名称（需配置）
localDir="update",localFile="update.json",//本地保存升级描述目录和文件名
keyUpdate="updateCheck",//取消升级键名
keyAbort="updateAbort",//忽略版本键名
checkInterval=0,//升级检查间隔，单位为ms,1天为1*24*60*60*1000=86400000, 如果每次启动需要检查设置值为0
dir=null;

/**
 * 检测程序升级
 */
function checkUpdate(plus,currentVer,callback) {
	// 判断升级检测是否过期
	var lastcheck = plus.storage.getItem( keyUpdate );
	if ( lastcheck ) {
		var dc = parseInt( lastcheck );
		var dn = (new Date()).getTime();
		if ( dn-dc < checkInterval ) {	// 未超过上次升级检测间隔，不需要进行升级检查
			return;
		}
		// 取消已过期，删除取消标记
		plus.storage.removeItem( keyUpdate );
	}
	// 读取本地升级文件
	dir.getFile( localFile, {create:false}, function(fentry){
		fentry.file( function(file){
			var reader = new plus.io.FileReader();
			reader.onloadend = function ( e ) {
				fentry.remove();
				var data = null;
				try{
					data=JSON.parse(e.target.result);
				}catch(e){
					console.log( '['+appName+']'+"读取本地升级文件，数据格式错误:"+e.message);
					return;
				}
				//在app升级前对服务端返回的更新数据处理
				callback(data);
				
				checkUpdateData(plus,data,currentVer);
				
			}
			reader.readAsText(file,'UTF-8');
		}, function(e){
			console.log( '['+appName+']'+"读取本地升级文件，获取文件对象失败："+e.message );
			fentry.remove();
		} );
	}, function(e){
		// 失败表示文件不存在，从服务器获取升级数据
		getUpdateData(plus,currentVer,callback);
	});
}

/**
 * 检查升级数据
 */
function checkUpdateData( plus,j ,currentVer){
	var curVer=currentVer;
	var inf = j[plus.os.name];
	//检测升级功能是否启用
	if(!inf.isCanUpdate) return;
	
	console.log('['+appName+']'+'当前版本：'+curVer);
	if ( inf ){
		var srvVer = inf.version;
		// 判断是否存在忽略版本号
		var vabort = plus.storage.getItem( keyAbort );
		if ( vabort && srvVer==vabort ) {
			// 忽略此版本
			return;
		}
		console.log('['+appName+']'+'最新版本：'+inf.version);
		//是否要安装包升级
		if(inf.isAPK||inf.isIPA){//安装包升级
		  updateAPK_(plus,currentVer);
		}else{
			// 判断是否需要升级
			if ( compareVersion(curVer,srvVer) ) {
				downWgt(plus,inf,srvVer,curVer);	// 下载升级包
			}else{
				console.log('['+appName+']'+'已经是最新版本了');
			}
		}
	}
}

/*
 * 下载升级包
 */
function downWgt(plus,inf,srvVer,curVer){
	// 提示用户是否升级
	//处理提示信息
	var note=getNoteInfo(inf.note,curVer)||'暂无更新信息';
	
	plus.nativeUI.confirm( note, function(i){
		if ( 0==i.index ) {
			plus.nativeUI.showWaiting("下载更新中...");
			console.log('['+appName+']'+"正在下载更新...");
			plus.downloader.createDownload( wgtUrl, {filename:"_doc/update/"}, function(d,status){
				plus.nativeUI.closeWaiting();
				if ( status == 200 ) { 
					console.log('['+appName+']'+"下载更新包成功："+d.filename);
					installWgt(plus,d.filename);	// 安装wgt包			
				} else {
					console.log('['+appName+']'+"下载更新包失败！");
				}
			}).start();
		} else if ( 1==i.index ) {
			plus.storage.setItem( keyAbort, srvVer );
			plus.storage.setItem( keyUpdate, (new Date()).getTime().toString() );
		} else {
			plus.storage.setItem( keyUpdate, (new Date()).getTime().toString() );
		}
	}, inf.title, ["立即更新","跳过此版本","取　　消"] );
}
/*
 * 更新应用资源
 */
function installWgt(plus,path){
	plus.nativeUI.showWaiting("升级中...");
	plus.runtime.install(path,{force:true},function(){//默认情况下安装wgt包时会进行版本校验的，即只能是高版本覆盖低版本（避免错误操作导致降级），如果确实需要降级，则可以通过force参数进行控制
		plus.nativeUI.closeWaiting();
		console.log('['+appName+']'+"安装更新包成功！");
		plus.nativeUI.alert("升级成功,重启更新",function(){
			plus.runtime.restart();
		});
	},function(e){
		plus.nativeUI.closeWaiting();
		console.log('['+appName+']'+"安装更新包失败["+e.code+"]："+e.message);
		plus.nativeUI.alert('['+appName+']'+"安装更新包失败["+e.code+"]："+e.message);
	});
}
/**
 * 从服务器获取升级数据
 */
function getUpdateData(plus,currentVer,callback){
	var xhr = new plus.net.XMLHttpRequest();
	xhr.onreadystatechange = function () {
        switch ( xhr.readyState ) {
            case 4:
                if ( xhr.status == 200 ) {
                	// 保存到本地文件中
                	dir.getFile( localFile, {create:true}, function(fentry){
                		fentry.createWriter( function(writer){
                			writer.onerror = function(){
                				console.log( '['+appName+']'+"获取并保存升级数据包失败" );
                			}
                			//console.log(xhr.responseText);
                			writer.write( xhr.responseText ); 
                			checkUpdate(plus,currentVer,callback);
                		}, function(e){
                			console.log( '['+appName+']'+"获取升级数据并创建写文件对象失败："+e.message );
                		} );
                	}, function(e){
                		console.log( '['+appName+']'+"获取升级数据包并打开保存文件失败："+e.message );
                	});
                } else {
                	console.log('['+appName+']'+ "获取升级数据包联网请求失败："+xhr.status );
                }
                break;
            default :
                break;
        }
	}
	xhr.open( "GET", server );
	xhr.send();
}

/**
 * 比较版本大小，如果新版本nv大于旧版本ov则返回true，否则返回false
 * @param {String} ov
 * @param {String} nv
 * @return {Boolean} 
 */
function compareVersion( ov, nv ){
	if ( !ov || !nv || ov=="" || nv=="" ){
		return false;
	}
	var b=false,
	ova = ov.split(".",4),
	nva = nv.split(".",4);
	
	for ( var i=0; i<ova.length&&i<nva.length; i++ ) {
		var so=ova[i],no=parseInt(so),sn=nva[i],nn=parseInt(sn);
		if ( nn>no || sn.length>so.length  ) {
			return true;
		} else if ( nn<no ) {
			return false;
		}
	}
	if ( nva.length>ova.length && 0==nv.indexOf(ov) ) {
		return true;
	}
}

/*
 * 处理更新提示信息
 */
function getNoteInfo(noteArray,curVer){
	var retStr='';
	
	for (var i = 0; i < noteArray.length; i++) {
		var note=noteArray[i];
		
		if(compareVersion(curVer,note.vers)){
			retStr+=note.tips;
			//console.log('retStr:'+retStr);
		}
		
	}
	
	return retStr;
}

/*
 * *************************以下为整个apk更新策略******************************
 */
/**
 * 检测程序升级APK/IPA
 */
function checkUpdateAPK(plus,currentVer) {
	// 读取本地升级文件
	dir.getFile( localFile, {create:false}, function(fentry){
		fentry.file( function(file){
			var reader = new plus.io.FileReader();
			reader.onloadend = function ( e ) {
				fentry.remove();
				var data = null;
				try{
					data=JSON.parse(e.target.result);
				}catch(e){
					console.log( '['+appName+']'+"读取安装包描述文件数据格式错误:"+e.message);
					return;
				}
				checkUpdateDataAPK(plus,data,currentVer );
			}
			reader.readAsText(file,'UTF-8');
		}, function(e){
			console.log( '['+appName+']'+"读取安装包描述文件获取文件对象失败："+e.message );
			fentry.remove();
		} );
	}, function(e){
		// 失败表示文件不存在，从服务器获取升级数据
		getUpdateDataAPK(plus,currentVer);
	});
}

/**
 * 检查升级数据APK/IPA
 */
function checkUpdateDataAPK( plus,j ,currentVer){
	var curVer=currentVer;
	var inf = j[plus.os.name];
	console.log('['+appName+']'+'当前版本：'+curVer);
	if ( inf ){
		var srvVer = inf.version;
		//处理提示信息
		var note=getNoteInfo(inf.note,curVer);
		console.log('['+appName+']'+'最新版本：'+inf.version);
		// 判断是否需要升级
		if (compareVersion(curVer,srvVer)) {
			if(mui.os.android){
				downAPK(plus,inf,srvVer,curVer);
			}else if(mui.os.ios){
				//处理提示信息
				var note=getNoteInfo(inf.note,curVer)||'暂无更新信息';
				plus.nativeUI.confirm( note, function(e){
					if(e.index==0) {
						plus.runtime.openURL(inf.url);
					}
				}, inf.title, ["立即更新","取　　消"] );
				
			}
		}else{
			console.log('['+appName+']'+'已经是最新版本了');
			if(mui.isShowVerTips){
				mui.toast('['+appName+']'+'已经是最新版本了');
			}
		}
	}
}

/**
 * 从服务器获取升级数据
 */
function getUpdateDataAPK(plus,currentVer){
	var xhr = new plus.net.XMLHttpRequest();
	xhr.onreadystatechange = function () {
        switch ( xhr.readyState ) {
            case 4:
                if ( xhr.status == 200 ) {
                	// 保存到本地文件中
                	dir.getFile( localFile, {create:true}, function(fentry){
                		fentry.createWriter( function(writer){
                			writer.onerror = function(){
                				console.log( '['+appName+']'+"获取安装包描述文件保存文件失败" );
                			}
                			//console.log(xhr.responseText);
                			writer.write( xhr.responseText ); 
                			checkUpdateAPK(plus,currentVer);
                		}, function(e){
                			console.log( '['+appName+']'+"获取安装包描述文件创建写文件对象失败："+e.message );
                		} );
                	}, function(e){
                		console.log( '['+appName+']'+ "获取安装包描述文件打开保存文件失败："+e.message );
                	});
                } else {
                	console.log( '['+appName+']'+"获取安装包描述文件联网请求失败："+xhr.status );
                }
                break;
            default :
                break;
        }
	}
	xhr.open( "GET", server );
	xhr.send();
}

/*
 * 下载APK并安装
 */
function downAPK(plus,inf,srvVer,curVer){
	//处理提示信息
	var note=getNoteInfo(inf.note,curVer)||'暂无更新信息';
	
	plus.nativeUI.confirm( note, function(e){
		if(e.index==0) {
			console.log('['+appName+']'+"正在下载APK文件...");
			plus.nativeUI.showWaiting("下载更新中...");
			plus.downloader.createDownload( inf.url, {filename:"_doc/update/"}, function(d,status){
				plus.nativeUI.closeWaiting();
				if ( status == 200 ) {
					console.log('['+appName+']'+"下载APK包成功："+d.filename);
					plus.runtime.openFile( d.filename );		
				} else {
					console.log('['+appName+']'+"下载APK包失败！");
				}
			}).start();
			
		}
	}, inf.title, ["立即更新","取　　消"] );
}

function updateAPK_(plus,currentVer){
	// 打开doc根目录
	plus.io.requestFileSystem( plus.io.PRIVATE_DOC, function(fs){
		fs.root.getDirectory( localDir, {create:true}, function(entry){
			dir = entry;
			checkUpdateAPK(plus,currentVer);
		}, function(e){
			console.log( '['+appName+']'+"准备升级操作打开update目录失败："+e.message );
		});
	},function(e){
		console.log( '['+appName+']'+"准备升级操作打开doc目录失败："+e.message );
	});
}

var exports={
	/*
	 *增量更新&包更新
	 */
	initUpdate:function(plus,currentVer,callback){
		//是否显示版本提示
		mui.isShowVerTips=false;
		// 打开doc根目录
		plus.io.requestFileSystem( plus.io.PRIVATE_DOC, function(fs){
			fs.root.getDirectory( localDir, {create:true}, function(entry){
				dir = entry;
				checkUpdate(plus,currentVer,callback);
			}, function(e){
				console.log( '['+appName+']'+"准备升级操作打开update目录失败："+e.message );
			});
		},function(e){
			console.log( '['+appName+']'+"准备升级操作打开doc目录失败："+e.message );
		});
	},
	/*
	 * 包更新
	 */
	updateAPK:function(plus,currentVer){
		//是否显示版本提示
		mui.isShowVerTips=true;
		updateAPK_(plus,currentVer);
	}
};
 
return exports;

})(window);