/*本地数据缓存*/
var cache={};

///水雨情-应急响应,记录上次最新记录时间
cache.yjxyUpdateTime=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('yjxyUpdateTime');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('yjxyUpdateTime');
        return;
    }
    plus.storage.setItem('yjxyUpdateTime', arguments[0]);
}
///水雨情-防汛通知,记录上次最新记录时间
cache.fxtzUpdateTime=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('fxtzUpdateTime');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('fxtzUpdateTime');
        return;
    }
    plus.storage.setItem('fxtzUpdateTime', arguments[0]);
}
///启动页-记录是否第一次登陆app
cache.isFirstLogin=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('isFirstLogin');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('isFirstLogin');
        return;
    }
    plus.storage.setItem('isFirstLogin', arguments[0]);
}

///清除本地缓存(暂时弃用)
cache.clear=function(){
	cache.isFirstLogin('');//清除启动页-记录是否第一次登陆app
}

///保存这界面风格样式（九宫格/插件形式）
cache.windowType=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('windowType');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('windowType');
        return;
    }
    plus.storage.setItem('windowType', arguments[0]);
}

///ios保存插件配置
cache.cardpluginConfig=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('cardpluginConfig');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('cardpluginConfig');
        return;
    }
    plus.storage.setItem('cardpluginConfig', arguments[0]);
}

///保存设备系统信息
//app版本号
cache.version=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('version');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('version');
        return;
    }
    plus.storage.setItem('version', arguments[0]);
}

///保存用户是否在试用期内试用app状态（在试用期内用户不能使用完整的app功能）
cache.isTryOutApp=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('isTryOutApp');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('isTryOutApp');
        return;
    }
    plus.storage.setItem('isTryOutApp', arguments[0]);
}

///保存县市区配置状态
cache.District=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('District');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('District');
        return;
    }
    plus.storage.setItem('District', arguments[0]);
}

///保存在安卓手机上是否提示卸载老版本安装包
cache.isDeleteAPK=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('isDeleteAPK');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('isDeleteAPK');
        return;
    }
    plus.storage.setItem('isDeleteAPK', arguments[0]);
}

///视频插件code
cache.VideoCode=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('VideoCode');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('VideoCode');
        return;
    }
    plus.storage.setItem('VideoCode', arguments[0]);
}

///App初始化本地存储
cache.AppInit=function(call){
	//设置使用主服务器访问
	plus.storage.setItem("ChangeMainServices", "false");
	
	call();
}

///存储系统消息ID
cache.AppAlertMsgID=function(){
	if(arguments.length == 0){
        return plus.storage.getItem('AppAlertMsgID');        
    }
    if(arguments[0] === ''){
        plus.storage.removeItem('AppAlertMsgID');
        return;
    }
    plus.storage.setItem('AppAlertMsgID', arguments[0]);
}
