var chart=(function(Highcharts){
	
	var exports={
		/*初始化*/
		initLinechart:function(dom){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'line',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			
			var chart = new Highcharts.Chart({
			chart:{
				renderTo:dom,
				type: 'line',
				plotShadow: false,
				borderWidth:0,
				backgroundColor:'',
				plotBackgroundColor: null,
				plotBorderWidth: null
			},
			credits:{enabled: false},
			legend:{enabled:false},
			title: {text: ''},
			xAxis : {
    			type : "datetime",
    			dateTimeLabelFormats:{
    				year:'%Y-%m-%d',
    				month:'%Y-%m-%d',
    				day: '%m-%d',
    				week:'%Y-%m-%d',
    				hour:'%H:%M',
    				minute:'%H:%M',
    				second:'%H:%M'
    			},
    			tickPixelInterval :100
    		},
    		yAxis : {
			    gridLineWidth: 1,
                title: {
                    text: '水位(m)'
                },
                endOnTick: true
    		},
    		series:[{}],
    		tooltip : {
    			formatter:function(){
    				return '时间：{0}<br/>水位：{1}m'.format(Highcharts.dateFormat('%Y-%m-%d %H:%M',this.x),this.y.toFixed(2));
    			}
    		},
    		plotOptions:{
    			line:{
    				marker:{enabled:false},
    				dataLabels: {
                    	enabled: false
                	},
                	enableMouseTracking: true
    			}
    		}
			});
			return chart;
		},
		initColumnchart:function(dom){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'column',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			
			var chart = new Highcharts.Chart({
				chart:{
					renderTo:dom,
					type: 'column',
					plotShadow: false,
					borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
					plotBorderWidth: null,					
				},
				legend:{enabled:false},
	            title: {text: ''},
	            tooltip : {
	    			formatter:function(){
	    				if(this.series.pointRange == 24*3600*1000){
	    					//return '时间：{0}<br/>雨量：{1}mm'.format(Highcharts.dateFormat('%m-%d',this.x),this.y.toFixed(1));
	    					return '时间：{0}<br/>雨量：{1}mm'.format(Highcharts.dateFormat('%m-%d',this.x),this.y.toFixed(1));
	    				}else{
	    					return '时间：{0}<br/>雨量：{1}mm'.format(Highcharts.dateFormat('%m-%d %H:%M',this.x),this.y.toFixed(1));
	    				}
	    			}
	    		},
	    		xAxis : {
	    			type : "datetime",
	    			dateTimeLabelFormats:{
	    				year:'%Y-%m-%d',
	    				month:'%Y-%m-%d',
	    				day: '%m-%d',
	    				week:'%Y-%m-%d',
	    				hour:'%H:%M',
	    				minute:'%H:%M',
	    				second:'%H:%M'
	    			},
	    			tickInterval:24*3600*1000
	    		},
	    		yAxis : {
				    gridLineWidth: 1,
	                title: {
	                    text: '雨量(mm)'
	                },
	                min:0,	          
	                endOnTick: false	             
	    		},
	    		plotOptions: {
	    			column: {
	                    dataLabels: {
	                        enabled: true,
	                        crop:false,
	                        overflow:"none",
	                        formatter:function(){return this.y.toFixed(1);},
	                        x:-1,
	                        y:2
	                    },
	                    pointRange:24*3600*1000,
	                    enableMouseTracking: true
	                }
	            },	      
	    		series:[{}]
				});
			return chart;
		},
		/*初始化县市区*/
		initDistrictRegionChart : function(dom,clickCall,tooltipCall){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'column',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			var disRegionChart = new Highcharts.Chart({
				chart:{
					renderTo:dom,
					type: 'column',
					plotShadow: false,
					borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
					plotBorderWidth: null
				},
				legend:{enabled:false},
	            title: {text: ''},
	            tooltip : {	     
	    			formatter:tooltipCall,
	    			useHTML:true
	    		},
	    		xAxis : {
	    			labels:{
	            		rotation: 0,
	            	}
	    		},
	    		yAxis : {
				    gridLineWidth: 1,
	                title: {
	                    text: '雨量(mm)'
	                },
	                endOnTick: false
	    		},
	    		plotOptions: {
	    			column: {
	    				events: {
		                    click: clickCall
		                },
	                    dataLabels: {
	                        enabled: true,
	                        crop:false,
	                        overflow:"none",
	                        formatter:function(){return this.y.toFixed(1);},
	                        x:-1,
	                        y:2
	                    },
	                    enableMouseTracking: true
	                }
	            },
	    		series:[{}]
			});
			return disRegionChart;
		},
		/*初始化大型水库*/
		initResRegionChart : function(dom,clickcall,tooltipsCall){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'column',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			var resRegionChart = new Highcharts.Chart({
				chart:{
					renderTo:dom,
					type: 'column',
					plotShadow: false,
					borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
					plotBorderWidth: null
				},
				legend:{enabled:false},
	            title: {text: ''},
	            tooltip : {
	    			formatter:tooltipsCall,
	    			useHTML: true,
	    		},
	    		xAxis : {},
	    		yAxis : {
				    gridLineWidth: 1,
	                title: {
	                    text: '雨量(mm)'
	                },
	                endOnTick: false
	    		},
	    		plotOptions: {
	    			column: {
	    				events: {
		                    click: clickcall
		               },
	                    dataLabels: {
	                        enabled: true,
	                        crop:false,
	                        overflow:"none",
	                        formatter:function(){return this.y.toFixed(1);},
	                        x:-1,
	                        y:2
	                    },
	                    enableMouseTracking: true
	                }
	            },
	    		series:[{}]
			});
			return resRegionChart;
		},
		/*初始化水情插件柱状图*/
		initSQpluginChart: function(dom){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'column',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			var thischart = new Highcharts.Chart({
				chart:{
					renderTo:dom,
					type: "column",
					plotShadow: false,
					borderWidth:0,
					backgroundColor:null,
					plotBackgroundColor: null,
					plotBorderWidth: 0
				},
				title:{
					text: ""
				},
				xAxis:{
					categories: ['大中型','小一型','小二型','河道', '潮位'],
					gridLineWidth: 0
				},
				yAxis: {
					min: 0,
					title:{
						text: ""
					},
					labels:{
						enabled: false
					},
					stackLabels:{
						enabled: true,
						style:{
							fontWeight: "bold",
							color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						},
						formatter:function(){return this.total;}
					},
					lineWidth: 0,
					gridLineWidth: 0,
					minorGridLineWidth: 0,
					tickWidth: 0
				},
				legend: {
					reversed: false,
					floating: false,
					verticalAlign: 'bottom',
					backgroundColor: null,
	                borderWidth: 0
				},
				plotOptions: {
					column: {
						dataLabels: {
							enabled: true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
		                    style: {
		                        textShadow: '0 0 3px black, 0 0 3px black'
		                    }
						}
					},
					series: {
		                stacking: 'normal'
		            }
				},
				series: [],
				credits:{enabled: false}
			});
			
			return thischart;
		},
		/*统计饼图*/
		initPieChart:function(dom){
			var chart = new Highcharts.Chart({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					renderTo:dom,
					type: "pie",
					plotShadow: false,
					borderWidth:0,
					backgroundColor:null,
					plotBackgroundColor: null,
					plotBorderWidth: 0
				},
				title: {
		            text: '',
		            align: 'center',
		            verticalAlign: 'middle',
		            y: -30,
		            style:{
		            	fontSize: "14px !important",
		            	fontFamily:"Microsoft YaHei !important"
		            }
		        },
		        subtitle: {
		            text: '',
		            align: 'center',
		            verticalAlign: 'middle',
		            y: -5,
		            style:{
		            	fontSize: "14px !important",
		            	fontFamily:"Microsoft YaHei !important"
		            }
		        },
				tooltip:{
					pointFormat: "{series.name}(万方):<b>{point.percentage:.1f}%</b>"
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
                		cursor: 'pointer',
						dataLabels: {
							enabled: true,
							distance: -30,
							style: {
								fontWeight: "normal",
								color: "white",
								textShadow: "0px 1px 2px black"
							},
							formatter:function(){return (this.y);}
						},
						showInLegend: true
					}
				},
				series: [],
		        /*legend: {
		        	enabled: true,
		            align: 'right',
		            verticalAlign: 'top',
		            layout: 'vertical',
		            borderWidth:0
		        },*/
				credits:{enabled: false}
			});
			
			return chart;
		},
		/*初始化取水概况统计柱状图*/
		initQSGKstaticChart : function(dom){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'column',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			var disRegionChart = new Highcharts.Chart({
				chart:{
					renderTo:dom,
					type: 'column',
					plotShadow: false,
					borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
					plotBorderWidth: null
				},
				legend:{enabled:false},
	            title: {text: ''},
	            tooltip : {
	    			formatter:function(){
	    				return '县市区：{0}<br/>取水量：{1}万方'.format(this.x,this.y.toFixed(0));
	    			}
	    		},
	    		xAxis : {
	    			labels:{
	            		rotation: 0,
	            	}
	    		},
	    		yAxis : {
				    gridLineWidth: 1,
	                title: {
	                    text: '取水量(万方)'
	                },
	                endOnTick: false
	    		},
	    		plotOptions: {
	    			column: {
	                    dataLabels: {
	                        enabled: true,
	                        crop:false,
	                        overflow:"none",
	                        formatter:function(){return this.y.toFixed(0);},
	                        x:-1,
	                        y:2
	                    },
	                    enableMouseTracking: true
	                }
	            },
	    		series:[{}]
			});
			return disRegionChart;
		},
		/*初始化取水概况详情面状图*/
		initQSGKdetailchart:function(dom){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'area',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			
			var chart = new Highcharts.Chart({
			chart:{
				renderTo:dom,
				type: 'area',
				plotShadow: false,
				borderWidth:0,
				backgroundColor:'',
				plotBackgroundColor: null,
				plotBorderWidth: null
			},
			credits:{enabled: false},
			legend:{enabled:false},
			title: {text: ''},
			xAxis : {
    			type : "datetime",
    			dateTimeLabelFormats:{
    				year:'%Y-%m-%d',
    				month:'%Y-%m-%d',
    				day: '%m-%d',
    				week:'%Y-%m-%d',
    				hour:'%H:%M',
    				minute:'%H:%M',
    				second:'%H:%M'
    			},
    			tickPixelInterval :100
    		},
    		yAxis : {
			    gridLineWidth: 1,
                title: {
                    text: '日取水量(万方)'
                },
                endOnTick: true
    		},
    		series:[{}],
    		tooltip : {
    			formatter:function(){
    				return '时间：{0}<br/>日取水量：{1}万方'.format(Highcharts.dateFormat('%Y-%m-%d',this.x),this.y.toFixed(0));
    			}
    		},
    		plotOptions:{
    			line:{
    				marker:{enabled:false},
    				dataLabels: {
                    	enabled: false
                	},
                	enableMouseTracking: true
    			}
    		}
			});
			return chart;
		},
		//初始化流量流速过程曲线
		initLiuliangchart:function(dom){
			Highcharts.setOptions({
				global: {
            		useUTC: false //默认使用UTC时间，与现在时刻相差8小时
        		},
				chart:{
					type: 'line',
			        plotShadow: false,
			        borderWidth:0,
					backgroundColor:'',
					plotBackgroundColor: null,
			        plotBorderWidth: null
				},
				credits:{enabled: false}
			});
			
			var chart = new Highcharts.Chart({
			chart:{
				renderTo:dom,
				type: 'line',
				plotShadow: false,
				borderWidth:0,
				backgroundColor:'',
				plotBackgroundColor: null,
				plotBorderWidth: null
			},
			credits:{enabled: false},
			legend:{enabled:false},
			title: {text: ''},
			xAxis : {
    			type : "datetime",
    			dateTimeLabelFormats:{
    				year:'%Y-%m-%d',
    				month:'%Y-%m-%d',
    				day: '%m-%d',
    				week:'%Y-%m-%d',
    				hour:'%H:%M',
    				minute:'%H:%M',
    				second:'%H:%M'
    			},
    			tickPixelInterval :100
    		},
    		yAxis : {
			    gridLineWidth: 1,
                title: {
                    text: '流量(㎥/s)'
                },
                endOnTick: true
    		},
    		series:[{}],
    		tooltip : {
    			formatter:function(){
    				return '时间：{0}<br/>流量：{1}㎥/s'.format(Highcharts.dateFormat('%Y-%m-%d %H:%M',this.x),this.y.toFixed(2));
    			}
    		},
    		plotOptions:{
    			line:{
    				marker:{enabled:false},
    				dataLabels: {
                    	enabled: false
                	},
                	enableMouseTracking: true
    			}
    		}
			});
			return chart;
		}
		
	};
	
	return exports;
})(Highcharts);
