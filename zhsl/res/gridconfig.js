/*
 * author:ysf
 * 描述：宫格界面配置文件
 */
var GRIDCONFIG={
	///水利新闻
	//工作动态
	gongzuodongtai:{isfire:"true",method:"/QueryNBSLGZDT!SPIDER",href:"html/slxw/list.html"},
	//公告通知
	gaozaotongzhi:{isfire:"true",method:"/QueryNBSLTZGG!SPIDER",href:"html/slxw/list.html"},
	//各地水利
	gedishuili:{isfire:"true",method:"/QueryNBSLGDSL!SPIDER",href:"html/slxw/list.html"},
	//重点工程
	zhongdiangongcheng:{isfire:"true",method:"/QueryNBSLZDGC!SPIDER",href:"html/slxw/list.html"},
	//招标公告
	zhaobiaogonggao:{isfire:"true",method:"/QueryNBSLZBGG!SPIDER",href:"html/slxw/list.html"},
	//中标公示
	zhongbiaogongshi:{isfire:"true",method:"/QueryNBSLZBGS!SPIDER",href:"html/slxw/list.html"},
	//人事任免
	renshirenmian:{isfire:"true",method:"/QueryNBSLRSRM!SPIDER",href:"html/slxw/list.html"},
	
	///北仑区水利新闻
	//最新动态
	bl_zuixingdongtai:{isfire:"true",method:"/QueryBLSLZXDT!SPIDER",href:"html/slxw/list.html"},
	//公告通知
	bl_gaozaotongzhi:{isfire:"true",method:"/QueryBLSLTZGG!SPIDER",href:"html/slxw/list.html"},
	//行业新闻
	bl_hangyexinwen:{isfire:"true",method:"/QueryBLSLHYXW!SPIDER",href:"html/slxw/list.html"},
	//防汛抗旱
	bl_fangxunkanghan:{isfire:"true",method:"/QueryBLSLFXKH!SPIDER",href:"html/slxw/list.html"},
	//水利工程
	bl_shuiligongcheng:{isfire:"true",method:"/QueryBLSLSLGC!SPIDER",href:"html/slxw/list.html"},
	//河道管理
	bl_hedaoguanglli:{isfire:"true",method:"/QueryBLSLHDGL!SPIDER",href:"html/slxw/list.html"},
	//农田水利
	bl_nongtianshuili:{isfire:"true",method:"/QueryBLSLNTSL!SPIDER",href:"html/slxw/list.html"},
	
	///气象信息
	//台风路径
	taifenlujing:{href:"html/qxxx/taifenlujing.html"},
	//卫星云图
	weixinyuntu:{isfire:"true",method:"/QueryNephograms!WEATHER",showMenu:"true",href:"html/qxxx/slider-template.html"},
	//雷达图
	leidatu:{isfire:"true",method:"/QueryRadars!WEATHER",showMenu:"true",href:"html/qxxx/slider-template.html"},
	//气象预报
	qixiangyubao:{href:"html/qxxx/qixiangyubaoLine.html",showMenu:"true",custIcon:"icon-qiehuan"},
	
	///水雨情
	//汛情简报
	xunqingjianbao:{href:"html/syq/xunqingjianbao.html"},
	//预警信息
	yujingxinxi:{showRefresh:"true",showMenu:"false",href:"html/syq/yujingxinxi.html"},
	//面雨量
	mianyuliang:{showTitle:"面雨量(mm)",showRefresh:"true",showMenu:"false",href:"html/syq/mianyuliang.html"},
	//点雨量
	dianyuliang:{showTitle:"点雨量(mm)",showRefresh:"false",showMenu:"true",href:"html/syq/dianyuliang.html"},
	//水库水情
	shuikushuiqing:{showTitle:"水库水情(m)",showRefresh:"false",showMenu:"true",href:"html/syq/shuikushuiqing.html"},
	//河道水情
	hedaoshuiqing:{showTitle:"河道水情(m)",showRefresh:"false",showMenu:"true",href:"html/syq/hedaoshuiqing.html"},
	//实时潮位
	shishichaowei:{showTitle:"实时潮位(m)",showRefresh:"false",showMenu:"true",href:"html/syq/shishichaowei.html"},
	//流量流速
	liuliangliusu:{isTryUser:'true',showTitle:"流量流速","open-type":"common",href:"html/syq/liuliangliusu.html"},
	
	///防汛业务信息
	//防汛通知
	fangxuntongzhi:{"open-type":"common",href:"html/syq/fangxuntongzhi.html"},
	//通讯录
	tongxunlu:{isTryUser:'true',"open-type":"common",href:"html/syq/tongxunlu.html"},
	yinjixiangying:{"open-type":"common",href:"html/syq/yinjixiangying.html"},
	
	///工情监控
	//水库工情
	shuikugongqin:{isTryUser:'true',"open-type":"common",href:"html/gqjk/shuikugongqing.html"},
	//闸泵工情
	zhabenggongqin:{isTryUser:'true',"open-type":"common",href:"html/gqjk/zhabenggongqin.html"},
	//视频监控
	"net.htwater.phone.swvideo":{"open-type":"openAPP",href:"http://htwater.net:8080/SmartWaterWeb/SWVideophone.apk"},
	
	///工程信息
	//水库
	shuiku:{"open-type":"common",href:"html/gcxx/shuiku.html"},
	//水闸
	shuizha:{"open-type":"common",href:"html/gcxx/shuizha.html"},
	//泵站
	bengzhan:{"open-type":"common",href:"html/gcxx/bengzhan.html"},
	//堤防
	difang:{"open-type":"common",href:"html/gcxx/difang.html"},
	//取水口
	qushuikou:{"open-type":"common",href:"html/gcxx/qushuikou.html"},
	//排污口
	paiwukou:{"open-type":"common",href:"html/gcxx/paiwukou.html"},
	
	///水资源监控
	//取水监控
	qushuijiankong:{"open-type":"common",href:"html/szyjk/qushuijiankong.html"},
	//排水监控
	paishuijiankong:{"open-type":"common",href:"html/szyjk/paishuijiankong.html"},
	//引水监控
	yinshuijiankong:{"open-type":"common",href:"html/szyjk/yinshuijiankong.html"},
	//水质监测
	shuizhijiance:{"open-type":"common",href:"html/szyjk/shuizhijiance.html"}
	
	
};