/*
 * 主页面js
 */
var idxjs=(function(){
	//内部处理函数
	
	return {
		/*天气预报插件页面动态构造*/
		getWeatherHTML:function(json){
			var dicon=json.infoimg,nicon=null,temp=json.temp,info=json.info,rmtemp=json.rmtemp,
		rmwind=json.rmwind,date=json.date,week=json.week,city=json.city;
		
		var t=rmtemp.split('℃');
		rmtemp=t[0]+'°';
		
		var ar=dicon.split(',');
		dicon=ar[0];
		nicon=ar[1];
		date=date+'&nbsp;&nbsp;'+week+'&nbsp;&nbsp;'+city;
		
		return '<div class="mui-table" style="height: 100px;padding-top: 10px;">'+
					'<div class="mui-table-cell mui-col-xs-8">'+
						'<span class="mui-icon iconfont icon-'+dicon+' wicon"></span>'+
						/*'<span class="mui-icon iconfont icon-'+nicon+' wicon"></span>'+*/
					'</div>'+
					'<div class="mui-table-cell mui-col-xs-4">'+
						'<span class="detail" style="font-size: 30px;font-weight: bold;color:#4d4d4d">'+rmtemp+'</span><br/>'+
						'<span class="detail">'+temp+'</span>'+
					'</div>'+
				'</div>'+
				'<div class="mui-table" style="height: 35px;color: #4D4D4D;">'+
					'<div class="mui-table-cell mui-col-xs-8">'+
						'<span class="detail">今天'+info+'</span>'+
					'</div>'+
					'<div class="mui-table-cell mui-col-xs-4">'+
						'<span class="detail">'+rmwind+'</span>'+
					'</div>'+
				'</div>'+
				'<div class="mui-table">'+
					'<div class="mui-table-cell">'+
						'<span style="color: #3188DE;font-size: 12px;">'+date+'</span>'+
					'</div>'+
				'</div>';
		},
		/*空气质量插件页面动态构造*/
		getKqzlHTML:function(json){
			var level=json.level||'--',iconcolor='',icon='',primarypollutant=json.primarypollutant||'--',
			affect=json.affect||'--',action=json.action||'--',pm2=json['PM2.5/1h']||'--',AQI=json.AQI||'--',pm10=json['PM10/1h']||'--';
			
			AQI='<font style="font-weight: bold;font-style: oblique;">'+AQI+'</font>';
			pm2='<font style="font-weight: bold;font-style: oblique;">'+pm2+'</font>';
			pm10='<font style="font-weight: bold;font-style: oblique;">'+pm10+'</font>';
			
			var maintip='<span style="font-size: 25px;font-weight: bold;color:#4d4d4d">'+level+'</span><br/>';
			
			if(level!=null&&level!='--'){
				if(level.indexOf('优')!=-1){
					level='';
					maintip='<span style="font-size: 25px;font-weight: bold;color:#4d4d4d">'+level+
					'</span><span style="padding-left:5px;font-size: 25px;color:#5ab941;" class="mui-icon iconfont icon-you"></span><br/>';
				}else if(level.indexOf('良')!=-1){				
					level='';
					maintip='<span style="font-size: 25px;font-weight: bold;color:#4d4d4d">'+level+
					'</span><span style="padding-left:5px;font-size: 25px;color:#eb8017;" class="mui-icon iconfont icon-liang"></span><br/>';
				}else if(level.indexOf('差')!=-1){				
					level='';
					maintip='<span style="font-size: 25px;font-weight: bold;color:#4d4d4d">'+level+
					'</span><span style="padding-left:5px;font-size: 25px;color:#d31724;" class="mui-icon iconfont icon-cha"></span><br/>';
				}
			}
			
			return '<div class="mui-table" style="height: 30%;padding-top: 20px;">'+
						'<div class="mui-table-cell">'+
							maintip+
							'<span class="mui-ellipsis-2" style="font-size: 12px;">'+primarypollutant+'</span>'+
						'</div>'+
						'<div class="mui-table-cell">'+
							'<span class="mui-ellipsis" style="font-size: 15px;">空气质量指数:'+AQI+'</span><br />'+
							'<span class="mui-ellipsis" style="font-size: 15px;">PM2.5:'+pm2+',PM10:'+pm10+'</span>'+
						'</div>'+
					'</div>'+
					'<div style="height: 70%;">'+
						'<ul class="mui-table-view">'+
							'<li class="mui-table-view-cell mui-disabled">'+
								'<div class="mui-table">'+
									'<div class="mui-table-cell">'+
										'<span class="mui-ellipsis-2 mui-text-left" style="font-size: 15px;"><span style="color: #29B1FF;" class="mui-icon iconfont icon-jiankang"></span><font style="font-size: 15px;font-weight: bold;padding-left: 5px;color:#4d4d4d">对健康影响情况：</font>'+affect+'</span>'+
									'</div>'+
								'</div>'+
							'</li>'+
							'<li class="mui-table-view-cell mui-disabled">'+
								'<div class="mui-table">'+
									'<div class="mui-table-cell">'+
										'<span class="mui-ellipsis-2 mui-text-left" style="font-size: 15px;"><span style="color: #F25109;" class="mui-icon iconfont icon-cuoshi"></span><font style="font-size: 15px;font-weight: bold;padding-left: 5px;color:#4d4d4d">建议采取的措施：</font>'+action+'</span>'+
									'</div>'+
								'</div>'+
							'</li>'+
						'</ul>'+
					'</div>';
		},
		/*雨情概况插件表头*/
		yqgktableHeader:function(){			
			return '<div class="tableheader">'+
							'<ul class="mui-table-view mui-table-view-striped mui-table-view-condensed">'+
								'<li class="mui-table-view-cell mui-disabled" style="overflow:visible;padding: 11px 15px 0px;">'+
						            '<div class="mui-table">'+
						                '<div class="mui-table-cell mui-col-xs-2">'+
						                	'<h5 class="mui-ellipsis-2">今天</h5>'+	                	
						               ' </div>'+
						                '<div class="mui-table-cell mui-col-xs-2">'+
						                	'<h5 class="mui-ellipsis-2">昨天</h5>'+                	
						                '</div>'+
						                '<div class="mui-table-cell mui-col-xs-2">'+
						                	'<h5 class="mui-ellipsis-2">前天</h5>'+	                	
						                '</div>'+
						                '<div class="mui-table-cell mui-col-xs-2">'+
						                	'<h5 class="mui-ellipsis-2">三天</h5>'+	                	
						                '</div>'+
						                '<div class="mui-table-cell mui-col-xs-2">'+
						                	'<h5 class="mui-ellipsis-2">七天</h5>'+	                	
						                '</div>'+
						                '<div class="mui-table-cell mui-col-xs-3 mui-text-right">'+
						                	'<h5 class="mui-ellipsis-2">雨量</h5>'+	                	
						                '</div>'+
						            '</div>'+
						        '</li>'+
							'</ul>'+
						'</div>';
	
		},
		/*插件表内容*/
		gettableContent:function(ulHTML){			
			return '<div class="tablecontent">'+
							ulHTML+
					'</div>';
		},
		/*雨情概况统计*/
		staticByGrade:function(array){			
			var arr=null,filarr=[{'todayRain':0,'yestodayRain':0,'byestodayRain':0,
			'sum3':0,'sum7':0,'grade':'>=200'},
			{'todayRain':0,'yestodayRain':0,'byestodayRain':0,
			'sum3':0,'sum7':0,'grade':'100~200'},
			{'todayRain':0,'yestodayRain':0,'byestodayRain':0,
			'sum3':0,'sum7':0,'grade':'50~100'},
			{'todayRain':0,'yestodayRain':0,'byestodayRain':0,
			'sum3':0,'sum7':0,'grade':'30~50'},
			{'todayRain':0,'yestodayRain':0,'byestodayRain':0,
			'sum3':0,'sum7':0,'grade':'0~30'}];
			
			/*>=200*/
			arr= array.filter(function(v){
				var av=v.yestodayRain;
				return parseFloat(av)>=200.0;
			});
			filarr[0].yestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.byestodayRain;
				return parseFloat(av)>=200.0;
			});
			filarr[0].byestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.todayRain;
				return parseFloat(av)>=200.0;
			});
			filarr[0].todayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum3;
				return parseFloat(av)>=200.0;
			});
			filarr[0].sum3=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum7;
				return parseFloat(av)>=200.0;
			});
			filarr[0].sum7=arr.length;
			
			/*100~200*/
			arr= array.filter(function(v){
				var av=v.yestodayRain;
				return parseFloat(av)>=100&&parseFloat(av)<200.0;
			});
			filarr[1].yestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.byestodayRain;
				return parseFloat(av)>=100&&parseFloat(av)<200.0;
			});
			filarr[1].byestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.todayRain;
				return parseFloat(av)>=100&&parseFloat(av)<200.0;
			});
			filarr[1].todayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum3;
				return parseFloat(av)>=100&&parseFloat(av)<200.0;
			});
			filarr[1].sum3=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum7;
				return parseFloat(av)>=100&&parseFloat(av)<200.0;
			});
			filarr[1].sum7=arr.length;
			
			/*50~100*/
			arr= array.filter(function(v){
				var av=v.yestodayRain;
				return parseFloat(av)>=50&&parseFloat(av)<100.0;
			});
			filarr[2].yestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.byestodayRain;
				return parseFloat(av)>=50&&parseFloat(av)<100.0;
			});
			filarr[2].byestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.todayRain;
				return parseFloat(av)>=50&&parseFloat(av)<100.0;
			});
			filarr[2].todayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum3;
				return parseFloat(av)>=50&&parseFloat(av)<100.0;
			});
			filarr[2].sum3=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum7;
				return parseFloat(av)>=50&&parseFloat(av)<100.0;
			});
			filarr[2].sum7=arr.length;
			
			/*30~50*/
			arr= array.filter(function(v){
				var av=v.yestodayRain;
				return parseFloat(av)>=30&&parseFloat(av)<50.0;
			});
			filarr[3].yestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.byestodayRain;
				return parseFloat(av)>=30&&parseFloat(av)<50.0;
			});
			filarr[3].byestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.todayRain;
				return parseFloat(av)>=30&&parseFloat(av)<50.0;
			});
			filarr[3].todayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum3;
				return parseFloat(av)>=30&&parseFloat(av)<50.0;
			});
			filarr[3].sum3=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum7;
				return parseFloat(av)>=30&&parseFloat(av)<50.0;
			});
			filarr[3].sum7=arr.length;
			
			/*0~30*/
			arr= array.filter(function(v){
				var av=v.yestodayRain;
				return parseFloat(av)>0&&parseFloat(av)<30.0;
			});
			filarr[4].yestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.byestodayRain;
				return parseFloat(av)>0&&parseFloat(av)<30.0;
			});
			filarr[4].byestodayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.todayRain;
				return parseFloat(av)>0&&parseFloat(av)<30.0;
			});
			filarr[4].todayRain=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum3;
				return parseFloat(av)>0&&parseFloat(av)<30.0;
			});
			filarr[4].sum3=arr.length;
			
			arr= array.filter(function(v){
				var av=v.sum7;
				return parseFloat(av)>0&&parseFloat(av)<30.0;
			});
			filarr[4].sum7=arr.length;
			
			return filarr;	
		},
		/*水库工程库容*/
		getKR:function(){
			return [
				{district:'鄞州',kr:44829,totalkr:191956.83},
				{district:'宁海',kr:42740,totalkr:191956.83},
				{district:'奉化',kr:31638,totalkr:191956.83},
				{district:'镇海',kr:4549,totalkr:191956.83},
				{district:'北仑',kr:4383,totalkr:191956.83},
				{district:'江北',kr:2168,totalkr:191956.83},
				{district:'余姚',kr:27004,totalkr:191956.83},
				{district:'象山',kr:17719,totalkr:191956.83},
				{district:'慈溪',kr:16927,totalkr:191956.83}
				];
		}
		
	};
})();
