/**
 * @Description:平板chart
 * @CreateDate: 2014-11-06 下午4:11:05
 * @author: AGNGA
 */
// charts begain
function checkInterval(interval) {

    var count = interval / 100;
    var mod = interval % 100;

    if (interval == 0) {
        return interval;
    }

    if (count >= 1) {
        if (mod / 50 > 1) {
            return (Math.floor(count) + 1) * 100;
        } else {
            return Math.floor(count) * 100 + 50;
        }
    } else {

        if (mod / 50 > 1) {
            return 100;
        } else {

            if (mod > 10) {
                return Math.ceil(interval / 10) * 10;
            } else {
                if (mod > 5) {
                    return 10;
                } else {
                    if (mod <= 1)
                        if (mod > 0.5) {
                            return 1;
                        } else {
                            if (mod == 0) {
                                return interval;
                            }
                            return 0.5;
                        }
                    else if (mod <= 2) {
                        return 2;
                    } else if (mod <= 4) {
                        return 4;
                    } else {
                        return 5;
                    }
                }
            }

        }

    }

}

function printInfo(str) {
    // alert(str);
}

function covertMax(value) {
    return Math.ceil(value);
}

function covertMin(value) {
    return Math.floor(value) - 1;
    if (value < 0) {

        var tempvalue = Math.abs(value);

        var count = tempvalue / 100;
        var mod = tempvalue % 100;

        if (count >= 1) {
            if (mod > 50) {
                return -(Math.floor(count) + 1) * 100;
            } else {
                return -(Math.floor(count) * 100 + 50);
            }
        } else {

            if (mod > 50) {

                return -100;
            } else {

                if (mod > 10) {
                    return -50;
                } else {

                    if (mod > 5) {
                        return -10;
                    } else {
                        return -5;
                    }
                }
            }

        }

    } else {

        var count = value / 100;
        var mod = value % 100;

        if (count > 1) {
            if (mod > 50) {
                return Math.floor(count) * 100 + 50;
            } else {
                return Math.floor(count) * 100;
            }
        } else {

            if (mod > 50) {

                return 50;
            } else {

                if (mod > 10) {
                    return 10;
                } else {

                    if (mod > 5) {
                        return 5;
                    } else {

                        if (mod > 1) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                }
            }

        }

    }

}

function getCount(min, max, maxCount) {

    var total = Math.abs(min) + max;
    var interval = checkInterval(Math.ceil(total / maxCount));
    return Math.ceil(total / interval);
}

function checkShouldShowMarker(val, planmarker) {

    for (var i = 0; i < planmarker.length; i++) {

        if (val == planmarker[i].TM) {
            return i;
        }
    }
    return -1;
}

var globaljson = null;
var globaljsonPlanArray = null;
var planmarker = [];
var nameOfTime = "SJ";
var nameOfRKLL = "RKLL";
var nameOfCKLL = "CKLL";
var nameOfSW = "SW";
var nameOfCW = "tide";
var nameOfRain = "JYL";
var container = "container";
var blHasP = false;
var blHasI = false;
var blHasQ = false;
var blHasZ = false;
var blHasT = false;
var blHasPlanP = false;
var blHasPlanI = false;
var blHasPlanQ = false;
var blHasPlanZ = false;
var blHasPlanT = false;
var yArea2Start = 0;
var planStart = 0;
var yArea3Start;
// 雨量起始位置
var yInterval = 0;
// y轴间隔
var gapArea2;
// 水位的比例值
var gapArea3;
// 雨量比例值
var yMaxCount = 10;
// 每一区域最多的格数
var yMax;
// Y轴的总长度
var maxArea1 = 0;
var minArea1 = 0;
var maxArea2 = 0;
var minArea2 = 0;
var maxArea3 = 0;
var minArea3 = 0;

var globalStartTime = 0;
var globalEndTime = 0;
var area1Count = 0;
var area2Count = 0;
var area3Count = 0;

function createChartByJson(title, subtitle, json, jsonPlanArray, chartTime, chartRKLL, charCKLL,
	chartSW, chartCW, chartRain, chartContainer, duration) {

    globalStartTime = 0;
    globalEndTime = 0;
    globaljson = json;
    globaljsonPlanArray = jsonPlanArray;
    planmarker = [];
    blHasP = false;
    blHasI = false;
    blHasQ = false;
    blHasZ = false;
    blHasT = false;
    blHasPlanP = false;
    blHasPlanI = false;
    blHasPlanQ = false;
    blHasPlanZ = false;
    blHasPlanT = false;

    if (null != chartTime) {
        nameOfTime = chartTime;
    }

    if (null != chartRKLL) {
        nameOfRKLL = chartRKLL;
    }

    if (null != charCKLL) {
        nameOfCKLL = charCKLL;
    }

    if (null != chartSW) {
        nameOfSW = chartSW;
    }

    if (null != chartCW) {
        nameOfCW = chartCW;
    }

    if (null != chartRain) {
        nameOfRain = chartRain;
    }

    if (null != chartContainer) {
        container = chartContainer;
    }

    var dataRkll = [];
    var dataZ = [];
    var dataCkll = [];
    var dataP = [];
    var dataT = [];

    var minTime = 0;
    var maxTime = 0;

    var maxI = 0;
    var minI = 0;
    var maxQ = 0;
    var minQ = 0;
    var maxZ = 0;
    var minZ = 0;
    var maxT = 0;
    var minT = 0;
    var maxP = 0;
    var minP = 0;

    // var yArea2Start; // 水位起始位置

    var blHasArea1 = false;
    var blHasArea2 = false;
    var blHasArea3 = false;

    var jsonLen = 0;
    var jsonPlanArrayLen = 0;

    if (null != json && undefined != json) {
        jsonLen = json.length;
    }

    if (null != jsonPlanArray && undefined != jsonPlanArray) {
        jsonPlanArrayLen = jsonPlanArray.length;
    }

    if (jsonLen == 0 && jsonPlanArrayLen == 0) {
        return;
    }

    if (jsonLen != 0) {

        if (json[0][nameOfRKLL] != undefined) {
            blHasI = true;
        }
        if (json[0][nameOfCKLL] != undefined) {
            blHasQ = true;
        }

        if (json[0][nameOfSW] != undefined) {
            blHasZ = true;
        }
        if (json[0][nameOfCW] != undefined) {
            blHasT = true;
        }
        if (json[0][nameOfRain] != undefined) {
            blHasP = true;
        }
    }

    if (jsonPlanArrayLen != 0) {

        for (var i = 0; i < jsonPlanArrayLen; i++) {
            if (jsonPlanArray[i].方案号 != 0) {
                if (!blHasPlanI) {
                    if (jsonPlanArray[i].data[0][nameOfRKLL] != undefined) {
                        blHasPlanI = true;
                    }
                }

                if (!blHasPlanQ) {
                    if (jsonPlanArray[i].data[0][nameOfCKLL] != undefined) {
                        blHasPlanQ = true;
                    }
                }

                if (!blHasPlanZ) {
                    if (jsonPlanArray[i].data[0][nameOfSW] != undefined) {
                        blHasPlanZ = true;
                    }
                }

                if (!blHasPlanT) {
                    if (jsonPlanArray[i].data[0][nameOfCW] != undefined) {
                        blHasPlanT = true;
                    }
                }

                if (!blHasPlanP) {
                    if (jsonPlanArray[i].data[0][nameOfRain] != undefined) {
                        blHasPlanP = true;
                    }
                }
            }
        }
    }

    if (blHasI) {
        maxI = json[0][nameOfRKLL];
        minI = json[0][nameOfRKLL];
        blHasArea1 = true;
    }

    if (blHasPlanI) {
        maxI = jsonPlanArray[0].data[0][nameOfRKLL];
        minI = jsonPlanArray[0].data[0][nameOfRKLL];
        blHasArea1 = true;
    }

    if (blHasQ) {
        maxQ = json[0][nameOfCKLL];
        minQ = json[0][nameOfCKLL];
        blHasArea1 = true;
    }

    if (blHasPlanQ) {
        maxQ = jsonPlanArray[0].data[0][nameOfCKLL];
        minQ = jsonPlanArray[0].data[0][nameOfCKLL];
        blHasArea1 = true;
    }

    if (blHasZ) {
        maxZ = json[0][nameOfSW];
        minZ = json[0][nameOfSW];
        blHasArea2 = true;
    }

    if (blHasPlanZ) {
        maxZ = jsonPlanArray[0].data[0][nameOfSW];
        minZ = jsonPlanArray[0].data[0][nameOfSW];
        blHasArea2 = true;
    }

    if (blHasT) {

        maxT = json[0][nameOfCW];
        minT = json[0][nameOfCW];
        blHasArea2 = true;
    }
    if (blHasPlanT) {

        maxT = jsonPlanArray[0].data[0][nameOfCW];
        minT = jsonPlanArray[0].data[0][nameOfCW];
        blHasArea2 = true;
    }

    if (blHasP) {
        maxP = json[0][nameOfRain];
        minP = json[0][nameOfRain];
        blHasArea3 = true;
    }

    if (blHasPlanP) {
        maxP = jsonPlanArray[0].data[0][nameOfRain];
        minP = jsonPlanArray[0].data[0][nameOfRain];
        blHasArea3 = true;
    }

    minTime = new Date(json[0][nameOfTime]).getTime();
    maxTime = new Date(json[jsonLen - 1][nameOfTime]).getTime();

    if (jsonLen != 0) {
        for (var i = 0; i < jsonLen; i++) {

            if (minTime > json[i][nameOfRKLL])

                if (blHasI) {
                    if (maxI < json[i][nameOfRKLL]) {
                        maxI = json[i][nameOfRKLL];
                    }

                    if (minI > json[i][nameOfRKLL]) {
                        minI = json[i][nameOfRKLL];
                    }
                }

            if (blHasQ) {
                if (maxQ < json[i][nameOfCKLL]) {
                    maxQ = json[i][nameOfCKLL];
                }

                if (minQ > json[i][nameOfCKLL]) {
                    minQ = json[i][nameOfCKLL];
                }
            }

            if (blHasZ) {
                if (maxZ < json[i][nameOfSW]) {
                    maxZ = json[i][nameOfSW];
                }

                if (minZ > json[i][nameOfSW]) {
                    minZ = json[i][nameOfSW];
                }
            }

            if (blHasT) {
                if (maxT < json[i][nameOfCW]) {
                    maxT = json[i][nameOfCW];
                }

                if (minT > json[i][nameOfCW]) {
                    minT = json[i][nameOfCW];
                }
            }

            if (blHasP) {
                if (maxP < json[i][nameOfRain]) {
                    maxP = json[i][nameOfRain];
                }

                if (minP > json[i][nameOfRain]) {
                    minP = json[i][nameOfRain];
                }
            }
        }
    }
    var plan = undefined;
    var jsonPlanminTime = 0;
    var jsonPlanMaxTime = 0;
    if (jsonPlanArrayLen != 0) {
        for (var i = 0; i < jsonPlanArrayLen; i++) {
            if (jsonPlanArray[i].方案号 != 0) {
                var curTimemin = new Date(jsonPlanArray[i].data[0][nameOfTime]).getTime();
                var curTimemax = new Date(jsonPlanArray[i].data[jsonPlanArray[i].data.length - 1][nameOfTime]).getTime();
                if (jsonPlanminTime > curTimemin) {
                    jsonPlanminTime = curTimemin;
                }
                if (jsonPlanMaxTime < curTimemax) {
                    jsonPlanMaxTime = curTimemax;
                }

                for (var j = 0; j < jsonPlanArray[i].data.length; j++) {
                    if (blHasPlanI) {
                        if (maxI < jsonPlanArray[i].data[j][nameOfRKLL]) {
                            maxI = jsonPlanArray[i].data[j][nameOfRKLL];
                        }

                        if (minI > jsonPlanArray[i].data[j][nameOfRKLL]) {
                            minI = jsonPlanArray[i].data[j][nameOfRKLL];
                        }
                    }

                    if (blHasPlanQ) {
                        if (maxQ < jsonPlanArray[i].data[j][nameOfCKLL]) {
                            maxQ = jsonPlanArray[i].data[j][nameOfCKLL];
                        }

                        if (minQ > jsonPlanArray[i].data[j][nameOfCKLL]) {
                            minQ = jsonPlanArray[i].data[j][nameOfCKLL];
                        }
                    }

                    if (blHasPlanZ) {
                        if (maxZ < jsonPlanArray[i].data[j][nameOfSW]) {
                            maxZ = jsonPlanArray[i].data[j][nameOfSW];
                        }

                        if (minZ > jsonPlanArray[i].data[j][nameOfSW]) {
                            minZ = jsonPlanArray[i].data[j][nameOfSW];
                        }
                    }

                    if (blHasPlanT) {
                        if (maxT < jsonPlanArray[i].data[j][nameOfCW]) {
                            maxT = jsonPlanArray[i].data[j][nameOfCW];
                        }

                        if (minT > jsonPlanArray[i].data[j][nameOfCW]) {
                            minT = jsonPlanArray[i].data[j][nameOfCW];
                        }
                    }

                    if (blHasPlanP) {
                        if (maxP < jsonPlanArray[i].data[j][nameOfRain]) {
                            maxP = jsonPlanArray[i].data[j][nameOfRain];
                        }

                        if (minP > jsonPlanArray[i].data[j][nameOfRain]) {
                            minP = jsonPlanArray[i].data[j][nameOfRain];
                        }
                    }
                }
            }
        }

    }

    if (jsonPlanminTime > 0) {

        minTime = minTime < jsonPlanminTime ? minTime : jsonPlanminTime;
    }

    if (jsonPlanMaxTime > 0) {

        maxTime = maxTime > jsonPlanMaxTime ? maxTime : jsonPlanMaxTime;
    }

    maxArea1 = maxI > maxQ ? maxI : maxQ;
    //maxArea1 = maxArea1 > maxDynFlow ? maxArea1 : maxDynFlow;
    maxArea1 = maxArea1 > 400 ? maxArea1 : 400;
    // AGANGA_Find
    // 最大值判断加上动态预报流量
    // maxArea1=100;
    minArea1 = minI < minQ ? minI : minQ;
    if (minArea1 == 0) {
        if (minQ == 0) {
            minArea1 = minI;
        } else {
            minArea1 = minQ;
        }
    }
    maxArea1 = covertMax(maxArea1);

    maxArea1 = maxArea1 < 200 ? 200 : maxArea1;

    minArea1 = covertMin(minArea1);

    maxArea2 = maxZ > maxT ? maxZ : maxT;
    minArea2 = minZ < minT ? minZ : minT;

    if (minArea2 == 0) {
        if (minZ == 0) {
            minArea2 = minT;
        } else {
            minArea2 = minZ;
        }
    }

    maxArea2 = covertMax(maxArea2);
    minArea2 = covertMin(minArea2);

    maxArea3 = maxP;
    minArea3 = minP;

    maxArea3 = covertMax(maxArea3);

    maxArea3 = maxArea3 < 30 ? 30 : maxArea3;

    minArea3 = covertMin(minArea3);

    // setYInterval();
    area1Count = getCount(minArea1, maxArea1, yMaxCount);
    area2Count = getCount(minArea2, maxArea2, 5);
    area3Count = getCount(minArea3, maxArea3, 4);

    // alert("area1Count->"+area1Count+"area2Count->"+area2Count +
    // "area3Count->"+area3Count);

    if (blHasArea1) {
        yInterval = checkInterval((Math.abs(minArea1) + maxArea1) / area1Count);
    } else {
        if (blHasArea2) {
            yInterval = checkInterval((Math.abs(minArea2) + maxArea2) / area2Count);
        } else {

            if (blHasArea3) {
                yInterval = checkInterval((Math.abs(minArea3) + maxArea3) / area3Count);
            }
        }
    }

    // setArea2Start();
    var gapTemp = 0;
    if (minArea2 < 0) {
        gapTemp += (Math.abs(minArea2)) / (Math.abs(minArea2) + maxArea2);
    }

    yArea2Start = maxArea1 * (1 + gapTemp);
    var count = Math.ceil(yArea2Start / yInterval);
    if (blHasArea1 && blHasArea2) {
        yArea2Start = (count + 2) * yInterval;
        planStart = (count + 1) * yInterval;
    } else {
        yArea2Start = (count + 1) * yInterval;
        planStart = (count) * yInterval;
    }
    // setgapArea2();
    var yArea2Interval = Math.ceil((maxArea2 - minArea2) / area2Count);
    yArea2Interval = checkInterval(yArea2Interval);
    if (yArea2Interval != 0) {
        gapArea2 = yInterval / yArea2Interval;
    } else {
        gapArea2 = 0;
    }
    // setArea3tart();

    // setArea3Start();
    var gapTemp = 0;
    if (minArea3 < 0) {
        gapTemp += (Math.abs(minArea3)) / (Math.abs(minArea3) + maxArea3);
    }

    yArea3Start = yArea2Start + area2Count * yInterval;

    // setgapArea3();
    var yArea3Interval = Math.ceil((maxArea3 - minArea3) / area3Count);
    yArea3Interval = checkInterval(yArea3Interval);
    if (yArea3Interval != 0) {
        gapArea3 = yInterval / yArea3Interval;
    } else {
        gapArea3 = 0;
    }

    // setYMax();
    yMax = yArea3Start + gapArea3 + yInterval * area3Count;

    yMax = Math.ceil(yMax / yInterval) * yInterval;

    printInfo("maxI->" + maxI + "maxQ->" + maxQ + "maxArea1->" + maxArea1 + "minArea1->" + minArea1 + "maxZ->" + maxZ + "\r\n" + "minZ->" + minZ + "minT->" + minT + "maxT->" + maxT + "maxArea2->" + maxArea2 + "minArea2->" + minArea2 + "\r\n" + "maxP->" + maxP + "minP->" + minP + "maxArea3->" + maxArea3 + "minArea3->" + minArea3 + "yArea2Start->" + yArea2Start + "yArea3Start->" + yArea3Start + "\r\n" + "yInterval->" + yInterval + "gapArea2->" + gapArea2 + "gapArea3->" + gapArea3 + "yArea2Interval->" + yArea2Interval + "\r\n" + "yMax->" + yMax + "blHasP->" + blHasP + "blHasI->" + blHasI + "blHasQ->" + blHasQ + "blHasZ->" + blHasZ + "blHasT->" + blHasT);

    $(json).each(function() {
        if (blHasI) {
            dataRkll.push([new Date(this[nameOfTime]).getTime(), this[nameOfRKLL]]);
        }

        if (blHasQ) {
            dataCkll.push([new Date(this[nameOfTime]).getTime(), this[nameOfCKLL]]);
        }
        if (blHasZ) {
            dataZ.push([new Date(this[nameOfTime]).getTime(), ((this[nameOfSW] - minArea2) * gapArea2 + yArea2Start)]);
        }

        if (blHasT) {
            dataT.push([new Date(this[nameOfTime]).getTime(), ((this[nameOfCW] - minArea2) * gapArea2 + yArea2Start)]);
        }

        if (blHasP) {
            dataP.push([new Date(this[nameOfTime]).getTime(), this[nameOfRain]]);
        }

    });

    var titletext = "";

    if (blHasArea1) {

        titletext = "流量(m3/s)";
    }

    if (blHasP) {

        if ("" != titletext) {

            titletext += "/雨量(mm)";
        } else {

            titletext = "雨量(mm)";
        }

    }

    if (blHasT) {

        if ("" != titletext) {

            titletext += "/潮位(m)";
        } else {

            titletext = "潮位(m)";
        }

    }

    var options = {
        chart : {
            renderTo : container,
            zoomType : 'xy',
            plotShadow: false,
			borderWidth:0,
			backgroundColor:'',
			plotBackgroundColor: null,
			plotBorderWidth: null
        },
        title : title,
        subtitle : subtitle,
        xAxis : {
            type : 'datetime',
            dateTimeLabelFormats : {
            	hour : '%m'+"月"+'%e' + "日" + ' %H',
				day : '%m'+"月"+'%e' + "日" + ' %H',
				week : '%e. %m',
				month : '%m \'%y'
				
                /*hour : '%b%e' + ' %H',
                day : '%b%e' + ' %H',
                week : '%e. %b',
                month : '%b \'%y' */    
            },
            labels : {
                align : 'center'
            },
        },
        credits: {
             text: '',
             href: ''
         },
        yAxis : [{// Primary yAxis
            gridLineWidth : 0,
            labels : {
                formatter : function() {

                    if (this.value >= yArea2Start) {
                        if (this.value > yArea3Start) {
                            return ((Math.floor(Math.abs(this.value - yMax) / yInterval)) * yArea3Interval).toFixed(0);
                        }
                        if (blHasArea2) {
                            return "";
                        } else {
                            if (blHasArea1) {
                                return (this.value).toFixed(0);
                            } else {
                                return "";
                            }
                        }
                    } else {

                        if (this.value > (Math.ceil(maxArea1 / yInterval)) * yInterval) {
                            return "";
                        }
                        if (blHasArea1) {
                            return (this.value).toFixed(0);
                        } else {
                            return "";
                        }
                    }
                },
                style : {
                    color : '#89A54E'
                }
            },
            title : {
                text : titletext,
                style : {
                    color : '#89A54E'
                }
            },
            min : 0,
            tickInterval : yInterval,/*yInterval*/
            max : yMax
        }],
        legend : {
            enabled : true
        },
        plotOptions : {

            spline : {
                marker : {
                    enabled : false,
                    states : {
                        hover : {
                            enabled : true,
                            radius : 1
                        }
                    }
                },
                shadow : true,
            }
        },

        tooltip : {
            // enabled:false,
            xDateFormat : '%H:%M:%S, %Y-%m-%d',
            shared : true,
            useHTML : true, // 采用自定义的html
            formatter : function() {
                var sw = undefined;
                var cw = undefined;
                var rl = undefined;
                var cl = undefined;
                var yl = undefined;

                var plansw = undefined;
                var plancw = undefined;
                var planrl = undefined;
                var plancl = undefined;
                var planyl = undefined;

                var dynYbrl = undefined;

                var strTemp = '<table id="chartTipTable">';

                for (var i = 0; i < this.points.length; i++) {

                    if (this.points[i].series.name == "入流") {
                        rl = this.points[i].y;
                    }

                    if (this.points[i].series.name == "动态预报入流") {
                        dynYbrl = this.points[i].y;
                    }

                    if (this.points[i].series.name == "出流") {
                        cl = this.points[i].y;
                    }

                    if (this.points[i].series.name == "水位") {
                        sw = this.points[i].y;
                    }

                    if (this.points[i].series.name == "潮位") {
                        cw = this.points[i].y;
                    }

                    if (this.points[i].series.name == "降雨") {
                        yl = this.points[i].y;
                    }

                    if (this.points[i].series.name == "方案入流") {
                        planrl = this.points[i].y;
                    }

                    if (this.points[i].series.name == "方案出流") {
                        plancl = this.points[i].y;
                    }

                    if (this.points[i].series.name == "方案水位") {
                        plansw = this.points[i].y;
                    }

                    if (this.points[i].series.name == "方案潮位") {
                        plancw = this.points[i].y;
                    }

                    if (this.points[i].series.name == "方案降雨") {
                        planyl = this.points[i].y;
                    }

                    // if (curSelectedIndex != -1) {
                    /*
                    for (var i = 0; i < globaljsonPlanArray.length; i++) {
                    if(globaljsonPlanArray[i].鏂规鍙�=curSelectedIndex){
                    // plan = globaljsonPlanArray[curSelectedIndex].name;
                    plan=globaljsonPlanArray[i].name;
                    }
                    }*/

                    //}
                    plan = gloFaName;
                }
                strTemp += '<tr><td>时间：</td>' + '<td style="font-weight:bold;" colspan=2>' + new Date(this.x).format("yyyy-MM-dd hh") + '</td>' + "</tr>";
                strTemp += '<tr>';
                /*if (plan != undefined) {
                    strTemp += '<td>当前方案:</td><td style="font-weight:bold;" colspan=2>' + plan + '</td>';
                    // strTemp += '<b style="color:#1d5987;font-weight:bold;">'
                    // + "当前方案:" + plan + "</b> <br />";
                }*/
                strTemp += '</tr>';
                strTemp += '<tr>';
                if (rl != undefined) {
                    strTemp += '<td>入流:</td><td style="font-weight:bold;">' + Number(rl).toFixed(2) + 'm3/s</td>';
                    // strTemp += '<b>' + "入流:" + rl + "m3/s</b> <br />";
                }

                if (planrl != undefined) {
                    strTemp += '<td>方案入流:</td><td style="font-weight:bold;">' + Number(planrl).toFixed(2) + 'm3/s</td>';
                    // strTemp += '<b style="color:#1d5987;font-weight:bold;">'
                    // + "方案入流:" + planrl + "m3/s</b> <br />";
                }
                if (dynYbrl != undefined) {
                    strTemp += '<td>动态预报入流:</td><td style="font-weight:bold;">' + Number(dynYbrl).toFixed(2) + 'm3/s</td>';
                    // strTemp += '<b style="color:#1d5987;font-weight:bold;">'
                    // + "方案入流:" + planrl + "m3/s</b> <br />";
                }

                strTemp += '</tr>';
                strTemp += '<tr>';
                if (cl != undefined) {
                    strTemp += '<td>出流:</td><td style="font-weight:bold;">' + Number(cl).toFixed(2) + 'm3/s</td>';
                    // strTemp += '<b>' + "出流:" + cl + "m3/s</b> <br />";
                }

                if (plancl != undefined) {
                    strTemp += '<td>方案出流:</td><td style="font-weight:bold;">' + Number(plancl).toFixed(2) + 'm3/s</td>';
                    // strTemp += '<b style="color:#1d5987;font-weight:bold;">'
                    // + "方案出流:" + plancl + "m3/s</b> <br />";
                }
                strTemp += '</tr>';
                strTemp += '<tr>';
                if (sw != undefined) {
                    strTemp += '<td>水位:</td><td style="font-weight:bold;">' + ((sw - yArea2Start) / gapArea2 + minArea2).toFixed(2) + 'm</b></td>';
                    /*
                     * strTemp += '<b>' + "水位:" + ((sw - yArea2Start) /
                     * gapArea2 + minArea2) .toFixed(2) + "m</b> <br />";
                     */
                }

                if (plansw != undefined) {
                    strTemp += '<td>方案水位:</td><td style="font-weight:bold;">' + ((plansw - yArea2Start) / gapArea2 + minArea2).toFixed(2) + 'm</td>';
                    /*
                     * strTemp += '<b style="color:#1d5987;font-weight:bold;">' +
                     * "方案水位:" + ((plansw - yArea2Start) / gapArea2 + minArea2)
                     * .toFixed(2) + "m</b> <br />";
                     */
                }
                strTemp += '</tr>';
                strTemp += '<tr>';
                if (cw != undefined) {
                    strTemp += '<td>潮位:</td><td style="font-weight:bold;">' + ((cw - yArea2Start) / gapArea2 + minArea2).toFixed(2) + 'm</td>';
                    /*
                     * strTemp += '<b>' + "潮位:" + ((cw - yArea2Start) /
                     * gapArea2 + minArea2) .toFixed(2) + "m</b> <br />"
                     */;
                }

                if (plancw != undefined) {
                    strTemp += '<td>方案潮位:</td><td style="font-weight:bold;">' + ((plancw - yArea2Start) / gapArea2 + minArea2).toFixed(2) + 'm</td>';
                    /*
                     * strTemp += '<b style="color:#1d5987;font-weight:bold;">' +
                     * "方案潮位:" + ((plancw - yArea2Start) / gapArea2 + minArea2)
                     * .toFixed(2) + "m</b> <br />";
                     */
                }
                strTemp += '</tr>';
                strTemp += '<tr>';
                if (yl != undefined) {
                    strTemp += '<td>降雨:</td><td style="font-weight:bold;">' + Number(yl).toFixed(1) + 'mm</td>';
                    // strTemp += '<b>' + "降雨:" + yl + "mm</b> <br />";
                }

                if (planyl != undefined) {
                    strTemp += '<td>方案降雨:</td><td style="font-weight:bold;">' + Number(planyl).toFixed(1) + 'mm</td>';
                    // strTemp += '<b style="color:#1d5987;font-weight:bold;">'
                    // + "方案降雨:" + planyl + "mm</b> <br />";
                }
                strTemp += '</tr>';
                strTemp += '<tr>';
                if ("" == strTemp) {
                    return null;
                }
                return strTemp;

            },
        },
        series : [],
        /*series : [{
            id : 'myplan',
            type : 'scatter',
            name : "方案",
            allowPointSelect : true,
            point : {
                events : {
                    click : function() {
                        // alert(this.options.index);
                        var curFaID = this.options.index;
                        // hsgz.createMainChart(hisHsName, hisDuration);
                        setTimeout(function() {
                            // showPointInPlanByNumber(curFaID);
                            //addPlanSeries(chart, curFaID);
                        }, 1000);
                        return;
                    }
                }
            },
            data : []
        }],*/
        exporting : {
            enabled : false
        }
    };

    if (blHasI) {
        options.series.push({
            name : '入流',
            color : '#4572A7',
            type : 'spline',
            data : dataRkll,
            yAxis : 0
        });
    }

    if (blHasQ) {
        options.series.push({
            name : '出流',
            type : 'spline',
            color : '#89A54E',
            data : dataCkll,
            yAxis : 0

        });
    }

    if (blHasZ) {

        options.series.push({
            name : '水位',
            type : 'spline',
            color : '#AA4643',
            data : dataZ,
            yAxis : 0,
            id : 'SWSerie',
        });
    }
    if (blHasT) {

        options.series.push({
            name : '潮位',
            type : 'spline',
            color : '#1AADCE',
            data : dataT,
            yAxis : 0,

        });
    }
    if (blHasP) {
		//console.log(yArea3Interval);
        options.yAxis.push({
            gridLineWidth : 0,
            reversed : true,
            min : 0,
            tickInterval : yArea3Interval,
            title : {
                text : '水位(m)',

                style : {
                    color : '#AA4643'
                }

            },
            opposite : true,
            labels : {
                formatter : function() {

                    if (this.value > Math.ceil(maxP / yArea3Interval) * yArea3Interval) {

                        var index = this.value / yArea3Interval;
                        var totalcount = yMax / yInterval;
                        var curindex = totalcount - index;

                        var y2StartIndex = yArea2Start / yInterval;
                        var y2EndIndex = y2StartIndex + area2Count;

                        var cury1value = curindex * yInterval;

                        if (curindex >= y2StartIndex && curindex <= y2EndIndex) {

                            if (blHasArea2) {
                                return ((cury1value - yArea2Start) / gapArea2 + minArea2).toFixed(0);
                            }

                        }
                        return "";
                    }
                    return "";
                },
                style : {
                    color : '#AA4643'
                }
            },
        });
        options.series.push({
            name : '降雨',
            type : 'column',
            color : '#058DC7',
            data : dataP,
            yAxis : 1,
            borderWidth : 2,
            borderColor : '#058DC7'

        });
    }

    getStartTime(duration);
    getEndTime(duration);
    curSelectedIndex = -1;
    chart = new Highcharts.Chart(options);

    // setPlanGroupByPlanArray(jsonPlanArray, -1);

    addPlanMarker(chart, json, jsonPlanArray, -1);

    return chart;
}

function getStartTime(duration) {
    var minPlanStartTime = 0;
    var minJsonStartTime = 0;
    var nStartTime;

    if (null != globaljsonPlanArray && globaljsonPlanArray.length > 0 && globaljsonPlanArray[0].方案号 != 0) {

        minPlanStartTime = new Date(globaljsonPlanArray[globaljsonPlanArray.length - 1].data[0].SJ).getTime();
        minJsonStartTime = new Date(globaljson[0].SJ).getTime();

        if (minPlanStartTime < minJsonStartTime) {

            nStartTime = minPlanStartTime;
        } else {

            nStartTime = minJsonStartTime;
        }

    } else {

        nStartTime = new Date(globaljson[0].SJ).getTime();
    }

    nStartTime -= 3600 * 1000;

    globalStartTime = nStartTime;

}

function getEndTime(duration) {

    var maxPlanEndTime = 0;
    var maxJsonEndTime = 0;
    var nEndTime;

    if (null != globaljsonPlanArray && globaljsonPlanArray.length > 0 && globaljsonPlanArray[0].方案号 != 0) {

        for (var i = 0; i < globaljsonPlanArray.length; i++) {

            for (var j = 0; j < globaljsonPlanArray[i].data.length; j++) {

                if (maxPlanEndTime < new Date(globaljsonPlanArray[i].data[j].SJ).getTime()) {

                    maxPlanEndTime = new Date(globaljsonPlanArray[i].data[j].SJ).getTime();
                }

            }
        }

        maxJsonEndTime = new Date(globaljson[globaljson.length - 1].SJ).getTime();

        if (maxPlanEndTime < maxJsonEndTime) {

            nEndTime = maxJsonEndTime;
        } else {

            nEndTime = maxPlanEndTime;
        }

    } else {

        nEndTime = new Date(globaljson[globaljson.length - 1].SJ).getTime();

    }

    nEndTime += 3600 * 1000;

    if (nEndTime - globalStartTime < duration * 1000) {

        globalEndTime = globalStartTime + duration * 1000;
    } else {

        globalEndTime = nEndTime;
    }

}

function checkPlanIdOnClick(val) {

    for (var i = 0; i < planmarker.length; i++) {

        if (planmarker[i].Name == val && val != "起点" && val != "终点") {
            return i;
        }
    }
    return -1;

}

function addPlanSeries(chart, faID) {

    var dataPlanRkll = [];
    var dataPlanZ = [];
    var dataPlanCkll = [];
    var dataPlanP = [];
    var dataPlanT = [];

    var planIserie = chart.get("planIserie");
    if (null != planIserie) {
        // planIserie.remove(true);
    }

    var planQSerie = chart.get("planQSerie");
    if (null != planQSerie) {
        // planQSerie.remove(true);
    }

    var planZSerie = chart.get("planZSerie");
    if (null != planZSerie) {
        // planZSerie.remove(true);
    }

    var planTSerie = chart.get("planTSerie");
    if (null != planTSerie) {
        // planTSerie.remove(true);
    }

    var planPSerie = chart.get("planPSerie");
    if (null != planPSerie) {
        // planPSerie.remove(true);
    }

    /*if (null != globaljsonPlanArray[id].data
    && globaljsonPlanArray[id].data.length > 0) {*/

    //var planData = globaljsonPlanArray[id].data;
    var planData = [];
    for (var j = 0; j < globaljsonPlanArray.length; j++) {
        if (globaljsonPlanArray[j].number == faID) {
            planData = globaljsonPlanArray[j].data;
        }
    }

    var maxSWIndex = -1;
    var tempSW = 0;

    var maxRKLLIndex = -1;
    var tempRKLL = 0;

    for (var i = 0; i < planData.length; i++) {

        if (tempSW < planData[i][nameOfSW]) {

            tempSW = planData[i][nameOfSW];
            maxSWIndex = i;
        }

        if (tempRKLL < planData[i][nameOfRKLL]) {
            tempRKLL = planData[i][nameOfRKLL];
            maxRKLLIndex = i;
        }

    }

    // alert("maxSWIndex:"+ maxSWIndex + "maxRKLLIndex:"+maxRKLLIndex);

    for (var i = 0; i < planData.length; i++) {

        if (blHasPlanI) {
            dataPlanRkll.push({
                x : new Date(planData[i][nameOfTime]).getTime(),
                y : planData[i][nameOfRKLL],
                index : faID,
                id : faID,

            });
        }

        if (blHasPlanQ) {
            dataPlanCkll.push({
                x : new Date(planData[i][nameOfTime]).getTime(),
                y : planData[i][nameOfCKLL],
                index : faID,
                id : faID,
            });
        }

        if (blHasPlanZ) {
            dataPlanZ.push({
                x : new Date(planData[i][nameOfTime]).getTime(),
                y : ((planData[i][nameOfSW] - minArea2) * gapArea2 + yArea2Start),
                index : faID,
                id : faID,
            });

        }

        if (blHasPlanT) {
            dataPlanT.push({
                x : new Date(planData[i][nameOfTime]).getTime(),
                y : ((planData[i][nameOfCW] - minArea2) * gapArea2 + yArea2Start),
                index : faID,
                id : faID,

            });

        }

        if (blHasPlanP) {
            dataPlanP.push({
                x : new Date(planData[i][nameOfTime]).getTime(),
                y : planData[i][nameOfRain],
                index : faID,
                id : faID,
            });
        }
    }
    //}

    if (blHasPlanI) {

        if (null == planIserie) {
            planIserie = {
                name : '方案入流',
                color : '#4572A7',
                data : dataPlanRkll,
                shadow : false,
                type : 'spline',
                yAxis : 0,
                showInLegend : true,
                lineWidth : 3,
                dashStyle : 'Dot',
                id : "planIserie",
            };
            chart.addSeries(planIserie);
        } else {
            planIserie.setData(dataPlanRkll);
        }
    }

    if (blHasPlanQ) {

        if (null == planQSerie) {
            planQSerie = {
                name : '方案出流',
                color : '#89A54E',
                data : dataPlanCkll,
                shadow : false,
                type : 'spline',
                yAxis : 0,
                showInLegend : true,
                lineWidth : 3,
                dashStyle : 'Dot',
                id : "planQSerie",
            };
            chart.addSeries(planQSerie);
        } else {

            planQSerie.setData(dataPlanCkll);
        }
    }

    if (blHasPlanZ) {
        if (null == planZSerie) {
            planZSerie = {
                name : '方案水位',
                color : '#AA4643',
                data : dataPlanZ,
                shadow : false,
                type : 'spline',
                yAxis : 0,
                showInLegend : true,
                lineWidth : 3,
                dashStyle : 'Dot',
                id : "planZSerie",
            };
            chart.addSeries(planZSerie);
        } else {

            planZSerie.setData(dataPlanZ);
        }
    }

    if (blHasPlanT) {

        if (null == planTSerie) {
            planTSerie = {
                name : '方案潮位',
                color : '#1AADCE',
                data : dataPlanT,
                shadow : false,
                type : 'spline',
                yAxis : 0,
                showInLegend : true,
                lineWidth : 3,
                dashStyle : 'Dot',
                id : "planTSerie",
            };
            chart.addSeries(planTSerie);
        } else {

            planTSerie.setData(dataPlanT);
        }
    }

    if (blHasPlanP) {

        if (null == planPSerie) {
            planPSerie = {
                name : '方案降雨',
                color : '#FF7F00',
                data : dataPlanP,
                shadow : false,
                type : 'column',
                yAxis : 1,
                showInLegend : true,
                borderWidth : 2,
                borderColor : '#FF7F00',
                id : "planPSerie",
            };
            chart.addSeries(planPSerie);
        } else {
            planPSerie.setData(dataPlanP);
        }
    }

    /*var myplanserie = chart.get("myplan");

     if (null != myplanserie) {

     addPlanMarker(chart, globaljson, globaljsonPlanArray, id);
     }*/

    lastSelectedIndex = curSelectedIndex;
    curSelectedIndex = faID;

}

var curSelectedIndex = -1;
var lastSelectedIndex = -1;

function onSelectPlan() {

    // addPlanSeries(chart, curSelectedIndex);

    var curPlan = getPlanByIndex(curSelectedIndex);
    curSelectedIndex = -1;

    selectPlanItem(curPlan.number);

}

function onCancelPlan() {

    curSelectedIndex = lastSelectedIndex;
}

function getPlanYByTM(tm) {
    var swSerie = chart.get("SWSerie");
    if (null != swSerie) {

        for (var i = 0; i < swSerie.data.length; i++) {
            var x = swSerie.data[i].x;
            if (tm == x) {
                return swSerie.data[i].y;
            }
        }

        if (tm > swSerie.data[0].x) {
            return swSerie.data[swSerie.data.length - 1].y;
        } else {
            return swSerie.data[0].y;
        }
    }
}

planGroup = [];
function setPlanGroupByPlanArray(jsonPlanArray, selectedIndex) {
    planGroup = [];

    for (var i = jsonPlanArray.length - 1; i >= 0; i--) {
        var curTime = new Date(jsonPlanArray[i].data[0].SJ).getTime();
        var blFound = false;
        for (var j = 0; j < planGroup.length; j++) {
            if (planGroup[j].TM == curTime) {
                planGroup[j].IndexArray.push(i);
                blFound = true;
                if (jsonPlanArray[i].category == "公共") {
                    planGroup[j].Selected = planGroup[j].IndexArray.length - 1;
                }
                break;
            }
        }

        if (!blFound) {
            planGroup.push({
                "TM" : curTime,
                "IndexArray" : [i],
                "Selected" : 0
            });
        }

    }

    if (selectedIndex != -1) {

        for (var i = 0; i < planGroup.length; i++) {
            for (var j = 0; j < planGroup[i].IndexArray.length; j++) {
                if (selectedIndex == planGroup[i].IndexArray[j]) {
                    planGroup[i].Selected = j;
                    return;
                }

            }
        }

    }

}

function getPlanByIndex(index) {

    return globaljsonPlanArray[index];
}
var gloFaName="";
function showPointInPlanByNumber(faID, faName) {
    gloFaName=faName;

    /*var index = -1;
     for (var i = 0; i < globaljsonPlanArray.length; i++) {
     if (globaljsonPlanArray[i].number == faID) {
     index = i;
     break;
     }
     }
     if (index != -1) {
     setPlanGroupByPlanArray(globaljsonPlanArray, index);
     addPlanMarker(chart, globaljson, globaljsonPlanArray, faID);
     addPlanSeries(chart, index);
     }*/

    addPlanMarker(chart, globaljson, globaljsonPlanArray, faID);
    addPlanSeries(chart, faID);
}

function addPlanMarker(chart, json, jsonPlanArray, faID) {
    planmarker = [];

    if (null != jsonPlanArray && jsonPlanArray.length > 0) {

        var start = {
            "TM" : globalStartTime,
            "Index" : -1,
            "Type" : 0,
            "Group" : -1,
        };

        planmarker.push(start);

        for (var i = 0; i < jsonPlanArray.length; i++) {
            if (jsonPlanArray[i].data != null) {
                marker = {
                    "TM" : new Date(jsonPlanArray[i].data[0].SJ).getTime(),
                    "Index" : jsonPlanArray[i].number,
                    "Type" : 1,
                };

                planmarker.push(marker);
            }

        }
        var end = {
            "TM" : globalEndTime,
            "Index" : -1,
            "Type" : 0,
            "Group" : -1,
        };
        planmarker.push(end);

    } else {

        var start = {
            "TM" : globalStartTime,
            "Index" : -1,
            "Type" : 0,
            "Group" : -1,
        };
        planmarker.push(start);

        var end = {
            "TM" : globalEndTime,
            "Index" : -1,
            "Type" : 0,
            "Group" : -1,
        };
        planmarker.push(end);

    }

    markerdata = [];
    var j = 0;
    for (var i = 0; i < planmarker.length; i++) {

        if (0 == planmarker[i].Type) {

            markerdata.push({
                x : planmarker[i].TM,
                y : getPlanYByTM(planmarker[i].TM),
                marker : {
                    enabled : false,
                },
                index : -1,
                group : -1
            });

        } else {
            if (planmarker[i].TM == planmarker[i - 1].TM) {
                j++;
            } else {
                j = 0;
            }
            if (faID == planmarker[i].Index) {

                markerdata.push({
                    x : planmarker[i].TM,
                    marker : {
                        enable : true,
                        symbol : 'url(admin/images/planmarkernewer.png)',
                    },

                    y : getPlanYByTM(planmarker[i].TM) + j * yInterval * 0.5,
                    index : planmarker[i].Index
                });
            } else {

                markerdata.push({
                    x : planmarker[i].TM,
                    marker : {
                        enable : true,
                        symbol : 'url(admin/images/planmarker.png)',
                    },

                    y : getPlanYByTM(planmarker[i].TM) + j * yInterval * 0.5,
                    index : planmarker[i].Index
                });
            }
        }

    }
   // console.log("markerData:");
    //console.log(markerdata);
    /*var myplanserie = chart.get("myplan");

    myplanserie.setData();
    myplanserie.setData(markerdata);*/

}

// charts end
