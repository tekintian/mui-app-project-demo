/**
 * 用户登录系列操作流程
 **/
(function($, owner) {
	/**
	 * 用户登录
	 **/
	owner.login = function(loginInfo, onSuccess,onError) {
		loginInfo = loginInfo || {};
		loginInfo.account = loginInfo.account || '';
		loginInfo.password = loginInfo.password || '';
		
		//var users = JSON.parse(localStorage.getItem('$users') || '[]');
		/*var authed = users.some(function(user) {
			return loginInfo.account == user.account && loginInfo.password == user.password;
		});*/
		
		mui.web_query('/Login!USER',{phoneno:loginInfo.account,
		psd:loginInfo.password}, onSuccess, onError, 5);
		
	};

	owner.createState = function(name, callback) {
		var state = owner.getState();
		state.account = name;
		state.token = "token123456789";
		owner.setState(state);
		return callback();
	};

	/**
	 * 新用户注册
	 **/
	owner.reg = function(regInfo, callback) {
		callback = callback || $.noop;
		regInfo = regInfo || {};
		regInfo.phoneno = regInfo.phoneno || '';
		regInfo.psd = regInfo.psd || '';
		if (regInfo.phoneno.length!=11) {
			return callback(regInfo,'请输入正确可用的手机号');
		}
		
		/*if (!checkEmail(regInfo.email)) {
			return callback('邮箱地址不合法');
		}*/
		//用户信息本地存储（注册成功后只保存账户不保存密码）
		owner.UserInfo(JSON.stringify({phoneno:regInfo.phoneno,psd:""}));
		
		return callback(regInfo);
	};
	
	/*
	 * 存储本地用户信息
	 */
	owner.UserInfo=function(){
		if(arguments.length == 0){
        	return plus.storage.getItem('UserInfo');        
	    }
	    if(arguments[0] === ''){
	        plus.storage.removeItem('UserInfo');
	        return;
	    }
	    plus.storage.setItem('UserInfo', arguments[0]);
	};
	
	/*
	 * 存储本地用户信息（为用户注销登录等操作准备所需的用户信息数据）
	 */
	owner.UserInfoForAny=function(){
		if(arguments.length == 0){
        	return plus.storage.getItem('UserInfoForAny');        
	    }
	    if(arguments[0] === ''){
	        plus.storage.removeItem('UserInfoForAny');
	        return;
	    }
	    plus.storage.setItem('UserInfoForAny', arguments[0]);
	};
	
	/**
	 * 获取当前状态
	 **/
	/*owner.getState = function() {
		var stateText = localStorage.getItem('$state') || "{}";
		return JSON.parse(stateText);
	};*/

	/**
	 * 设置当前状态
	 **/
//	owner.setState = function(state) {
//		state = state || {};
//		localStorage.setItem('$state', JSON.stringify(state));
//		
//	};

	var checkEmail = function(email) {
		email = email || '';
		return (email.length > 3 && email.indexOf('@') > -1);
	};

	/**
	 * 找回密码
	 **/
	/*owner.forgetPassword = function(params, callback) {
		callback = callback || $.noop;
		
		return callback(null, '新的随机密码已经发送到您的邮箱，请查收邮件。');
	};*/

	/**
	 * 获取应用本地配置
	 **/
	owner.setSettings = function(settings) {
		settings = settings || {};
		//由于localStorage存储在最新的ios9手机上容易出现存储失败情况，故放弃
		//localStorage.setItem('$settings', JSON.stringify(settings));
		
		plus.storage.setItem('Settings', JSON.stringify(settings));
		
	};

	/**
	 * 设置应用本地配置
	 **/
	owner.getSettings = function() {
		//由于localStorage存储在最新的ios9手机上容易出现存储失败情况，故放弃
		//var settingsText = localStorage.getItem('$settings') || "{}";
		
		var settingsText = plus.storage.getItem('Settings') || "{}";
		return JSON.parse(settingsText);
	};
	
	/**
	 * 清除账户信息
	 **/
	owner.setClearAccount = function(callback) {
		if(plus){
			plus.storage.removeItem("Settings");
			plus.storage.removeItem('UserInfo');
			//owner.setSettings();
			//owner.UserInfo('');
			return callback("账户信息已销毁");
		}else{
			return callback("清除失败");
		}
	};

	
	/**
	 * 服务端获取公钥
	 **/
	owner.getSecurityCode=function(callback){
		mui.web_query('/QueryPublicKey!USER',{},function(json){
			//console.log(JSON.stringify(json));
			callback(json);
		},function(code,msg){
			//plus.nativeUI.toast('服务端请求公钥失败，'+msg);
			callback();
		}, 5);
	};
	
}(mui, window.app = {}));