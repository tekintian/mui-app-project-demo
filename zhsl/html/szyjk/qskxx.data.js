/*
 * 取水口信息
 */
var szjcData = [{
	value: '',
	text: '所有类型',
	children: [{
		value:'',
		text:'全市'
	},{
		value:'bl',
		text:'北仑区'
	},{
		value:'cx',
		text:'慈溪市'
	},{
		value:'fh',
		text:'奉化市'
	},{
		value:'nh',
		text:'宁海县'
	},{
		value:'xs',
		text:'象山县'
	},{
		value:'yz',
		text:'鄞州区'
	},{
		value:'zh',
		text:'镇海区'
	},{
		value:'jd',
		text:'江东区'
	},{
		value:'hs',
		text:'海曙区'
	},{
		value:'jb',
		text:'江北区'
	},{
		value:'yy',
		text:'余姚市'
	}]
}, {
	value: 'dxs',
	text: '地下水',
	children: [{
		value:'',
		text:'全市'
	},{
		value:'bl',
		text:'北仑区'
	},{
		value:'cx',
		text:'慈溪市'
	},{
		value:'fh',
		text:'奉化市'
	},{
		value:'nh',
		text:'宁海县'
	},{
		value:'xs',
		text:'象山县'
	},{
		value:'yz',
		text:'鄞州区'
	},{
		value:'zh',
		text:'镇海区'
	},{
		value:'jd',
		text:'江东区'
	},{
		value:'hs',
		text:'海曙区'
	},{
		value:'jb',
		text:'江北区'
	},{
		value:'yy',
		text:'余姚市'
	}]
}, {
	value: 'dbs',
	text: '地表水',
	children: [{
		value:'qs',
		text:'全市'
	},{
		value:'bl',
		text:'北仑区'
	},{
		value:'cx',
		text:'慈溪市'
	},{
		value:'fh',
		text:'奉化市'
	},{
		value:'nh',
		text:'宁海县'
	},{
		value:'xs',
		text:'象山县'
	},{
		value:'yz',
		text:'鄞州区'
	},{
		value:'zh',
		text:'镇海区'
	},{
		value:'jd',
		text:'江东区'
	},{
		value:'hs',
		text:'海曙区'
	},{
		value:'jb',
		text:'江北区'
	},{
		value:'yy',
		text:'余姚市'
	}]
},{
	value: 'slfd',
	text: '水力发电',
	children: [{
		value:'qs',
		text:'全市'
	},{
		value:'bl',
		text:'北仑区'
	},{
		value:'cx',
		text:'慈溪市'
	},{
		value:'fh',
		text:'奉化市'
	},{
		value:'nh',
		text:'宁海县'
	},{
		value:'xs',
		text:'象山县'
	},{
		value:'yz',
		text:'鄞州区'
	},{
		value:'zh',
		text:'镇海区'
	},{
		value:'jd',
		text:'江东区'
	},{
		value:'hs',
		text:'海曙区'
	},{
		value:'jb',
		text:'江北区'
	},{
		value:'yy',
		text:'余姚市'
	}]
}
]