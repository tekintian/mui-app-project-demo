/*
 * 水质监察
 */
var szjcData = [{
	value: 'syd',
	text: '水源地',
	children: [{
		value:'qs',
		text:'全市'
	},{
		value:'bl',
		text:'北仑区'
	},{
		value:'cx',
		text:'慈溪市'
	},{
		value:'fh',
		text:'奉化市'
	},{
		value:'nh',
		text:'宁海县'
	},{
		value:'xs',
		text:'象山县'
	},{
		value:'yz',
		text:'鄞州区'
	},{
		value:'zh',
		text:'镇海区'
	},{
		value:'jd',
		text:'江东区'
	},{
		value:'hs',
		text:'海曙区'
	},{
		value:'jb',
		text:'江北区'
	},{
		value:'yy',
		text:'余姚市'
	}]
}, {
	value: 'zyjh',
	text: '主要江河',
	children: [{
		value:'qs',
		text:'全市'
	},{
		value:'bl',
		text:'北仑'
	},{
		value:'cx',
		text:'慈溪'
	},{
		value:'fh',
		text:'奉化'
	},{
		value:'nh',
		text:'宁海'
	},{
		value:'xs',
		text:'象山'
	},{
		value:'yz',
		text:'鄞州'
	},{
		value:'zh',
		text:'镇海'
	},{
		value:'jd',
		text:'江东'
	},{
		value:'hs',
		text:'海曙'
	},{
		value:'jb',
		text:'江北'
	},{
		value:'yy',
		text:'余姚'
	}]
}, {
	value: 'pyhw',
	text: '平原河网',
	children: [{
		value:'qs',
		text:'全市'
	},{
		value:'bl',
		text:'北仑'
	},{
		value:'cx',
		text:'慈溪'
	},{
		value:'fh',
		text:'奉化'
	},{
		value:'nh',
		text:'宁海'
	},{
		value:'xs',
		text:'象山'
	},{
		value:'yz',
		text:'鄞州'
	},{
		value:'zh',
		text:'镇海'
	},{
		value:'jd',
		text:'江东'
	},{
		value:'hs',
		text:'海曙'
	},{
		value:'jb',
		text:'江北'
	},{
		value:'yy',
		text:'余姚'
	}]
}]